<?php 
class Settings{
  
  public static function dirPath(){
    return realpath(dirname(__FILE__)) . '/';
  } 
  
  public static function database(){
    return array(
      'host'     =>  Config::get('database')->host, 
      'user'     =>  Config::get('database')->username,
      'password' =>  Config::get('database')->password,
      'dbname'   =>  Config::get('database')->dbname,
    );
  }
  
  public static function path(){
    return (object)array(
      'server'      => '/',
      'application' => 'application/',
      'tpl'         => 'application/templates/',
      'models'      => 'application/models/',
      'tplCompile'  => 'data/smarty_compile/',
      'controllers' => 'application/controllers/',
      'vendor'      => 'lib/',
      'cache'       => 'cache/',
      'luceneIndex' => 'data/lucene/',
      'tests'       => 'tests/',
      'dir'         => self::dirPath(),
    );
  }
  
}