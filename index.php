<?php
ini_set('session.gc_maxlifetime', 2*60*60);
session_start();
set_time_limit(120);
ini_set('display_errors', 'On');
ini_set('error_reporting', 'E_ALL');
error_reporting(E_ALL);
header('Content-type: text/html; charset=UTF-8');

require_once 'settings.php';
require_once 'functions_smarty.php';

require_once Settings::path()->models . 'App.php';

App::instance()->bootstrap();

/* PLACE CUSTOM ROUTES HERE */
//auth
App::instance()->addRouteWildcard( 'login/*', 'auth', 'login', 'login' );
App::instance()->addRouteWildcard( 'logout/*', 'auth', 'logout' );
App::instance()->addRouteWildcard( 'phpinfo/*', 'index', 'phpinfo' );

App::instance()->run();
App::instance()->render();