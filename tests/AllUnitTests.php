<?php 
class AllUnitTests extends TestSuite{
  
  public function __construct(){
    parent::__construct();
    $this->addTestCasesFromDir( Settings::path()->tests . 'unit' );
  }
  
  protected function addTestCasesFromDir( $dirname ){
    $this->collect( $dirname, new SimplePatternCollector('/.test.php/') );
    $dirContent = scandir($dirname.'/');
    foreach( $dirContent as $v ){
      if ( $v == '.' or $v == '..' ){ continue; }
      if ( is_dir($dirname . '/' . $v) ){
        $this->addTestCasesFromDir( $dirname . '/' . $v );
      }
    }
  }
  
}