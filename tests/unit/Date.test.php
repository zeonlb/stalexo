<?php 
class DateTest extends UnitTestCase{
  
  public function testJs2PhpTime(){
    $this->assertEqual( '1355301900', Date::js2PhpTime('12/12/2012 10:45') );
  }
  
  public function testPhp2JsTime(){
    $this->assertEqual( '12/12/2012 10:45', Date::php2JsTime(1355301900) );
  }
  
  public function testPhp2MySqlTime(){
    $this->assertEqual( '2012-12-12 10:45:00', Date::php2MySqlTime(1355301900) );
  }
  
  public function testMySql2PhpTime(){
    $this->assertEqual( 1355301900, Date::mySql2PhpTime('2012-12-12 10:45:00') );
  }
  
}