<?php 
class RegistryTest extends UnitTestCase{
  
  public function testSingletonImplementation(){
    $registry = Registry::getInstance();
    $this->assertIdentical( $registry, Registry::getInstance() );
    $this->assertIsA( $registry, 'Registry' );
    unset($registry);
    $this->assertIdentical( Registry::getInstance(), Registry::getInstance() );    
  }
  
}