<?php
class ConfigTest extends UnitTestCase{
  
  public function setUp(){
    Config::setIniFilename('config.test.ini');
  }
  
  public function testConfig(){    
    $config = Config::getInstance();
    $this->assertIsA( Config::getInstance(), 'Config' );
    $this->assertIdentical( Config::getInstance(), Config::getInstance() );
    $this->assertIdentical( $config, Config::getInstance() );
    
    Config::setIniFilename('config1.test.ini');       
    $this->expectException( 'ConfigException', 'Invalid config path' );
    Config::getInstance(); 
  }
  
  public function testAccessors(){
    $this->assertEqual( Config::get('info')->key1, 'value1' );
    $this->assertEqual( Config::get('info')->array->key2, 'value2' );
    $this->assertNull( Config::get('nokey') );
    $this->assertIsA( Config::get('info'), 'stdClass' );
    
    $this->assertEqual( Config::get('array'), (object)array( 'key' => (object)array( 2,3,4 ) ) );
    $this->assertEqual( Config::get('array')->key, (object)array( 2,3,4 ) );
  }
  
}