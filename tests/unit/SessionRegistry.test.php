<?php 
class SessionRegistryTest extends UnitTestCase{
  
  public function setUp(){
    $this->mockArray = array( 'level1' => 25, 'level2' => array( 'nodeL2' => 31, 'nodeL3' => 32 ) );
    SessionRegistry::set('mockArray', $this->mockArray );
  }
  
  public function testGet(){
    $this->assertNull( SessionRegistry::get('somekey') );
    $this->assertEqual( $this->mockArray, SessionRegistry::get('mockArray') );
  }
  
  public function testSet(){
    SessionRegistry::set('val1', '1');
    $this->assertEqual( SessionRegistry::get('val1'), 1 );
    
    SessionRegistry::set('val2', array('1',2,3));
    $this->assertEqual( SessionRegistry::get('val2'), array('1',2,3) );
  }
  
}