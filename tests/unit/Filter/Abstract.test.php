<?php 
class FilterAbstractTest extends UnitTestCase{
  
  public function testRequestParser(){   
    Filter_Abstract::addMarker('page');
    Filter_Abstract::addMarker('price');
    Filter_Abstract::addMarker('type');   
    
    App::getRequest()->setGet(array(
      'shop','category','sport','price','10','2500','page','1','type'
    ));
    
    $filterParameters = Filter_Abstract::getParameters();
    
    $this->assertEqual( $filterParameters, array( 'price' => array( 10, 2500 ), 'page' => array(1) ) ); 
    
    Filter_Abstract::dropMarker('price');
    $filterParameters = Filter_Abstract::getParameters();

    $this->assertNotEqual( $filterParameters, array( 'price' => array( 10, 2500 ), 'page' => array(1) ) );
    $this->assertEqual( $filterParameters, array( 'page' => array(1) ) );
    
    Filter_Abstract::addMarker('price');
    $filterParameters = Filter_Abstract::getParameters();
    $this->assertEqual( $filterParameters, array( 'price' => array( 10, 2500 ), 'page' => array(1) ) ); 
  }
  
}