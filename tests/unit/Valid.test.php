<?php 
class ValidTest extends UnitTestCase{
  
  public function testAlias(){
    $this->assertFalse(Valid::alias('a'));
    $this->assertFalse(Valid::alias(251));
    $this->assertFalse(Valid::alias('5alias'));
    $this->assertFalse(Valid::alias('-alias'));
    $this->assertFalse(Valid::alias('asdsa@alias'));
    $this->assertFalse(Valid::alias('al%ias'));
    $this->assertFalse(Valid::alias('ali*as'));
    $this->assertFalse(Valid::alias('ali/as'));
    $this->assertFalse(Valid::alias('aliasaliasaliasaliasaliasaliasaliasaliasaliasaliasal'));
    
    $this->assertTrue(Valid::alias('ali'));
    $this->assertTrue(Valid::alias('different'));
    $this->assertTrue(Valid::alias('alias'));
    $this->assertTrue(Valid::alias('alias21312312'));
    $this->assertTrue(Valid::alias('a34242lias'));
    $this->assertTrue(Valid::alias('a2l-ias'));
    $this->assertTrue(Valid::alias('al_ias1212-1212'));
  }
  
  public function testPhone(){
    $this->assertTrue(Valid::phone('+38(066)-412-17-14'));
    $this->assertTrue(Valid::phone('38(066)-412-17-14'));
    $this->assertTrue(Valid::phone('38(066)4121714'));
    $this->assertTrue(Valid::phone('38066-412-17-14'));
    $this->assertTrue(Valid::phone('380664121714'));
    $this->assertTrue(Valid::phone('(066)-412-17-14'));
    $this->assertTrue(Valid::phone('0664121714'));
    $this->assertTrue(Valid::phone('066-412-17-14'));
    
    $this->assertFalse(Valid::phone('38(066)+412-17-14'));
    $this->assertFalse(Valid::phone('412-17-14'));
    $this->assertFalse(Valid::phone('моб412-17-14'));
    $this->assertFalse(Valid::phone('066-412-17-14, 350-296-95-87'));
  }
  
  public function testTime(){
    $this->assertTrue(Valid::time('12:30'));
    $this->assertFalse(Valid::time('12:1'));
    $this->assertFalse(Valid::time('5:26'));
    $this->assertFalse(Valid::time('1212'));
    $this->assertFalse(Valid::time('a2:12'));
  }
  
  public function testDate(){
    $this->assertTrue(Valid::date('2012-10-10'));
    $this->assertFalse(Valid::date('2012/12/12'));
    $this->assertFalse(Valid::date('12-12-2012'));
  }
  
  public function testLatLng(){
    $this->assertTrue(Valid::latLng('-77.037852'));
    $this->assertTrue(Valid::latLng('38.898556'));
    $this->assertTrue(Valid::latLng('38.898'));
    $this->assertTrue(Valid::latLng('138.898556'));
    $this->assertTrue(Valid::latLng('-138.8985'));
    $this->assertTrue(Valid::latLng('38.12345678'));
    
    $this->assertFalse(Valid::latLng('38.123456789'));
    $this->assertFalse(Valid::latLng('1238.123456'));
    $this->assertFalse(Valid::latLng('8.123456'));
    $this->assertFalse(Valid::latLng('38.1'));
    $this->assertFalse(Valid::latLng('381234567'));
    $this->assertFalse(Valid::latLng('38.123456789'));
    $this->assertFalse(Valid::latLng('+38.123456'));
  }
  
  public function testId1c(){
    $this->assertTrue(Valid::id1c('2343'));
    $this->assertTrue(Valid::id1c('00000002343'));
    $this->assertTrue(Valid::id1c('00000052343'));
    $this->assertTrue(Valid::id1c('4'));
    
    $this->assertFalse(Valid::id1c('488888888888'));
  }
  
  public function testDate1c(){
    $this->assertTrue(Valid::date1c('20.20.2012'));
    $this->assertFalse(Valid::date1c('2012.12.05'));
    $this->assertFalse(Valid::date1c('2012412605'));
  }
  
  public function testDate1c2php(){
    $this->assertEqual(Date::php2Date1c(1354701207), '05.12.2012');
    $this->assertEqual(Date::php2Date1c(1354658400), '05.12.2012');
    
    $this->assertEqual(Date::date1c2php('05.12.2012'), 1354658400);
    
  }
  
}