<?php 
class Auth_Type_AbstractTest extends UnitTestCase{
  
  public function testGeneratePassword(){
    $this->assertTrue( Valid::password(Auth_Type_Abstract::generatePassword(10)) );
    $this->assertTrue( Valid::password(Auth_Type_Abstract::generatePassword(15)) );
    $this->assertTrue( Valid::password(Auth_Type_Abstract::generatePassword(11)) );
    $this->assertTrue( Valid::password(Auth_Type_Abstract::generatePassword(12)) );
    $this->assertTrue( Valid::password(Auth_Type_Abstract::generatePassword(7)) );
    $this->assertTrue( Valid::password(Auth_Type_Abstract::generatePassword(6)) );
    $this->assertTrue( Valid::password(Auth_Type_Abstract::generatePassword(8)) );
    $this->assertTrue( Valid::password(Auth_Type_Abstract::generatePassword(9)) );
    $this->assertTrue( Valid::password(Auth_Type_Abstract::generatePassword(13)) );
    
    $this->assertFalse( Valid::password(Auth_Type_Abstract::generatePassword(5)) );
    $this->assertFalse( Valid::password(Auth_Type_Abstract::generatePassword(16)) );
  }
  
}