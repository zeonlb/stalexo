<?php 
class Controller_FrontTest extends UnitTestCase{
  
  public function setUp(){
    $this->request = App::getRequest();
  }
  
  public function testRoutesFactory(){
    $fc = new Controller_Front();
    $fc->setRequest( $this->request );
    $fc->parseUrl();
 
    $this->request->setGet(array('auth'));
    $fc->parseUrl();
    $this->assertEqual( $fc->getControllerName(), 'AuthController' );

    $this->request->setGet(array('auth','send-any-message'));
    $fc->parseUrl();
    $this->assertEqual( $fc->getControllerName(), 'AuthController' );
    $this->assertEqual( $fc->getActionName(), 'sendAnyMessageAction' );    
  }
  
  public function testActionParser(){
    $this->assertEqual(Controller_Front::parseAction('index'), 'index');
    $this->assertEqual(Controller_Front::parseAction('index-my'), 'indexMy');
    $this->assertEqual(Controller_Front::parseAction('index-my-name'), 'indexMyName');
    
    $this->assertNotEqual(Controller_Front::parseAction('indexmy'), 'indexMy');
    $this->assertNotEqual(Controller_Front::parseAction('inde-xmy'), 'indexMy');
    $this->assertNotEqual(Controller_Front::parseAction('inde-xmy'), 'indexmy');
    $this->assertNotEqual(Controller_Front::parseAction('index-my-name'), 'indexmyname');
  }
  
}