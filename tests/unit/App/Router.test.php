<?php 
class App_RouterTest extends UnitTestCase{
  
  public function setUp(){
    App_Router::dropRoutes();
    $this->staticRoute = new App_Route_Static('shop/shop', false, 'shop', 'index', 'shop-smap');
    $this->wildcardRoute = new App_Route_Wildcard('shop/*', false, 'products', 'index', 'products-smap');
    $this->regexpRoute = new App_Route_RegExp('@^shop(-[a-z]+)?/[a-z\-]+@', false, 'shopbycategory', 'buy');
  }
  
  public function testRouteStatic(){
    App_Router::addRoute($this->staticRoute);
    $this->assertIdentical( App_Router::getRoute('shop/shop'), $this->staticRoute );
  }
  
  public function testRouteWildcard(){
    App_Router::addRoute($this->wildcardRoute);
    $this->assertIdentical( App_Router::getRoute('shop/index'), $this->wildcardRoute );
    $this->assertIdentical( App_Router::getRoute('shop/cart'), $this->wildcardRoute );
  }
  
  public function testRouteRegExp(){
    App_Router::addRoute($this->regexpRoute);
    $this->assertIdentical( App_Router::getRoute('shop-sale/index'), $this->regexpRoute );
    $this->assertIdentical( App_Router::getRoute('shop-buy/cart'), $this->regexpRoute );
    
    $this->assertFalse( App_Router::getRoute('shop-buy/1234') );
  }
  
  public function testRuRegExp(){
    App_Router::dropRoutes();
    $regexpRoute = new App_Route_RegExp('@^shop(-[a-zа-яъёіыиї]+)?/[a-z]+@iu', false, 'shopbycategory', 'buy');
    App_Router::addRoute($regexpRoute);
    
    $this->assertIdentical( App_Router::getRoute('shop-qwerty/index'), $regexpRoute );
    $this->assertIdentical( App_Router::getRoute('shop-екшин/index'), $regexpRoute );
    $this->assertIdentical( App_Router::getRoute('shop-ыъёіїхц/index'), $regexpRoute );
  }
   
}