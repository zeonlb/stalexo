<?php /* Smarty version Smarty-3.1.2, created on 2013-08-18 17:08:35
         compiled from "application/templates/android/sync.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5554054125210d5630c0469-10906281%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '40d235972a45c54aa5e409d6d53e338adab10494' => 
    array (
      0 => 'application/templates/android/sync.tpl',
      1 => 1367156452,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5554054125210d5630c0469-10906281',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'request' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.2',
  'unifunc' => 'content_5210d5631d6ae',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5210d5631d6ae')) {function content_5210d5631d6ae($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("android/menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<br>
<style type="text/css">
  
  .query{
  background-color: #DED395;
  width: 500px;
  }
  
</style>

<form action="/android/sync" method="post">
  action <input type="text" name="action" value="<?php echo $_smarty_tpl->tpl_vars['request']->value->post('action');?>
" /><br>
  device <input type="text" name="device" value="<?php echo $_smarty_tpl->tpl_vars['request']->value->post('device');?>
" /><br>
  token<input type="text" name="token" value="<?php echo $_smarty_tpl->tpl_vars['request']->value->post('token');?>
"><br>
  target<input type="text" name="target" value="<?php echo $_smarty_tpl->tpl_vars['request']->value->post('target');?>
"><br>
  date<input type="text" name="date" value="<?php echo $_smarty_tpl->tpl_vars['request']->value->post('date');?>
"><br>
  <br>
  <input type="submit" name="submit" value="send">
</form>   <br><br>
<b>Target:</b>
<ul>
  <li><b>get-sequence</b></li>
  <li>clients</li>
  <li>price_types</li>
  <li>distribution_points 
    <br>
    <div class="query">
      <i>SELECT * FROM `distribution_points` WHERE id_client = [ID_CLIENT]</i>
    </div>
  </li>
  <li>storage</li>
  <li>sku_groups</li>
  <li>sku
    <br>
    <div class="query">
    <i>
      SELECT * FROM( <br>
      SELECT s.id, s.title, s.units, sp.price, sa.availability,<br>
      ([SHIPMENT_DATE] - sp.date) as time_diff <br>
      FROM `sku` s  <br>
      JOIN `sku_prices` sp ON s.id = sp.id_sku AND sp.date <= [SHIPMENT_DATE]  <br>
      JOIN `price_types` pt ON pt.id = sp.id_price_type  <br>
      JOIN `distribution_points` dp ON dp.id_price_type = pt.id AND dp.id = [ID_DISTRIBUTION_POINT] <br>
      JOIN `sku_availability` sa ON sa.id_sku = s.id AND sa.id_storage = [ID_STORAGE] <br>
      ORDER BY s.id, time_diff  <br>
      ) as `sku` GROUP BY id  <br>
      </i>
    </div>
  </li>
  <li>sku_availability</li>
  <li>sku_prices</li>
</ul>

<h3>Tables</h3>
<div class="query">
CREATE TABLE `callback` (
  `id` INTEGER NOT NULL PRIMARY KEY,
  `date` INTEGER NULL DEFAULT NULL,
  `id_distribution_point` INTEGER UNSIGNED ZEROFILL NULL DEFAULT NULL
);
CREATE INDEX `index_callback_date` ON callback ( date );
CREATE INDEX `index_callback_id_distribution_point` ON callback ( id_distribution_point);


CREATE TABLE `callback_has_sku` (
  `id_callback` INTEGER(10) NOT NULL DEFAULT '0',
  `id_sku` INTEGER ZEROFILL UNSIGNED  NOT NULL,
  `quantity` DECIMAL(10,3) NULL DEFAULT NULL,
  `reason` TEXT NULL,
  PRIMARY KEY (`id_callback`, `id_sku`),
  FOREIGN KEY (`id_callback`) REFERENCES `callback` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE `order` (
  `id` INTEGER UNSIGNED NOT NULL DEFAULT '0' PRIMARY KEY,
  `id_distribution_point` INTEGER UNSIGNED ZEROFILL NULL DEFAULT NULL,
  `date` INTEGERUNSIGNED NULL DEFAULT NULL,
  `exported` VARCHAR(3) NULL DEFAULT 'no'
);


CREATE INDEX `index_order_dt` ON `order` (`id_distribution_point`);
CREATE INDEX `index_order_date` ON `order` (`date`);

CREATE TABLE `order_has_sku` (
  `id_order` INTEGER UNSIGNED NOT NULL DEFAULT '0',
  `id_sku` INTEGER UNSIGNED ZEROFILL NOT NULL ,
  `quantity` DECIMAL(10,3) NOT NULL,
  PRIMARY KEY (`id_order`, `id_sku`),
  FOREIGN KEY (`id_order`) REFERENCES `order` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE `remains` (
  `id` INTEGER(10) NOT NULL  PRIMARY KEY ,
  `date` INTEGER(11) NULL DEFAULT '0',
  `id_distribution_point` INTEGER UNSIGNED ZEROFILL NOT NULL DEFAULT '00000000000'
);

CREATE INDEX `index_remains_date` ON `remains` (`date`);
CREATE INDEX `index_remains_id_distribution_point` ON `remains` (`id_distribution_point`);

CREATE TABLE `remains_has_sku` (
  `id_remains` INTEGER(10) NOT NULL DEFAULT '0',
  `id_sku` INTEGER(11) UNSIGNED ZEROFILL NOT NULL,
  `quantity` DECIMAL(10,3) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_remains`, `id_sku`),
  FOREIGN KEY (`id_remains`) REFERENCES `remains` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE `remains_has_sku` (
  `id_remains` INTEGER(10) NOT NULL DEFAULT '0',
  `id_sku` INTEGER UNSIGNED ZEROFILL NOT NULL,
  `quantity` DECIMAL(10,3) NOT NULL,
  PRIMARY KEY (`id_remains`, `id_sku`),
  FOREIGN KEY (`id_remains`) REFERENCES `remains` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
);
</div>


<?php }} ?>