<?php /* Smarty version Smarty-3.1.2, created on 2013-12-12 08:48:57
         compiled from "application/templates/direction/photo/list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:58336237152a95c12a4ac60-56320169%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c18d67da7233a806864ae548073b59e89d580525' => 
    array (
      0 => 'application/templates/direction/photo/list.tpl',
      1 => 1386830937,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '58336237152a95c12a4ac60-56320169',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.2',
  'unifunc' => 'content_52a95c12ad655',
  'variables' => 
  array (
    'orders' => 0,
    'prevDate' => 0,
    'o' => 0,
    'total' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52a95c12ad655')) {function content_52a95c12ad655($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/salevel.com/lib/smarty/plugins/modifier.date_format.php';
?><div class="row">
  <div class="span12">
    <h4>Заказы</h4>
    <?php if (!$_smarty_tpl->tpl_vars['orders']->value->count()){?>
    <p class="lead">Нет данных</p>
    <?php }else{ ?>
    <table class="table table-bordered table-hover">
      <thead>
        <tr>
          <th>ID</th>
          <th>Время</th>
          <th>Менеджер</th>
          <th>Торговая точка</th>
         
          <th></th>
        </tr>
      </thead>
      <tbody>
      <?php $_smarty_tpl->tpl_vars['prevDate'] = new Smarty_variable(null, null, 0);?>
      <?php $_smarty_tpl->tpl_vars['total'] = new Smarty_variable(0, null, 0);?>
      <?php  $_smarty_tpl->tpl_vars['o'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['o']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['orders']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['o']->key => $_smarty_tpl->tpl_vars['o']->value){
$_smarty_tpl->tpl_vars['o']->_loop = true;
?>
        <?php if ($_smarty_tpl->tpl_vars['prevDate']->value!=smarty_modifier_date_format($_smarty_tpl->tpl_vars['o']->value->date,'%d%m%Y')){?>
        <?php if ($_smarty_tpl->tpl_vars['prevDate']->value){?>
        <tr>
          <td colspan="4"></td>
          <td colspan="2"><b>Всего: <?php echo number_format($_smarty_tpl->tpl_vars['total']->value,'2','.',' ');?>
 грн.</b></td>
        </tr>
        <?php $_smarty_tpl->tpl_vars['total'] = new Smarty_variable(0, null, 0);?>
        <?php }?>
        <?php $_smarty_tpl->tpl_vars['prevDate'] = new Smarty_variable(smarty_modifier_date_format($_smarty_tpl->tpl_vars['o']->value->date,'%d%m%Y'), null, 0);?>
        <tr class="warning" >
          <td colspan="6"><div class="lead" style="margin-bottom: 0; text-align: center;">&nbsp;<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['o']->value->date,'%d/%m/%Y');?>
</div></td>
        </tr>
        <?php }?>
        <tr>
          <td><?php echo $_smarty_tpl->tpl_vars['o']->value->id;?>
</td>
          <td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['o']->value->date,'%H:%M');?>
</td>
          <td><?php echo preg_replace('!<[^>]*?>!', ' ', stripslashes($_smarty_tpl->tpl_vars['o']->value->manager_name));?>
</td>
          <td>s<img src="/<?php echo preg_replace('!<[^>]*?>!', ' ', stripslashes($_smarty_tpl->tpl_vars['o']->value->image));?>
"></td>
          
          <td>&nbsp;&nbsp;<a class="btn" title="Детали" href="/direction/orders/view/<?php echo $_smarty_tpl->tpl_vars['o']->value->id;?>
">Детали</a></td>
        </tr>
        <?php $_smarty_tpl->tpl_vars['total'] = new Smarty_variable($_smarty_tpl->tpl_vars['total']->value+$_smarty_tpl->tpl_vars['o']->value->total, null, 0);?>
      <?php } ?>
      </tbody>
    </table>
    <?php echo $_smarty_tpl->tpl_vars['orders']->value->pager();?>

    <?php }?>
  </div>
</div><?php }} ?>