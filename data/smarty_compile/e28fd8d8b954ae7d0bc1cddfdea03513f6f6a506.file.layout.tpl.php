<?php /* Smarty version Smarty-3.1.2, created on 2013-10-23 18:59:19
         compiled from "application/templates/index/layout.tpl" */ ?>
<?php /*%%SmartyHeaderCode:73134389520a531569ebd6-82192409%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e28fd8d8b954ae7d0bc1cddfdea03513f6f6a506' => 
    array (
      0 => 'application/templates/index/layout.tpl',
      1 => 1382543695,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '73134389520a531569ebd6-82192409',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.2',
  'unifunc' => 'content_520a53156f6f3',
  'variables' => 
  array (
    'description' => 0,
    'keywords' => 0,
    'css' => 0,
    'path' => 0,
    'js' => 0,
    'title' => 0,
    'content' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_520a53156f6f3')) {function content_520a53156f6f3($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="<?php echo $_smarty_tpl->tpl_vars['description']->value;?>
" />
  <meta name="keywords" content="<?php echo $_smarty_tpl->tpl_vars['keywords']->value;?>
" />
  <meta name="robots" content="index,follow" />
  <meta name="audience" content="all" />
  <meta name="content-language" content="RU" />
  <link href="/images/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
  <?php  $_smarty_tpl->tpl_vars['path'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['path']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['css']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['path']->key => $_smarty_tpl->tpl_vars['path']->value){
$_smarty_tpl->tpl_vars['path']->_loop = true;
?>
  <link href="<?php echo $_smarty_tpl->tpl_vars['path']->value;?>
" rel="stylesheet" type="text/css" />
  <?php } ?>
  <?php  $_smarty_tpl->tpl_vars['path'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['path']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['js']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['path']->key => $_smarty_tpl->tpl_vars['path']->value){
$_smarty_tpl->tpl_vars['path']->_loop = true;
?>
  <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['path']->value;?>
"></script>
  <?php } ?>
  <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>
</head>
<body>
<?php echo $_smarty_tpl->tpl_vars['content']->value;?>

</body>
</html><?php }} ?>