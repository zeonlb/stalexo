<?php /* Smarty version Smarty-3.1.2, created on 2013-10-31 23:16:11
         compiled from "application/templates/backend/devices/add.tpl" */ ?>
<?php /*%%SmartyHeaderCode:937938832520c5b9c910a60-64065331%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cf938850101f17c0267fb248f9a7513a50f29231' => 
    array (
      0 => 'application/templates/backend/devices/add.tpl',
      1 => 1382543706,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '937938832520c5b9c910a60-64065331',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.2',
  'unifunc' => 'content_520c5b9c9d7f5',
  'variables' => 
  array (
    'd' => 0,
    'staff' => 0,
    's' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_520c5b9c9d7f5')) {function content_520c5b9c9d7f5($_smarty_tpl) {?><div class="span4 well">
  <legend><?php if ($_smarty_tpl->tpl_vars['d']->value->isLoaded()){?>Редактировать<?php }else{ ?>Добавить<?php }?> планшет</legend>
  <form accept-charset="UTF-8" action="" method="post">
    <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['d']->value->id_staff;?>
">
    <input class="span3" name="device" <?php if ($_smarty_tpl->tpl_vars['d']->value->isLoaded()){?>value="<?php echo $_smarty_tpl->tpl_vars['d']->value->device;?>
"<?php }?> placeholder="ID-DEVICE" type="text">
    <input class="span3" name="password" placeholder="<?php if ($_smarty_tpl->tpl_vars['d']->value->isLoaded()){?>New password<?php }else{ ?>Password<?php }?>" type="password"> 
    <select name="staff">
      <?php  $_smarty_tpl->tpl_vars['s'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['s']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['staff']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['s']->key => $_smarty_tpl->tpl_vars['s']->value){
$_smarty_tpl->tpl_vars['s']->_loop = true;
?>
      <option <?php if ($_smarty_tpl->tpl_vars['s']->value->id_staff&&$_smarty_tpl->tpl_vars['s']->value->id_staff!=$_smarty_tpl->tpl_vars['d']->value->id_staff){?>disabled="disabled"<?php }?> <?php if ($_smarty_tpl->tpl_vars['d']->value->id_staff==$_smarty_tpl->tpl_vars['s']->value->id){?>selected="selected"<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['s']->value->id;?>
"><?php echo stripslashes($_smarty_tpl->tpl_vars['s']->value->name);?>
<?php if ($_smarty_tpl->tpl_vars['s']->value->id_staff!=$_smarty_tpl->tpl_vars['d']->value->id_staff&&$_smarty_tpl->tpl_vars['s']->value->device){?> <?php echo $_smarty_tpl->tpl_vars['s']->value->device;?>
<?php }?></option>
      <?php } ?>
    </select>
    <br>
    <button class="btn btn-primary" type="submit">Сохранить</button>
    <a class="btn" href="/backend/devices">Отмена</a>
  </form>
</div><?php }} ?>