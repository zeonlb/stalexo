<?php /* Smarty version Smarty-3.1.2, created on 2013-10-31 23:15:55
         compiled from "application/templates/layout/backend.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1359904453520c5a4a7fdd32-06601278%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3439202d923625d9b32f348fc81b091a17cba0cf' => 
    array (
      0 => 'application/templates/layout/backend.tpl',
      1 => 1382543695,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1359904453520c5a4a7fdd32-06601278',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.2',
  'unifunc' => 'content_520c5a4a87e0f',
  'variables' => 
  array (
    'user' => 0,
    'request' => 0,
    'message' => 0,
    'content' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_520c5a4a87e0f')) {function content_520c5a4a87e0f($_smarty_tpl) {?>
<style type="text/css">
  body {
  //padding-top: 60px;
  padding-bottom: 40px;
  height: 100%;
  }

</style>

<div id="wrap">
  <div class="navbar nav">
    <div class="navbar-inner">
      <div class="container">
        <a class="brand" href="/backend">&nbsp;&nbsp;Backend&nbsp;&nbsp;</a>
        
        <div class="nav">
          <ul class="nav">
            <?php if ($_smarty_tpl->tpl_vars['user']->value->hasPermission('users_view')){?>
            <li <?php if ($_smarty_tpl->tpl_vars['request']->value->get(1)=='users'){?>class="active"<?php }?>><a href="/backend/users"><i class="icon-user"></i> Users</a></li>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['user']->value->hasPermission('users_permission')){?>
            <li <?php if ($_smarty_tpl->tpl_vars['request']->value->get(1)=='permission'){?>class="active"<?php }?>><a href="/backend/permission"><i class="icon-lock"></i> Права</a></li>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['user']->value->hasPermission('users_crud_devices')){?>
            <li <?php if ($_smarty_tpl->tpl_vars['request']->value->get(1)=='devices'){?>class="active"<?php }?>><a href="/backend/devices"><i class="icon-briefcase"></i> Devices</a></li>
            <?php }?>
             <li <?php if ($_smarty_tpl->tpl_vars['request']->value->get(1)=='settings'){?>class="active"<?php }?>><a href="/backend/settings"><i class="icon-briefcase"></i> Настройки планшетов</a></li>
           
          </ul>
        </div>

      </div>
    </div>
  </div>
   
  <div class="container">
    <?php echo $_smarty_tpl->tpl_vars['message']->value;?>

    <?php echo $_smarty_tpl->tpl_vars['content']->value;?>

    <br>
  </div>

  <div id="push"></div>
</div><?php }} ?>