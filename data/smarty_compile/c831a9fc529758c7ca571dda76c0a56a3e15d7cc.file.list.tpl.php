<?php /* Smarty version Smarty-3.1.2, created on 2013-08-14 17:29:09
         compiled from "application/templates/direction/sku/list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1898639729520b94355b90b2-03375820%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c831a9fc529758c7ca571dda76c0a56a3e15d7cc' => 
    array (
      0 => 'application/templates/direction/sku/list.tpl',
      1 => 1371204868,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1898639729520b94355b90b2-03375820',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'skuStats' => 0,
    'collection' => 0,
    'id_group' => 0,
    'sku' => 0,
    'thumb' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.2',
  'unifunc' => 'content_520b943566b2e',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_520b943566b2e')) {function content_520b943566b2e($_smarty_tpl) {?><div class="row">
  <div class="span12">
    <h5>Sku: <?php echo $_smarty_tpl->tpl_vars['skuStats']->value['items'];?>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sku с изображением: <?php echo $_smarty_tpl->tpl_vars['skuStats']->value['items_with_image'];?>
</h5>
    <table class="table table-bordered table-hover">
      <thead>
        <tr>
          <th>ID</th>
          <th>Название</th>
          <th>ед.</th>
          <th>Лого</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <?php $_smarty_tpl->tpl_vars['id_group'] = new Smarty_variable(null, null, 0);?>
        <?php  $_smarty_tpl->tpl_vars['sku'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sku']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['collection']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['sku']->key => $_smarty_tpl->tpl_vars['sku']->value){
$_smarty_tpl->tpl_vars['sku']->_loop = true;
?>
        <?php if ($_smarty_tpl->tpl_vars['id_group']->value!=$_smarty_tpl->tpl_vars['sku']->value->id_group){?>
        <?php $_smarty_tpl->tpl_vars['id_group'] = new Smarty_variable($_smarty_tpl->tpl_vars['sku']->value->id_group, null, 0);?>
        <tr class="warning" >
          <td colspan="6"><div class="lead" style="margin-bottom: 0; text-align: center;">&nbsp;<?php echo stripslashes($_smarty_tpl->tpl_vars['sku']->value->group_title);?>
</div></td>
        </tr>
        <?php }?>
        <tr>
          <td style="vertical-align: middle;"><?php echo $_smarty_tpl->tpl_vars['sku']->value->id;?>
</td>
          <td style="vertical-align: middle;"><?php echo stripslashes($_smarty_tpl->tpl_vars['sku']->value->title);?>
</td>
          <td style="vertical-align: middle;"><?php echo trim(stripslashes($_smarty_tpl->tpl_vars['sku']->value->units));?>
</td>
          <td style="text-align: center; width: 80px;">
          <?php $_smarty_tpl->tpl_vars['thumb'] = new Smarty_variable($_smarty_tpl->tpl_vars['sku']->value->getImageSmall(), null, 0);?>
          <img <?php if ($_smarty_tpl->tpl_vars['thumb']->value){?>src="/<?php echo $_smarty_tpl->tpl_vars['thumb']->value;?>
"<?php }else{ ?>data-src="holder.js/80x80"<?php }?>>
          </td>
          <td style="vertical-align: middle;"><a class="btn" title="Детали" href="/direction/sku/view/<?php echo $_smarty_tpl->tpl_vars['sku']->value->id;?>
">Детали</a></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
    <?php echo $_smarty_tpl->tpl_vars['collection']->value->pager();?>

  </div>
</div><?php }} ?>