<?php /* Smarty version Smarty-3.1.2, created on 2013-12-03 17:53:29
         compiled from "application/templates/direction/remains/list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2005506094520b20b626d033-81434754%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2b96dae0511850a03b9fba612f5def74540965a1' => 
    array (
      0 => 'application/templates/direction/remains/list.tpl',
      1 => 1382543709,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2005506094520b20b626d033-81434754',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.2',
  'unifunc' => 'content_520b20b6336bf',
  'variables' => 
  array (
    'collection' => 0,
    'prevDate' => 0,
    'r' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_520b20b6336bf')) {function content_520b20b6336bf($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/salevel.com/lib/smarty/plugins/modifier.date_format.php';
?><div class="row">
  <div class="span12">
    <h4>Остатки</h4>
    <?php if (!$_smarty_tpl->tpl_vars['collection']->value->count()){?>
    <p class="lead">Нет данных</p>
    <?php }else{ ?>
    <table class="table table-bordered table-hover">
      <thead>
        <tr>
          <th>ID</th>
          <th>Время</th>
          <th>Менеджер</th>
          <th>Торговая точка</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
      <?php $_smarty_tpl->tpl_vars['prevDate'] = new Smarty_variable(null, null, 0);?>
      <?php  $_smarty_tpl->tpl_vars['r'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['r']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['collection']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['r']->key => $_smarty_tpl->tpl_vars['r']->value){
$_smarty_tpl->tpl_vars['r']->_loop = true;
?>
        <?php if ($_smarty_tpl->tpl_vars['prevDate']->value!=smarty_modifier_date_format($_smarty_tpl->tpl_vars['r']->value->date,'%d%m%Y')){?>
        <?php $_smarty_tpl->tpl_vars['prevDate'] = new Smarty_variable(smarty_modifier_date_format($_smarty_tpl->tpl_vars['r']->value->date,'%d%m%Y'), null, 0);?>
        <tr class="warning" >
          <td colspan="6"><div class="lead" style="margin-bottom: 0; text-align: center;">&nbsp;<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['r']->value->date,'%d/%m/%Y');?>
</div></td>
        </tr>
        <?php }?>
        <tr>
          <td><?php echo $_smarty_tpl->tpl_vars['r']->value->id;?>
</td>
          <td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['r']->value->date,'%H:%M');?>
</td>
          <td><?php echo preg_replace('!<[^>]*?>!', ' ', stripslashes($_smarty_tpl->tpl_vars['r']->value->manager_name));?>
</td>
          <td><?php echo preg_replace('!<[^>]*?>!', ' ', stripslashes($_smarty_tpl->tpl_vars['r']->value->address));?>
</td>
          <td>&nbsp;&nbsp;<a class="btn" title="Детали" href="/direction/remains/view/<?php echo $_smarty_tpl->tpl_vars['r']->value->id;?>
">Детали</a></td>
        </tr>
      <?php } ?>
      </tbody>
    </table>
    <?php echo $_smarty_tpl->tpl_vars['collection']->value->pager();?>

    <?php }?>
  </div>
</div><?php }} ?>