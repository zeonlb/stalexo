<?php /* Smarty version Smarty-3.1.2, created on 2013-10-31 23:15:46
         compiled from "application/templates/pager/jumping-stalexo.tpl" */ ?>
<?php /*%%SmartyHeaderCode:668315421520a532542d9a6-48851862%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7a82e8d23197cda00c42258f21debdaf2f332ac4' => 
    array (
      0 => 'application/templates/pager/jumping-stalexo.tpl',
      1 => 1382543695,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '668315421520a532542d9a6-48851862',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.2',
  'unifunc' => 'content_520a5325609cb',
  'variables' => 
  array (
    'pagesCount' => 0,
    'page' => 0,
    'prev' => 0,
    'url' => 0,
    'maxButtonsInPager' => 0,
    'p' => 0,
    'float' => 0,
    'currentPageRange' => 0,
    'buttonsInSequence' => 0,
    'next' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_520a5325609cb')) {function content_520a5325609cb($_smarty_tpl) {?><?php if ($_smarty_tpl->tpl_vars['pagesCount']->value>1){?>
<div class="pagination">
<ul>
<li <?php if ($_smarty_tpl->tpl_vars['page']->value==$_smarty_tpl->tpl_vars['prev']->value){?>class="disabled"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
/page/<?php echo $_smarty_tpl->tpl_vars['prev']->value;?>
">&laquo;</a></li>

<?php if ($_smarty_tpl->tpl_vars['pagesCount']->value<$_smarty_tpl->tpl_vars['maxButtonsInPager']->value){?>
<?php $_smarty_tpl->tpl_vars['p'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['p']->step = 1;$_smarty_tpl->tpl_vars['p']->total = (int)ceil(($_smarty_tpl->tpl_vars['p']->step > 0 ? $_smarty_tpl->tpl_vars['pagesCount']->value+1 - (1) : 1-($_smarty_tpl->tpl_vars['pagesCount']->value)+1)/abs($_smarty_tpl->tpl_vars['p']->step));
if ($_smarty_tpl->tpl_vars['p']->total > 0){
for ($_smarty_tpl->tpl_vars['p']->value = 1, $_smarty_tpl->tpl_vars['p']->iteration = 1;$_smarty_tpl->tpl_vars['p']->iteration <= $_smarty_tpl->tpl_vars['p']->total;$_smarty_tpl->tpl_vars['p']->value += $_smarty_tpl->tpl_vars['p']->step, $_smarty_tpl->tpl_vars['p']->iteration++){
$_smarty_tpl->tpl_vars['p']->first = $_smarty_tpl->tpl_vars['p']->iteration == 1;$_smarty_tpl->tpl_vars['p']->last = $_smarty_tpl->tpl_vars['p']->iteration == $_smarty_tpl->tpl_vars['p']->total;?>
  <?php if ($_smarty_tpl->tpl_vars['p']->value==$_smarty_tpl->tpl_vars['page']->value){?>
  <li class="active"><span><?php echo $_smarty_tpl->tpl_vars['p']->value;?>
</span></li>
  <?php }else{ ?>
  <li><a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
/page/<?php echo $_smarty_tpl->tpl_vars['p']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['p']->value;?>
</a></li>
  <?php }?>
<?php }} ?>
<?php }elseif($_smarty_tpl->tpl_vars['float']->value=='center'){?>
<li><a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
/">1</a></li>
<li><span>...</span></li>
<?php $_smarty_tpl->tpl_vars['p'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['p']->step = 1;$_smarty_tpl->tpl_vars['p']->total = (int)ceil(($_smarty_tpl->tpl_vars['p']->step > 0 ? ($_smarty_tpl->tpl_vars['page']->value+$_smarty_tpl->tpl_vars['currentPageRange']->value)+1 - (($_smarty_tpl->tpl_vars['page']->value-$_smarty_tpl->tpl_vars['currentPageRange']->value)) : ($_smarty_tpl->tpl_vars['page']->value-$_smarty_tpl->tpl_vars['currentPageRange']->value)-(($_smarty_tpl->tpl_vars['page']->value+$_smarty_tpl->tpl_vars['currentPageRange']->value))+1)/abs($_smarty_tpl->tpl_vars['p']->step));
if ($_smarty_tpl->tpl_vars['p']->total > 0){
for ($_smarty_tpl->tpl_vars['p']->value = ($_smarty_tpl->tpl_vars['page']->value-$_smarty_tpl->tpl_vars['currentPageRange']->value), $_smarty_tpl->tpl_vars['p']->iteration = 1;$_smarty_tpl->tpl_vars['p']->iteration <= $_smarty_tpl->tpl_vars['p']->total;$_smarty_tpl->tpl_vars['p']->value += $_smarty_tpl->tpl_vars['p']->step, $_smarty_tpl->tpl_vars['p']->iteration++){
$_smarty_tpl->tpl_vars['p']->first = $_smarty_tpl->tpl_vars['p']->iteration == 1;$_smarty_tpl->tpl_vars['p']->last = $_smarty_tpl->tpl_vars['p']->iteration == $_smarty_tpl->tpl_vars['p']->total;?>
 <?php if ($_smarty_tpl->tpl_vars['p']->value==$_smarty_tpl->tpl_vars['page']->value){?>
  <li class="active"><span><?php echo $_smarty_tpl->tpl_vars['p']->value;?>
</span></li>
  <?php }else{ ?>
  <li><a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
/page/<?php echo $_smarty_tpl->tpl_vars['p']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['p']->value;?>
</a></li>
  <?php }?>
<?php }} ?>
<li><span>...</span></li>
<li><a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
/page/<?php echo $_smarty_tpl->tpl_vars['pagesCount']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['pagesCount']->value;?>
</a></li>
<?php }elseif($_smarty_tpl->tpl_vars['float']->value=='left'){?>
<?php $_smarty_tpl->tpl_vars['p'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['p']->step = 1;$_smarty_tpl->tpl_vars['p']->total = (int)ceil(($_smarty_tpl->tpl_vars['p']->step > 0 ? $_smarty_tpl->tpl_vars['buttonsInSequence']->value+1 - (1) : 1-($_smarty_tpl->tpl_vars['buttonsInSequence']->value)+1)/abs($_smarty_tpl->tpl_vars['p']->step));
if ($_smarty_tpl->tpl_vars['p']->total > 0){
for ($_smarty_tpl->tpl_vars['p']->value = 1, $_smarty_tpl->tpl_vars['p']->iteration = 1;$_smarty_tpl->tpl_vars['p']->iteration <= $_smarty_tpl->tpl_vars['p']->total;$_smarty_tpl->tpl_vars['p']->value += $_smarty_tpl->tpl_vars['p']->step, $_smarty_tpl->tpl_vars['p']->iteration++){
$_smarty_tpl->tpl_vars['p']->first = $_smarty_tpl->tpl_vars['p']->iteration == 1;$_smarty_tpl->tpl_vars['p']->last = $_smarty_tpl->tpl_vars['p']->iteration == $_smarty_tpl->tpl_vars['p']->total;?>
 <?php if ($_smarty_tpl->tpl_vars['p']->value==$_smarty_tpl->tpl_vars['page']->value){?>
  <li class="active"><span><?php echo $_smarty_tpl->tpl_vars['p']->value;?>
</span></li>
  <?php }else{ ?>
  <li><a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
/page/<?php echo $_smarty_tpl->tpl_vars['p']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['p']->value;?>
</a></li>
  <?php }?>
<?php }} ?>
<li><span>...</span></li>
<li><a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
/page/<?php echo $_smarty_tpl->tpl_vars['pagesCount']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['pagesCount']->value;?>
</a></li>
<?php }elseif($_smarty_tpl->tpl_vars['float']->value=='right'){?>
<li><a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
/page/1">1</a></li>
<li><span>...</span></li>
<?php $_smarty_tpl->tpl_vars['p'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['p']->step = 1;$_smarty_tpl->tpl_vars['p']->total = (int)ceil(($_smarty_tpl->tpl_vars['p']->step > 0 ? $_smarty_tpl->tpl_vars['pagesCount']->value+1 - (($_smarty_tpl->tpl_vars['pagesCount']->value-$_smarty_tpl->tpl_vars['buttonsInSequence']->value+1)) : ($_smarty_tpl->tpl_vars['pagesCount']->value-$_smarty_tpl->tpl_vars['buttonsInSequence']->value+1)-($_smarty_tpl->tpl_vars['pagesCount']->value)+1)/abs($_smarty_tpl->tpl_vars['p']->step));
if ($_smarty_tpl->tpl_vars['p']->total > 0){
for ($_smarty_tpl->tpl_vars['p']->value = ($_smarty_tpl->tpl_vars['pagesCount']->value-$_smarty_tpl->tpl_vars['buttonsInSequence']->value+1), $_smarty_tpl->tpl_vars['p']->iteration = 1;$_smarty_tpl->tpl_vars['p']->iteration <= $_smarty_tpl->tpl_vars['p']->total;$_smarty_tpl->tpl_vars['p']->value += $_smarty_tpl->tpl_vars['p']->step, $_smarty_tpl->tpl_vars['p']->iteration++){
$_smarty_tpl->tpl_vars['p']->first = $_smarty_tpl->tpl_vars['p']->iteration == 1;$_smarty_tpl->tpl_vars['p']->last = $_smarty_tpl->tpl_vars['p']->iteration == $_smarty_tpl->tpl_vars['p']->total;?>
 <?php if ($_smarty_tpl->tpl_vars['p']->value==$_smarty_tpl->tpl_vars['page']->value){?>
  <li class="active"><span><?php echo $_smarty_tpl->tpl_vars['p']->value;?>
</span></li>
  <?php }else{ ?>
  <li><a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
/page/<?php echo $_smarty_tpl->tpl_vars['p']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['p']->value;?>
</a></li>
  <?php }?>
<?php }} ?>
<?php }?>

<li <?php if ($_smarty_tpl->tpl_vars['page']->value==$_smarty_tpl->tpl_vars['next']->value){?>class="disabled"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
/page/<?php echo $_smarty_tpl->tpl_vars['next']->value;?>
">&raquo;</a></li>

</ul>
</div>
<?php }?><?php }} ?>