<?php /* Smarty version Smarty-3.1.2, created on 2013-12-16 10:22:33
         compiled from "application/templates/direction/routes/agents-by-date.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19223550985220b742766546-23321359%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '57fa7f73e698204af9887825971dcfe6b4081755' => 
    array (
      0 => 'application/templates/direction/routes/agents-by-date.tpl',
      1 => 1382543710,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19223550985220b742766546-23321359',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.2',
  'unifunc' => 'content_5220b74293cc6',
  'variables' => 
  array (
    'message' => 0,
    'agents' => 0,
    'a' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5220b74293cc6')) {function content_5220b74293cc6($_smarty_tpl) {?><?php echo (($tmp = @$_smarty_tpl->tpl_vars['message']->value)===null||$tmp==='' ? '' : $tmp);?>


<style type="text/css">
.table-row{
  cursor: pointer;
}

.ignore-margin-left{
  margin-left: 0;
}
</style>

<div class="row">
<?php echo $_smarty_tpl->getSubTemplate ("direction/routes/map-legend.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<script src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>

<div class="span12" id="map" style="height:645px"></div>

<script type="text/javascript">

var agents = [];
<?php  $_smarty_tpl->tpl_vars['a'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['a']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['agents']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['a']->key => $_smarty_tpl->tpl_vars['a']->value){
$_smarty_tpl->tpl_vars['a']->_loop = true;
?>
  agents.push({
    id:          "<?php echo $_smarty_tpl->tpl_vars['a']->value->id;?>
",
    name:        "<?php echo $_smarty_tpl->tpl_vars['a']->value->name;?>
",
    timelines:   <?php echo json_encode($_smarty_tpl->tpl_vars['a']->value->timelinesArray);?>
,
    routePoints: <?php echo json_encode($_smarty_tpl->tpl_vars['a']->value->pointsArray);?>

  });
<?php } ?>


$(document).ready(function(){
  var SXMap = new Map();

  SXMap.display();

});

var Map = function(){

  var $this = this;

  this.instance = null;

  this.display = function(center, points){
    ymaps.ready(function(){
      $this.instance = new ymaps.Map("map", {
        center: center || [46.46, 30.7],
        zoom: 11
      });

      if ( agents && agents.length ){
        for( i = 0; i < agents.length; i++ ){
          $this.drawPlanRoute(agents[i].routePoints, agents[i].name);
          $this.drawFactRoute(agents[i].timelines,  agents[i].name);
        }
      }

    },
    function (error) {
      alert('Возникла ошибка: ' + error.message);
    });
  };

  this.drawFactRoute = function(points, agentName){
    if ( ! (points && points.length) ){
      return false;
    }
    var routePoints = [];

    $.each(points, function(i,v){
      routePoints.push({ 'type': 'wayPoint', point: [v.lat, v.lng] });
    });

    ymaps.route(routePoints,
      {mapStateAutoApply: false}
    ).then(function(route){
      ////////////////////////////
      route.getPaths().options.set({
        strokeColor: '1636FD', opacity: 0.7,

      });

      var i = 0;
      route.getWayPoints().each(function(v){
        var icon = "twirl#darkblueIcon";
        if( points[i].start ){
          icon = 'twirl#darkblueDotIcon';
        } else if( points[i].end ){
          icon = 'twirl#nightDotIcon';
        } else if ( ! points[i].enter ){
          icon = "twirl#nightIcon";
        } else if ( (! points[i].valid) || (points[i].enter && points[i].invalid_distance) ){
          icon  = "twirl#redIcon";
        } else if( points[i].invalid_begintime == true ){
          icon = "twirl#darkorangeIcon";
        }

        v.options.set({
          preset: icon
        });

        v.properties.set({
          'iconContent': (points[i].index+1),
          balloonContentBody:
          ( points[i].enter ? ('<b>'+agentName+'</b><br>'+points[i].title+'<br>'+points[i].time_begin+' <br><i>'+points[i].id_dp+'</i> '+points[i].address+(points[i].content ? '<hr>'+points[i].content : ''))
          : ( points[i].start ? 'Старт работы '+points[i].time+'<br>'+points[i].agent : ( points[i].end ? 'Окончание работы '+points[i].time+'<br>'+points[i].agent: 'Выход '+points[i].time_end) ) )
        });

        i++;
      });

      $.each(points, function(i,v){
        if ( ! v.enter && !v.start && ! v.end){
          var segment = route.getPaths().get((i-1));
          if ( segment ){
            segment.options.set({
              strokeColor: 'FE1010',
              opacity: 0.6,
              strokeWidth: 5
            });
          } else {
            //console.log('invalid segment '+i)
          }

        }
      })

      $this.instance.geoObjects.add(route);
      ///////////////////////////////
    });

  };

  this.drawPlanRoute = function(points, agentName){
    if ( ! (points && points.length) ){
      return false;
    }
    var routePoints = [];
    $.each(points, function(i,v){
      routePoints.push({ 'type': 'wayPoint', point: [v.lat, v.lng] });
    });

    ymaps.route(routePoints,
      {mapStateAutoApply: false}
    ).then(function(route){
      ////////////////////////////
      route.getPaths().options.set({
        strokeColor: '4E9D3C', opacity: 0.9,

      });

      var i = 0;
      route.getWayPoints().each(function(v){

        route.getWayPoints().options.set({
          preset: "twirl#darkgreenIcon"
        });

        v.properties.set({
          'iconContent': (i+1),
          balloonContentBody: '<b>'+agentName+'</b><br>'+points[i].title +'<br>'+ points[i].time+' '+points[i].address
        });

        i++;
      });

      $this.instance.geoObjects.add(route);
      ///////////////////////////////
    });

  };

  this.drawPoints = function(points){
    if ( ! (points && points.length) ){
      return false;
    }

    $.each(points, function(i,v){
      var placemark = new ymaps.Placemark([v.lat, v.lng], {
          balloonContent: '<p>'+v.address+'<br> '+v.time+'</p>',
          iconContent: v.prior
      }, {
          preset: "twirl#darkgreenIcon",
          // Disabling the Close Balloon button.
          balloonCloseButton: true,
          // The balloon will be opened and closed by clicking the placemark icon.
          hideIconOnBalloonOpen: true
      });
      $this.instance.geoObjects.add(placemark);
    });
  };

  this.construct = function(){

  };

  $this.construct();

};

</script>
<div class="clearfix"></div>

</div><?php }} ?>