<?php /* Smarty version Smarty-3.1.2, created on 2013-10-31 23:16:31
         compiled from "application/templates/backend/permission/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:683078653520c5a4ca90072-29096753%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '038fa88620c8eec7999cdf87ced9ce7dd99dc98c' => 
    array (
      0 => 'application/templates/backend/permission/index.tpl',
      1 => 1382543706,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '683078653520c5a4ca90072-29096753',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.2',
  'unifunc' => 'content_520c5a4cbb493',
  'variables' => 
  array (
    'user' => 0,
    'roles' => 0,
    'r' => 0,
    'permission' => 0,
    'group' => 0,
    'key' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_520c5a4cbb493')) {function content_520c5a4cbb493($_smarty_tpl) {?><div class="btn-toolbar">
  <button class="btn btn-primary<?php if (!$_smarty_tpl->tpl_vars['user']->value->hasPermission('users_crud_permission')){?> disabled<?php }?>" <?php if ($_smarty_tpl->tpl_vars['user']->value->hasPermission('users_crud_permission')){?>onclick="javascript:location='/backend/permission/add';"<?php }else{ ?>title="Недостаточно полномочий"<?php }?>>Добавить разрешение</button>
  <?php if ($_smarty_tpl->tpl_vars['user']->value->hasPermission('users_crud_permission_groups')){?>
  <a class="btn btn-link" href="/backend/permission/groups">Групы полномочий</a>
  <?php }?>
  <?php if ($_smarty_tpl->tpl_vars['user']->value->hasPermission('users_crud_roles')){?>
  <a class="btn btn-link" href="/backend/permission/roles">Роли в системе</a>
  <?php }?>
</div>
<div class="row">
  <div class="span12">
    <form name="role-permission-form" action="/backend/permission/edit-role-permission" method="POST">
    <table class="table table-condensed table-hover">
      <thead>
        <tr>
          <th> </th>
          <?php  $_smarty_tpl->tpl_vars['r'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['r']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['roles']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['r']->key => $_smarty_tpl->tpl_vars['r']->value){
$_smarty_tpl->tpl_vars['r']->_loop = true;
?>
          <th style="text-align: center;"><?php echo $_smarty_tpl->tpl_vars['r']->value->name;?>
</th>
          <?php } ?>
        </tr>
      </thead>
      <tbody>
        <?php $_smarty_tpl->tpl_vars['group'] = new Smarty_variable(false, null, 0);?>
        <?php  $_smarty_tpl->tpl_vars['permission'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['permission']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['roles']->value[0]->children('permissions'); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['permission']->key => $_smarty_tpl->tpl_vars['permission']->value){
$_smarty_tpl->tpl_vars['permission']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['permission']->key;
?>       
        <?php if ($_smarty_tpl->tpl_vars['permission']->value->id_group!=$_smarty_tpl->tpl_vars['group']->value){?>
        <?php $_smarty_tpl->tpl_vars['group'] = new Smarty_variable($_smarty_tpl->tpl_vars['permission']->value->id_group, null, 0);?>
        <tr class="warning">
        <td colspan="<?php echo count($_smarty_tpl->tpl_vars['roles']->value)+1;?>
" style="text-align: center;">
          <h4><?php echo stripslashes($_smarty_tpl->tpl_vars['permission']->value->group_name);?>
</h4>
        </td>      
        </tr>
        <?php }?>
        <tr>
          <td><a href="/backend/permission/add/<?php echo $_smarty_tpl->tpl_vars['permission']->value->id;?>
"><i class="icon-pencil"></i></a>&nbsp;&nbsp; <b><?php echo $_smarty_tpl->tpl_vars['permission']->value->name;?>
</b> <br><a href="/backend/permission/delete/<?php echo $_smarty_tpl->tpl_vars['permission']->value->id;?>
"><i class="icon-remove"></i></a>&nbsp;&nbsp; (<?php echo $_smarty_tpl->tpl_vars['permission']->value->alias;?>
)</td>
          <?php  $_smarty_tpl->tpl_vars['r'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['r']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['roles']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['r']->key => $_smarty_tpl->tpl_vars['r']->value){
$_smarty_tpl->tpl_vars['r']->_loop = true;
?>
          <td style="text-align: center;"><input name="permission[<?php echo $_smarty_tpl->tpl_vars['r']->value->id;?>
][<?php echo $_smarty_tpl->tpl_vars['permission']->value->id;?>
]" value="1" type="checkbox" <?php if ($_smarty_tpl->tpl_vars['r']->value->children['permissions'][$_smarty_tpl->tpl_vars['key']->value]->allowed){?>checked="checked"<?php }?> /></td>
          <?php } ?>
        </tr>
        <?php } ?>
        
        <tr>
          <td> </td>
          <?php  $_smarty_tpl->tpl_vars['r'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['r']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['roles']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['r']->key => $_smarty_tpl->tpl_vars['r']->value){
$_smarty_tpl->tpl_vars['r']->_loop = true;
?>
            <td style="text-align: center;"><button name="edit_role" value="<?php echo $_smarty_tpl->tpl_vars['r']->value->id;?>
" class="btn btn-success" href="#">Сохранить</button></td>
          <?php } ?>
       </tr>
      </tbody>
    </table>
    </form>
  </div>
</div><?php }} ?>