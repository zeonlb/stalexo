<?php /* Smarty version Smarty-3.1.2, created on 2013-12-03 17:53:31
         compiled from "application/templates/direction/callback/list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1618455141520b20b86df776-17622612%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '76ba2604a3b8cbe79b13e08a999f15aa2bde0586' => 
    array (
      0 => 'application/templates/direction/callback/list.tpl',
      1 => 1382543707,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1618455141520b20b86df776-17622612',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.2',
  'unifunc' => 'content_520b20b87cd8d',
  'variables' => 
  array (
    'collection' => 0,
    'prevDate' => 0,
    'c' => 0,
    'total' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_520b20b87cd8d')) {function content_520b20b87cd8d($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/salevel.com/lib/smarty/plugins/modifier.date_format.php';
?><div class="row">
  <div class="span12">
    <h4>Возвраты</h4>
    <?php if (!$_smarty_tpl->tpl_vars['collection']->value->count()){?>
    <p class="lead">Нет данных</p>
    <?php }else{ ?>
    <table class="table table-bordered table-hover">
      <thead>
        <tr>
          <th>ID</th>
          <th>Время</th>
          <th>Менеджер</th>
          <th>Торговая точка</th>
          <th>Сумма</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
      <?php $_smarty_tpl->tpl_vars['prevDate'] = new Smarty_variable(null, null, 0);?>
      <?php $_smarty_tpl->tpl_vars['total'] = new Smarty_variable(0, null, 0);?>
      <?php  $_smarty_tpl->tpl_vars['c'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['c']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['collection']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['c']->key => $_smarty_tpl->tpl_vars['c']->value){
$_smarty_tpl->tpl_vars['c']->_loop = true;
?>
        <?php if ($_smarty_tpl->tpl_vars['prevDate']->value!=smarty_modifier_date_format($_smarty_tpl->tpl_vars['c']->value->date,'%d%m%Y')){?>
        <?php if ($_smarty_tpl->tpl_vars['prevDate']->value){?>
        <tr>
          <td colspan="5"></td>
          <td colspan="2"><b>Всего: <?php echo number_format($_smarty_tpl->tpl_vars['total']->value,'2','.',' ');?>
 грн.</b></td>
        </tr>
        <?php $_smarty_tpl->tpl_vars['total'] = new Smarty_variable(0, null, 0);?>
        <?php }?>
        <?php $_smarty_tpl->tpl_vars['prevDate'] = new Smarty_variable(smarty_modifier_date_format($_smarty_tpl->tpl_vars['c']->value->date,'%d%m%Y'), null, 0);?>
        <tr class="warning" >
          <td colspan="7"><div class="lead" style="margin-bottom: 0; text-align: center;">&nbsp;<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['c']->value->date,'%d/%m/%Y');?>
</div></td>
        </tr>
        <?php }?>
        <tr>
          <td><?php echo $_smarty_tpl->tpl_vars['c']->value->id;?>
</td>
          <td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['c']->value->date,'%H:%M');?>
</td>
          <td><?php echo preg_replace('!<[^>]*?>!', ' ', stripslashes($_smarty_tpl->tpl_vars['c']->value->manager_name));?>
</td>
          <td><?php echo preg_replace('!<[^>]*?>!', ' ', stripslashes($_smarty_tpl->tpl_vars['c']->value->address));?>
</td>
          <td style="text-align: right;"><?php echo number_format($_smarty_tpl->tpl_vars['c']->value->total,'2','.',' ');?>
 грн.</td>
          <td>&nbsp;&nbsp;<a class="btn" title="Детали" href="/direction/callback/view/<?php echo $_smarty_tpl->tpl_vars['c']->value->id;?>
">Детали</a></td>
        </tr>
        <?php $_smarty_tpl->tpl_vars['total'] = new Smarty_variable($_smarty_tpl->tpl_vars['total']->value+$_smarty_tpl->tpl_vars['c']->value->total, null, 0);?>
      <?php } ?>
      </tbody>
    </table>
    <?php echo $_smarty_tpl->tpl_vars['collection']->value->pager();?>

    <?php }?>
  </div>
</div><?php }} ?>