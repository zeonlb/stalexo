<?php /* Smarty version Smarty-3.1.2, created on 2013-10-31 23:16:28
         compiled from "application/templates/backend/users/list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1173919599520c7452991ee0-56478559%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3bebe2966880c975649f730ba2940a369349a1d5' => 
    array (
      0 => 'application/templates/backend/users/list.tpl',
      1 => 1382543707,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1173919599520c7452991ee0-56478559',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.2',
  'unifunc' => 'content_520c7452a4e35',
  'variables' => 
  array (
    'user' => 0,
    'users' => 0,
    'u' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_520c7452a4e35')) {function content_520c7452a4e35($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/salevel.com/lib/smarty/plugins/modifier.date_format.php';
?><div class="btn-toolbar">
  <button class="btn btn-primary<?php if (!$_smarty_tpl->tpl_vars['user']->value->hasPermission('users_add')){?> disabled<?php }?>" <?php if ($_smarty_tpl->tpl_vars['user']->value->hasPermission('users_add')){?>onclick="javascript:location='/backend/users/add';"<?php }else{ ?>title="Недостаточно полномочий"<?php }?>>Добавить пользователя</button>
</div>
<div class="well">
  <table class="table">
    <thead>
      <tr>
        <th>#</th>
        <th>Email</th>
        <th>Дата</th>
        <th>Роль</th>
        <th style="width: 36px;"></th>
      </tr>
    </thead>
    <tbody>
      <?php  $_smarty_tpl->tpl_vars['u'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['u']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['users']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['u']->key => $_smarty_tpl->tpl_vars['u']->value){
$_smarty_tpl->tpl_vars['u']->_loop = true;
?>
      <tr>
        <td><?php echo $_smarty_tpl->tpl_vars['u']->value->userid;?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['u']->value->email;?>
</td>
        <td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['u']->value->joindate,'%Y/%m/%d');?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['u']->value->role_name;?>
</td>
        <td>
          <a href="/backend/users/add/<?php echo $_smarty_tpl->tpl_vars['u']->value->userid;?>
"><i class="icon-pencil"></i></a>
          <a href="/backend/users/delete/<?php echo $_smarty_tpl->tpl_vars['u']->value->userid;?>
" role="button" data-toggle="modal"><i class="icon-remove"></i></a>
        </td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
</div><?php }} ?>