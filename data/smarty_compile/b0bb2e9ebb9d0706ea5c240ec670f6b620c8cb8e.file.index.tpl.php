<?php /* Smarty version Smarty-3.1.2, created on 2013-11-08 12:38:51
         compiled from "application/templates/direction/routes/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2823586005214998bcb7e69-21338880%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b0bb2e9ebb9d0706ea5c240ec670f6b620c8cb8e' => 
    array (
      0 => 'application/templates/direction/routes/index.tpl',
      1 => 1382543710,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2823586005214998bcb7e69-21338880',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.2',
  'unifunc' => 'content_5214998bf339e',
  'variables' => 
  array (
    'message' => 0,
    'pointsArray' => 0,
    'timelinesArray' => 0,
    'timelines' => 0,
    't' => 0,
    'o' => 0,
    'r' => 0,
    'c' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5214998bf339e')) {function content_5214998bf339e($_smarty_tpl) {?><?php echo (($tmp = @$_smarty_tpl->tpl_vars['message']->value)===null||$tmp==='' ? '' : $tmp);?>


<style type="text/css">
.table-row{
  cursor: pointer;
}

.ignore-margin-left{
  margin-left: 0;
}
</style>

<div class="row">
<?php echo $_smarty_tpl->getSubTemplate ("direction/routes/map-legend.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<script src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>

<div class="span12" id="map" style="height:645px"></div>

<script type="text/javascript">

var routePoints = null;
var timelines = null;

<?php if (isset($_smarty_tpl->tpl_vars['pointsArray']->value)){?>
  var routePoints = <?php echo json_encode($_smarty_tpl->tpl_vars['pointsArray']->value);?>
;
<?php }?>
<?php if (isset($_smarty_tpl->tpl_vars['timelinesArray']->value)){?>
  var timelines = <?php echo json_encode($_smarty_tpl->tpl_vars['timelinesArray']->value);?>
;
<?php }?>


$(document).ready(function(){
  var SXMap = new Map();

  if ( routePoints ){
    SXMap.display();
  }
});

var Map = function(){

  var $this = this;

  this.instance = null;

  this.display = function(center, points){
    ymaps.ready(function(){
      $this.instance = new ymaps.Map("map", {
        center: center || [46.46, 30.7],
        zoom: 11
      });

      $this.drawPlanRoute(routePoints);

      $this.drawFactRoute(timelines);

    },
    function (error) {
      alert('Возникла ошибка: ' + error.message);
    });
  };

  this.drawFactRoute = function(points){
    if ( ! (points && points.length) ){
      return false;
    }
    var routePoints = [];

    $.each(points, function(i,v){
      routePoints.push({ 'type': 'wayPoint', point: [v.lat, v.lng] });
    });

    ymaps.route(routePoints,
      {mapStateAutoApply: true}
    ).then(function(route){
      ////////////////////////////
      route.getPaths().options.set({
        strokeColor: '1636FD', opacity: 0.7,

      });

      var i = 0;
      route.getWayPoints().each(function(v){
        var icon = "twirl#darkblueIcon";
        if( points[i].start ){
          icon = 'twirl#darkblueDotIcon';
        } else if( points[i].end ){
          icon = 'twirl#nightDotIcon';
        } else if ( ! points[i].enter ){
          icon = "twirl#nightIcon";
        } else if ( (! points[i].valid) || (points[i].enter && points[i].invalid_distance) ){
          icon  = "twirl#redIcon";
        } else if( points[i].invalid_begintime == true ){
          icon = "twirl#darkorangeIcon";
        }

        v.options.set({
          preset: icon
        });

        v.properties.set({
          'iconContent': ( (points[i].start || points[i].end) ? '' : (points[i].index+1) ),
          balloonContentBody: ( points[i].enter ?
          (points[i].time_begin+' <br><i>'+points[i].id_dp+'</i> '+points[i].address+(points[i].content ? '<hr>'+points[i].content : '') )
          : ( points[i].start ? 'Старт работы '+points[i].time+'<br>'+points[i].agent : ( points[i].end ? 'Окончание работы '+points[i].time+'<br>'+points[i].agent: 'Выход '+points[i].time_end) ) )
        });

        i++;
      });

      $.each(points, function(i,v){
        if ( ! v.enter && !v.start && ! v.end ){
          var segment = route.getPaths().get((i-1));
          if ( segment ){
            segment.options.set({
              strokeColor: 'FE1010',
              opacity: 0.6,
              strokeWidth: 5
            });
          } else {
            //console.log('invalid segment '+i)
          }

        }
      })

      $this.instance.geoObjects.add(route);
      ///////////////////////////////
    });

  };

  this.drawPlanRoute = function(points){
    if ( ! (points && points.length) ){
      return false;
    }
    var routePoints = [];
    $.each(points, function(i,v){
      routePoints.push({ 'type': 'wayPoint', point: [v.lat, v.lng] });
    });

    ymaps.route(routePoints,
      {mapStateAutoApply: true}
    ).then(function(route){
      ////////////////////////////
      route.getPaths().options.set({
        strokeColor: '4E9D3C', opacity: 0.9,

      });

      var i = 0;
      route.getWayPoints().each(function(v){

        route.getWayPoints().options.set({
          preset: "twirl#darkgreenIcon"
        });

        v.properties.set({
          'iconContent': (i+1),
          balloonContentBody: points[i].time+' '+points[i].address
        });

        i++;
      });

      $this.instance.geoObjects.add(route);
      ///////////////////////////////
    });

  };

  this.drawPoints = function(points){
    if ( ! (points && points.length) ){
      return false;
    }

    $.each(points, function(i,v){
      var placemark = new ymaps.Placemark([v.lat, v.lng], {
          balloonContent: '<p>'+v.address+'<br> '+v.time+'</p>',
          iconContent: v.prior
      }, {
          preset: "twirl#darkgreenIcon",
          // Disabling the Close Balloon button.
          balloonCloseButton: true,
          // The balloon will be opened and closed by clicking the placemark icon.
          hideIconOnBalloonOpen: true
      });
      $this.instance.geoObjects.add(placemark);
    });
  };

  this.construct = function(){

  };

  $this.construct();

};

</script>
<div class="clearfix"></div>
<br>
  <div class="span12">
  <?php if (isset($_smarty_tpl->tpl_vars['timelines']->value)&&count($_smarty_tpl->tpl_vars['timelines']->value)){?>
   <span class="pull-right"><a id="t_hide_all" href="javascript:;">Скрыть детали</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:;" id="t_show_all">Показать детали</a></span>
   <div class="clearfix"></div>
   <table class="table table-condensed table-hover">
        <thead>
          <tr>
            <th>#</th>
            <th>Клиент</th>
            <th>ТТ</td>
            <th>Адрес</th>
            <th>Вход</th>
            <th>Выход</th>
            <th>Вход план</th>
            <th>Выход план</th>
            <th>Длительность</th>
            <th>Длительность план</th>
          </tr>
        </thead>
        <tbody>
          <?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['timelines']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value){
$_smarty_tpl->tpl_vars['t']->_loop = true;
?>
          <tr class="table-row">
            <td><?php echo $_smarty_tpl->tpl_vars['t']->value->index+1;?>
</td>
            <td><?php echo stripslashes($_smarty_tpl->tpl_vars['t']->value->title);?>
</td>
            <td><i><?php echo $_smarty_tpl->tpl_vars['t']->value->id_dp;?>
</i> </td>
            <td><?php echo preg_replace('!<[^>]*?>!', ' ', stripslashes($_smarty_tpl->tpl_vars['t']->value->address));?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['t']->value->time_begin;?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['t']->value->time_end;?>
</td>
            <td><?php if ($_smarty_tpl->tpl_vars['t']->value->plan){?><?php echo $_smarty_tpl->tpl_vars['t']->value->plan->time;?>
<?php }?></td>
            <td><?php if ($_smarty_tpl->tpl_vars['t']->value->plan){?><?php echo $_smarty_tpl->tpl_vars['t']->value->plan->time_end;?>
<?php }?></td>
            <td style="text-align: center;"><?php echo $_smarty_tpl->tpl_vars['t']->value->time_diff;?>
 мин.</td>
            <td><?php if ($_smarty_tpl->tpl_vars['t']->value->plan){?><?php echo $_smarty_tpl->tpl_vars['t']->value->plan->time_diff;?>
 мин.<?php }?></td>
          </tr>
          <tr class="hide" style="background-color: #FFFFE5;">
            <td colspan="10">
            <div class="span3 ignore-margin-left">
              <b>Заказы:</b><br>
              <?php  $_smarty_tpl->tpl_vars['o'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['o']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['t']->value->children['orders']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['o']->key => $_smarty_tpl->tpl_vars['o']->value){
$_smarty_tpl->tpl_vars['o']->_loop = true;
?>
              <a href="/direction/orders/view/<?php echo $_smarty_tpl->tpl_vars['o']->value->id;?>
"><i>#<?php echo $_smarty_tpl->tpl_vars['o']->value->id;?>
</i>&nbsp;&nbsp;&nbsp;<?php echo $_smarty_tpl->tpl_vars['o']->value->total;?>
 грн.</a><br>
              <?php } ?>
              <?php if ($_smarty_tpl->tpl_vars['t']->value->children['total']['orders']){?>
              <br>
              Всего: <?php echo $_smarty_tpl->tpl_vars['t']->value->children['total']['orders'];?>
 грн.
              <?php }?>
            </div>
            <div class="span3 ignore-margin-left">
              <b>Заказы план:</b><br>
            </div>
            <div class="span3 ignore-margin-left">
              <b>Остатки:</b><br>
              <?php  $_smarty_tpl->tpl_vars['r'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['r']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['t']->value->children['remains']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['r']->key => $_smarty_tpl->tpl_vars['r']->value){
$_smarty_tpl->tpl_vars['r']->_loop = true;
?>
              <a href="/direction/remains/view/<?php echo $_smarty_tpl->tpl_vars['r']->value->id;?>
"><i>#<?php echo $_smarty_tpl->tpl_vars['r']->value->id;?>
</i></a><br>
              <?php } ?>
            </div>
            <div class="span3 ignore-margin-left">
              <b>Возвраты:</b><br>
              <?php  $_smarty_tpl->tpl_vars['c'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['c']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['t']->value->children['callbacks']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['c']->key => $_smarty_tpl->tpl_vars['c']->value){
$_smarty_tpl->tpl_vars['c']->_loop = true;
?>
              <a href="/direction/callback/view/<?php echo $_smarty_tpl->tpl_vars['c']->value->id;?>
"><i>#<?php echo $_smarty_tpl->tpl_vars['c']->value->id;?>
</i>&nbsp;&nbsp;&nbsp;<?php echo $_smarty_tpl->tpl_vars['c']->value->total;?>
 грн.</a><br>
              <?php } ?>
              <?php if ($_smarty_tpl->tpl_vars['t']->value->children['total']['callbacks']){?>
              <br>
              Всего: <?php echo $_smarty_tpl->tpl_vars['t']->value->children['total']['callbacks'];?>
 грн.
              <?php }?>
            </div>
            </td>
          </tr>
          <?php } ?>
        </tbody>
  </table>
  <?php }?>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
  $('.table-row').click(function(){
    $(this).next().toggleClass('hide');
  });

  $('#t_show_all').click(function(){
    $('.table-row').next().removeClass('hide');
  });

  $('#t_hide_all').click(function(){
    $('.table-row').next().addClass('hide');
  });

});
</script><?php }} ?>