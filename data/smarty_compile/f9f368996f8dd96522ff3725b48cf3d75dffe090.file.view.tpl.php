<?php /* Smarty version Smarty-3.1.2, created on 2013-08-30 18:08:06
         compiled from "application/templates/direction/remains/view.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16213418755220b556ef4277-62237592%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f9f368996f8dd96522ff3725b48cf3d75dffe090' => 
    array (
      0 => 'application/templates/direction/remains/view.tpl',
      1 => 1367156451,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '16213418755220b556ef4277-62237592',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'remain' => 0,
    's' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.2',
  'unifunc' => 'content_5220b5570771c',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5220b5570771c')) {function content_5220b5570771c($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/stalexoi/stalexo.com/lib/smarty/plugins/modifier.date_format.php';
?><div class="row">
  <div class="span6">
    <h4>&nbsp;Остатки #<?php echo $_smarty_tpl->tpl_vars['remain']->value->id;?>
 &nbsp;&nbsp; <a class="btn" onclick="javascript:history.back();"><< Назад</a></h4>
    <table class="table table-bordered">
      <tr>
        <td>Торговая точка:</td>
        <td><?php echo $_smarty_tpl->tpl_vars['remain']->value->address;?>
</td>
      </tr>
      <tr>
        <td>Дата:</td>
        <td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['remain']->value->date,'%H:%M %d/%m/%Y');?>
</td>
      </tr>
      <tr>
        <td>Торговый агент:</td>
        <td><?php echo $_smarty_tpl->tpl_vars['remain']->value->manager_name;?>
</td>
      </tr>
    </table>
  </div>
  <div class="span7">
    <table class="table table-bordered">
      <thead>
        <th>Название</th>
        <th> Ед. </th>
        <th>Количество</th>
      </thead>
      <tbody>
        <?php  $_smarty_tpl->tpl_vars['s'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['s']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['remain']->value->children('sku'); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['s']->key => $_smarty_tpl->tpl_vars['s']->value){
$_smarty_tpl->tpl_vars['s']->_loop = true;
?>
        <tr>
          <td><?php echo stripslashes($_smarty_tpl->tpl_vars['s']->value->title);?>
</td>
          <td><?php echo $_smarty_tpl->tpl_vars['s']->value->units;?>
</td>
          <td><?php echo $_smarty_tpl->tpl_vars['s']->value->quantity;?>
</td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div><?php }} ?>