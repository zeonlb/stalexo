<?php /* Smarty version Smarty-3.1.2, created on 2013-12-16 10:20:14
         compiled from "application/templates/backend/users/add.tpl" */ ?>
<?php /*%%SmartyHeaderCode:610648980523335daece5d4-84205751%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '291b37318adf75a90c01fff4621bc10bbbacba32' => 
    array (
      0 => 'application/templates/backend/users/add.tpl',
      1 => 1382543707,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '610648980523335daece5d4-84205751',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.2',
  'unifunc' => 'content_523335db052b5',
  'variables' => 
  array (
    'u' => 0,
    'user' => 0,
    'roles' => 0,
    'r' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_523335db052b5')) {function content_523335db052b5($_smarty_tpl) {?><div class="span4 well">
    <legend><?php if ($_smarty_tpl->tpl_vars['u']->value->isLoaded()){?>Редактировать<?php }else{ ?>Добавить<?php }?> пользователя</legend>
    <form accept-charset="UTF-8" action="" method="post">
        <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['u']->value->userid;?>
">
        <input <?php if ($_smarty_tpl->tpl_vars['u']->value->isLoaded()){?>disabled="disabled"<?php }?> class="span3" name="email" <?php if ($_smarty_tpl->tpl_vars['u']->value->isLoaded()){?>value="<?php echo $_smarty_tpl->tpl_vars['u']->value->email;?>
"<?php }?> placeholder="Email" type="text">
        <input class="span3" name="password" placeholder="<?php if ($_smarty_tpl->tpl_vars['u']->value->isLoaded()){?>New password<?php }else{ ?>Password<?php }?>" type="password"> 
        <?php if ($_smarty_tpl->tpl_vars['u']->value->isLoaded()&&$_smarty_tpl->tpl_vars['user']->value->hasPermission('users_edit_advanced')){?>
        <h3>TODO: CAN BE DELETED</h3>
        <?php }?>
        <select <?php if (!$_smarty_tpl->tpl_vars['user']->value->hasPermission('users_change_role')){?>disabled="disabled"<?php }?> name="role">
          <?php  $_smarty_tpl->tpl_vars['r'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['r']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['roles']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['r']->key => $_smarty_tpl->tpl_vars['r']->value){
$_smarty_tpl->tpl_vars['r']->_loop = true;
?>
          <option <?php if ($_smarty_tpl->tpl_vars['u']->value->id_role==$_smarty_tpl->tpl_vars['r']->value->id){?>selected="selected"<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['r']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['r']->value->name;?>
</option>
          <?php } ?>
        </select>
        <br>
        <button class="btn btn-primary" type="submit">Сохранить</button>
        <a class="btn" href="/backend/users">Отмена</a>
    </form>
</div><?php }} ?>