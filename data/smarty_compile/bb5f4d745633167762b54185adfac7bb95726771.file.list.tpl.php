<?php /* Smarty version Smarty-3.1.2, created on 2013-10-31 23:15:58
         compiled from "application/templates/backend/devices/list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1296826608520c5b1068e063-04779168%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bb5f4d745633167762b54185adfac7bb95726771' => 
    array (
      0 => 'application/templates/backend/devices/list.tpl',
      1 => 1382543706,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1296826608520c5b1068e063-04779168',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.2',
  'unifunc' => 'content_520c5b1076e6c',
  'variables' => 
  array (
    'user' => 0,
    'devices' => 0,
    'd' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_520c5b1076e6c')) {function content_520c5b1076e6c($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/salevel.com/lib/smarty/plugins/modifier.date_format.php';
?><div class="btn-toolbar">
  <button class="btn btn-primary<?php if (!$_smarty_tpl->tpl_vars['user']->value->hasPermission('users_crud_devices')){?> disabled<?php }?>" <?php if ($_smarty_tpl->tpl_vars['user']->value->hasPermission('users_crud_devices')){?>onclick="javascript:location='/backend/devices/add';"<?php }else{ ?>title="Недостаточно полномочий"<?php }?>>Добавить</button>
</div>
<div class="row">
  <div class="span12">
  <table class="table table-condensed table-hover">
      <thead>
        <tr>
          <th>#</th>
          <th></th>
          <th>ID-DEVICE</th>
          <th>Агент</th>
          <th>Последний логин</th>         
        </tr>
      </thead>
      <tbody>
      <?php  $_smarty_tpl->tpl_vars['d'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['d']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['devices']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['d']->key => $_smarty_tpl->tpl_vars['d']->value){
$_smarty_tpl->tpl_vars['d']->_loop = true;
?>
      <tr>
        <td><?php echo $_smarty_tpl->tpl_vars['d']->value->id_staff;?>
</td>
        <td>
          <a title="Edit" href="/backend/devices/add/<?php echo $_smarty_tpl->tpl_vars['d']->value->id_staff;?>
"><i class="icon-pencil"></i></a>
          <a title="Delete" href="/backend/devices/delete/<?php echo $_smarty_tpl->tpl_vars['d']->value->id_staff;?>
"><i class="icon-remove"></i></a>
        </td>
        <td><?php echo $_smarty_tpl->tpl_vars['d']->value->device;?>
</td>
        <td><?php echo stripslashes($_smarty_tpl->tpl_vars['d']->value->name);?>
</td>
        <td><?php if ($_smarty_tpl->tpl_vars['d']->value->token_date){?><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['d']->value->token_date,'%Y-%m-%d %H:%M');?>
<?php }?></td>
      </tr>
      <?php } ?>
      </tbody>
    </table>
  </div>
</div><?php }} ?>