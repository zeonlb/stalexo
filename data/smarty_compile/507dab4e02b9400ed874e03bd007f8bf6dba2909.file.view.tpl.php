<?php /* Smarty version Smarty-3.1.2, created on 2013-11-28 12:55:02
         compiled from "application/templates/direction/order/view.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19372027305220b541b55156-69546390%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '507dab4e02b9400ed874e03bd007f8bf6dba2909' => 
    array (
      0 => 'application/templates/direction/order/view.tpl',
      1 => 1382543708,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19372027305220b541b55156-69546390',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.2',
  'unifunc' => 'content_5220b541ca58c',
  'variables' => 
  array (
    'order' => 0,
    's' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5220b541ca58c')) {function content_5220b541ca58c($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/salevel.com/lib/smarty/plugins/modifier.date_format.php';
?><div class="row">
  <div class="span6">
    <h4>&nbsp;Заказ #<?php echo $_smarty_tpl->tpl_vars['order']->value->id;?>
 &nbsp;&nbsp; <a class="btn" onclick="javascript:history.back();"><< Назад</a></h4>
    <table class="table table-bordered">
      <tr>
        <td>Торговый агент:</td>
        <td><?php echo $_smarty_tpl->tpl_vars['order']->value->manager_name;?>
</td>
      </tr>
      <tr>
        <td>Клиент:</td>
        <td><?php echo stripslashes($_smarty_tpl->tpl_vars['order']->value->client_title);?>
</td>
      </tr>
      <tr>
        <td>Торговая точка:</td>
        <td><?php echo stripslashes($_smarty_tpl->tpl_vars['order']->value->address);?>
</td>
      </tr>
      <tr>
        <td>Договор:</td>
        <td><?php echo stripslashes($_smarty_tpl->tpl_vars['order']->value->contract_title);?>
</td>
      </tr>
      <tr>
        <td>Дата:</td>
        <td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['order']->value->date,'%H:%M %d/%m/%Y');?>
</td>
      </tr>
      <tr>
        <td>Сумма:</td>
        <td><?php echo $_smarty_tpl->tpl_vars['order']->value->total;?>
 грн.</td>
      </tr>
    </table>
  </div>
  <div class="span7">
    <table class="table table-bordered">
      <thead>
        <th>Название</th>
        <th> Ед. </th>
        <th>Количество</th>
        <th>Цена</th>
        <th>Всего</th>
      </thead>
      <tbody>
        <?php  $_smarty_tpl->tpl_vars['s'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['s']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['order']->value->children('sku'); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['s']->key => $_smarty_tpl->tpl_vars['s']->value){
$_smarty_tpl->tpl_vars['s']->_loop = true;
?>
        <tr>
          <td><?php echo stripslashes($_smarty_tpl->tpl_vars['s']->value->title);?>
</td>
          <td><?php echo $_smarty_tpl->tpl_vars['s']->value->units;?>
</td>
          <td><?php echo $_smarty_tpl->tpl_vars['s']->value->quantity;?>
</td>
          <td><?php echo $_smarty_tpl->tpl_vars['s']->value->price;?>
</td>
          <td><?php echo number_format(($_smarty_tpl->tpl_vars['s']->value->quantity*$_smarty_tpl->tpl_vars['s']->value->price),'2','.','');?>
</td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div><?php }} ?>