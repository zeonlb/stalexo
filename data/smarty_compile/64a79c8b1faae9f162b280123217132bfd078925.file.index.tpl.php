<?php /* Smarty version Smarty-3.1.2, created on 2013-11-26 13:22:14
         compiled from "application/templates/direction/location/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1120411624520b20baa6c273-70164475%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '64a79c8b1faae9f162b280123217132bfd078925' => 
    array (
      0 => 'application/templates/direction/location/index.tpl',
      1 => 1382543708,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1120411624520b20baa6c273-70164475',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.2',
  'unifunc' => 'content_520b20bab92ef',
  'variables' => 
  array (
    'user' => 0,
    'points' => 0,
    'pointsInfo' => 0,
    'center' => 0,
    'orders' => 0,
    'callbacks' => 0,
    'remains' => 0,
    'encashment' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_520b20bab92ef')) {function content_520b20bab92ef($_smarty_tpl) {?><div id="locations">
</div>
<div>
<?php if (0&&$_smarty_tpl->tpl_vars['user']->value->userid==3){?>
<button type="button" class="btn btn-primary" id="show_route_points" data-toggle="button">Отображать точки маршрута</button>
<script type="text/javascript">
$(document).ready(function(){
  $('#show_route_points').click(function(){
    $(this).toggleClass('active');
    if ( $(this).hasClass('active') ){
      SXMap.instance
      //add point
    } else {
      //hide points
    }
  });
});
</script>
<?php }?>
</div>
<br>
<script src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>

<script type="text/javascript">
  /*
  $(document).ready(function(){
    $('#locations [name="agent"]').change(function(){
      var selected = $('#locations [name="agent"] :selected');
      if ( selected.val() == '0' ){ return; }
      $.ajax({
        url: '/direction/location/ajax',
        type: 'POST',
        dataType: 'json',
        async: false,
        data: { action: 'get-date-range', agent: selected.val() },
        success: function(json){
          if ( json.range ){
            var option = '<option>'+$('#locations [name="date"] option:first').text()+'</option>';
            $.each( json.range, function(i, v){
              option += '<option value="'+v+'">'+v+'</option>';
            });

            $('#locations [name="date"]').html(option);
          }
        }
      });
    });
    var SXMap = new Map();

    $('#show_locations').click(function(){
      SXMap.destroy();
      var agent = $('#locations [name="agent"]').val();
      var date  = $('#locations [name="date"]').val();
      if ( agent == 0 || date == 0 ){ return; }
      $.ajax({
        url: '/direction/location/ajax',
        type: 'POST',
        dataType: 'json',
        async: false,
        data: { action: 'get-locations', agent: agent, date: date },
        success: function(json){
          if ( json.status ){
            SXMap.display(json.center, json.points);
          }
        }
      });
    });
  });
  */
  var wayPoints = <?php echo json_encode($_smarty_tpl->tpl_vars['points']->value);?>
;
  var pointsInfo = <?php echo json_encode($_smarty_tpl->tpl_vars['pointsInfo']->value);?>
;

  var mapCenter = <?php echo json_encode($_smarty_tpl->tpl_vars['center']->value);?>
;
  var orders    = <?php echo json_encode($_smarty_tpl->tpl_vars['orders']->value);?>
;
  var callbacks = <?php echo json_encode($_smarty_tpl->tpl_vars['callbacks']->value);?>
;
  var remains   = <?php echo json_encode($_smarty_tpl->tpl_vars['remains']->value);?>
;
  var encashment= <?php echo json_encode($_smarty_tpl->tpl_vars['encashment']->value);?>
;

  var Map = function(){

      var $this = this;

      this.instance = null;

      this.display = function(center, points){
        ymaps.ready(function(){
          $this.instance = new ymaps.Map("map", {
            center: center || [46.46, 30.44],
            zoom: 10
          });

          $this.drawRoute(points);

          $this.drawOrders(orders);

          $this.drawCallbacks(callbacks);

          $this.drawRemains(remains);

          $this.drawEncashment(encashment);
        },
        function (error) {
          alert('Возникла ошибка: ' + error.message);
        });
      };

      this.drawRoute = function(points){

        if ( points && points.length ){
          ymaps.route(points, {
            mapStateAutoApply:true
          }).then(function (route) {

            route.getWayPoints().options.set({
              hasBalloon: false,
              iconImageHref: 'http://api.yandex.ru/maps/doc/jsapi/2.x/ref/images/styles/clusterer/blue/blue-small.png',
              iconImageSize: [0,0],
              iconImageOffset: [0,0]
            });

            var i = 0;
            route.getWayPoints().each(function(v){

              v.properties.set({
              'iconContent':'',
              //'balloonContent': pointsInfo[i].time
              });

              i++;
            });

            $this.instance.geoObjects.add(route);
          });
        }

      };

      this.destroy = function(){
        if ( instance ){
          $this.instance.destroy();
        }
      };

      this.drawOrders = function(orders){
        if ( orders && orders.length ){
          $.each(orders, function(i,v){
            var placemark = new ymaps.Placemark([v.lat, v.lng], {
                balloonContent: '<p><a target="_blank" href="'+v.url+'">Заказ #'+v.id+' '+v.time+'</a><br>'+v.total+' грн.'+'</p>',
                iconContent: "#"+v.id
            }, {
                preset: "twirl#shopIcon",
                // Disabling the Close Balloon button.
                balloonCloseButton: true,
                // The balloon will be opened and closed by clicking the placemark icon.
                hideIconOnBalloonOpen: true
            });
            $this.instance.geoObjects.add(placemark);
          });
        }
      };

      this.drawCallbacks = function(items){
        if ( items && items.length ){
          $.each(items, function(i,v){
            var placemark = new ymaps.Placemark([v.lat, v.lng], {
                balloonContent: '<p><a target="_blank" href="'+v.url+'">Возврат #'+v.id+' '+v.time+'</a></p>',
                iconContent: "#"+v.id
            }, {
                preset: "twirl#truckIcon",
                // Disabling the Close Balloon button.
                balloonCloseButton: true,
                // The balloon will be opened and closed by clicking the placemark icon.
                hideIconOnBalloonOpen: true
            });
            $this.instance.geoObjects.add(placemark);
          });
        }
      };

      this.drawRemains = function(items){
        if ( items && items.length ){
          $.each(items, function(i,v){
            var placemark = new ymaps.Placemark([v.lat, v.lng], {
                balloonContent: '<p><a target="_blank" href="'+v.url+'">Снятие остатков #'+v.id+' '+v.time+'</a></p>',
                iconContent: "#"+v.id
            }, {
                preset: "twirl#storehouseIcon",
                // Disabling the Close Balloon button.
                balloonCloseButton: true,
                // The balloon will be opened and closed by clicking the placemark icon.
                hideIconOnBalloonOpen: true
            });
            $this.instance.geoObjects.add(placemark);
          });
        }
      };

      this.drawEncashment = function(items){
        if ( items && items.length ){
          $.each(items, function(i,v){
            var placemark = new ymaps.Placemark([v.lat, v.lng], {
                balloonContent: '<p><a target="_blank" href="'+v.url+'">Инкассация #'+v.id+' '+v.time+'</a><br>'+v.amount+' грн.</p>',
                iconContent: "#"+v.id
            }, {
                preset: "twirl#bankIcon",
                // Disabling the Close Balloon button.
                balloonCloseButton: true,
                // The balloon will be opened and closed by clicking the placemark icon.
                hideIconOnBalloonOpen: true
            });
            $this.instance.geoObjects.add(placemark);
          });
        }
      };

      this.getInstance = function(){};

    };

    $(document).ready(function(){
      if ( mapCenter ){
        var SXMap = new Map();
        SXMap.display(mapCenter, wayPoints, orders);
      }
    });

</script>

<div class="span11" id="map" style="height:645px"></div>

<?php }} ?>