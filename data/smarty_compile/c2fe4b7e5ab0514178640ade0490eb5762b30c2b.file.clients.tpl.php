<?php /* Smarty version Smarty-3.1.2, created on 2013-08-14 17:29:06
         compiled from "application/templates/direction/sales/clients.tpl" */ ?>
<?php /*%%SmartyHeaderCode:624899615520b94324cb990-16542672%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c2fe4b7e5ab0514178640ade0490eb5762b30c2b' => 
    array (
      0 => 'application/templates/direction/sales/clients.tpl',
      1 => 1371069408,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '624899615520b94324cb990-16542672',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'message' => 0,
    'clients' => 0,
    'c' => 0,
    'salesRatio' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.2',
  'unifunc' => 'content_520b94325751a',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_520b94325751a')) {function content_520b94325751a($_smarty_tpl) {?><div class="row">
  <div class="span12">
  <?php echo (($tmp = @$_smarty_tpl->tpl_vars['message']->value)===null||$tmp==='' ? '' : $tmp);?>

  <h4>Продажи за месяц</h4>
  <table class="table table-condensed table-hover">
    <thead>
      <tr>
        <th colspan="5"></th>
      </tr>
      <tr>
        <th>Клиент</th>
        <th>План</th>
        <th>Факт</th>
        <th>Возвраты</th>
        <th>План/Факт %</th>
      </tr>
      <tr>
        <th colspan="5"></th>
      </tr>
    </thead>
    <tbody>
      <?php if ($_smarty_tpl->tpl_vars['clients']->value->count()){?>
      <?php  $_smarty_tpl->tpl_vars['c'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['c']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['clients']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['c']->key => $_smarty_tpl->tpl_vars['c']->value){
$_smarty_tpl->tpl_vars['c']->_loop = true;
?>
      <tr>
        <td><?php echo stripslashes($_smarty_tpl->tpl_vars['c']->value->title);?>
</td>
        <td><?php echo number_format($_smarty_tpl->tpl_vars['c']->value->sales_plan,'0','.',' ');?>
 грн.</td>
        <td><?php echo number_format($_smarty_tpl->tpl_vars['c']->value->orders_total,'0','.',' ');?>
 грн.</td>
        <td><?php echo number_format($_smarty_tpl->tpl_vars['c']->value->callbacks_total,'0','.',' ');?>
 грн.</td>
        <td>
        <?php $_smarty_tpl->tpl_vars['salesRatio'] = new Smarty_variable($_smarty_tpl->tpl_vars['c']->value->getSalesOrdersRatio(), null, 0);?>               
        <?php if ($_smarty_tpl->tpl_vars['salesRatio']->value>99){?>
        &nbsp;<span> <?php echo $_smarty_tpl->tpl_vars['salesRatio']->value;?>
%</span>
        <div class="span2 progress progress-success active">
          <div class="bar" style="width: 100%;"></div>
        </div>
        <?php }elseif($_smarty_tpl->tpl_vars['salesRatio']->value<35){?>
        &nbsp;<span> <?php echo $_smarty_tpl->tpl_vars['salesRatio']->value;?>
%</span>
        <div class="span2 progress progress-danger active">
          <div class="bar" style="width: <?php echo $_smarty_tpl->tpl_vars['salesRatio']->value;?>
%;"></div>
        </div>
        <?php }else{ ?>
        &nbsp;<span> <?php echo $_smarty_tpl->tpl_vars['salesRatio']->value;?>
%</span>
        <div class="span2 progress progress-info active">
          <div class="bar" style="width: <?php echo $_smarty_tpl->tpl_vars['salesRatio']->value;?>
%;"></div>
        </div>
        <?php }?>
        </td>
      </tr>
      <?php } ?>
      <?php }?>
    </tbody>
  </table>
  </div>
</div><?php }} ?>