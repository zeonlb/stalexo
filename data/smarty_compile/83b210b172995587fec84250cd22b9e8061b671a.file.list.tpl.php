<?php /* Smarty version Smarty-3.1.2, created on 2013-12-03 17:53:26
         compiled from "application/templates/direction/encashment/list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6624243520b20b25b50c5-75459673%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '83b210b172995587fec84250cd22b9e8061b671a' => 
    array (
      0 => 'application/templates/direction/encashment/list.tpl',
      1 => 1382543708,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6624243520b20b25b50c5-75459673',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.2',
  'unifunc' => 'content_520b20b27048d',
  'variables' => 
  array (
    'collection' => 0,
    'prevDate' => 0,
    'e' => 0,
    'total' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_520b20b27048d')) {function content_520b20b27048d($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/salevel.com/lib/smarty/plugins/modifier.date_format.php';
?><div class="row">
  <div class="span12">
    <h4>Инкассация</h4>
    <?php if (!$_smarty_tpl->tpl_vars['collection']->value->count()){?>
    <p class="lead">Нет данных</p>
    <?php }else{ ?>
    <table class="table table-bordered table-hover">
      <thead>
        <tr>
          <th>ID</th>
          <th>Время</th>
          <th>Менеджер</th>
          <th>Торговая точка</th>
          <th>№ номен-ры</th>
          <th>Дата</th>
          <th>Сумма</th>
        </tr>
      </thead>
      <tbody>
      <?php $_smarty_tpl->tpl_vars['prevDate'] = new Smarty_variable(null, null, 0);?>
      <?php $_smarty_tpl->tpl_vars['total'] = new Smarty_variable(0, null, 0);?>
      <?php  $_smarty_tpl->tpl_vars['e'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['e']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['collection']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['e']->key => $_smarty_tpl->tpl_vars['e']->value){
$_smarty_tpl->tpl_vars['e']->_loop = true;
?>
        <?php if ($_smarty_tpl->tpl_vars['prevDate']->value!=smarty_modifier_date_format($_smarty_tpl->tpl_vars['e']->value->date,'%d%m%Y')){?>
        <?php if ($_smarty_tpl->tpl_vars['prevDate']->value){?>
        <tr>
          <td colspan="6"></td>
          <td colspan="2"><b>Всего: <?php echo number_format($_smarty_tpl->tpl_vars['total']->value,'2','.',' ');?>
 грн.</b></td>
        </tr>
        <?php $_smarty_tpl->tpl_vars['total'] = new Smarty_variable(0, null, 0);?>
        <?php }?>
        <?php $_smarty_tpl->tpl_vars['prevDate'] = new Smarty_variable(smarty_modifier_date_format($_smarty_tpl->tpl_vars['e']->value->date,'%d%m%Y'), null, 0);?>
        <tr class="warning" >
          <td colspan="8"><div class="lead" style="margin-bottom: 0; text-align: center;">&nbsp;<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['e']->value->date,'%d/%m/%Y');?>
</div></td>
        </tr>
        <?php }?>
        <tr>
          <td><?php echo $_smarty_tpl->tpl_vars['e']->value->id;?>
</td>
          <td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['e']->value->date,'%H:%M');?>
</td>
          <td><?php echo preg_replace('!<[^>]*?>!', ' ', stripslashes($_smarty_tpl->tpl_vars['e']->value->manager_name));?>
</td>
          <td><?php echo preg_replace('!<[^>]*?>!', ' ', stripslashes($_smarty_tpl->tpl_vars['e']->value->address));?>
</td>
          <td><?php echo preg_replace('!<[^>]*?>!', ' ', stripslashes($_smarty_tpl->tpl_vars['e']->value->nom_number));?>
</td>
          <td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['e']->value->nom_date,'%d/%m/%Y');?>
</td>
          <td style="text-align: right;"><?php echo number_format($_smarty_tpl->tpl_vars['e']->value->amount,'2','.',' ');?>
</td>
          <?php $_smarty_tpl->tpl_vars['total'] = new Smarty_variable($_smarty_tpl->tpl_vars['total']->value+$_smarty_tpl->tpl_vars['e']->value->amount, null, 0);?>
        </tr>
      <?php } ?>
      </tbody>
    </table>
    <?php echo $_smarty_tpl->tpl_vars['collection']->value->pager();?>

    <?php }?>
  </div>
</div><?php }} ?>