<?php /* Smarty version Smarty-3.1.2, created on 2013-10-31 23:16:59
         compiled from "application/templates/backend/settings/list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1479726368520c5b78770aa7-68688884%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c406575bed7b2dfda5a3ad281ed8d65d3555a016' => 
    array (
      0 => 'application/templates/backend/settings/list.tpl',
      1 => 1382543706,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1479726368520c5b78770aa7-68688884',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.2',
  'unifunc' => 'content_520c5b787ef5e',
  'variables' => 
  array (
    'user' => 0,
    'devices' => 0,
    'd' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_520c5b787ef5e')) {function content_520c5b787ef5e($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/salevel.com/lib/smarty/plugins/modifier.date_format.php';
?><div class="btn-toolbar">
  <button class="btn btn-primary<?php if (!$_smarty_tpl->tpl_vars['user']->value->hasPermission('users_crud_devices')){?> disabled<?php }?>" <?php if ($_smarty_tpl->tpl_vars['user']->value->hasPermission('users_crud_devices')){?>onclick="javascript:location='/backend/devices/add';"<?php }else{ ?>title="Недостаточно полномочий"<?php }?>>Добавить</button>
</div>
<div class="row">
  <div class="span12">
  <table class="table table-condensed table-hover">
      <thead>
        <tr>
          <th>#</th>
          <th></th>
          <th>ID-DEVICE</th>
          <th>Агент</th>
          <th>Последний логин</th>         
        </tr>
      </thead>
      <tbody>
      <?php  $_smarty_tpl->tpl_vars['d'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['d']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['devices']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['d']->key => $_smarty_tpl->tpl_vars['d']->value){
$_smarty_tpl->tpl_vars['d']->_loop = true;
?>
      <tr>
        <td><?php echo $_smarty_tpl->tpl_vars['d']->value->id_staff;?>
</td>
        <td>
          <a title="Edit" href="/backend/settings/add/<?php echo $_smarty_tpl->tpl_vars['d']->value->id_staff;?>
"><i class="icon-pencil"></i></a>
         </td>
        <td><?php echo $_smarty_tpl->tpl_vars['d']->value->device;?>
</td>
        <td><?php echo stripslashes($_smarty_tpl->tpl_vars['d']->value->name);?>
</td>
        <td><?php if ($_smarty_tpl->tpl_vars['d']->value->token_date){?><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['d']->value->token_date,'%Y-%m-%d %H:%M');?>
<?php }?></td>
      </tr>
      <?php } ?>
      </tbody>
    </table>
  </div>
</div><?php }} ?>