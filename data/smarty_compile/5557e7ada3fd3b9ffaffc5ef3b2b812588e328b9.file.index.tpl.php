<?php /* Smarty version Smarty-3.1.2, created on 2013-08-30 11:52:00
         compiled from "application/templates/direction/plan/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:210391837152205d30211eb7-39238281%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5557e7ada3fd3b9ffaffc5ef3b2b812588e328b9' => 
    array (
      0 => 'application/templates/direction/plan/index.tpl',
      1 => 1369988882,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '210391837152205d30211eb7-39238281',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'date' => 0,
    'agent' => 0,
    'ordersPlannedTotal' => 0,
    'ordersTotal' => 0,
    'collection' => 0,
    'i' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.2',
  'unifunc' => 'content_52205d30316e3',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52205d30316e3')) {function content_52205d30316e3($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/stalexoi/stalexo.com/lib/smarty/plugins/modifier.date_format.php';
?><div class="row">
<table class="table table-condensed table-hover">
  <thead>
  <tr>
    <th colspan="3"><h5><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['date']->value,'%Y - %m');?>
 &nbsp;&nbsp;&nbsp;&nbsp;<?php if ($_smarty_tpl->tpl_vars['agent']->value->isLoaded()){?><?php echo stripslashes($_smarty_tpl->tpl_vars['agent']->value->name);?>
<?php }?></h5> </th>
    <th colspan="5"><h4>План (<?php echo number_format($_smarty_tpl->tpl_vars['ordersPlannedTotal']->value,'0','.',' ');?>
 грн.) / Факт (<?php echo number_format($_smarty_tpl->tpl_vars['ordersTotal']->value,'0','.',' ');?>
 грн.)</h4></th>
  </tr>
  <tr>
    <th>ТТ</th>
    <th>Клиент</th>
    <th>Адрес</th>
    <th>Контракт</th>
    <th>Агент</th>
    <th style="text-align: right;">План заказов/месяц</th>
    <th style="text-align: right;">Факт заказов/месяц</th>
    <th style="text-align: right;">Возвраты</th>
  </tr>
  <tr>
    <td colspan="8"></td>
  </tr>
  </thead>
  <tbody>
    <?php  $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['i']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['collection']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['i']->key => $_smarty_tpl->tpl_vars['i']->value){
$_smarty_tpl->tpl_vars['i']->_loop = true;
?>
    <tr>
      <td><i><?php echo $_smarty_tpl->tpl_vars['i']->value->id_dp;?>
</i></td>
      <td><?php echo stripslashes($_smarty_tpl->tpl_vars['i']->value->client_name);?>
</td>
      <td><?php echo stripslashes($_smarty_tpl->tpl_vars['i']->value->address);?>
</td>
      <td><i><?php echo $_smarty_tpl->tpl_vars['i']->value->id_dc;?>
</i></td>
      <td><?php echo stripslashes($_smarty_tpl->tpl_vars['i']->value->manager_name);?>
</td>
      <td style="text-align: right;"><?php echo number_format($_smarty_tpl->tpl_vars['i']->value->plan,'0','.',' ');?>
 грн.</td>
      <td style="text-align: right;"><?php echo number_format($_smarty_tpl->tpl_vars['i']->value->orders_total,'0','.',' ');?>
 грн.</td>
      <td style="text-align: right;"><?php echo number_format($_smarty_tpl->tpl_vars['i']->value->callbacks_total,'0','.',' ');?>
 грн.</td>
    </tr>
    <?php } ?>
  </tbody>
</table>

<?php echo $_smarty_tpl->tpl_vars['collection']->value->pager();?>

</div><?php }} ?>