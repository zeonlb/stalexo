<?php
require_once 'SmartyBC.class.php';

class smartyInit extends SmartyBC
{
    public function __construct(){
        parent::__construct(); 
        $this->template_dir = clGlobPath::$full_path_tpl;
        $this->compile_dir  = clGlobPath::$dir_path.'smarty/templates_c/';
        $this->config_dir   = clGlobPath::$dir_path.'smarty/configs/';
        $this->cache_dir    = clGlobPath::$dir_path.'smarty/cache/';
        
        $this->assign('server_name',clGlobPath::$server_name);
        $this->assign('server_path',clGlobPath::$server_path);
        $this->assign('module_name',clGlob::$module_name);
        //$this->caching = true;
        $this->caching = false;
        
        //For dev environment
        //$this->compile_check = false;
        //$this->force_compile = true;       
    }
}