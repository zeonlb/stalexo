<?php
function smarty_function_makeurl($params, & $smarty){
  $url = '';
  $m = isset($params['m']) ? $params['m'] : App::getRequest()->getModule();
  $c = isset($params['c']) ? $params['c'] : App::getRequest()->getController();
  $a = isset($params['a']) ? $params['a'] : App::getRequest()->getAction();
  
  if ( isset($params['url']) ){
    $url .= $params['url'];
  }
  
  if ( $a != 'index' or isset($params['url']) ){
    $url = '/' . $a . $url;
  }
  
  if ( $c != 'index' or ($a != 'index' or isset($params['url']) ) ){
    $url = '/' . $c . $url;
  }
  
  if ( $m ){
    $url = '/' . $m . $url;
  }
  
  if ( ! $url ){
    $url = '/';
  }
  
  return $url;
}