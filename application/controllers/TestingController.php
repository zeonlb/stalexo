<?php 
class TestingController extends Controller_Action{
  
  protected function init(){
    if ( ! Debug::isDeveloper() ){
      App::render404();
    }
    require_once Settings::path()->vendor . 'simpletest/autorun.php';
  }
  
  public function indexAction(){
    $this->unitAction();
  }
  
  public function unitAction(){
    require_once Settings::path()->tests . 'AllUnitTests.php';
    exit();
  }

  
}