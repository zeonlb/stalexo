<?php
class ImportController extends Controller_Action {

  protected function init(){
    $key = App::getRequest()->get(2);
    if ( $key != Config::get('import')->key ) {
      App::render404('Invalid secure key');
    }
  }

  public function indexAction(){
    App::render404('Invalid action');
  }

  public function clientsAction(){
    $import = new Import_Xml_Clients( Config::get('import')->client );
    $import->execute();
    echo $import->affected; exit;
  }

  public function distributionPointsAction(){
    $import = new Import_Xml_DP( Config::get('import')->distribution_point );
    $import->execute();
    echo $import->affected; exit;
  }

  public function distributionPointContractAction(){
    $import = new Import_Xml_DPContract( Config::get('import')->distribution_point_contract );
    $import->execute();
    echo $import->affected; exit;
  }

  public function skuAction(){
    $import = new Import_Xml_Sku( Config::get('import')->sku );
    $import->execute();
    echo $import->affected; exit;
  }

  public function priceTypesAction(){
    $import = new Import_Xml_PriceType( Config::get('import')->price_type );
    $import->execute();
    echo $import->affected; exit;
  }

  public function skuPricesAction(){
    $import = new Import_Xml_SkuPrice( Config::get('import')->sku_price );
    $import->execute();
    echo $import->affected; exit;
  }

  public function skuAvailabilityAction(){
    $import = new Import_Xml_SkuAvailability( Config::get('import')->sku_availability );
    $import->execute();
    echo $import->affected; exit;
  }

  public function staffAction(){
    $import = new Import_Xml_Staff( Config::get('import')->users );
    $import->execute();
    echo $import->affected; exit;
  }

}