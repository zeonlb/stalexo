<?php
class Direction_SkuController extends Controller_Action{

  public function indexAction(){
    App::view()->js()->add('/assets/js/bootstrap.holder.js');
    $collection = new Stalexo_Sku_Collection();

    $collection->pager()->onPage = 20;
    $collection->pager()->url    = '/direction/sku';

    $collection->queryJoinGroup();
    $collection->loadAll();

    $skuStats = array('items' => 0, 'items_with_image' => 0);
    $r = App::db()->query("SELECT COUNT(id) as items, COUNT(img_big) items_with_image FROM `sku`");
    if ( $r ){
      $skuStats = $r->fetch_assoc();
    }

    $this->smarty->assign(array('collection' => $collection, 'skuStats' => $skuStats));
    $this->render($this->smarty->fetch('direction/sku/list.tpl'));
  }

  public function viewAction(){
    $id = App::getRequest()->get(3);
    if ( ! is_numeric($id) ){ App::render404(); }
    $sku = new Stalexo_Sku_Model($id);
    if ( ! $sku->isLoaded() ){ App::render404(); }

    if ( App::getRequest()->isPost() ){
      if ( $_FILES ){
        require_once Settings::path()->vendor . 'wideimage/WideImage.php';
        try{
          $image = WideImage::load('input_sku_image');
          $pathSmall = 'images/sku/' . $sku->id . '_80.png';
          $pathBig = 'images/sku/' . $sku->id . '_600.png';

          $image->resize(80, 80, 'inside', 'down')->saveToFile($pathSmall);
          $image->resize(600, 600, 'inside', 'down')->saveToFile($pathBig);

          $sku->img_big   = $pathBig;
          $sku->img_small = $pathSmall;
          $sku->save();
          $uploaded = true;
          Messenger::addNotification('Изображение сохранено');
        } catch( Exception $e ){ Messenger::addError('Ошибка при сохранении изображения, проверте формат'); }
      }
      App::redirect('/direction/sku/view/' . $sku->id);
    }

    App::view()->js()->add('/assets/js/bootstrap.holder.js');

    $message = Messenger::getMessage();
    $this->smarty->assign(array(
      'sku'   => $sku,
      'message' => $message
    ));

    $this->render($this->smarty->fetch('direction/sku/view.tpl'));
  }

  public function deleteImageAction(){
    if ( ! (App::getRequest()->isPost() and is_numeric(App::getRequest()->post('id_sku'))) ){
      App::render404();
    }
    $sku = new Stalexo_Sku_Model(App::getRequest()->post('id_sku'));
    if ( ! $sku->isLoaded() ){ App::render404(); }
    $result = $sku->dropImages();
    if ( $result ){
      Messenger::addNotification('Изображение удалено');
    } else {
      Messenger::addError('Изображения нет на сервере');
    }
    App::redirect('/direction/sku/view/' . $sku->id);
  }

}