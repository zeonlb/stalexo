<?php
class Direction_PhotoController extends Controller_Action{

  public function indexAction(){
    $orders = new Stalexo_Photo_Collection();

    $orders->pager()->onPage = 20;
    $orders->pager()->url = '/direction/photo';
	
    $orders->loadAll();
    $this->smarty->assign(array(
      'orders' => $orders
    ));

    $this->render($this->smarty->fetch('direction/photo/list.tpl'));
  }

  public function viewAction(){
    $id = App::getRequest()->get(3);
    $collection = new Stalexo_Order_Collection();

    $order = $collection->getById(array($id));

    if ( ! $order->isLoaded() ){
      App::render404();
    }

    $order->calculateTotal();
    $order->loadOrderedSku();

    $this->smarty->assign(array(
      'order' => $order
    ));

    $this->render($this->smarty->fetch('direction/order/view.tpl'));
  }

}