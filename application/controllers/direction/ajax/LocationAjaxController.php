<?php
class Direction_LocationAjaxController extends Controller_Ajax{
  
  public function getDateRangeAction(){
    $agent = (int)App::getRequest()->post('agent');

    $ymd = Location_Model::fetchByQuery(
      Location_Model::getFetchQuery()
      ->clearSelect()
      ->select('DATE_FORMAT(FROM_UNIXTIME(location.`date`), "%Y-%m-%d") as `ymd`')
      ->join('staff_device sd', 'sd.id_staff = "'. $agent.'" AND sd.device = location.device')
      ->groupBy('ymd')->orderBy('ymd DESC')
    );
    
    $range = array();
    foreach ( $ymd as $val ){
      $range[] = $val->ymd;
    }
    
    $this->output['range']  = $range;
    $this->output['status'] = 1;
  }
  
  public function getLocationsAction(){
    $agent = (int)App::getRequest()->post('agent');
    $date =  App::getRequest()->post('date');
    
    if ( ! preg_match('@^([0-9]{4})-([0-9]{2})-([0-9]{2})$@', $date, $m) ){
      return false;
    }
    $year  = $m[1];
    $month = $m[2];
    $day   = $m[3];
    
    $timestamp = Date::makeTimestamp($year, $month, $day);
    
    $locations = Location_Model::fetchByQuery(
      Location_Model::getFetchQuery()
      ->select('ROUND(location.date/40, 0) as minutes')
      ->join('staff_device sd', 'sd.id_staff = "'. $agent .'" AND sd.device = location.device')
      ->where('(location.date BETWEEN "'. $timestamp .'" AND "'. ($timestamp + 86400) .'")')
      ->groupBy('minutes')
      ->orderBy('location.id ASC')
      ->limit(550) 
    );
    
    $points = array();
    $center = null;
    
    foreach($locations as $l ){     
      $points[] = array(number_format($l->lat, 2), number_format($l->lng,2));
    }
    if ( count($points) ){
      $center = $points[0];
    }

    $this->output['center'] = $center;
    $this->output['points'] = $points;
    $this->output['status'] = 1;
  }
  
}