<?php
class Direction_EncashmentController extends Controller_Action{

  public function indexAction(){
    $collection = new Stalexo_Encashment_Collection();
    $collection->attachFilter(new Filter_Page_Standart($collection->pager()));
    $collection->pager()->onPage = 50;
    $collection->pager()->url = '/direction/encashment';

    $collection->queryRestrictByManager( Stalexo_Direction::getAgent() );
    $collection->queryRestrictByDay( Date::ymd2timestamp( Stalexo_Direction::getDate() ),Date::ymd2timestamp( Stalexo_Direction::getDate2()  ) );

    $collection->loadAll();

    $this->smarty->assign(
      array('collection' => $collection)
    );
    $this->render($this->smarty->fetch('direction/encashment/list.tpl'));
  }
  /*
  public function viewAction(){
    $id = App::getRequest()->get(3);

    $collection = new Stalexo_Encashment_Collection();
    $e = $collection->getById($id);

    if ( ! $e->isLoaded() ){
      App::render404();
    }

    $this->smarty->assign(array(
      'encashment' => $e
    ));
    $this->render($e->id);
  }
  */
}