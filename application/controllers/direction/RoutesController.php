<?php
class Direction_RoutesController extends Controller_Action{

  public function indexAction(){
    $tpl = 'direction/routes/index.tpl';
    $agent = Stalexo_Direction::getAgent();
    $date  = Date::ymd2timestamp( Stalexo_Direction::getDate() );

    if ( ! $date ){
      Stalexo_Messenger::addMessage('Выберите дату', 'info');
    } elseif( ! $agent->isLoaded() ){
      /**
      * Render all routes by day
      */
      $agents = User_Staff::fetchByQuery(
        User_Staff::getFetchQuery()
        ->join('staff_device sd', 'staff.id = sd.id_staff AND sd.is_production = 1')
        ->groupBy('staff.id')
      );

      foreach ( $agents as $a ){
        $route = Stalexo_Route_Model::getByAgentAndDay($a, date('N', $date));
        if ( $route->isLoaded() ){
          $route->loadPoints();
        }

        $timelines = Stalexo_Timeline_Model::getByAgentAndPeriod($a, $date, ($date+24*3600));
        foreach ( $route->children as $k => $r ){
          if ( array_key_exists($k, $timelines) ){
            $r->isValidTimeline($timelines[$k], $date);
          }
        }

        $timelinesArray = array();
        $pointsArray    = array();

        foreach( $timelines as $k => $t ) {
          $t->calculateEntryDistance();
          $t->loadActivity();
          $t->time_begin = date('H:i', $t->date_begin);
          $t->time_end   = date('H:i', $t->date_end);
          $t->time_diff  = round((($t->date_end - $t->date_begin)+0.01) / 60);

          $t->enter = true;
          $t->lat = $t->lat_begin;
          $t->lng = $t->lng_begin;
          $t->index = $k;
          $t->valid = true;

          $t->content = '';
          if ( count($t->children['orders']) ){
            foreach( $t->children['orders'] as $i ){
              $t->content .= '<a target="_blank" href="/direction/orders/view/'.$i->id.'">Заказ # '.$i->id.'</a><br>';
            }
          }
          if ( count($t->children['callbacks']) ){
            foreach( $t->children['callbacks'] as $i ){
              $t->content .= '<a target="_blank" href="/direction/callback/view/'.$i->id.'">Возврат # '.$i->id.'</a><br>';
            }
          }
          if ( count($t->children['remains']) ){
            foreach( $t->children['remains'] as $i ){
              $t->content .= '<a target="_blank" href="/direction/remains/view/'.$i->id.'">Остатки # '.$i->id.'</a><br>';
            }
          }

          $t->invalid_distance  = $t->invalid_distance;
          $t->invalid_begintime = $t->invalid_begintime;

          $timelineArr = $t->getAsArray();

          if ( ($timelineArr['entry_distance'] > Config::get('agent_routes')->entry_distance) ){
            $timelineArr['valid'] = false;
            $timelinesArray[] = $timelineArr;

            $timelineArr['valid'] = true;
            $timelineArr['lat'] = $timelineArr['lat_end'];
            $timelineArr['lng'] = $timelineArr['lng_end'];
            $timelineArr['enter'] = false;
            $timelinesArray[] = $timelineArr;
          } else {
            $timelinesArray[] = $timelineArr;
          }

        }

        foreach ( $route->children as $r ){
          $pointsArray[] = $r->getAsArray();
        }

        //add start/end point to timelines array


        $workStartEnd = Location_Model::getStartEndByAgentAndPeriod($a, $date, ($date+24*3600));
        $workStartEnd['end']['agent']   = $a->name;
        $workStartEnd['start']['agent'] = $a->name;
        $timelinesArray[] = $workStartEnd['end'];
        array_unshift($timelinesArray, $workStartEnd['start']);

        $a->timelinesArray = $timelinesArray;
        $a->pointsArray    = $pointsArray;
      }

	  $this->smarty->assign(array('agents' => $agents));

      $tpl = "direction/routes/agents-by-date.tpl";
    } else {
      ////////////////////////////////////////////////////////////////////
      $route = Stalexo_Route_Model::getByAgentAndDay($agent, date('N', $date));

      if ( $route->isLoaded() ){
        $route->loadPoints();
      } else {
        Stalexo_Messenger::addMessage('Маршрут не задан', 'info');
      }

      $timelines = Stalexo_Timeline_Model::getByAgentAndPeriod($agent, $date, ($date+24*3600));

      foreach ( $route->children as $k => $r ){
        if ( array_key_exists($k, $timelines) ){
          $r->isValidTimeline($timelines[$k], $date);
        }
      }

      $timelinesArray = array();
      $pointsArray    = array();

      foreach( $timelines as $k => $t ) {
        $t->calculateEntryDistance();
        $t->loadActivity();
        $t->time_begin = date('H:i', $t->date_begin);
        $t->time_end   = date('H:i', $t->date_end);
        $t->time_diff  = round((($t->date_end - $t->date_begin)+0.01) / 60);

        $t->enter = true;
        $t->lat = $t->lat_begin;
        $t->lng = $t->lng_begin;
        $t->index = $k;
        $t->valid = true;
        $t->content = '';
        if ( count($t->children['orders']) ){
          foreach( $t->children['orders'] as $i ){
            $t->content .= '<a target="_blank" href="/direction/orders/view/'.$i->id.'">Заказ # '.$i->id.'</a><br>';
          }
        }
        if ( count($t->children['callbacks']) ){
          foreach( $t->children['callbacks'] as $i ){
            $t->content .= '<a target="_blank" href="/direction/callback/view/'.$i->id.'">Возврат # '.$i->id.'</a><br>';
          }
        }
        if ( count($t->children['remains']) ){
          foreach( $t->children['remains'] as $i ){
            $t->content .= '<a target="_blank" href="/direction/remains/view/'.$i->id.'">Остатки # '.$i->id.'</a><br>';
          }
        }

        $t->invalid_distance  = $t->invalid_distance;
        $t->invalid_begintime = $t->invalid_begintime;

        $timelineArr = $t->getAsArray();

        if ( ($timelineArr['entry_distance'] > Config::get('agent_routes')->entry_distance) ){
          $timelineArr['valid'] = false;
          $timelinesArray[] = $timelineArr;

          $timelineArr['valid'] = true;
          $timelineArr['lat'] = $timelineArr['lat_end'];
          $timelineArr['lng'] = $timelineArr['lng_end'];
          $timelineArr['enter'] = false;
          $timelinesArray[] = $timelineArr;
        } else {
          $timelinesArray[] = $timelineArr;
        }

      }

      foreach ( $route->children as $r ){
        $pointsArray[] = $r->getAsArray();
      }

      //map timeline - plan

      foreach($timelines as $t){
        foreach($route->children as $point ){
          if( $t->id_dp == $point->id_dp ){
            $t->plan = $point;
          }
        }
      }

      $workStartEnd = Location_Model::getStartEndByAgentAndPeriod($agent, $date, ($date+24*3600));
      $workStartEnd['end']['agent']   = $agent->name;
      $workStartEnd['start']['agent'] = $agent->name;
      $timelinesArray[] = $workStartEnd['end'];
      array_unshift($timelinesArray, $workStartEnd['start']);

      $this->smarty->assign(array(
        'pointsArray'    => $pointsArray,
        'timelinesArray' => $timelinesArray,
        'timelines'      => $timelines
      ));
      ////////////////////////////////////////////////////////////////////
    }

    $this->smarty->assign(array(
      'message' => Stalexo_Messenger::getMessage()
    ));

    $this->render($this->smarty->fetch($tpl));
  }

}