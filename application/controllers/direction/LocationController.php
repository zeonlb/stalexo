<?php
class Direction_LocationController extends Controller_Action{

  public function indexAction(){

    $timestamp = Date::ymd2timestamp( Stalexo_Direction::getDate() );
    if ( ! $timestamp ){
      $timestamp = Date::ymd2timestamp( date('Y-m-d') );
    }
    $agent = Stalexo_Direction::getAgent();

    $points             = array();
    $pointsInfo         = array();
    $center             = null;
    $locations          = array();
    $ordersPlacemark    = array();
    $callbacksPlacemark = array();
    $remainsPlacemark   = array();
    $encashment         = array();

    if ( $agent->isLoaded() ){
      $locations = Location_Model::fetchByQuery(
        Location_Model::getFetchQuery()
        ->select('ROUND(location.date/30, 0) as minutes')
        ->where('location.id_staff = "'.$agent->id.'"')
        ->where('(location.date BETWEEN "'. $timestamp .'" AND "'. ($timestamp + 86400) .'")')
        ->groupBy('minutes')
        ->orderBy('location.id ASC')
        ->limit(549)
      );

      $orders = new Stalexo_Order_Collection();
      $orders->pager()->onPage = 20;
      $orders->queryRestrictByManager( $agent );
      $orders->queryRestrictByDay( $timestamp );
      $orders->queryJoinLocation();
      $orders->loadAll(true);

      foreach( $orders as $o ){
        $ordersPlacemark[] = array('id' => $o->id, 'total' => $o->total, 'lat' => $o->lat, 'lng' => $o->lng, 'time' => date('H:i',$o->date), 'url'  => '/direction/orders/view/' . $o->id  );
      }

      $callbacks = new Stalexo_Callback_Collection();
      $callbacks->pager()->onPage = 20;
      $callbacks->queryRestrictByManager( $agent );
      $callbacks->queryRestrictByDay( $timestamp  );
      $callbacks->queryJoinLocation();
      $callbacks->loadAll();

      foreach( $callbacks as $c ){
        $callbacksPlacemark[] = array('id' => $c->id, 'lat' => $c->lat, 'lng' => $c->lng, 'time' => date('H:i',$c->date), 'url'  => '/direction/callback/view/' . $c->id  );
      }

      $remains = new Stalexo_Remain_Collection();
      $remains->pager()->onPage = 20;
      $remains->queryRestrictByManager( $agent );
      $remains->queryRestrictByDay( $timestamp );
      $remains->queryJoinLocation();
      $remains->loadAll();

      foreach( $remains as $r ){
        $remainsPlacemark[] = array(
        'id' => $r->id,
        'lat' => $r->lat,
        'lng' => $r->lng,
        'time' => date('H:i',$r->date),
        'url'  => '/direction/remains/view/' . $r->id
        );
      }

      $encash = new Stalexo_Encashment_Collection();
      $encash->pager()->onPage = 20;
      $encash->queryRestrictByManager( $agent );
      $encash->queryRestrictByDay( $timestamp );
      $encash->queryJoinLocation();
      $encash->loadAll();

      foreach( $encash as $e ){
        $encashment[] = array(
        'id'     => $e->id,
        'lat'    => $e->lat,
        'lng'    => $e->lng,
        'amount' => $e->amount,
        'time'   => date('H:i',$e->date),
        'url'    => '/direction/encashment/view/' . $e->id
        );
      }

    }

    foreach($locations as $l ){

      $points[] = array(number_format($l->lat, 3), number_format($l->lng,3));
      $pointsInfo[] = array('time' => date('H:i',$l->date));
      //$points[] = array( 'type' => 'wayPoint' , 'point' => array(number_format($l->lat, 3), number_format($l->lng,3)) );
    }

    if ( count($points) ){
      $center = $points[0];
    }

    $this->smarty->assign(array(
      'points'    => $points,
      'pointsInfo'=> $pointsInfo,
      'center'    => $center,
      'orders'    => $ordersPlacemark,
      'callbacks' => $callbacksPlacemark,
      'remains'   => $remainsPlacemark,
      'encashment'=> $encashment
    ));

    $this->render($this->smarty->fetch('direction/location/index.tpl'));
  }

}