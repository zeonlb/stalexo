<?php
class Direction_RemainsController extends Controller_Action{

  public function indexAction(){
    $collection = new Stalexo_Remain_Collection();
    $collection->pager()->onPage = 20;
    $collection->pager()->url = '/direction/remains';

    $collection->queryRestrictByManager( Stalexo_Direction::getAgent() );
    $collection->queryRestrictByDay( Date::ymd2timestamp( Stalexo_Direction::getDate() ),Date::ymd2timestamp( Stalexo_Direction::getDate2() ) );   
    
    $collection->queryAddDetails();
    $collection->loadAll();

    $this->smarty()->assign(array(
      'collection' => $collection
    ));
    $this->render($this->smarty->fetch('direction/remains/list.tpl'));
  }

  public function viewAction(){
    $id = App::getRequest()->get('3');

    $collection = new Stalexo_Remain_Collection();
    $collection->queryAddDetails();
    $remain = $collection->getById($id);

    if( ! $remain->isLoaded() ){
      App::render404();
    }

    $remain->loadSku();
    $this->smarty()->assign(array('remain' => $remain));
    $this->render($this->smarty->fetch('direction/remains/view.tpl'));
  }

}