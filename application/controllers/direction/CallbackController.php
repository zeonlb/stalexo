<?php
class Direction_CallbackController extends Controller_Action{

  public function indexAction(){
    $collection = new Stalexo_Callback_Collection();
    $collection->pager()->onPage = 20;
    $collection->pager()->url = '/direction/callback';

    $collection->queryRestrictByManager( Stalexo_Direction::getAgent() );
    $collection->queryRestrictByDay( Date::ymd2timestamp( Stalexo_Direction::getDate() ),Date::ymd2timestamp( Stalexo_Direction::getDate2() )  );
    
    $collection->queryAddDetails();
    $collection->loadAll();

    $this->smarty()->assign(array(
      'collection' => $collection
    ));
    $this->render($this->smarty->fetch('direction/callback/list.tpl'));
  }

  public function viewAction(){
    $id = App::getRequest()->get('3');

    $collection = new Stalexo_Callback_Collection();
    $collection->queryAddDetails();
    $callback = $collection->getById($id);

    if( ! $callback->isLoaded() ){
      App::render404();
    }

    $callback->loadSku();
    $this->smarty()->assign(array('callback' => $callback));
    $this->render($this->smarty->fetch('direction/callback/view.tpl'));
  }

}