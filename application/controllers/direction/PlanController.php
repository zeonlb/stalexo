<?php
class Direction_PlanController extends Controller_Action{

  public function indexAction(){
    $agent = Stalexo_Direction::getAgent();
    $date  = Date::ymd2timestamp( Stalexo_Direction::getDate() );

    $collection = new Stalexo_Dp_Plan_Collection();
    $collection->pager()->onPage = 20;
    $collection->pager()->url = '/direction/plan';

    $collection->byDate($date);
    $collection->queryJoinDetails();
    $collection->byManager($agent);
    $collection->loadAll();

    $collection->eachLoadOrdersTotal();
    $collection->eachLoadCallbacksTotal();
    
    $q = clone $collection->query();

    $this->smarty->assign(array(
      'collection'  => $collection,
      'ordersTotal' => Stalexo_Dp_Plan_Collection::getOrdersTotal($date, $agent),
      'ordersPlannedTotal' => Stalexo_Dp_Plan_Collection::getOrdersPlannedTotal($date, $agent),
      'date'               => ($date ? $date : time()),
      'agent'              => $agent
    ));

    $this->render($this->smarty->fetch('direction/plan/index.tpl'));
  }

}