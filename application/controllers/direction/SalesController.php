<?php
class Direction_SalesController extends Controller_Action{
  
  public function indexAction(){
    $agent = Stalexo_Direction::getAgent();
    $date  = Date::ymd2timestamp( Stalexo_Direction::getDate() );
    $clients = new Stalexo_Client_Collection();
       
    if ( ! $agent->isLoaded() ){
      Stalexo_Messenger::addMessage('Выберите агента', 'info');
    } else {
      
      $clients->queryJoinManager($agent); 
      $clients->queryJoinSalesPlan($date);   
      $clients->query()->groupBy('clients.id');
      $clients->loadAll();
      
      $clients->eachLoadPlanDetail($date, $agent);
    }
    
    $this->smarty->assign(array(
      'clients' => $clients,
      'message' => Stalexo_Messenger::getMessage()
    ));
      
    $this->render($this->smarty->fetch('direction/sales/clients.tpl'));
  }
  
}