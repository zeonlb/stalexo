<?php
class Direction_OrdersController extends Controller_Action{

  public function indexAction(){
    $orders = new Stalexo_Order_Collection();

    $orders->pager()->onPage = 20;
    $orders->pager()->url = '/direction/orders';
	
	$orders->queryRestrictByManager( Stalexo_Direction::getAgent() );
	
	$orders->queryRestrictByClient( Stalexo_Direction::getClient() );
	$orders->queryRestrictByAddress( Stalexo_Direction::getAddress() );
	
	
    
	
	
	
	
    $orders->queryRestrictByDay( Date::ymd2timestamp( Stalexo_Direction::getDate() ),Date::ymd2timestamp( Stalexo_Direction::getDate2() ) );
    $orders->loadAll(true);

    $this->smarty->assign(array(
      'orders' => $orders
    ));

    $this->render($this->smarty->fetch('direction/order/list.tpl'));
  }

  public function viewAction(){
    $id = App::getRequest()->get(3);
    $collection = new Stalexo_Order_Collection();

    $order = $collection->getById(array($id));

    if ( ! $order->isLoaded() ){
      App::render404();
    }

    $order->calculateTotal();
    $order->loadOrderedSku();

    $this->smarty->assign(array(
      'order' => $order
    ));

    $this->render($this->smarty->fetch('direction/order/view.tpl'));
  }

}