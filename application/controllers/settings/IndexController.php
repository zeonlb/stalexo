<?php
class Settings_IndexController extends Controller_Action{

  public function indexAction(){
    if ( App::getRequest()->isPost() ){

      $staff  = App::getRequest()->post('staff');
      $device = App::getRequest()->post('device');
      $password = App::getRequest()->post('password');

      if ( ! Valid::password($password) ) {
        $this->smarty->assign('result', 'Invalid password');
      } else {
        try {
          $user = Api_Auth::addApiUser($password, $device, $staff);

          if ( $user->isLoaded() ){
            $this->smarty->assign('result', 'Success');
          } else {
            $this->smarty->assign('result', 'Fail');
          }
        } catch ( StaffUserNotExists $e ) {
          $this->smarty->assign('result', 'Select user');
        } catch( ApiUserAlreadyExists $e ){
          $this->smarty->assign('result', 'User already have device');
        }
      }
    }

    $staff = new User_Staff_Collection();
    $staff->pager()->onPage = 1000;
    $staff->loadUsers();

    $this->smarty->assign(array(
      'staff' => $staff,
    ));
    $this->render($this->smarty()->fetch('auth/api/form.tpl'));
  }

}