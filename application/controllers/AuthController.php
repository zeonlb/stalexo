<?php
class AuthController extends Controller_Action{

  public function indexAction(){
    App::render404("action forbidden");
  }

  public function loginAction(){
    if ( App::user()->loggedIn() ){
      App::redirectAuthHome();
    }

    $this->render($this->smarty->fetch("auth/login/form.tpl"));
  }

  public function socialAction(){
    $social = App::getRequest()->get(2);

    switch($social){
      case "fb":
        try{
          $auth = new Auth_Adapter( new Auth_Type_Facebook() );
          $auth->authenticate( App::getRequest()->request() );
          Auth_Type_Abstract::closeSocialWindow();
        } catch ( Auth_Exception_AccessDenied $e ){
          Messenger::addError('Facebook: Отказано в доступе');
          Auth_Type_Abstract::closeSocialWindow();
        } catch ( Registration_Exception_InvalidInput $e ){
          Messenger::addError('Неправильный формат данных');
          Auth_Type_Abstract::closeSocialWindow();
        }
        break;
      case "google":
        try {
          $auth = new Auth_Adapter( new Auth_Type_Google() );
          $auth->authenticate( App::getRequest()->request() );
          Auth_Type_Abstract::closeSocialWindow();
        } catch( Auth_Exception_AccessDenied $e ){
          Messenger::addError('Google: Отказано в доступе');
          Auth_Type_Abstract::closeSocialWindow();
        } catch( Registration_Exception_InvalidInput $e ){
          Messenger::addError('Неправильный формат данных');
          Auth_Type_Abstract::closeSocialWindow();
        }
        break;
      default:
        App::render404('Invalid parameters');
        break;
    }

  }

  public function activationAction(){
    if ( App::user()->loggedIn() ){
      App::redirectAuthHome();
    }
    $hash = App::getRequest()->get(2);
    if ( ! $hash ){
      $hash = 'undefined';
    }
    try{
      $userid = Auth_Registration::activate($hash);
      //$_SESSION['auth']['userid'] = $userid;
      $this->render($this->smarty->fetch('auth/registration/activation-success.tpl'));
    } catch(Registration_Exception_InvalidActivation $e){
      $this->render($this->smarty->fetch('auth/registration/activation.tpl'));
    }
  }

  public function passwordRecoveryAction(){
    if ( App::user()->loggedIn() ){
      App::redirect();
    }
    $hash = App::getRequest()->get(2);
    if ( $hash ){
      try {
        $userRow = Auth_Registration::getUserRowByPassword( $hash );
        $linkValid = true;
      } catch ( Registration_Exception_UserNotExists $e ){
        $linkValid = false;
      }

      $this->smarty->assign(array(
        'linkValid' => $linkValid,
        'hash' => $hash
      ));
      $this->render( $this->smarty->fetch('auth/registration/password-recovery.tpl') );
    } else {
      $this->render( $this->smarty->fetch('auth/registration/password-recovery-link.tpl') );
    }
  }

  public function logoutAction(){
    if ( App::user()->loggedIn() ){
      App::user()->logout();
    }
    App::redirect();
  }

  public function registrationAction(){
    if ( App::user()->loggedIn() ){
      App::redirect();
    }
    Tray::push(new Tray_Node('Главная', '/'));
    Tray::push(new Tray_Node('Регистрация', ''));

    $this->smarty()->assign(array( 'tray' => new Block_Tray() ));
    $this->render( $this->smarty->fetch('auth/registration/form.tpl') );
  }

}