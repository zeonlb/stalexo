<?php
class ExportController extends Controller_Action{

  public function indexAction(){
    App::render404();
  }

  public function ordersAction(){
    $path = 'data/api/export/orders.xml';

    if ( file_exists($path) ){
      echo "Stay in queue";
      exit;
    }

    $collection = new Stalexo_Order_Collection();
    $collection->query()->where('`order`.exported = "no"');
    $collection->pager()->onPage = 3;
    $collection->loadAllForExport();

    $this->smarty->assign(array('orders' => $collection));
    $xml = $this->smarty->fetch('export/orders.tpl');

    file_put_contents($path, $xml);
    chmod($path, 0777);

    echo "<b>" . $collection->count() . " orders exported</b>";
    exit;
  }

}