<?php 
class DebugController extends Controller_Action{
  
  public function indexAction(){
    if ( App::getRequest()->get('dev') ){
      Debug::setDeveloperMode();
    } 
    if ( App::getRequest()->get('user') ){
      Debug::setUserMode();
    } 
    App::redirect('/');
  }
  
}