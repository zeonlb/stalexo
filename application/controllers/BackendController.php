<?php
class BackendController extends Controller_Module{

  public function init(){
    if ( ! App::user()->hasPermission('backend') ){
      App::renderForbidden();
    }
    App::view()->setWrapper(new Layout_Backend());
  }

  public function indexAction(){
    $permissions = User_Permission_Model::fetchByQuery(
      User_Permission_Model::getFetchQuery()
      ->leftJoin('user_role_has_permission urhp', 'urhp.id_permission = user_permissions.id AND urhp.id_role = "'.App::user()->id_role.'"')
      ->select('IF(urhp.id_role IS NULL, 0, 1) as allowed')
    );

    $this->smarty->assign('permissions', $permissions);

    $this->render($this->smarty->fetch('backend/index.tpl'));
  }

}