<?php
class AndroidController extends Controller_Action{
  
  protected function init(){
    App::view()->js()->add('/assets/js/jquery.js');
  }
  
  public function indexAction(){
    
    if ( App::getRequest()->isPost() ){
      $post = App::getRequest()->post();
      $result = App_Request::sendPost( 'http://stalexo.com/api', $post);
      $this->smarty->assign('result', $result);
    }
    
    $this->render($this->smarty->fetch('android/request.tpl'));
  }
  
  public function orderAction(){
    App::view()->js()->add('/assets/js/jquery.js');
    
    $manager = new User_Api('254');
    
    $clients = new Stalexo_Client_Collection();
    $clients->loadByManager($manager); 
    
    $this->smarty->assign(array(
      'clients' => $clients
    ));
    
    $this->render($this->smarty->fetch('android/order.tpl'));
  }
  
  public function syncAction(){
    
    if ( App::getRequest()->isPost() ){
      $post = App::getRequest()->post();
      $result = App_Request::sendPost( 'http://stalexo.com/api', $post);
      $this->smarty->assign('result', $result);
    }
    
    $this->render($this->smarty->fetch('android/sync.tpl'));
  }
  
}