<?php
class ApiAjaxController extends Controller_Ajax{
  
  public function addOpinionAction(){
    $id = App::getRequest()->post('vote');
    $voting = Voting_Opinion::addOpinion($id);

    $results = Voting_Model::makeBlock($voting);
    $this->output['status'] = 1;
    $this->output['content'] = $results;
  }
  
}