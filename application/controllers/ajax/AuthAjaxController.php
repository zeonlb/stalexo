<?php 
class AuthAjaxController extends Controller_Ajax{
  
  public function addUserAction(){
    try{
      $data = App::getRequest()->post();
      Auth_Registration::addUser($data);
      $this->output['status']  = 1;
      $this->output['message'] = $this->smarty->fetch('auth/registration/complete.tpl');
    } catch( Registration_Exception_UserAlreadyExists $e ){
      $this->output['error'] = 'user-exists';
    } catch( Registration_Exception_InvalidInput $e ){
      $this->output['error'] = 'input';
    }
  }
  
  public function loginAction(){
    $login    = isset($_POST['email']) ? trim($_POST['email']) : "";
    $password = isset($_POST['password']) ? trim($_POST['password']) : "";
    try{
      $auth = new Auth_Adapter( new Auth_Type_Default() );
      $user = $auth->authenticate( array( 'email' => $login, 'password' => $password ) );             
      $this->output['status'] = 1;
    } catch( Auth_Exception_WrongCredentials $e ){
      $this->output['error'] = "credentials";
    } catch( Auth_Exception_WrongPassword $e ){
      $this->output['error'] = "password";
    } catch( Auth_Exception_WrongLogin $e ){
      $this->output['error'] = "login";
    } catch( Auth_Exception_AccountNotActive $e ){
      $this->output['error'] = "accNotActive";
    }
  }
                  
  public function mailRecoveryLinkAction(){
    $email = isset($_POST['email']) ? $_POST['email'] : false;
    try {
      $link = Auth_Registration::getRecoverPasswordLink( $email );
      Mailer_Adapter::send( "Восстановление пароля", $email, new Mailer_Content_Auth_PasswordRecovery($link) );
      $this->output['status'] = 1; 
    } catch( Registration_Exception_InvalidInput $e ){
      $this->output['error'] = "email";
    } catch( Registration_Exception_UserNotExists $e ){
      $this->output['error'] = "userNotExists";
    }   
  }
  
  public function changePasswordAction(){
    $hash = isset($_POST['hash']) ? $_POST['hash'] : 0;
    $pass = isset($_POST['password']) ? trim($_POST['password']) : 0;
    try {
      $userRow = Auth_Registration::getUserRowByPassword( $hash );
      $user = new User_Customer($userRow['userid']);
      Auth_Registration::changePassword( $user, $pass );
      $this->output['status'] = 1;
    } catch ( Registration_Exception_UserNotExists $e ){
      $this->output['error'] = "link";
    } catch ( Registration_Exception_InvalidInput $e ){
      $this->output['error'] = "password";
    }
  }
  
  public function addCoachAction(){
    if ( ! App::user()->hasPermission('office', 'coaches', 'add') ){
      $this->output['error'] = 'permission';
      return false;
    }
    
    try{
      $data = App::getRequest()->post();
      Auth_Registration::addCoach($data);
      $this->output['status']  = 1;
      $this->output['message'] = 'success';
    } catch( Registration_Exception_UserAlreadyExists $e ){
      $this->output['error'] = 'user-exists';
    } catch( Registration_Exception_InvalidInput $e ){
      $this->output['error'] = 'input';
    }
    
  }
  
}