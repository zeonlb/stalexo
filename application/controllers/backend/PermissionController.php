<?php
class Backend_PermissionController extends Controller_Action{
  
  public function indexAction(){
    $roles = User_Role_Model::fetchAll();
    
    array_map(function($r){ $r->loadPermissionList(); }, $roles);
    
    $this->smarty->assign(array(
      'roles' => $roles
    ));
    $this->render($this->smarty->fetch('backend/permission/index.tpl'));
  }
  
  public function rolesAction(){
    if ( ! App::user()->hasPermission('users_crud_roles') ){
      Stalexo_Messenger::addMessage('<b>Недостаточно полномочий</b>', 'info');
      App::redirect('/backend/permission');
    }
    
    $roles = User_Role_Model::fetchAll();
    $this->smarty->assign(array(
      'roles' => $roles
    ));
    $this->render($this->smarty->fetch('backend/permission/role-list.tpl'));
  }
  
  public function addRoleAction(){
    if ( ! App::user()->hasPermission('users_crud_roles') ){
      Stalexo_Messenger::addMessage('<b>Недостаточно полномочий</b>', 'info');
      App::redirect('/backend/permission/roles');
    }
    
    if ( App::getRequest()->isPost() ){
      $id    = (int)App::getRequest()->post('id');
      $alias = App::getRequest()->post('alias');
      $name  = App::getRequest()->post('name');
      
      if ( $id > 0 ){
        //edit
        $role = new User_Role_Model($id);
        if ( $role->isLoaded() ){
          $role->name = $name;
          $role->save();
          Stalexo_Messenger::addMessage('<b>Роль <i>'.$role->name.'</i> успешно отредактирована</b>', 'success');     
        } 
      } else {
        //add
        if ( $name and Valid::alias($alias) ){
          $role = new User_Role_Model();
          $role->alias = $alias;
          $role->name = $name;
          $role->save();
          Stalexo_Messenger::addMessage('<b>Роль <i>'.$role->name.'</i> успешно добавлена</b>', 'success');     
        } else {
          Stalexo_Messenger::addMessage('<b>Неверный формат данных</b>', 'error');
        }
      }
      
      App::redirect('/backend/permission/roles');
    }
    
    $role = new User_Role_Model();
    $id = App::getRequest()->get(3);
    if ( $id ){
      $role = new User_Role_Model($id);
    } 
    
    $this->smarty->assign('role', $role);
    $this->render($this->smarty->fetch('backend/permission/role-add.tpl'));
  }
  
  public function deleteRoleAction(){
    Stalexo_Messenger::addMessage('<b>Недостаточно полномочий</b>', 'info');
    App::redirect('/backend/permission/roles');
  }
  
  public function addAction(){
    if ( ! App::user()->hasPermission('users_crud_permission') ){
      Stalexo_Messenger::addMessage('<b>Недостаточно полномочий</b>', 'info');
      App::redirect('/backend/permission');
    }
    
    if ( App::getRequest()->isPost() ){
      $id = App::getRequest()->post('id');
      $name = App::getRequest()->post('name');
      $group = App::getRequest()->post('group');
      if ( $id ){
        //edit
        $p = new User_Permission_Model($id);
        if ( $p->isLoaded() ){
          $p->name = $name;
          $p->id_group = $group;
          $p->save();
          Stalexo_Messenger::addMessage('<b>Изменения сохранены</b>', 'success');
        } else {
          Stalexo_Messenger::addMessage('<b>Ошибка</b>', 'error');
        }       
      } else {
        //add
        $alias = App::getRequest()->post('alias');
        
        if ( ! $name or ! Valid::alias($alias) ){
          Stalexo_Messenger::addMessage('<b>Неверный формат данных</b>', 'error');
        } else {
          $p = new User_Permission_Model();
          $p->alias = $alias;
          $p->name  = $name;
          $p->id_group = $group;
          $p->save();
          Stalexo_Messenger::addMessage('<b>Доступ <i>'.$p->name.'</i> добавлен</b>', 'success');
        }       
      }
      App::redirect('/backend/permission');
    }
    $p = new User_Permission_Model();
    if ( is_numeric(App::getRequest()->get(3)) ){
      $p = new User_Permission_Model((int)App::getRequest()->get(3));
    }
    
    $this->smarty->assign(array(
      'p'      => $p,
      'groups' => User_Permission_Group::fetchAll()
    ));
    $this->render($this->smarty->fetch('backend/permission/add.tpl'));
  }
  
  public function deleteAction(){
    $id = App::getRequest()->get(3);
    if( App::user()->hasPermission('users_crud_permission') ){
      $p = new User_Permission_Model($id); 
      if ( $p->isLoaded() ){
        Stalexo_Messenger::addMessage('<b>Полномочие <i>'.$p->name.'</i> удалено</b>', 'success');
        $p->delete();
      }
    } else {
      Stalexo_Messenger::addMessage('<b>Недостаточно полномочий</b>', 'info');
    }
    App::redirect('/backend/permission');
  }
  
  public function editRolePermissionAction(){
    if ( ! App::getRequest()->isPost() ){ App::render404(); }
        
    if ( App::user()->hasPermission('users_permission') ){
      $role = new User_Role_Model((int)App::getRequest()->post('edit_role'));
      if ( ! $role->isLoaded() ){ Stalexo_Messenger::addMessage('<b>Ошибка</b>', 'error'); }
      $permission = App::getRequest()->post('permission');
      $role->unjoinPermissions();
      if (is_array($permission) and array_key_exists($role->id, $permission) and is_array($permission[$role->id])) {
        foreach( $permission[$role->id] as $idPermission => $val ){
          $role->joinPermission($idPermission);
        }
      }
      Stalexo_Messenger::addMessage('<b>Права для роли <i>'.$role->name.'</i> изменены</b>', 'success');  
    } else {
      Stalexo_Messenger::addMessage('<b>Недостаточно полномочий</b>', 'info');
    }
    
    App::redirect('/backend/permission');
  }
  
  public function groupsAction(){
    
    $this->smarty->assign(array(
      'groups' => User_Permission_Group::fetchAll()
    ));
    
    $this->render($this->smarty->fetch('backend/permission/group-list.tpl'));
  }
  
  public function addGroupAction(){   
    if ( ! App::user()->hasPermission('users_crud_permission_groups') ){
      Stalexo_Messenger::addMessage('<b>Недостаточно полномочий</b>', 'info');
      App::redirect('/backend/permission/groups');
    }
       
    if ( App::getRequest()->isPost() ){
      $id = App::getRequest()->post('id');
      $name = App::getRequest()->post('name');
      if ( $id ){
        $group = new User_Permission_Group($id);
        if ( $name and $group->isLoaded() ){
          $group->name = $name;
          $group->save();
          Stalexo_Messenger::addMessage('<b>Изменения сохранены</b>', 'success'); 
        } else {
          Stalexo_Messenger::addMessage('<b>Ошибка</b>', 'error');
        }
      } else {
        $group = new User_Permission_Group();
        $group->name = $name;
        if ( $name and $group->save() ){
          Stalexo_Messenger::addMessage('<b>Група <i>'.$group->name.'</i> добавлена</b>', 'success');
        } else {
          Stalexo_Messenger::addMessage('<b>Ошибка</b>', 'error');
        }
      }
      App::redirect('/backend/permission/groups');
    }
    $id = App::getRequest()->get(3);
    $group = new User_Permission_Group();
    if ( $id ){
      $group = new User_Permission_Group($id);
    }
    
    $this->smarty->assign('group', $group);
    $this->render($this->smarty->fetch('backend/permission/group-add.tpl'));
  }
  
  public function deleteGroupAction(){
    if ( ! App::user()->hasPermission('users_crud_permission_groups') ){
      Stalexo_Messenger::addMessage('<b>Недостаточно полномочий</b>', 'info');
      App::redirect('/backend/permission/groups');
    }
    $id = App::getRequest()->get(3);
    $group = new User_Permission_Group($id);
    if ( $group->isLoaded() ){
      Stalexo_Messenger::addMessage('<b>Группа <i>'.$group->name.'</i> удалена</b>', 'info');
      $group->delete();
    } else {
      Stalexo_Messenger::addMessage('<b>Ошибка</b>', 'error');
    }
    App::redirect('/backend/permission/groups');
  }
  
}