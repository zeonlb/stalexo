<?php
class Backend_SettingsController extends Controller_Action{

  public function indexAction(){
    $devices = User_Staff_Device::fetchByQuery(
      User_Staff_Device::getFetchQuery()
      ->join('staff s', 's.id = staff_device.id_staff')
      ->select('s.name')
    );
    $this->smarty->assign('devices', $devices);
    $this->render($this->smarty->fetch('backend/settings/list.tpl'));
  }

  public function addAction(){
    if ( ! App::user()->hasPermission('users_crud_devices') ){
      Stalexo_Messenger::addMessage('<b>Недостаточно полномочий</b>', 'info');
      App::redirect('/backend/settings');
    }
    if ( App::getRequest()->isPost() ){
      $id       = App::getRequest()->post('id');
      $staff    = App::getRequest()->post('staff');
      $device   = App::getRequest()->post('device');
      $password = App::getRequest()->post('password');

      if ( $id ){
        $d = new User_Staff_Device($id);
        if ( $d->isLoaded() ){
          $d->device = $device;
         
          //begin
        $result = App::db()->query("DELETE from staff_settings_opt where id_staff=$d->id_staff");
           
        $result = App::db()->query("SELECT * FROM staff_settings");
        if ( $result ){
          while( $row = $result->fetch_assoc() ){
                  
                  foreach(App::getRequest()->post('int_'.$row['alias']) as $k=>$v){
                          $r1 = App::getRequest()->post('int_'.$row['alias']);
                          $r2 = App::getRequest()->post('str_'.$row['alias']);
                          $r3 = App::getRequest()->post('text_'.$row['alias']);
                          $r4 = App::getRequest()->post('bool_'.$row['alias']);
                          if(!empty($r1[$k]) || !empty($r2[$k]) || !empty($r3[$k]) || !empty($r4[$k]))
                          $result2 = App::db()->query("insert into staff_settings_opt values(null,".$row['id'].",".$d->id_staff.",'".$r1[$k]."','".$r2[$k]."','".$r3[$k]."','".$r4[$k]."')");
                  }

          }
        }

   
          if ( $d->isLoaded() ){
            Stalexo_Messenger::addMessage('<b>Данные сохранены</b>', 'success');
          } else {
            Stalexo_Messenger::addMessage('<b>Ошибка при редактировании</b>', 'error');
          }
        } else {
          Stalexo_Messenger::addMessage('<b>Ошибка. Запись не найдена</b>', 'error');
        }
      } else {
        try {
          if ( ! (Valid::id1c($staff) and Valid::password($password) and $device) ){
            Stalexo_Messenger::addMessage('<b>Неверный формат данных</b>', 'error');
            App::redirect('/backend/devices');
          }
          $user = Api_Auth::addApiUser($password, $device, $staff);

          if ( $user->isLoaded() ){
            $staff = new User_Staff_Model($user->id_staff);
            Stalexo_Messenger::addMessage('<b>Для агента <i>'.$staff->name.'</i> назначен планшет <i>'.$user->device.'</i> </b>', 'success');
          } else {
            Stalexo_Messenger::addMessage('<b>Ошибка</b>', 'error');
          }
        } catch ( StaffUserNotExists $e ) {
          Stalexo_Messenger::addMessage('<b>Агент не указан</b>', 'info');
        } catch( ApiUserAlreadyExists $e ){
          Stalexo_Messenger::addMessage('<b>У агента уже есть планшет</b>', 'info');
        }
      }

      App::redirect('/backend/settings');
    }

    //Наполнение данными
    $staff = User_Staff_Settings::fetchByQuery(
      User_Staff_Settings::getFetchQuery()
      ->where('so.id_staff = 4')->orderBy('staff_settings.name')
      ->select('so.id_staff')
      ->select('so.device')
      ->leftJoin('staff_settings_opt so', 'so.id_set = staff_settings.id')
    );

    $d = new User_Staff_Device();
    if ( App::getRequest()->get(3) and is_numeric(App::getRequest()->get(3)) ){
      $d = new User_Staff_Device(App::getRequest()->get(3));
      if ( ! $d->isLoaded() ){
        Stalexo_Messenger::addMessage('<b>Ошибка</b>', 'error');
        App::redirect('/backend/devices');
      }
    }

      $result = App::db()->query("SELECT * FROM staff_settings s left join staff_settings_opt so on s.id=so.id_set and so.id_staff=".App::getRequest()->get(3)." order by s.alias");
        if ( $result ){
          while( $row = $result->fetch_assoc() ){
            $items[] = $row;
          }
        }
    $this->smarty->assign(array(
      'staff'  => $items,
      'd'      => $d
    ));

    $this->render($this->smarty->fetch('backend/settings/add.tpl'));
  }

  
  public function errorAction(){
    App::render404();
  }

}