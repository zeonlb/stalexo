<?php
class Backend_UsersController extends Controller_Action{
  
  public function indexAction(){
    if ( ! App::user()->hasPermission('users_view') ){
      Stalexo_Messenger::addMessage('<b>Недостаточно полномочий</b>', 'info');
      App::redirect('/backend');
    }
    
    $users = User_Stalexo::fetchByQuery(
      User_Stalexo::getFetchQuery()->orderBy('users.userid')
    );
    
    $this->smarty->assign(array(
      'users' => $users
    ));
    
    $this->render($this->smarty->fetch('backend/users/list.tpl'));
  }
  
  public function deleteAction(){
    if ( ! App::user()->hasPermission('users_delete') ){
      Stalexo_Messenger::addMessage('<b>Недостаточно полномочий</b>', 'info');
      App::redirect('/backend/users');
    }
    $id = App::getRequest()->get(3);
    
    if ( App::user()->userid == $id ){
      Stalexo_Messenger::addMessage('<b>Нельзя удалить себя</b>', 'info');
      App::redirect('/backend/users');
    }
    
    $user = new User_Stalexo($id);
    if ( $user->isLoaded() ){
      Stalexo_Messenger::addMessage('<b>Пользователь <i>'.$user->email.'</i> был удален</b>', 'success');
      if ( $user->can_be_deleted == 'yes' ){
        $user->delete();
      } else {
        Stalexo_Messenger::addMessage('<b>Пользователь <i>'.$user->email.'</i> не может быть удален</b>', 'info');
      }          
    } else {
      Stalexo_Messenger::addMessage('<b>Пользователь не найден</b>', 'info');
    }
    App::redirect('/backend/users');
  }
  
  public function addAction(){
    if ( ! App::user()->hasPermission('users_add') ){
      Stalexo_Messenger::addMessage('<b>Недостаточно полномочий</b>', 'info');
      App::redirect('/backend/users');
    }
    
    if ( App::getRequest()->isPost() ){
      Stalexo_Messenger::addMessage('Hello', 'info');
      $id = App::getRequest()->post('id');
      if ( $id ){
        //update user
        $user = new User_Stalexo($id);
        if ( $user->isLoaded() ){
          Stalexo_Messenger::addMessage('<b>Изменения сохранены</b>', 'success');
          
          $role     = App::getRequest()->post('role');
          $password = App::getRequest()->post('password');
          
          if ( $role != $user->id_role and App::user()->hasPermission('users_change_role') ){
            $user->id_role = $role;
            $user->save();
          }

          if ( $password  ){
            try {
              Auth_Registration::changePassword($user, $password);
            } catch (Exception $e){
              Stalexo_Messenger::addMessage('<b>Неверный формат пароля</b>', 'error');          
            }          
          }         
          
        } else {
          Stalexo_Messenger::addMessage('<b>Пользователь не найден</b>', 'info');
        }
      } else {
        //new user
        try{
          $data = App::getRequest()->post();
          if ( ! App::user()->hasPermission('users_change_role') ){
            $data['role'] = 1;
          }
          Auth_Registration::addUser($data);
          Stalexo_Messenger::addMessage('<b>Пользователь <i>'. App::getRequest()->post('email') .'</i> успешно создан</b>', 'success');
        } catch (Registration_Exception_UserAlreadyExists $e){
          Stalexo_Messenger::addMessage('<b>Такой пользователь существует</b>', 'info');
        } catch (Registration_Exception_InvalidInput $e){
          Stalexo_Messenger::addMessage('<b>Неверный формат данных</b>', 'error');
        }
      }
      
      App::redirect('/backend/users');
    }
    
    $u = new User_Stalexo();
    $id = App::getRequest()->get(3);
    if ( is_numeric($id) and $id > 0 ){
      $u = new User_Stalexo($id);
    }
    
    $q = User_Role_Model::getFetchQuery();
    if ( ! App::user()->hasPermission('users_add_admin') ){
      $q->where('user_roles.alias != "admin"');
    }
    
    $roles = User_Role_Model::fetchByQuery($q);
    
    $this->smarty->assign(array(
      'u'     => $u,
      'roles' => $roles
    ));
    
    $this->render($this->smarty->fetch('backend/users/add.tpl'));
  }
  
}