<?php
class Backend_DevicesController extends Controller_Action{

  public function indexAction(){
    $devices = User_Staff_Device::fetchByQuery(
      User_Staff_Device::getFetchQuery()
      ->join('staff s', 's.id = staff_device.id_staff')
      ->select('s.name')
    );
    $this->smarty->assign('devices', $devices);
    $this->render($this->smarty->fetch('backend/devices/list.tpl'));
  }

  public function addAction(){
    if ( ! App::user()->hasPermission('users_crud_devices') ){
      Stalexo_Messenger::addMessage('<b>Недостаточно полномочий</b>', 'info');
      App::redirect('/backend/devices');
    }
    if ( App::getRequest()->isPost() ){
      $id       = App::getRequest()->post('id');
      $staff    = App::getRequest()->post('staff');
      $device   = App::getRequest()->post('device');
      $password = App::getRequest()->post('password');

      if ( $id ){
        $d = new User_Staff_Device($id);
        if ( $d->isLoaded() ){
          $d->device = $device;
          if ( $password ){
            if (Valid::password($password)){
              $d->password = Api_Auth::makePasswordHash($password, $device);
            } else {
              Stalexo_Messenger::addMessage('<b>Неверный формат пароля</b>', 'error');
            }
          }

          if ( $d->id_staff != $staff ){
            $dNew = clone $d;
            $dNew->resetPk();
            $dNew->id_staff = $staff;
            $d->delete();
            $d = $dNew;
          }

          $result = $d->save();
          if ( $d->isLoaded() ){
            Stalexo_Messenger::addMessage('<b>Данные сохранены</b>', 'success');
          } else {
            Stalexo_Messenger::addMessage('<b>Ошибка при редактировании</b>', 'error');
          }
        } else {
          Stalexo_Messenger::addMessage('<b>Ошибка. Запись не найдена</b>', 'error');
        }
      } else {
        try {
          if ( ! (Valid::id1c($staff) and Valid::password($password) and $device) ){
            Stalexo_Messenger::addMessage('<b>Неверный формат данных</b>', 'error');
            App::redirect('/backend/devices');
          }
          $user = Api_Auth::addApiUser($password, $device, $staff);

          if ( $user->isLoaded() ){
            $staff = new User_Staff_Model($user->id_staff);
            Stalexo_Messenger::addMessage('<b>Для агента <i>'.$staff->name.'</i> назначен планшет <i>'.$user->device.'</i> </b>', 'success');
          } else {
            Stalexo_Messenger::addMessage('<b>Ошибка</b>', 'error');
          }
        } catch ( StaffUserNotExists $e ) {
          Stalexo_Messenger::addMessage('<b>Агент не указан</b>', 'info');
        } catch( ApiUserAlreadyExists $e ){
          Stalexo_Messenger::addMessage('<b>У агента уже есть планшет</b>', 'info');
        }
      }

      App::redirect('/backend/devices');
    }

    $staff = User_Staff_Model::fetchByQuery(
      User_Staff_Model::getFetchQuery()
      ->where('staff.id_parent > 0')->orderBy('staff.name')
      ->select('sd.id_staff')
      ->select('sd.device')
      ->leftJoin('staff_device sd', 'sd.id_staff = staff.id')
    );

    $d = new User_Staff_Device();
    if ( App::getRequest()->get(3) and is_numeric(App::getRequest()->get(3)) ){
      $d = new User_Staff_Device(App::getRequest()->get(3));
      if ( ! $d->isLoaded() ){
        Stalexo_Messenger::addMessage('<b>Ошибка</b>', 'error');
        App::redirect('/backend/devices');
      }
    }

    $this->smarty->assign(array(
      'staff'  => $staff,
      'd'      => $d
    ));

    $this->render($this->smarty->fetch('backend/devices/add.tpl'));
  }

  public function deleteAction(){
    if ( ! App::user()->hasPermission('users_crud_devices') ){
      Stalexo_Messenger::addMessage('<b>Недостаточно полномочий</b>', 'info');
      App::redirect('/backend/devices');
    }
    $id = App::getRequest()->get(3);
    $d = new User_Staff_Device($id);
    if ( $d->isLoaded() ){
      Stalexo_Messenger::addMessage('<b>Планшет <i>'.$d->device.'</i> удален</b>', 'success');
      $d->delete();
    } else {
      Stalexo_Messenger::addMessage('<b>Ошибка</b>', 'error');
    }
    App::redirect('/backend/devices');
  }

  public function errorAction(){
    App::render404();
  }

}