<?php
class IndexController extends Controller_Action{

  public function indexAction(){
    if ( App::user()->loggedIn() ){
      App::redirect('/direction');
    }

    App::view()->css()->add('/assets/css/bootstrap.min.css');
    App::view()->css()->add('/assets/css/bootstrap-responsive.min.css');
    App::view()->js()->add('/assets/js/jquery.js');

    $this->render($this->smarty->fetch('auth/login/form.tpl'));
  }

  public function errorAction(){
    App::render404( 'Invalid action ' . App::getRequest()->getAction()  );
  }

  public function imagesAction(){
    exit;
    $sku = Stalexo_Sku_Model::fetchAll();

    foreach ($sku as $s){
      $big    = $s->id.'_600.png';
      $small  = $s->id.'_80.png';
      if ( file_exists('images/sku/'. $big) ){
        $s->img_big   = 'images/sku/'. $big;
        $s->img_small = 'images/sku/'. $small;
        $s->save();
      }

    }
    exit;
  }

  public function phpinfoAction(){
    phpinfo();
    exit;
  }
  /*
  public function geoAction(){
    require_once Settings::path()->vendor . '/google-map/GoogleMap.php';
    $g = new GoogleMapAPI();

    $dp = Stalexo_Dp_Model::fetchByQuery(
      Stalexo_Dp_Model::getFetchQuery()->where('lat IS NULL OR lng = 0')->limit(30)
    );

    foreach (  $dp as $d ){
      $result = $g->geoGetCoords( 'Украина, Одесса, '. $d->address);
      if ( $result ){
        echo '<br>'.$d->address.'<br>';
        $d->lat = str_replace(',', '.', $result['lat']);
        $d->lng = str_replace(',', '.', $result['lon']);
        $d->save();
      }
    }

    exit;
  }
  */
  /*
  public function routesAction(){
    $timelines = Stalexo_Timeline_Model::fetchByQuery(
      Stalexo_Timeline_Model::getFetchQuery()
      ->where('id_staff = 33')
      ->where('id >= '. 89 )
    );

    $route = new Stalexo_Route_Model(16);
    if ( $route->isLoaded() ){
      foreach ( $timelines as  $k => $t ){
        $randSecons = mt_rand(60, 1500);


        App::db()->query("
        INSERT INTO `routes_has_dp`
        SET id_route = '". $route->id ."',
        id_dp = '". $t->id_dp ."',
        time_work = 600,
        time_plan = '". ($t->date_begin - 1369181100 + $randSecons) ."',
        prior = '". ($k+1) ."'
        ");

      }
    }


    Debug::var_dump( $timelines );
  }
  */
}