<?php
class ApiController extends Controller_Action{

  protected $output = array('status' => 0);
  protected $request;

  protected function init(){
    $this->request = new Api_Request();
    if ( ! $this->request->user->isLoaded() and ! in_array($this->request->action, array('login', 'add-location')) ){
      $this->output['error'] = 'Has no permission';
      $this->sendResponse();
    }
    if ( ! $this->request->user->is_production and ! in_array($this->request->action, array('login', 'sync', 'add-location')) ){
      App::instance()->switchDevDb();
    }
  }

  public function indexAction(){
    if ( $this->request->isValid() ){
      $actionName = Controller_Front::parseAction($this->request->action) . 'Action';
      if ( method_exists($this, $actionName) ){
        $this->{$actionName}();
      } else {
        $this->errorAction();
      }
    }
    $this->logResponse();
    $this->sendResponse();
  }

  protected function sendResponse(){
    echo json_encode($this->output); exit();
  }

  protected function logResponse(){
    ob_start();
    var_dump($this->output);
    $output = ob_get_contents();;
    ob_clean();
    @file_put_contents('data/logs/output.txt', $output);
  }

  public function errorAction(){
    $this->output['status'] = 0;
    $this->output['error'] = 'Invalid Action';
  }

  public function loginAction(){
    $device = App::getRequest()->post('device');
    $password = App::getRequest()->post('password');
    $apiUser = Api_Auth::login($device, $password);

    if ( $apiUser->isLoaded() ){
      $this->output['status'] = 1;
      $this->output['token'] = $apiUser->token;
      $this->output['id_user'] = $apiUser->id_staff;
      $this->output['app_version'] = (App::getRequest()->post('app_version')<28?1:0); 
    } else { 
      $this->output['error'] = 'Invalid credentials';
    }
    $this->output['serverdate'] =  date('Y, m, d, H, i, s');

  }

  public function syncAction(){
    $device  = App::getRequest()->post('device');
    $token   = App::getRequest()->post('token');
    $target  = App::getRequest()->post('target');
    $date    = App::getRequest()->post('date');
    $limit    = App::getRequest()->post('limit');
    $limit_iter   = App::getRequest()->post('limit_iter');

    if ( ! ($date and preg_match('@^[0-9]{9,11}$@', $date)) ) {
      $date = null;
    }

   if($this->request->user->id_staff==82){$test=1;$this->request->user->id_staff=32;}
    
    switch($target){
      case "get-sequence":
        $this->output['status'] = 1;
              
        $this->output['sequence'] = array(
          'clients',
          'price_types',
          'distribution_points',
          'distribution_point_contracts',
          'storage',
          'sku_groups',
          'sku',
          'sku_availability',
          'sku_prices',
          'callback_reasones',
          'ta_routes',
          'ta_routes_has_dp',
          'shipment',
          'sales_progress',
          'sku_sales',
          'staff_settings',
          'staff_settings_opt',
          'amount',
          'shipment_has_sku'
        );
      /* if($test==1){
$this->output['sequence'] = array(
     'amount',
    'clients',
          'price_types',
          'distribution_points',
          'distribution_point_contracts',
          'storage',
          'sku_groups',
          'sku',
          'sku_availability',
          'sku_prices',
          'callback_reasones',
          'ta_routes',
          'ta_routes_has_dp',
          'shipment',
          'shipment_has_sku',
          'sales_progress',
          'sku_sales',
          'staff_settings',
          'staff_settings_opt'
        );
        $this->request->user->id_staff=73;
    }
       */
        break;
        
        case "amount":
        $items = array();

        $sql  = "SELECT a.* FROM amount a inner join distribution_point_contracts dpc on dpc.id_client=a.id_client and id_manager=".$this->request->user->id_staff." limit ".$limit.", ".$limit_iter;
        $r = App::db()->query($sql);
        if( $r ){
          while( $row = $r->fetch_assoc() ) {
            $items[] = $row;
          }
        }

        $this->output['status'] = 1;
        $this->output['rows'] = $items;
        //$this->output['rows'] = str_replace("},{","},\r\n{", $this->output['rows']);
        break;
        
        case "staff_settings":
        $items = array();

        $sql  = "SELECT * FROM staff_settings";
        $r = App::db()->query($sql);
        if( $r ){
          while( $row = $r->fetch_assoc() ) {
            $items[] = $row;
          }
        }

        $this->output['status'] = 1;
        $this->output['rows'] = $items;
        break;
        
      case "staff_settings_opt":
        $items = array();

        $sql  = "SELECT id,id_set,val_int,val_str,val_text,val_bool FROM staff_settings_opt where id_staff = '". $this->request->user->id_staff ."'";
        $r = App::db()->query($sql);
        if( $r ){
          while( $row = $r->fetch_assoc() ) {
            $items[] = $row;
          }
        }

        $this->output['status'] = 1;
        $this->output['rows'] = $items;
        break;
        
      case "clients":
        $collection = new Stalexo_Client_Collection();
        $collection->loadByManager($this->request->user, $date,$limit,$limit_iter);
        $clients = array();
        foreach ( $collection as $c ) {
          $clients[] = array(
            'id' => $c->id,
            'title' => $c->title,
            'amount' => $c->amount
          );
        }
        $this->output['status'] = 1;
        $this->output['rows'] = $clients;
        break;
      case "price_types":
        $sql = "SELECT id, title FROM `price_types`";
        if ( $date ){ $sql .= " WHERE date_modify > FROM_UNIXTIME('". $date ."')"; }
        
        $sql.=" limit ".$limit.", ".$limit_iter;
        $r = App::db()->query($sql);
        $pt = array();
        if( $r ){
          while( $row = $r->fetch_assoc() ){
            $pt[] = $row;
          }
        }
        $this->output['status'] = 1;
        $this->output['rows'] = $pt;
        break;
      case "distribution_points":
        $items = array();

        $sql  = "SELECT dp.lat,dp.lng,dp.id, dp.id_client, REPLACE(dp.address,'&quote;', '\"') as address, dp.type, dp.chanel, dp.category, dp.date_modify FROM `distribution_points` dp
        JOIN `distribution_point_contracts` dps ON dps.id_distribution_point = dp.id
        AND dps.id_manager = '". $this->request->user->id_staff ."'";
        if ( $date ){ $sql .= " AND dp.date_modify > FROM_UNIXTIME('". $date ."')"; }
        $sql.= " GROUP BY dp.id";
        $sql.=" limit ".$limit.", ".$limit_iter;
        $r = App::db()->query($sql);
        if( $r ){
          while( $row = $r->fetch_assoc() ) {
            $items[] = $row;
          }
        }

        $this->output['status'] = 1;
        $this->output['rows'] = $items;
        break;
      case "distribution_point_contracts":
        $dp = array();
        $sql = "SELECT id, id_client, id_distribution_point, id_price_type, title, order_type, amount,debt_period FROM `distribution_point_contracts` WHERE id_manager = '". $this->request->user->id_staff ."'";
        if ( $date ){ $sql .= " AND date_modify > FROM_UNIXTIME('". $date ."')"; }
        $sql.=" limit ".$limit.", ".$limit_iter;
        $r = App::db()->query($sql);    
        if( $r ){
          while( $row = $r->fetch_assoc() ) {
            $dp[] = $row;
          }
        }
        $this->output['status'] = 1;
        $this->output['rows'] = $dp;
        break;
      case "storage":
        $storages = array();
        $r = App::db()->query("SELECT * FROM `storage`"." limit ".$limit.", ".$limit_iter);
        if( $r ){
          while( $row = $r->fetch_assoc() ){
            $storages[] = $row;
          }
        }
        $this->output['status'] = 1;
        $this->output['rows'] = $storages;
        break;
      case "sku_groups":
        $groups = array();
        $sql = "SELECT id, id_parent, title FROM `sku_groups`";
        if ( $date ){ $sql .= " WHERE date_modify > FROM_UNIXTIME('". $date ."')"; }
        $sql.=" limit ".$limit.", ".$limit_iter;
        $r = App::db()->query($sql);
        if( $r ){
          while( $row = $r->fetch_assoc() ){
            $groups[] = $row;
          }
        }
        $this->output['status'] = 1;
        $this->output['rows'] = $groups;
        break;
      case "sku":
        $sku = array();

        $sql = "SELECT id, REPLACE(title, '&quote;', '\"') as title, top, day_reserve, units, id_group, img_big, img_small FROM `sku`";

        if ( $date ){ $sql .= " WHERE date_modify > FROM_UNIXTIME('". $date ."')"; }
$sql.=" limit ".$limit.", ".$limit_iter;
        $r = App::db()->query($sql);
        if( $r ){
          while( $row = $r->fetch_assoc() ){
            $sku[] = $row;
          }
        }

        $this->output['status'] = 1;
        $this->output['rows'] = $sku;
        break;
      case "sku_availability":
        $sa = array();
        $sql = "SELECT id_sku, id_storage, availability, availability_free FROM `sku_availability`";
        if ( $date ){ $sql .= " WHERE date_modify > FROM_UNIXTIME('". $date ."')"; }
        $sql.=" limit ".$limit.", ".$limit_iter;
        $r = App::db()->query($sql);
        if( $r ){
          while( $row = $r->fetch_assoc() ){
            $sa[] = $row;
          }
        }
        $this->output['status'] = 1;
        $this->output['rows'] = $sa;
        break;
      case "sku_prices":
        $sp = array();
        $sql = "SELECT  sp.id_sku, sp.id_price_type, sp.date, sp.status, (select price from sku_prices where id_sku=sp.id_sku and id_price_type=pt.id and date<=CURRENT_TIMESTAMP order by date desc limit 1) as price FROM `sku_prices` sp
        JOIN `price_types` pt ON sp.id_price_type = pt.id
        JOIN `distribution_point_contracts` dps ON dps.id_price_type = pt.id
        WHERE dps.id_manager = '". $this->request->user->id_staff ."' GROUP BY id_sku,id_price_type";
        
        
       // if ( $date ){ $sql .= " AND sp.date_modify > FROM_UNIXTIME('". $date ."')"; }
        $sql.=" limit ".$limit.", ".$limit_iter;
        $r = App::db()->query($sql);
        if( $r ){
          while( $row = $r->fetch_assoc() ){
            $sp[] = $row;
          }
        }
        $this->output['status'] = 1;
        $this->output['rows'] = $sp;
        break;
      case "callback_reasones":
        $sql = "SELECT * FROM `callback_reasones`";
        if ( $date ){ $sql .= " WHERE date_modify > FROM_UNIXTIME('". $date ."')"; }
        $sql.=" limit ".$limit.", ".$limit_iter;
        $r = App::db()->query($sql);
        $items = array();
        if( $r ){
          while( $row = $r->fetch_assoc() ){
            $items[] = $row;
          }
        }
        $this->output['status'] = 1;
        $this->output['rows'] = $items;
        break;
      case "ta_routes":
        $items = array();
        $result = App::db()->query("SELECT * FROM routes WHERE id_staff = '". $this->request->user->id_staff ."' limit ".$limit.", ".$limit_iter);
        if ( $result ){
          while( $row = $result->fetch_assoc() ){
            $items[] = $row;
          }
        }
        $this->output['status'] = 1;
        $this->output['rows'] = $items;
        break;
      case "ta_routes_has_dp":
        $items = array();
        $result = App::db()->query(
          "SELECT rhd.* FROM routes_has_dp rhd JOIN routes r ON rhd.id_route = r.id AND r.id_staff = '". $this->request->user->id_staff ."' limit ".$limit.", ".$limit_iter
        );
        if ( $result ){
          while( $row = $result->fetch_assoc() ){
            $items[] = $row;
          }
        }
        $this->output['status'] = 1;
        $this->output['rows'] = $items;
        break;
      case "shipment":
        $items = array();
        $result = App::db()->query("SELECT s.id,s.id_distribution_point_contract,s.id_order,s.date,s.amount,s.status FROM `shipment` s JOIN `distribution_point_contracts` dps ON s.id_distribution_point_contract = dps.id AND dps.id_manager = '".$this->request->user->id_staff."' WHERE  s.date>=".strtotime("-6 month")." GROUP BY s.id limit ".$limit.", ".$limit_iter);
        if ( $result ){
          while( $row = $result->fetch_assoc() ){
            $items[] = $row;
          }
        }
        $this->output['status'] = 1;
        $this->output['rows'] = $items;
        break;
      case "shipment_has_sku":
        $items = array();
        $result = App::db()->query("SELECT shs.* FROM `shipment_has_sku` shs
        JOIN `shipment` s ON shs.id_shipment = s.id ".($date? " and s.date>='". $date ."'":"")." and s.date>=".strtotime("-1 month")."
        JOIN `distribution_point_contracts` dps ON s.id_distribution_point_contract = dps.id AND dps.id_manager = '".$this->request->user->id_staff."'  limit ".$limit.", ".$limit_iter);
        if ( $result ){
          while( $row = $result->fetch_assoc() ){
            $items[] = $row;
          }
        }
        $this->output['status'] = 1;
        $this->output['rows'] = $items;
        break;
        
      case "sales_progress":
        $date = time();
        $agent = $this->request->user;
        $clients = new Stalexo_Client_Collection();
        $clients->queryJoinManager($agent);
        $clients->queryJoinSalesPlan($date);
        $clients->query()->groupBy('clients.id');
        $clients->loadAll();
        $clients->eachLoadPlanDetail($date, $agent);
        $rows = array();

        foreach( $clients as $c ){
          $rows[] = array(
            'sales_plan'        => $c->sales_plan,
            'orders_total'      => $c->orders_total,
            'callbacks_total'   => $c->callbacks_total,
            'id_client'         => $c->id,
            'client_title'       => stripslashes($c->title)
          );
        }

        $this->output['status'] = 1;
        $this->output['rows'] = $rows;
        break;
      case "sku_sales":
        $items = array();
         
        $sql = "SELECT ss.id_sku ,ss.id_distribution_point_contract ,ss.date_from ,ss.date_to ,ss.quantity ,ss.price FROM sku_sales ss JOIN distribution_point_contracts dpc ON dpc.id = ss.id_distribution_point_contract AND dpc.id_manager = '".$this->request->user->id_staff;
         if ( $date ){ $sql .= " WHERE date_modify > FROM_UNIXTIME('". $date ."') "; }
         $sql.= "' limit ".$limit.", ".$limit_iter;
         $result = App::db()->query($sql);
        if ( $result ){
          while( $row = $result->fetch_assoc() ){
            $items[] = $row;
          }
        }
        $this->output['status'] = 1;
        $this->output['rows'] = $items;
        break;
      default:
        $this->output['status'] = 0;
        break;
    }

  }

  public function addOrderAction(){
    //$data = stripslashes(file_get_contents('order.txt'));
    $this->output['order'] = array();
    $data = stripslashes($this->request->getValue('order'));
    $data = str_replace(array('"[', ']"'), array('[', ']'), $data);
    $data = json_decode($data, true);

    if ( $data ){
      foreach( $data as $orderData ){
        if ( ! (isset($orderData['items']) and count($orderData['items'])) ){
          /*
          $this->output['status'] = 0;
          $this->output['error'] = "Invalid request";
          */
          continue;
        }
        $order = Stalexo_Order_Model::addOrder($orderData, $this->request->user);
        if ( $order->isLoaded() ) {
          $this->output['order'][] = array( "id" => $order->id_external, "id_api" => $order->id );
          $order->calculateTotal();
          $order->save();
        }
      }
    }

    $this->output['status'] = 1;
  }

  public function addCallbackAction(){
    $this->output['callback'] = array();
    $data = stripslashes($this->request->getValue('callback'));
    $data = str_replace(array('"[', ']"'), array('[', ']'), $data);
    $data = json_decode($data, true);

    if ( $data ){
      foreach( $data as $callbackData ){

        $callback = new Stalexo_Callback_Model();

        $callback->date = $callbackData['date'];
        $callback->id_external = $callbackData['id'];
        $callback->id_distribution_point_contract = $callbackData['id_distribution_point_contract'];
        $callback->id_manager = $this->request->user->id_staff;
        $callback->st_date = $callbackData['date'];

        $callback->save();

        if ( $callback->isLoaded() ){
          foreach( $callbackData['items'] as $callbackSku ){
            $reason = isset($callbackSku['id_reason']) ? $callbackSku['id_reason'] : 0;
            $callback->joinSku($callbackSku['id_sku'], $callbackSku['quantity'], $reason);
          }
          $callback->calculateTotal();
          $callback->save();
          $this->output['callback'][] = array( "id" => $callback->id_external, "id_api" => $callback->id );
        }
      }
    }

    $this->output['status'] = 1;
  }

  public function addRemainsAction(){
    //$data = stripslashes(file_get_contents('order.txt'));
    $this->output['remains'] = array();
    $data = stripslashes($this->request->getValue('remains'));
    $data = str_replace(array('"[', ']"'), array('[', ']'), $data);
    $data = json_decode($data, true);

    if ( $data ){
      foreach( $data as $remainData ){
        $remain = new Stalexo_Remain_Model();

        $remain->date = $remainData['date'];
        $remain->id_external = $remainData['id'];
        $remain->id_distribution_point_contract = $remainData['id_distribution_point_contract'];
        $remain->id_manager = $this->request->user->id_staff;
        $remain->st_date = $remainData['date'];
        $remain->save();

        if ( $remain->isLoaded() ){
          foreach( $remainData['items'] as $remainSku ){
            $remain->joinSku($remainSku['id_sku'], $remainSku['quantity']);
          }
          $this->output['remains'][] = array( "id" => $remain->id_external, "id_api" => $remain->id );
        }
      }
    }

    $this->output['status'] = 1;
  }

  public function addEncashAction(){
    $this->output['encashment'] = array();

    $data = stripslashes($this->request->getValue('encash'));
    $data = str_replace(array('"[', ']"'), array('[', ']'), $data);
    $data = json_decode($data, true);

    foreach( $data as $encashmentData ){
      $encashment = new Stalexo_Encashment_Model();
      $encashment->date = $encashmentData['date'];
      $encashment->id_manager = $this->request->user->id_staff;
      $encashment->id_distribution_point_contract = $encashmentData['id_distribution_point_contract'];
      $encashment->amount = $encashmentData['amount'];
      $encashment->id_external = $encashmentData['id'];
      $encashment->nom_number = $encashmentData['nom_number'];
      $encashment->nom_date = $encashmentData['nom_date'];
      $encashment->st_date = $encashmentData['date'];
      $encashment->save();
      if ( $encashment->isLoaded() ){
        $this->output['encash'][] = array('id' => $encashment->id_external, 'id_api' => $encashment->id);
      }
    }

    $this->output['status'] = 1;
  }

  public function addLocationAction(){
    $this->output['location'] = array();

    $device = $this->request->getValue('device');
    $apiUser = new User_Api($device);

    if ( ! $apiUser->isLoaded() ){
      $this->output['status'] = 1;
      $this->output['error'] = 'Invalid device';
      $this->sendResponse();
    }

    $data = stripslashes($this->request->getValue('locations'));
    $data = str_replace(array('"[', ']"'), array('[', ']'), $data);
    $data = json_decode($data, true);

    foreach( $data as $locData ){
      $location = new Stalexo_Location_Model();
      $location->date = $locData['date'];
      $location->lat  = $locData['latitude'];
      $location->lng  = $locData['longitude'];
      $location->alt  = $locData['altitude'];
      $location->batteryLevel =  $locData['batteryLevel'];
      $location->id_staff = $apiUser->id_staff;
      $location->save();
      if( $location->isLoaded() ){
        $this->output['location'][] = array('id' => $locData['id']);
      }
    }

    $this->output['status'] = 1;
  }

  public function addLocationDpAction(){
    $this->output['location'] = array();
    $data = stripslashes($this->request->getValue('location-dp'));
    $data = str_replace(array('"[', ']"'), array('[', ']'), $data);
    $data = json_decode($data, true);

    foreach( $data as $locData ){
      $dp = new Stalexo_Dp_Model($locData['id']);

      if ( $dp->isLoaded() ){
        $dp->lat =  $locData['latitude'];
        $dp->lng =  $locData['longitude'];
        $dp->save();
      }

      if( $dp->isLoaded() ){
        $this->output['location'][] = array('id' => $locData['id']);
      }
    }

    $this->output['status'] = 1;
  }

  public function addPhotoAction(){
    require_once Settings::path()->vendor . 'wideimage/WideImage.php';
    $date    = App::getRequest()->post('date');
    $distributionPoint = App::getRequest()->post('id_distribution_point');

    try {
      if ( ! ($date and $distributionPoint) ){
        throw new Exception('Invalid data');
      }

      $path = 'images/merchandising/' . $this->request->user->id_staff . '_' . str_replace('.','',microtime(true)) .'.png';
      $image = WideImage::load('uploadedfile');
      $image->resize(1000, 1000, 'inside', 'down')->saveToFile($path);

      $m = new Stalexo_Merchandising_Model();
      $m->id_manager = $this->request->user->id_staff;
      $m->id_distribution_point = $distributionPoint;
      $m->date = $date;
      $m->image = $path;
      $m->save();

      $this->output['status'] = 1;
    } catch( Exception $e ){
      $this->output['error'] = 'Invalid data';
      $this->output['status'] = 0;
    }

  }

  public function addTimelineAction(){
    $this->output['timeline'] = array();
    $data = stripslashes($this->request->getValue('timeline'));
    $data = str_replace(array('"[', ']"'), array('[', ']'), $data);
    $data = json_decode($data, true);

    foreach( $data as $itemData ){
      $tl = new Stalexo_Timeline_Model();

      $tl->id_staff   = $this->request->user->id_staff;
      $tl->id_route   = $itemData['id_route'];
      $tl->date_begin = $itemData['time_start'];
      $tl->date_end   = $itemData['time_finish'];
      $tl->id_dp      = $itemData['id_dp'];
      $tl->save();

      if( $tl->isLoaded() ){
        $this->output['timeline'][] = array('id' => $itemData['id']);
      }
    }

    $this->output['status'] = 1;
  }

  public function logAction(){
    $device = stripslashes($this->request->getValue('device'));
    $target = stripslashes($this->request->getValue('target'));

    $log = new Stalexo_Log_Model();
    $log->date     = time();
    $log->device   = $device;
    $log->id_staff = $this->request->user->id_staff;
    $log->content  = $target;
    $log->save();

    $this->output['status'] = 1;
  }
  
}