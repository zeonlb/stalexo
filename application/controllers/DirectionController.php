<?php
class DirectionController extends Controller_Module{

  public function init(){
    App::view()->setWrapper(new Layout_Direction());
    App::view()->css()->add('/assets/css/bootstrap.min.css');
    App::view()->css()->add('/assets/css/bootstrap-responsive.min.css');
    //App::view()->js()->add('/assets/js/bootstrap.min.js');
    App::view()->js()->add('/assets/js/jquery.js');
    App::view()->js()->add('/assets/js/bootstrap-datepicker.js');
    App::view()->css()->add('/assets/css/datepicker.css');

    if ( ! App::user()->loggedIn() ){
      App::redirect();
    }

    Stalexo_Direction::setAgent(
      isset($_SESSION['direction']['agent']) ? $_SESSION['direction']['agent'] : null
    );
	
	Stalexo_Direction::setDirector(
      isset($_SESSION['direction']['director']) ? $_SESSION['direction']['director'] : null
    );
	
	Stalexo_Direction::setClient(
      isset($_SESSION['direction']['client']) ? $_SESSION['direction']['client'] : null
    );
	
	Stalexo_Direction::setAddress(
      isset($_SESSION['direction']['address']) ? $_SESSION['direction']['address'] : null
    );

    Stalexo_Direction::setDate(
      isset($_SESSION['direction']['date']) ? $_SESSION['direction']['date'] : null
    );

    Stalexo_Direction::setDate2(
      isset($_SESSION['direction']['date2']) ? $_SESSION['direction']['date2'] : null
    );
        
  }

  public function indexAction(){
    /*
    App::view()->js()->add('/assets/js/bootstrap-datepicker.js');
    App::view()->css()->add('/assets/css/datepicker.css');

    $agents = User_Staff::fetchByQuery(
      User_Staff::getFetchQuery()
      ->join('staff_device sd', 'sd.id_staff = staff.id')
      ->where('staff.id_parent > 0')
      ->orderBy('staff.name')
    );

    $this->smarty->assign(array(
      'agents'     => $agents,
      'agent'      => Stalexo_Direction::getAgent(),
      'filterDate' => Stalexo_Direction::getDate()
    ));
    */
    $this->render($this->smarty->fetch('direction/index.tpl'));
  }

  public function saveSettingsAction(){
    $returnUrl = '/direction';
    if ( App::getRequest()->isPost() ){
      $agent = App::getRequest()->post('agent');
	  $director = App::getRequest()->post('director');
	  $client = App::getRequest()->post('client');
	  $address = App::getRequest()->post('address');
      $date  = App::getRequest()->post('filter_date');
      $date2  = App::getRequest()->post('filter_date2');
      $h1 = App::getRequest()->post('hours1');
      $m1 = App::getRequest()->post('minutes1');
      $h2 = App::getRequest()->post('hours2');
      $m2 = App::getRequest()->post('minutes2');

      if ( App::getRequest()->post('reset_filter') ){
        $agent = null;
		$director = null;
		$client = null;
		$address = null;
        $date =  null;
        $date2 =  null;
      }

      $_SESSION['direction']['agent'] = $agent;
	  $_SESSION['direction']['director'] = $director;
	  $_SESSION['direction']['client'] = $client;
	  $_SESSION['direction']['address'] = $address;
      $_SESSION['direction']['date']  = $date."--".($h1<10?'0'.$h1:$h1)."-".($m1<10?'0'.$m1:$m1);
      $_SESSION['direction']['date2']  = $date2."--".($h2<10?'0'.$h2:$h2)."-".($m2<10?'0'.$m2:$m2);
      $returnUrl = $_SERVER['HTTP_REFERER'];
    }
    App::redirect($returnUrl);
  }

}