<?php
class Stalexo_Timeline_Model extends Dao{

  static protected $pk    = 'id';
  static protected $table = 'routes_timeline';

  static public function getByAgentAndPeriod(User_Staff $agent, $dateBegin, $dateEnd){
    if ( ! ($dateBegin and $dateEnd) ){
      return array();
    }
    $timelinesArray = Stalexo_Timeline_Model::fetchByQuery(
      "SELECT rt.*, dp.address, c.title,
      (SELECT lat FROM location  WHERE id_staff = rt.id_staff AND `date` > rt.date_begin LIMIT 1) as lat_begin,
      (SELECT lng FROM location  WHERE id_staff = rt.id_staff AND `date` > rt.date_begin LIMIT 1) as lng_begin,
      (SELECT lat FROM location  WHERE id_staff = rt.id_staff AND `date` > rt.date_end LIMIT 1) as lat_end,
      (SELECT lng FROM location  WHERE id_staff = rt.id_staff AND `date` > rt.date_end LIMIT 1) as lng_end
      FROM `routes_timeline` rt
      JOIN distribution_points dp ON dp.id = rt.id_dp
      JOIN clients c ON c.id = dp.id_client
      WHERE rt.id_staff = '". $agent->id ."' AND rt.date_begin BETWEEN '". $dateBegin ."' AND '". $dateEnd ."'"
    );

    return $timelinesArray;
  }

  public function calculateEntryDistance(){
    $this->entry_distance = 9999;
    if ( !( $this->lng_begin and $this->lat_begin and $this->lat_end and $this->lng_end ) ){
      return;
    }
    $distance = Geo_Facade::getDistance($this->lat_begin, $this->lng_begin, $this->lat_end, $this->lng_end);
    if ( $distance < 0 ) {
      $distance = 9999;
    }
    $this->entry_distance = $distance * 1000;
  }

  public function loadActivity(){
    $this->children['total']['orders'] = 0;
    $this->children['total']['callbacks'] = 0;
    $this->children['total']['remains'] = 0;


    $this->children['orders']     = Stalexo_Order_Model::fetchByQuery(
      Stalexo_Order_Model::getFetchQuery()
      ->select('dp.address')
      ->join('distribution_point_contracts dps', 'dps.id = `order`.id_distribution_point_contract AND `order`.id_manager = "'. $this->id_staff .'"')
      ->join('distribution_points dp', 'dp.id = dps.id_distribution_point AND dp.id = "'. $this->id_dp .'"')
      ->where( ' `order`.st_date BETWEEN '.$this->date_begin.' AND '. $this->date_end )
    );

    $this->children['remains']    = Stalexo_Remain_Model::fetchByQuery(
      Stalexo_Remain_Model::getFetchQuery()
      ->select('dp.address')
      ->join('distribution_point_contracts dps', 'dps.id = remains.id_distribution_point_contract AND dps.id_manager = "'. $this->id_staff .'"')
      ->join('distribution_points dp', 'dp.id = dps.id_distribution_point AND dp.id = "'. $this->id_dp .'"')
      ->where('`remains`.st_date BETWEEN '.$this->date_begin.' AND '. $this->date_end)
    );

    $this->children['callbacks']  = Stalexo_Callback_Model::fetchByQuery(
      Stalexo_Callback_Model::getFetchQuery()
      ->join('distribution_point_contracts dps', 'dps.id = `callback`.id_distribution_point_contract AND dps.id_manager = "'. $this->id_staff .'"')
      ->join('distribution_points dp', 'dp.id = dps.id_distribution_point AND dp.id = "'. $this->id_dp .'"')
      ->where('`callback`.st_date BETWEEN '.$this->date_begin.' AND '. $this->date_end)
    );

    foreach( $this->children['orders'] as $o ){
      $this->children['total']['orders'] += $o->total;
    }

    foreach( $this->children['callbacks'] as $c ){
      $this->children['total']['callbacks'] += $c->total;
    }

  }

}
