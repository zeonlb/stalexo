<?php
class Stalexo_Photo_Model extends Dao{

  static protected $table = "merchandising";
  static protected $pk = "id";

  public function loadOrderedSku(){
    if ( ! $this->isLoaded() ){ return; }
    $this->children['sku'] = array();

    $q = "SELECT * FROM(
    SELECT s.id, s.title, s.units, sp.price, ohs.quantity,
    (". $this->date_shipping ."- sp.date) as time_diff
    FROM `sku` s
    JOIN `sku_prices` sp ON s.id = sp.id_sku AND sp.date <= ". $this->date_shipping ."
    JOIN `price_types` pt ON pt.id = sp.id_price_type
    JOIN `distribution_point_contracts` dps ON dps.id_price_type = pt.id AND dps.id = ". $this->id_distribution_point_contract ."
    JOIN `order_has_sku` ohs ON ohs.id_sku = s.id AND ohs.id_order = ". $this->id ."
    ORDER BY s.id, time_diff
    ) as `sku` GROUP BY id ";

    $result = App::db()->query($q);
    if ( $result ){
      while( $row = $result->fetch_assoc() ){
        $this->children['sku'][] = new Stalexo_Sku_Model($row);
      }
    }
  }

}