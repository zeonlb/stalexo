<?php
class Stalexo_Encashment_Collection extends Collection{

  protected function initQuery(){
    $this->query = Stalexo_Encashment_Model::getFetchQuery();
  }

  protected function initPager(){
    $this->pager = new Pager_StalexoJumping();
  }

  public function loadAll(){
    $this->query()
    ->enableFoundRows()
    ->select('dp.address')
    ->select('staff.name as manager_name')
    ->join('distribution_point_contracts dps', 'dps.id = encashment.id_distribution_point_contract')
    ->join('distribution_points dp', 'dp.id = dps.id_distribution_point')
    ->join('staff staff', 'staff.id = encashment.id_manager')->orderBy('encashment.date DESC');
    $result = $this->executeQuery();
    if ( $result ){
      while( $row = $result->fetch_assoc() ){
        $this->add( new Stalexo_Encashment_Model($row) );
      }
    }
  }

  /**
  * @param mixed $id
  * @return Stalexo_Encashment_Model
  */
  public function getById($id){
    if ( is_numeric($id) ){
      $this->query()
      ->disableFoundRows()
      ->where('encashment.id = "'. $id .'"');
      $this->loadAll();
      if ( $this->count() ){
        return $this->current();
      }
    }
    return new Stalexo_Encashment_Model();
  }

  public function queryRestrictByManager( User_Staff $manager ){
    if ( $manager->isLoaded() ) {
      $this->query()->where('encashment.id_manager = "'.  $manager->id .'"');
    }
  }

  public function queryRestrictByDay( $timestamp ){
    if ( $timestamp and is_numeric($timestamp) and (time() > $timestamp) ){
      $this->query()->where('encashment.date BETWEEN "'. $timestamp .'" AND "'.($timestamp + 86400).'" ');
    }
  }

  public function queryJoinLocation(){
    $this->query()->select('l.lat, l.lng')
    ->join('location l', 'l.id_staff = `encashment`.id_manager AND `encashment`.date <= l.date')
    ->groupBy('`encashment`.id');
  }

}