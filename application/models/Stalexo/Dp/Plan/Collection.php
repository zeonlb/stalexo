<?php
class Stalexo_Dp_Plan_Collection extends Collection{

  protected function initPager(){
    $this->pager = new Pager_StalexoJumping();
  }

  protected function initQuery(){
    $this->query = Stalexo_Dp_Plan_Model::getFetchQuery()->enableFoundRows();
  }

  public function loadAll(){
    $this->attachFilter(new Filter_Page_Standart($this->pager()));

    $result = $this->executeQuery();
    if ( $result ){
      while( $row = $result->fetch_assoc() ){
        $this->add(new Stalexo_Dp_Plan_Model($row));
      }
    }
  }

  public function byDate($date){
    if ( $date ){
      $year  = date('Y', $date);
      $month = date('m', $date);;
    } else {
      $year  = date('Y');
      $month = date('m');
    }
    $this->query()->where('(distribution_plans.year = "'.$year.'" AND distribution_plans.month = "'.$month.'")');
  }

  public function queryJoinDetails(){
    $this->query()
    ->select('staff.name as manager_name')->join('staff staff', 'staff.id = distribution_plans.id_staff')
    ->select('dp.address')
    ->select('dp.id as id_dp')
    ->select('clients.title as client_name')
    ->join('distribution_point_contracts dpc', 'dpc.id = distribution_plans.id_dc')
    ->join('distribution_points dp', 'dp.id = dpc.id_distribution_point')
    ->join('clients clients', 'dp.id_client = clients.id');
  }

  public function byManager($manager){
    if ( $manager->isLoaded() ){
      $this->query()->where('distribution_plans.id_staff = "'. $manager->id .'"');
    }
  }

  public function eachLoadOrdersTotal(){
    foreach( $this->array as $plan ){
      $dateBegin = Date::makeTimestamp($plan->year, $plan->month, 1);
      $dateEnd   = Date::makeTimestamp($plan->year, $plan->month, date('t', $dateBegin)) + 86400;

      $orders = Stalexo_Order_Model::fetchByQuery(
        Stalexo_Order_Model::getFetchQuery()
        ->where('`order`.id_distribution_point_contract = "'. $plan->id_dc .'"')
        ->where('`order`.id_manager = "'. $plan->id_staff .'"')
        ->where('`order`.st_date BETWEEN '. $dateBegin .' AND '.$dateEnd)
      );
      $total = 0;
      foreach( $orders as $o ){
        $total += $o->total;
      }
      $plan->orders_total = $total;
    }
  }

  public function eachLoadCallbacksTotal(){
    foreach( $this->array as $plan ){
      $dateBegin = Date::makeTimestamp($plan->year, $plan->month, 1);
      $dateEnd   = Date::makeTimestamp($plan->year, $plan->month, date('t', $dateBegin)) + 86400;

      $callbacks = Stalexo_Callback_Model::fetchByQuery(
        Stalexo_Callback_Model::getFetchQuery()
        ->where('`callback`.id_distribution_point_contract = "'. $plan->id_dc .'"')
        ->where('`callback`.id_manager = "'. $plan->id_staff .'"')
        ->where('`callback`.date BETWEEN '. $dateBegin .' AND '.$dateEnd)
      );
      $total = 0;
      foreach( $callbacks as $c ){
        $total += $c->total;
      }
      $plan->callbacks_total = $total;
    }
  }

  public static function getOrdersTotal($date, $manager){
    if ( $date ){
      $year  = date('Y', $date);
      $month = date('m', $date);;
    } else {
      $year  = date('Y');
      $month = date('m');
    }

    $dateBegin = Date::makeTimestamp($year, $month, 1);
    $dateEnd   = Date::makeTimestamp($year, $month, date('t', $dateBegin)) + 86400;

    $q = Stalexo_Dp_Plan_Model::getFetchQuery()
    ->join('`order` o', 'o.id_distribution_point_contract = distribution_plans.id_dc AND o.st_date BETWEEN "'. $dateBegin .'" AND "'. $dateEnd .'"')
    ->where('(distribution_plans.`year` = "'. $year .'" AND distribution_plans.`month` = "'. $month .'")')
    ->clearSelect()->select('SUM(`o`.total) as total');
    if ( $manager and $manager->isLoaded() ){
      $q->where('distribution_plans.id_staff = "'. $manager->id .'"')->where('o.id_manager = "'. $manager->id .'"');
    }

    $total = 0;
    $result = App::db()->query($q);
    if ( $result and $result->num_rows ){
      $row = $result->fetch_assoc();
      $total = $row['total'];
    }
    return $total;
  }

  public static function getOrdersPlannedTotal($date, $manager = null){
    if ( $date ){
      $year  = date('Y', $date);
      $month = date('m', $date);;
    } else {
      $year  = date('Y');
      $month = date('m');
    }
    $q = Stalexo_Dp_Plan_Model::getFetchQuery()
    ->where('(distribution_plans.year = "'. $year .'" AND distribution_plans.month = "'. $month .'")')
    ->clearSelect()->select('SUM(distribution_plans.plan) as total');
    if ( $manager and $manager->isLoaded() ){
      $q->where('distribution_plans.id_staff = "'. $manager->id .'"');
    }
    $total = 0;
    $result = App::db()->query($q);
    if ( $result and $result->num_rows ){
      $row = $result->fetch_assoc();
      $total = $row['total'];
    }
    return $total;
  }

}