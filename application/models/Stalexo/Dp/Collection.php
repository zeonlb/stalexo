<?php
class Stalexo_Dp_Collection extends Collection{

  protected function initQuery(){
    $this->query = Stalexo_Dp_Model::getFetchQuery();
  }

  public function loadAll(){
    $result = $this->executeQuery();
    if ( $result ){
      while( $row = $result->fetch_assoc() ){
        $this->add( new Stalexo_Dp_Model($row) );
      }
    }
  }

  public function loadByClient( $client ){
    if (Valid::id1c($client)) {
      $this->query()->where('distribution_points.id_client = "'.  $client .'"');
      return $this->loadAll();
    }
  }

  public function queryJoinAgent(){
    $this->query->join('staff staff', 'staff.id = distribution_points.id_manager')
    ->select('staff.name as manager_name');
  }

}