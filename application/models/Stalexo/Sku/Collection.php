<?php 
class Stalexo_Sku_Collection extends Collection{
  
  protected function initQuery(){
    $this->query = Stalexo_Sku_Model::getFetchQuery()->enableFoundRows();
  }
  
  protected function initPager(){
    $this->pager = new Pager_StalexoJumping();
  }
  
  public function loadAll(){
    $this->attachFilter(new Filter_Page_Standart($this->pager()));
    
    $result = $this->executeQuery();
    if ( $result ){
      while( $row = $result->fetch_assoc() ){
        $this->add( new Stalexo_Sku_Model($row));
      }
    }
  }
  
  public function queryJoinGroup(){
    $this->query()
    ->select('sg.title as group_title')
    ->join('sku_groups sg', 'sku.id_group = sg.id')
    ->orderBy('sg.title, sku.title');
  }
  
} 