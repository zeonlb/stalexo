<?php
class Stalexo_Sku_Model extends Dao{

  static protected $pk    = 'id';
  static protected $table = 'sku';

  public function getImageBig(){
    if ( $this->img_big and file_exists($this->img_big) ){
      return $this->img_big;
    }
    return null;
  }

  public function getImageSmall(){
    if ( $this->img_small and file_exists($this->img_small) ){
      return $this->img_small;
    }
    return null;
  }

  public function dropImages(){
    $isModified = false;
    if ( $small = $this->getImageSmall() ){
      unlink($small);
      $isModified = true;
    }
    if ( $big = $this->getImageBig() ){
      unlink($big);
      $isModified = true;
    }
    $this->img_small = '';
    $this->img_big = '';
    if ( $isModified ){
      $this->save();
    }
    return $isModified;
  }

}