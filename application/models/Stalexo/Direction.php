<?php
class Stalexo_Direction {
  
  static $agent = null;
  static $director = null;
  static $client = null;
  static $address = null;
  static $date  = null;
  static $date2  = null;
  
  static public function setAgent($id){
    if ( Valid::id1c($id) ){
      static::$agent = new User_Staff($id);
    } else{
      static::$agent = new User_Staff();
    }
  }
  
  static public function getAgent(){
    return static::$agent;
  }
  
  static public function setDirector($id){
    if ( Valid::id1c($id) ){
      static::$director = new User_Staff($id);
    } else{
      static::$director = new User_Staff();
    }
  }
  
  static public function getDirector(){
    return static::$director;
  }
  
  static public function setClient($id){
    if ( (int)$id ){
      static::$client = $id;
    } else{
      static::$client = null;
    }
  }
  
  static public function getClient(){
    return static::$client;
  }
  
  static public function setAddress($id){
    if ( (int)$id ){
      static::$address = $id;
    } else{
      static::$address = null;
    }
  }
  
  static public function getAddress(){
    return static::$address;
  }
  
  static public function setDate($date){
    static::$date = $date;
  }
  
    static public function setDate2($date2){
    static::$date2 = $date2;
  }
  
  static public function getDate(){
    return static::$date;
  }
   static public function getDate2(){
    return static::$date2;
  } 
}