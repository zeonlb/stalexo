<?php
class Stalexo_Callback_Collection extends Collection{

  protected function initPager(){
    $this->pager = new Pager_StalexoJumping();
  }

  protected function initQuery(){
    $this->query = Stalexo_Callback_Model::getFetchQuery()->enableFoundRows();
  }

  public function loadAll(){
    $this->attachFilter(new Filter_Page_Standart($this->pager()));

    $result = $this->executeQuery();
    if ( $result ){
      while( $row = $result->fetch_assoc() ){
        $this->add(new Stalexo_Callback_Model($row));
      }
    }
  }

  public function queryAddDetails(){
    $this->query()
    ->select('dp.address')
    ->select('staff.name manager_name')
    ->join('staff staff', 'staff.id = callback.id_manager')
    ->join('distribution_point_contracts dps', 'dps.id = callback.id_distribution_point_contract')
    ->join('distribution_points dp', 'dp.id = dps.id_distribution_point')
    ->orderBy('`callback`.date DESC');;
  }

  public function getById($id){
    if ( is_numeric($id) ) {
      $this->query()
      ->disableFoundRows()
      ->where('callback.id = "'. $id .'"');
      $this->loadAll();
      if ( $this->count() ){
        return $this->current();
      }
    }
    return new Stalexo_Callback_Model();
  }

  public function queryRestrictByManager( User_Staff $manager ){
    if ( $manager->isLoaded() ) {
      $this->query()->where('callback.id_manager = "'.  $manager->id .'"');
    }
  }

  public function queryRestrictByDay( $timestamp ){
    if ( $timestamp and is_numeric($timestamp) and (time() > $timestamp) ){
      $this->query()->where('callback.date BETWEEN "'. $timestamp .'" AND "'.($timestamp + 86400).'" ');
    }
  }

  public function queryJoinLocation(){
    $this->query()->select('l.lat, l.lng')
    ->join('location l', 'l.id_staff = `callback`.id_manager AND `callback`.date <= l.date')
    ->groupBy('`callback`.id');
  }

}