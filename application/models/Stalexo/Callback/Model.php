<?php
class Stalexo_Callback_Model extends Dao{

  static protected $table = "callback";
  static protected $pk = "id";

  public function joinSku($idSku, $quantity = 1, $reason = 0){
    if ( $this->isLoaded() and Valid::id1c($idSku) and Valid::quantity($quantity) ){
      App::db()->query("INSERT IGNORE INTO `callback_has_sku` SET id_callback = '". $this->id ."', quantity = '". $quantity ."', id_sku = '". $idSku ."', id_reason = '". $reason ."'");
    }
  }

  public function loadSku(){
    $this->children['sku'] = array();
    if ( ! $this->isLoaded() ){ return false; }
    $q = Stalexo_Sku_Model::getFetchQuery()
    ->select('chs.quantity')
    ->select('cr.text as reason')
    ->join('callback_has_sku chs', 'chs.id_callback = "'. $this->id .'" AND chs.id_sku = sku.id')
    ->join('callback_reasones cr', 'cr.id = chs.id_reason');
    $result = App::db()->query($q);
    if ( $result ){
      while( $row = $result->fetch_assoc() ){
        $this->children['sku'][] = new Stalexo_Sku_Model($row);
      }
    }
  }

  public function calculateTotal(){
    if ( ! $this->isLoaded() ){
      return;
    }
    $q = "SELECT SUM(quantity*price) as total FROM(
    SELECT `chs`.*,
    (SELECT price FROM(
    SELECT s.id, s.title, s.units, sp.price,
    (". $this->date ." - sp.date) as time_diff
    FROM `sku` s
    JOIN `sku_prices` sp ON s.id = sp.id_sku AND sp.date <= ". $this->date ."
    JOIN `price_types` pt ON pt.id = sp.id_price_type
    JOIN `distribution_point_contracts` dps ON dps.id_price_type = pt.id AND dps.id = ". $this->id_distribution_point_contract ."
    ORDER BY s.id, time_diff
    ) as `sku` WHERE chs.id_sku = id GROUP BY id ) as price
    FROM `callback` `callback`
    JOIN `callback_has_sku` chs ON `callback`.id = chs.id_callback AND `callback`.id = ". $this->id ."
    ) as ordered_sku GROUP BY id_callback";
    $result = App::db()->query($q);
    if ( $result ){
      $row = $result->fetch_assoc();
      $this->total = number_format($row['total'], 2, '.', '');
    }
  }

}