<?php
class Stalexo_Order_Model extends Dao{

  static protected $table = "order";
  static protected $pk = "id";

  public function joinSku($idSku, $quantity = 1){
    if ( $this->isLoaded() and Valid::id1c($idSku) and Valid::quantity($quantity) ){
      App::db()->query("INSERT IGNORE INTO `order_has_sku` SET id_order = '". $this->id ."', quantity = '". $quantity ."', id_sku = '". $idSku ."'");
    }
  }

  /**
  * Expects <array> [ "id" => [0-9]+, ... , "items": [ { ... }, { ... } ] ]
  *
  * @param mixed $orderData
  * @param User_Api $manager
  * @return Stalexo_Order_Model
  */
  static public function addOrder($orderData, User_Api $manager){
    $order = new Stalexo_Order_Model();
    $order->date = $orderData['date'];
    $order->id_external = $orderData['id'];
    $order->date_shipping = $orderData['date_ship'];
    $order->id_distribution_point_contract = $orderData['id_distribution_point_contract'];
    $order->id_callback = $orderData['id_callback'];
    $order->id_manager = $manager->id_staff;
    $order->st_date = $orderData['date'];
    $order->save();

    if ( $order->isLoaded() ){
      foreach( $orderData['items'] as $orderedSku ){
        $order->joinSku($orderedSku['id_sku'], $orderedSku['quantity']);
      }
    }

    return $order;
  }

  public function calculateTotal(){
    if ( ! $this->isLoaded() ){
      return;
    }
    $q = "SELECT SUM(quantity*price) as total FROM(
    SELECT `ohs`.*,
    (SELECT price FROM(
    SELECT s.id, s.title, s.units, sp.price,
    (". $this->date_shipping ." - sp.date) as time_diff
    FROM `sku` s
    JOIN `sku_prices` sp ON s.id = sp.id_sku AND sp.date <= ". $this->date_shipping ."
    JOIN `price_types` pt ON pt.id = sp.id_price_type
    JOIN `distribution_point_contracts` dps ON dps.id_price_type = pt.id AND dps.id = ". $this->id_distribution_point_contract ."
    ORDER BY s.id, time_diff
    ) as `sku` WHERE ohs.id_sku = id GROUP BY id ) as price
    FROM `order` `order`
    JOIN `order_has_sku` ohs ON `order`.id = ohs.id_order AND `order`.id = ". $this->id ."
    ) as ordered_sku GROUP BY id_order";
    $result = App::db()->query($q);
    if ( $result ){
      $row = $result->fetch_assoc();
      $this->total = number_format($row['total'], 2, '.', '');
    }
  }

  public function loadOrderedSku(){
    if ( ! $this->isLoaded() ){ return; }
    $this->children['sku'] = array();

    $q = "SELECT * FROM(
    SELECT s.id, s.title, s.units, sp.price, ohs.quantity,
    (". $this->date_shipping ."- sp.date) as time_diff
    FROM `sku` s
    JOIN `sku_prices` sp ON s.id = sp.id_sku AND sp.date <= ". $this->date_shipping ."
    JOIN `price_types` pt ON pt.id = sp.id_price_type
    JOIN `distribution_point_contracts` dps ON dps.id_price_type = pt.id AND dps.id = ". $this->id_distribution_point_contract ."
    JOIN `order_has_sku` ohs ON ohs.id_sku = s.id AND ohs.id_order = ". $this->id ."
    ORDER BY s.id, time_diff
    ) as `sku` GROUP BY id ";

    $result = App::db()->query($q);
    if ( $result ){
      while( $row = $result->fetch_assoc() ){
        $this->children['sku'][] = new Stalexo_Sku_Model($row);
      }
    }
  }

}