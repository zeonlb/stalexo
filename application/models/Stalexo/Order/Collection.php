<?php
class Stalexo_Order_Collection extends Collection{

  protected function initQuery(){
    $this->query = Stalexo_Order_Model::getFetchQuery()->enableFoundRows()
    ->select('s.name as manager_name')
    ->select('dp.address')
    ->select('dps.title as contract_title')
    ->join('distribution_point_contracts dps', 'dps.id = `order`.id_distribution_point_contract')
    ->join('distribution_points dp', 'dp.id = dps.id_distribution_point')
    ->join('staff s', 's.id = `order`.id_manager')->orderBy('`order`.date DESC');
  }

  protected function initPager(){
    $this->pager = new Pager_StalexoJumping();
  }

  public function loadAll($calculateTotal = false){
    $this->attachFilter(new Filter_Page_Standart($this->pager()));

    $result = $this->executeQuery();
    if ( $result ){
      while( $row = $result->fetch_assoc() ){
        $order = new Stalexo_Order_Model($row);
        if ( $calculateTotal ){
          $order->calculateTotal();
        }
        $this->add( $order );
      }
    }
  }
  
  public function queryJoinLocation(){
    $this->query()->select('l.lat, l.lng')
    ->join('location l', 'l.id_staff = `order`.id_manager AND `order`.date <= l.date')
    ->groupBy('`order`.id');
  }

  public function loadAllForExport(){
    $this->attachFilter(new Filter_Page_Standart($this->pager()));
    $result = $this->executeQuery();
    if ( $result ){
      while( $row = $result->fetch_assoc() ){
        $order = new Stalexo_Order_Model($row);
        $order->loadOrderedSku();
        $this->add( $order );
      }
    }
  }

  /**
  * @return Stalexo_Order_Model
  * @param Array $id
  */
  public function getById(Array $id){
    $this->query
    ->select('c.title as client_title')
    ->join('clients c', 'c.id = dps.id_client')
    ->where("`order`.id IN ('". implode("', '", $id) ."')");
    $order = new Stalexo_Order_Model();
    $result = $this->executeQuery();
    if ( $result ){
      $row = $result->fetch_assoc();
      $order = new Stalexo_Order_Model($row);
    }

    return $order;
  }


  public function queryRestrictByManager( User_Staff $manager ){
    if ( $manager->isLoaded() ) {
      $this->query()->where('order.id_manager = "'.  $manager->id .'"');
    }
  }
  
  public function queryRestrictByClient( $id = null ){
	if ($id)
		$this->query()->where('dp.id_client = "'.  $id .'"');
  }
  
  public function queryRestrictByAddress( $id = null ){
	if ($id)
		$this->query()->where('dp.id = "'.  $id .'"');
  }

  public function queryRestrictByDay( $timestamp, $timestamp2 ){
    if ( $timestamp and is_numeric($timestamp) and (time() > $timestamp) ){
      $this->query()->where('order.date BETWEEN "'. $timestamp .'" AND "'.$timestamp2.'" ');
      var_dump('order.date BETWEEN "'. $timestamp .'" AND "'.$timestamp2.'" ');
    }
  }

}