<?php
class Stalexo_Route_Point extends Dao{

  static protected $pk    = 'id_route';
  static protected $table = 'routes_has_dp';
  static protected $tableQueryAlias = 'rp';

  public function isValidTimeline(Stalexo_Timeline_Model $timeline, $date){
    if ( ($timeline->isLoaded() and $timeline->date_end < time()) ){

      $distance = round( Geo_Facade::getDistance($this->lat, $this->lng, $timeline->lat_begin, $timeline->lng_begin)*1000 );

      if ( $distance > Config::get('agent_routes')->entry_distance ){
        $timeline->invalid_distance = true;
      }

      if ( abs(Date::makeTimestamp(date('Y', $date), date('m', $date), date('d', $date)) + $this->time_plan - $timeline->date_begin) > Config::get('agent_routes')->begintime_range ){
        $timeline->invalid_begintime = true;
      }

    }
  }

}