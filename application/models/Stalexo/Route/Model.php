<?php
class Stalexo_Route_Model extends Dao{

  static protected $pk    = 'id';
  static protected $table = 'routes';

  static public function getByAgentAndDay(User_Staff $agent, $day){
    if ( $agent->isLoaded() and Valid::day($day) ){
      $route = Stalexo_Route_Model::fetchByQuery(
        Stalexo_Route_Model::getFetchQuery()
        ->where('id_staff = "'. $agent->id .'"')
        ->where('day = "'. $day .'"')
        ->where('`date` < UNIX_TIMESTAMP()')
        ->orderBy('`date` DESC')
        ->limit(1)
      );

      if ( count($route) ){
        return $route[0];
      }
    }
    return new Stalexo_Route_Model();
  }

  public function loadPoints(){
    $this->children = Stalexo_Route_Point::fetchByQuery(
      Stalexo_Route_Point::getFetchQuery()
      ->select('dp.lat')
      ->select('dp.lng')
      ->select('dp.address')
      ->select('clients.title')
      ->join('distribution_points dp', 'dp.id = rp.id_dp')
      ->join('clients clients', 'clients.id = dp.id_client')
      ->where('rp.id_route = "'. $this->id .'"')
      ->orderBy('rp.prior')
    );

    foreach ( $this->children as $k => $p ){
      $beginSec = Date::makeTimestamp(2000, 1, 1)+$p->time_plan;
      $endSec = $beginSec+$p->time_work;
      $this->children[$k]->time      = date('H:i', $beginSec);
      $this->children[$k]->time_end  = date('H:i', $endSec);
      $this->children[$k]->time_diff = ceil(($endSec - $beginSec)/60);
    }

  }

}
