<?php
class Stalexo_Remain_Model extends Dao{

  static protected $pk = 'id';
  static protected $table = 'remains';

  public function loadSku(){
    $this->children['sku'] = array();
    if ( ! $this->isLoaded() ){ return false; }
    $q = Stalexo_Sku_Model::getFetchQuery()
    ->select('rhs.quantity')
    ->join('remains_has_sku rhs', 'rhs.id_remains = "'. $this->id .'" AND rhs.id_sku = sku.id');
    $result = App::db()->query($q);
    if ( $result ){
      while( $row = $result->fetch_assoc() ){
        $this->children['sku'][] = new Stalexo_Sku_Model($row);
      }
    }
  }

  public function joinSku($idSku, $quantity = 1){
    if ( $this->isLoaded() and Valid::id1c($idSku) and Valid::quantity($quantity) ){
      App::db()->query("INSERT IGNORE INTO `remains_has_sku` SET id_remains = '". $this->id ."', quantity = '". $quantity ."', id_sku = '". $idSku ."'");
    }
  }

}