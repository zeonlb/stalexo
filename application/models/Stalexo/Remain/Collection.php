<?php
class Stalexo_Remain_Collection extends Collection{

  protected function initPager(){
    $this->pager = new Pager_StalexoJumping();
  }

  protected function initQuery(){
    $this->query = Stalexo_Remain_Model::getFetchQuery()->enableFoundRows();
  }

  public function loadAll(){
    $this->attachFilter(new Filter_Page_Standart($this->pager()));

    $result = $this->executeQuery();
    if ( $result ){
      while( $row = $result->fetch_assoc() ){
        $this->add(new Stalexo_Remain_Model($row));
      }
    }
  }

  public function queryAddDetails(){
    $this->query()
    ->select('dp.address')
    ->select('staff.name manager_name')
    ->join('staff staff', 'staff.id = remains.id_manager')
    ->join('distribution_point_contracts dps', 'dps.id = remains.id_distribution_point_contract')
    ->join('distribution_points dp', 'dp.id = dps.id_distribution_point')
    ->orderBy('`remains`.date DESC');;
  }

  public function getById($id){
    if ( is_numeric($id) ) {
      $this->query()
      ->disableFoundRows()
      ->where('remains.id = "'. $id .'"');
      $this->loadAll();
      if ( $this->count() ){
        return $this->current();
      }
    }
    return new Stalexo_Remain_Model();
  }

  public function queryRestrictByManager( User_Staff $manager ){
    if ( $manager->isLoaded() ) {
      $this->query()->where('remains.id_manager = "'.  $manager->id .'"');
    }
  }

  public function queryRestrictByDay( $timestamp ){
    if ( $timestamp and is_numeric($timestamp) and (time() > $timestamp) ){
      $this->query()->where('remains.date BETWEEN "'. $timestamp .'" AND "'.($timestamp + 86400).'" ');
    }
  }

  public function queryJoinLocation(){
    $this->query()->select('l.lat, l.lng')
    ->join('location l', 'l.id_staff = `remains`.id_manager AND `remains`.date <= l.date')
    ->groupBy('`remains`.id')->orderBy('l.date');
  }

}