<?php 
class Stalexo_Client_Model extends Dao{
  
  static protected $table = 'clients';
  static protected $pk    = 'id';
  
  public function loadOrdersTotal($date, $agent){
    $this->orders_total = 0;
    if ( ! $agent->id ){
      $agent->id = $agent->id_staff;
    }
    if ( ! $date ){
      $date = time();
    }
    $year =  date('Y', $date);
    $month = date('m', $date);
    $dateBegin = Date::makeTimestamp($year, $month, 1);
    $dateEnd   = Date::makeTimestamp($year, $month, date('t', $dateBegin)) + 86400;
    
    $result = App::db()->query('SELECT SUM(o.total) as orders_total FROM `order` o 
    JOIN distribution_plans dpl2 ON dpl2.id_dc = o.id_distribution_point_contract AND o.id_manager = "'. $agent->id .'" 
    JOIN distribution_point_contracts dpc ON dpc.id = dpl2.id_dc
    JOIN distribution_points dp ON dp.id = dpc.id_distribution_point AND dp.id_client = "'. $this->id .'"
    AND o.id_manager = dpl2.id_staff AND dpl2.year = "'.$year.'" AND dpl2.month = "'. $month .'"
    AND o.date_shipping BETWEEN "'.$dateBegin.'" AND "'. $dateEnd .'"');
    if ( $result ){
      $row = $result->fetch_assoc();
      $this->orders_total = $row['orders_total'];
    }
  }
  
  public function loadCallbacksTotal($date, $agent){
    $this->callbacks_total = 0;
    if ( ! $agent->id ){
      $agent->id = $agent->id_staff;
    }
    if ( ! $date ){
      $date = time();
    }
    $year =  date('Y', $date);
    $month = date('m', $date);
    $dateBegin = Date::makeTimestamp($year, $month, 1);
    $dateEnd   = Date::makeTimestamp($year, $month, date('t', $dateBegin)) + 86400;
    
    $result = App::db()->query('SELECT SUM(c.total) as callbacks_total FROM `callback` c 
    JOIN distribution_plans dpl ON dpl.id_dc = c.id_distribution_point_contract AND c.id_manager = "'. $agent->id .'" 
    JOIN distribution_point_contracts dpc ON dpc.id = dpl.id_dc
    JOIN distribution_points dp ON dp.id = dpc.id_distribution_point AND dp.id_client = "'. $this->id .'"
    AND c.id_manager = dpl.id_staff AND dpl.year = "'.$year.'" AND dpl.month = "'. $month .'"
    AND c.date BETWEEN "'.$dateBegin.'" AND "'. $dateEnd .'"');
    if ( $result ){
      $row = $result->fetch_assoc();
      $this->callbacks_total = $row['callbacks_total'];
    }
  }
  
  public function getSalesOrdersRatio(){
    if ( ! $this->sales_plan ) {
      return 0;
    }
    if ( ! $this->orders_total ){
      return 0;
    }
    
    $r = round($this->orders_total/$this->sales_plan * 100);
    return $r;
  }
  
}