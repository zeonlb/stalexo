<?php
class Stalexo_Client_Collection extends Collection{

  protected function initQuery(){
    $this->query = Stalexo_Client_Model::getFetchQuery();
  }

  public function loadAll(){
    $result = $this->executeQuery();
    if ( $result ){
      while ( $row = $result->fetch_assoc() ){
        $this->add( new Stalexo_Client_Model($row) );
      }
    }
  }

  public function loadByManager($manager, $date = null, $limit,$limit_iter){
    $this->query
    ->join('distribution_point_contracts dpc', 'dpc.id_manager = "'. ($manager->id_staff ? $manager->id_staff : $manager->id) .'" AND dpc.id_client = clients.id')
    ->join('clients_group cg', 'clients.id_group = cg.id')
    ->orderBy('clients.title')->groupBy('clients.id')
    ->limit($limit, $limit_iter);

    if ( $date ){
      $this->query->where('clients.date_modify > FROM_UNIXTIME("'. $date .'")');
    }

    $this->loadAll();
  }
  
  public function queryJoinSalesPlan($date){
    if ( ! $date ){
      $date = time();
    }
    $year  = date('Y', $date);
    $month = date('m', $date);
    $this->query
    ->select('SUM(dpl.plan) as sales_plan')
    ->join('distribution_plans dpl', 'dpl.id_dc = dpc.id AND dpl.id_staff = dpc.id_manager AND dpl.year = "'.$year.'" AND dpl.month = "'. $month .'"');
  }
  
  public function queryJoinManager($manager){
    $this->query->join('distribution_point_contracts dpc', 'dpc.id_manager = "'. ($manager->id_staff ? $manager->id_staff : $manager->id) .'" AND dpc.id_client = clients.id');
  }
  
  public function eachLoadPlanDetail($date, $agent){
    foreach( $this->array as $item ){
      $item->loadOrdersTotal($date, $agent);
      $item->loadCallbacksTotal($date, $agent);
    }
  }
  
}