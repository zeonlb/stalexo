<?php
class Stalexo_Messenger{
 
  static protected $key = "stalexo_message";
  
  static protected $types = array(
    'info',
    'error',
    'success'
  );
  
  static protected $tpl = 'messenger/stalexo/index.tpl';
  
  static public function addMessage($text, $type = false){
    if ( ! in_array($type, self::$types) ) {
      $type = false;
    }
    $_SESSION[self::$key]['text'] = $text;
    $_SESSION[self::$key]['type'] = $type;
  }
  
  static public function getMessage(){
    if (isset($_SESSION[self::$key]) and !empty($_SESSION[self::$key])){
      $message = $_SESSION[self::$key];
      $s = App::newSmarty();
      $s->assign('message', $message);
      $_SESSION[self::$key] = array();
      return $s->fetch(self::$tpl);
    }
    return '';
  }
  
}