<?php
class Date {

  public static function js2PhpTime($jsdate){
    if(preg_match('@(\d+)/(\d+)/(\d+)\s+(\d+):(\d+)@', $jsdate, $matches)==1){
      $ret = mktime($matches[4], $matches[5], 0, $matches[1], $matches[2], $matches[3]);
    }else if(preg_match('@(\d+)/(\d+)/(\d+)@', $jsdate, $matches)==1){
      $ret = mktime(0, 0, 0, $matches[1], $matches[2], $matches[3]);
    }
    return $ret;
  }

  public static function php2JsTime($phpDate){
    return date("m/d/Y H:i", $phpDate);
  }

  /*
  * $phpDate - unix timestamp
  */
  public static function php2MySqlDate($phpDate){
    return date("Y-m-d", $phpDate);
  }

  public static function php2MySqlTime($phpDate){
    return date("Y-m-d H:i:s", $phpDate);
  }

  public static function mySql2PhpTime($sqlDate){
    $arr = date_parse($sqlDate);
    return mktime($arr["hour"],$arr["minute"],$arr["second"],$arr["month"],$arr["day"],$arr["year"]);

  }

  /**
  * timestamp to dd.mm.yyyy
  *
  */
  public static function php2Date1c($timestamp){
    return date('d.m.Y', $timestamp);
  }

  /**
  * dd.mm.yyyy to timestamp
  *
  */
  public static function date1c2php($val){
    if ( preg_match('@^([0-9]{2})\.([0-9]{2})\.([0-9]{4})$@', $val, $matches) ){
      return mktime(0, 0, 0, $matches[2], $matches[1], $matches[3]);
    }
    return '';
  }

  public static function getMkDate( $mktime ){
    $date = getdate($mktime);
    $obj = (object)array();
    foreach ($date as $k=>$v){
      if($k){
        $obj->{$k} = $v;
      }else{
        $obj->mk = $v;
      }
    }

    $month = array(
     1=>'Января'
    ,2=>'Февраля'
    ,3=>'Марта'
    ,4=>'Апреля'
    ,5=>'Мая'
    ,6=>'Июня'
    ,7=>'Июля'
    ,8=>'Августа'
    ,9=>'Сентября'
    ,10=>'Октября'
    ,11=>'Ноября'
    ,12=>'Декабря'
    );

    $days = array(
     1=>'Понедельник'
    ,2=>'Вторник'
    ,3=>'Среда'
    ,4=>'Четверг'
    ,5=>'Пятница'
    ,6=>'Суббота'
    ,0=>'Воскресенье'
    );
    $d = array(
     1=>'Пн'
    ,2=>'Вт'
    ,3=>'Ср'
    ,4=>'Чт'
    ,5=>'Пт'
    ,6=>'Сб'
    ,0=>'Вс'
    );

    $obj->month = $month[$obj->mon];
    $obj->weekday = $days[$obj->wday];
    $obj->wd = $d[$obj->wday];
    $obj->date = $obj->mday.'.'.$obj->mon.'.'.$obj->year;
    $obj->time = $obj->hours.':'.$obj->minutes.':'.$obj->seconds;

    return $obj;
  }

  public static function declinationHours( $int ){
    $int = (int)$int;
    $lastNumber = ( $int < 15 ) ? $int : substr( $int, -1, 1 );
    $declinationArray = array(
      0 => "часов",
      1 => "час",
      2 => "часа",
      3 => "часа",
      4 => "часа",
      5 => "часов",
      6 => "часов",
      7 => "часов",
      8 => "часов",
      9 => "часов",
      10 => "часов",
      11 => "часов",
      12 => "часов",
      13 => "часов",
      14 => "часов",
    );

    return $declinationArray[$lastNumber];
  }

  public static function getDateText( $dateBegin, $timeBegin, $dateEnd = 0, $timeEnd = 0 ){
    $output = "";
    $begin = self::getMkDate($dateBegin);
    $beginTimeStr = is_numeric($timeBegin) ? date("H:i",$timeBegin) : "";
    if ( $dateEnd ){
      $end = self::getMkDate($dateEnd);
      $endTimeStr = is_numeric($timeEnd) ? date("H:i",$timeEnd) : "";
      if ( $begin->mday == $end->mday and $begin->month == $end->month ) {
        $output = "Начало ".$begin->mday." ".$begin->month." ".$begin->year.", ".$begin->weekday." ".($timeBegin ? $beginTimeStr . ($timeEnd ? " &mdash; ".$endTimeStr : "") : "");
      } elseif ( $begin->mday != $end->mday and $begin->month == $end->month ) {
        $output = "Начало ".$begin->mday." &mdash; ". $end->mday." " .$begin->month." ".$begin->year.", ".$begin->weekday." &mdash; ". $end->weekday.( $timeBegin ? " ".$beginTimeStr . ($timeEnd ? " &mdash; ".$endTimeStr : "") : "");
      } else {
        $output = "Начало ".$begin->mday." ".$begin->month." ".$begin->year.", ".$begin->weekday." ".(  $timeBegin ? $beginTimeStr : "");
        $output .= " &mdash; завершение ".$end->mday." ".$end->month." ".$end->year.", ".$end->weekday." ".($timeEnd ? $endTimeStr : "");

      }
    } else {
      $output = "Начало ".$begin->mday." ".$begin->month." ".$begin->year.", ".$begin->weekday." ".($timeBegin ? $beginTimeStr : "");
    }
    return $output;
  }

  public static function getDateTextShort( $dateBegin, $dateEnd = 0 ){
    $outputStr = "";
    $begin = self::getMkDate( $dateBegin );
    $end = 0;
    if ( $dateEnd and $dateBegin and $dateBegin < $dateEnd ){
      $end = self::getMkDate( $dateEnd );
    }

    if ( $end  ){
      if ( $begin->mon == $end->mon ){
        $outputStr = ( $begin->mday == $end->mday ) ? $begin->mday : ($begin->mday . " - " . $end->mday . " " . $begin->month);
      } else {
        $outputStr = $begin->mday ." ". $begin->month . " - " . $end->mday . " " . $end->month;
      }
    } else {
      $outputStr = $begin->mday ." ".$begin->month;
    }

    $outputStr .= ' ' . $begin->year;

    return $outputStr;
  }


  /**
  * Make timestamp from date range from 0001.01.01 to 2999.12.31
  *
  * @param int $year
  * @param int $month
  * @param int $day
  */
  public static function makeTimestamp($year, $month = 1, $day = 1, $hours=0, $minutes=0){
    $month = ($month > 0 and $month < 13 ) ? $month : false;
    $day   = ($day > 0 and $day < 32 ) ? $day : false;
    $year  = ($year > 0 and $year < 3000) ? $year : false;
    if ( $month and $day and $year ){
      return mktime( $hours, $minutes, 0, $month, $day, $year);
    }
    return false;
  }
  /**
  * make timestamp from yyyy-mm-dd
  *
  * @param string $date
  */
  public static function ymd2timestamp($date){
    if ( ! preg_match('@^([0-9]{4})-([0-9]{2})-([0-9]{2})--([0-9]{2})-([0-9]{2})$@', $date, $m) ){
      return 0;
    }
    return static::makeTimestamp($m[1], $m[2], $m[3], $m[4], $m[5]);
  }


}