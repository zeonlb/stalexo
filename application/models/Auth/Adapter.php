<?php 
class Auth_Adapter{
  
  protected $authType;
  
  public function __construct( Auth_Type_Interface $authType ){
    $this->authType = $authType;
  }
  
  public function authenticate( Array $args ){
    return $this->authType->authenticate( $args );
  }
  
}