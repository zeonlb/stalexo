<?php 
interface Auth_Type_Interface{
  
  public function authenticate( $args );
  
}

class Auth_Exception_WrongCredentials extends Exception {}

class Auth_Exception_WrongPassword extends Exception {}

class Auth_Exception_WrongLogin extends Exception {}

class Auth_Exception_AccountNotActive extends Exception {}

class Auth_Exception_AccessDenied extends Exception{};

class Auth_Exception_UserNotExists extends Exception{};