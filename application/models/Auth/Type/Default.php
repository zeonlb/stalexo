<?php
class Auth_Type_Default extends Auth_Type_Abstract implements Auth_Type_Interface{

  /**
  * @return User_Abstract
  * @throws Auth_Exception_WrongCredentials, Auth_Exception_WrongPassword, Auth_Exception_WrongLogin, Auth_Exception_AccountNotActive
  *
  * @param Array $args
  */
  public function authenticate( $args ){
    if ( ! isset($args['email']) or ! isset($args['password']) or ! Valid::email($args['email']) ){
      throw new Auth_Exception_WrongCredentials();
    }

    $userRow = self::getUserRow( $args['email'] );
    if ( ! $userRow ){
      throw new Auth_Exception_WrongLogin();
    }

    if ( Auth_Registration::makePasswordHash( $args['password'], $userRow['salt'] ) == $userRow['password'] ){
      $user = new User_Abstract($userRow[User_Abstract::getPk()]);

      if ( $user->isLoaded() ){
        $_SESSION['auth']['userid'] = $userRow[User_Abstract::getPk()];
        return $user;
      }
    }
    throw new Auth_Exception_WrongPassword();
  }

}

