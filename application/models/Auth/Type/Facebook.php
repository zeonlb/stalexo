<?
class Auth_Type_Facebook extends Auth_Type_Abstract implements Auth_Type_Interface{
  
  const REDIRECT_URL = "/login/social/fb";
  const PERMISSION   = "email";
  
  protected $appId;
  protected $secret;
  
  public function __construct(){
    $this->appId = Config::get('socials')->facebook->appId;
    $this->secret = Config::get('socials')->facebook->secret;
  }
  
  /**
  * Authenticate user in system by facebook account
  * 
  * @throws Auth_Exception_AccessDenied, Registration_Exception_InvalidInput
  * @param Array $args
  */
  public function authenticate( $args ){
    $state = isset($args['state']) ? $args['state'] : false;
    $code  = isset($args['code'])  ? $args['code'] : false;
    $error = isset($args['error']) ? $args['error'] : false;

    $userFB = $this->getFbUser( $code, $state, $error );
    $userRow = self::getUserRow( $userFB->email );   
    if ( $userRow and isset($userRow['id_user']) ){
      $_SESSION['auth']['userid'] = $userRow['id_user'];
      if ( $userRow['status'] == 'registred' ){
        //Activate acc if not activated yet
        Auth_Registration::activateByUserid( $userRow['id_user'] );
      }
      return $userRow;
    } else {
      $user = Auth_Registration::addBySocials( $userFB->email, $userFB->first_name, $userFB->last_name );
      $_SESSION['auth']['userid'] = $user->id_user;
      return $user;
    }    
  }
  
  /**
  * @throws Auth_Exception_AccessDenied
  * 
  * @param mixed $code
  * @param mixed $state
  * @param mixed $error
  */
  public function getFbUser( $code, $state, $error ){
    if ( ! (! $error and $code and $state === self::getState()) ) {
      throw new Auth_Exception_AccessDenied();
    }  
    $fbArgs = array( 
      'client_id'     => $this->appId,
      'redirect_uri'  => "http://" . $_SERVER['SERVER_NAME'] . self::REDIRECT_URL,
      'client_secret' => $this->secret,
      'code' => $code
    );
    $response = App_Request::sendGET( "https://graph.facebook.com/oauth/access_token", $fbArgs );
    parse_str($response, $params);
    
    $accessToken = isset($params['access_token']) ? $params['access_token'] : false;
    if ( ! $accessToken ){
      throw new Auth_Exception_AccessDenied();
    }
    
    $graph_url = "https://graph.facebook.com/me?access_token=" . $accessToken;

    $userData = App_Request::sendGET( 'https://graph.facebook.com/me', array( 'access_token' => $accessToken ) );
    $userFB = json_decode($userData);
    return $userFB;
  }
  
  public static function getLoginLink(){    
    $state = md5(microtime());
    self::setState($state);
    $appId = Config::get('socials')->facebook->appId;
    $redirectUrl = "http://" . $_SERVER['SERVER_NAME'] . self::REDIRECT_URL;
    
    $link = "https://www.facebook.com/dialog/oauth?client_id=".$appId."&scope=".self::PERMISSION."&redirect_uri=". $redirectUrl ."&state=". $state;
    
    return $link;   
  }
  
  public static function getState(){
    $state = isset($_SESSION['facebook']['state']) ? $_SESSION['facebook']['state'] : ""; 
    return $state;
  }
  
  public static function setState( $state ){
    $_SESSION['facebook']['state'] = $state;
  }
  
}