<?php 
abstract class Auth_Type_Abstract{
  
  public static function getUserRow( $email ){
    if ( Valid::email($email) ){
      $sql = "SELECT * FROM `". User_Abstract::getTableName() ."` "
      ." WHERE email = '". App::db()->escape($email) ."' LIMIT 1 ";
      
      $result = App::db()->query($sql);
      
      if ( $result->num_rows ){
        $row = $result->fetch_assoc();
        return $row;
      }
    }
    return false;
  }
  
  /**
  * Render javscript to close popup window and reload parent window
  * 
  */
  public static function closeSocialWindow(){
    $smarty = App::newSmarty();
    $page = $smarty->fetch( "auth/login/socials-close.tpl" );
    echo $page;
    exit();
  }
  
  public function generatePassword( $length = 10){
    $chars = '1234567890ABCDEFGHIGKLMOOPQRSTUVWXYZabsdefghigklmnopqrstuvwxyz0987654321'; 
    return substr(str_shuffle($chars), 0, $length);
  }
  
}