<?php
class Auth_Type_Google extends Auth_Type_Abstract implements Auth_Type_Interface{
  
  const REDIRECT_URL = "/login/social/google";
  
  protected $secret;
  protected $redirectUri;
  protected $devloperKey;
  protected $clientId;
  
  public function __construct(){
    require_once Settings::path()->vendor . 'google/apiClient.php';
    require_once Settings::path()->vendor . 'google/contrib/apiOauth2Service.php';
    
    $this->clientId    = Config::get('socials')->google->clientId;
    $this->devloperKey = Config::get('socials')->google->key;
    $this->redirectUri = "http://". $_SERVER['SERVER_NAME'] . self::REDIRECT_URL;
    $this->secret      = Config::get('socials')->google->secret;
  }
  
  /**
  * Authenticate user in system via google account
  * @throws Auth_Exception_UserNotExists, Auth_Exception_AccessDenied
  * @param Array $args
  */
  public function authenticate( $args ){
    
    $client = $this->getClient();
    $oauth2 = new apiOauth2Service($client);
    
    if ( isset($_REQUEST["code"]) ){
      $client->authenticate();
      $_GET = $_REQUEST;
      $token = $client->getAccessToken();
      $_SESSION['google']['token'] = $token;
    }
    
    if ( $_SESSION['google']['token'] ) {
      $client->setAccessToken( $_SESSION['google']['token'] );
    }
    
    if ( $client->getAccessToken() ){
      $user = $oauth2->userinfo->get();
      $email = filter_var($user['email'], FILTER_SANITIZE_EMAIL);
      $userRow = self::getUserRow( $email );
      
      if ( ! $userRow or ! $userRow['id_user'] ){
        $user = Auth_Registration::addBySocials( $email, $user['given_name'], $user['family_name']);
        $_SESSION['auth']['userid'] = $user->id_user;
        return $user;
      }
      
      $_SESSION['auth']['userid'] = $userRow['id_user'];
      unset($_SESSION['google']['token']);
      if ( $userRow['status'] == 'registred' ){
        //Activate acc if not activated yet
        Auth_Registration::activateByUserid( $userRow['id_user'] );
      }
      return $userRow;
    } else {
      throw new Auth_Exception_AccessDenied();
    }
    
  }
  
  /**
  * @return apiClient google api client
  * 
  */
  protected function getClient(){
    $client = new apiClient();
    $client->setClientId( $this->clientId );
    $client->setClientSecret( $this->secret );
    $client->setRedirectUri( $this->redirectUri );
    $client->setDeveloperKey( $this->devloperKey );
    return $client;
  }
  
  protected function getAuthUrl(){
    $client = $this->getClient();
    $client->setScopes( array( 'email', 'profile' ) );
    $authUrl = $client->createAuthUrl();
    return $authUrl;
  }
  
  public static function getLoginLink(){
    $google = new Auth_Type_Google();
    return $google->getAuthUrl() ;
  }
  
} 