<?php
class Auth_Registration{

  const activationLinkLiveTime = 604800;
  const RECOVER_PASSWORD_URL = "/auth/password-recovery/";
  const ACTIVATION_URL = '/auth/activation/';

  /**
  * Register new user in system; mail account activation link
  *
  * @return bool
  * @throws Registration_Exception_InvalidInput, Registration_Exception_UserAlreadyExists
  * @param Array $data
  */
  public static function addUser ( $data ) {
    $login = isset($data['email']) ? $data['email'] : "";
    $pass = isset($data['password']) ? $data['password'] : "";
    $name = isset($data['username']) ? $data['username'] : "";
    $secondname = isset($data['secondname']) ? $data['secondname'] : "";
    $role = (isset($data['role']) and is_numeric($data['role'])) ? $data['role'] : 1;

    $userExists = Auth_Type_Abstract::getUserRow( $login );
    if ( $userExists ) {
      throw new Registration_Exception_UserAlreadyExists();
    }

    if ( ! self::isValidData( $login, $pass ) ){
      throw new Registration_Exception_InvalidInput();
    }

    $user = new User_Abstract();
    $user->email = $login;
    $user->username = $name;

    if ( Valid::username($secondname) ){
      $user->secondname = $secondname;
    }

    $user->salt = self::makeSalt();
    $user->id_role = $role;
    $user->passworddate = date('Y-m-d');
    $user->password = self::makePasswordHash( $pass, $user->salt );
    $user->joindate = time();
    $user->save();
    /*
    $activation = new User_Activation_Model();
    $activation->userid = $user->userid;
    $activation->dateline = time();
    $activation->activationid = self::makeActivationid( $user->userid );
    $activation->type = 0;
    $activation->usergroupid = 1;
    $activation->save();
    */
    //self::mailActivationLink( $user, self::ACTIVATION_URL . $activation->activationid );

    return true;
  }

  /**
  * Returns recover password link by user email
  *
  * @throws Registration_Exception_InvalidInput, Registration_Exception_UserNotExists
  * @param string $email
  */
  public static function getRecoverPasswordLink( $email ){
    if ( ! Valid::email( $email ) ){
      throw new Registration_Exception_InvalidInput();
    }

    $result = App::db()->query("SELECT password FROM vb_user WHERE email='".App::db()->escape($email)."' LIMIT 1");

    if( $result->num_rows == 0 ) {
      throw new Registration_Exception_UserNotExists();
    }

    $row = $result->fetch_assoc();
    $link = "http://".$_SERVER['SERVER_NAME']. self::RECOVER_PASSWORD_URL .$row['password'];
    return $link;
  }

  /**
  * Returns user row from db by password hash
  *
  * @throws Registration_Exception_UserNotExists
  * @param string $password
  */
  public function getUserRowByPassword( $password ){
    $result = App::db()->query("SELECT * FROM `".User_Abstract::getTableName()."` WHERE password = '".App::db()->escape($password)."' LIMIT 1");
    if ( $result->num_rows == 0 ){
      throw new Registration_Exception_UserNotExists();
    }
    $row = $result->fetch_assoc();
    return $row;
  }

  public static function changePassword( User_Abstract $user, $password ){
    if ( ! Valid::password($password) ) {
      throw new Registration_Exception_InvalidInput();
    }
    $hash = self::makePasswordHash( $password, $user->salt );
    $user->password = $hash;
    $user->passworddate = date('Y-m-d');
    $user->save();
    return true;
  }

  /**
  * Activate account in system by activationid
  *
  * @throws Registration_Exception_InvalidActivation
  * @return bool
  * @param string $activationid
  */
  public static function activate( $activationid ){
    $sql = "SELECT act.*, u.usergroupid as usergroup FROM `vb_useractivation` act
    JOIN `vb_user` u ON act.userid = u.userid AND act.activationid = '". App::db()->escape($activationid) ."' AND (dateline + '". self::activationLinkLiveTime ."' ) > UNIX_TIMESTAMP() LIMIT 1 ";

    $result = App::db()->query( $sql );
    if ( $result->num_rows ){
      $act = $result->fetch_assoc();
      if ( $act['usergroup'] == 1 ){
        App::db()->query( "UPDATE `vb_useractivation` SET type = 1, usergroupid = 2 WHERE useractivationid = '". $act['useractivationid'] ."' LIMIT 1" );
        App::db()->query( "UPDATE `vb_user` SET usergroupid = 2 WHERE userid = '". $act['userid'] ."' LIMIT 1" );
        return $act['userid'];
      }
    }
    throw new Registration_Exception_InvalidActivation();
  }

  public static function activateByUserid( $id ){
    App::db()->query( "UPDATE `vb_user` SET usergroupid = 2 WHERE userid = '". (int)$id ."' LIMIT 1" );
    App::db()->query( "UPDATE `vb_useractivation` SET type = 1, usergroupid = 2 WHERE usergroupid != 2 AND userid = '".(int)$id."' LIMIT 1" );
  }

  protected static function isValidData( $login, $pass ){
    $valid = true;
    if ( ! Valid::email($login) ){ $valid = false; }
    if ( ! Valid::password($pass) ){ $valid = false; }
    return $valid;
  }

  protected static function makeSalt($length = 15) {
    $salt = '';

    for ($i = 0; $i < $length; $i++)
    {
      $salt .= chr(rand(33, 126));
    }

    return $salt;
  }

  public static function makePasswordHash( $password, $salt ){
    $hash = md5( md5($password) . substr($salt,0,3) );
    return $hash;
  }

  protected static function makeActivationid( $userId ){
    return md5(md5($userId) . (mktime() * rand(1, 99)));
  }

  protected static function mailActivationLink( User_Abstract $user, $activationLink ){
    Mailer_Adapter::send(
      $_SERVER['SERVER_NAME'] . " - активация аккаунта на сайте",
      $user->email,
      new Mailer_Content_Auth_Activation( $user, $activationLink )
    );
  }

}

class Registration_Exception_InvalidInput extends Exception{}

class Registration_Exception_UserAlreadyExists extends Exception{}

class Registration_Exception_UserNotExists extends Exception{}

class Registration_Exception_InvalidActivation extends Exception{}