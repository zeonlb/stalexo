<?php 
class Block_Tray implements Block_Interface{
  
  protected $html = null;
  
  public function getHtml(){
    if ( $this->html ){
      return $this->html;
    }
    $smarty = App::newSmarty();
    $smarty->assign(array(
      'breadcrumbs' => Tray::getNodes()
    ));
    $this->html = $smarty->fetch('breadcrumbs/index.tpl');
    return $this->html;
  }
  
  public function __toString(){
    return $this->getHtml();
  }
  
}