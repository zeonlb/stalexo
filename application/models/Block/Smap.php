<?php 
class Block_Smap implements Block_Interface{
  
  protected $html = false;
  
  public function getHtml(){
    if ( $this->html ){
      return $this->html;
    }
    $delivery = new Smap_Model(array(
      'id'    => 166,
      'alias' => 'delivery',
      'type'  => 'page'
    ));
    $payment = new Smap_Model(array(
      'id'    => 167,
      'alias' => 'payment',
      'type'  => 'page'
    ));
    $smarty = App::newSmarty();
    $smarty->assign(array( 
      'smapDelivery' => $delivery->getPageContent(), 
      'smapPayment'  => $payment->getPageContent() 
    ));
    $this->html = $smarty->fetch('block/smap-popup.tpl');
    return $this->html;
  }
  
  public function __toString(){
    return $this->getHtml();
  }
  
}