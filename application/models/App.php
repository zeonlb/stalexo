<?php
class App{

  static protected $instance = null;

  protected $db = null;
  protected $view = null;
  protected $user = null;
  protected $languages = null;
  protected $activeLanguage = null;
  protected $currencies = null;
  protected $activeCurrency = null;
  protected $smarty = null;
  protected $multiLanguage = false;
  protected $multiCurrency = false;
  protected $userCanLogin = true;
  private   $run = false;
  private   $bootstrap = false;
  protected $request = null;
  protected $debug = false;

  /**
  * @return App
  *
  */
  public static function instance(){
    if ( ! self::$instance instanceof self ){
      self::$instance = new self();
    }
    return self::$instance;
  }

  public function run(){
    if ( $this->run ){
      throw new AppException('Application is already running');
    }
    try{
      $fc = new Controller_Front();
      $fc->run( App::getRequest() );
    } catch ( Exception $e ){
      self::render404( $e->getMessage() );
    }
  }

  protected function __construct(){}

  public function bootstrap(){
    if ( $this->bootstrap ){
      return false;
    }
    $this->bootstrap = true;
    try{
      $this->initAutoloader();
      $this->view = new App_View();
      $this->initDebug();
      $this->initDb();
      //$this->initLanguage();
      //$this->initCurrency();
      $this->initUser();
      $this->view()->setLayout(new Layout_Main());
    } catch ( Exception $e ){
      echo $e->getMessage();
      exit();
    }
  }

  protected function initDebug(){
    $this->debug = Debug::isDeveloper();
    if ( $this->debug ){
      Debug::startDebug();
    }
  }

  protected function initAutoloader(){
    require_once Settings::path()->models . 'App/Autoloader.php';
    App_Autoloader::init();
  }

  protected function initCurrency(){

    if ( $this->multiCurrency ){
      //load currencies and set active currency
    }
  }

  protected function initLanguage(){
    $this->languages['ru'] = new Lang_Model( array( 'lang_alias' => 'ru', 'lang_name' => 'Russian' ) );
    $this->activeLanguage = $this->languages['ru'];
    if ( $this->multiLanguage ){
      //load languages and set active
    }
  }

  protected function initUser(){
    if ( $this->userCanLogin ){
      $this->user = User_Abstract::factory();
    } else {
      $this->user = new User_Reader();
    }
  }

  protected function initDb(){
    $dbConfig = Settings::database();
    $this->db = new Db_Connection(
      $dbConfig['host'],
      $dbConfig['user'],
      $dbConfig['password'],
      $dbConfig['dbname'],
      Debug::instance()
    );
  }

  public function switchDevDb(){
    $this->db = new Db_Connection(
      Config::get('database_dev')->host,
      Config::get('database_dev')->username,
      Config::get('database_dev')->password,
      Config::get('database_dev')->dbname,
      Debug::instance()
    );
  }

  public static function smarty(){
    return self::instance()->smarty;
  }

  public static function newSmarty(){
    $smarty = new App_Smarty();
    $smarty->assign(array('request' => App::getRequest(), 'user' => App::user()));
    return $smarty;
  }

  /**
  * @return Db_Query
  */
  public static function newQuery(){
    return new Db_Query();
  }

  /**
  * @return App_View
  *
  */
  public static function view(){
    return self::instance()->view;
  }

  /**
  * @return Db_Connection
  *
  */
  public function getConnection(){
    return $this->db;
  }

  public static function db(){
    return self::instance()->getConnection();
  }

  /**
  * @return App_Request
  */
  public static function getRequest(){
    if ( ! self::$instance->request instanceof App_Request ){
      self::$instance->request = new App_Request( $_GET, $_POST, $_REQUEST, $_SERVER );
    }
    return self::$instance->request;
  }

  public function activeCurrency(){
    return $this->activeCurrency;
  }

  public function activeLanguage(){
    return $this->activeLanguage;
  }

  public function getLanguages(){
    return $this->languages;
  }

  /**
  * @return User_Abstract
  */
  public function getUser(){
    return $this->user;
  }

  static public function user(){
    return self::instance()->getUser();
  }

  public function isMultiLanguage(){
    return $this->multiLanguage;
  }

  public function isMultiCurrency(){
    return $this->multiCurrency;
  }

  public function isDebug(){
    return $this->debug;
  }

  public function render(){
    App::view()->display();
    if ( $this->debug ){
      Debug::endDebug();
    }
  }

  public static function render404( $message = false ){
    if ( ! self::instance()->isDebug() ){
      $message = false;
    }
    self::instance()->view()->render404( $message );
  }

  public static function renderForbidden( $message = false ){
    if ( ! self::instance()->isDebug() ){
      $message = false;
    }
    self::instance()->view()->renderForbidden( $message );
  }

  public static function redirect( $url = '/' ){
    App_Request::redirect($url);
  }

  /**
  * redirect to login, if content need auth
  *
  */
  public static function forceAuth(){
    $_SESSION['auth']['forceUrl'] = $_SERVER['REQUEST_URI'];
    self::redirect('/login');
  }

  /**
  * redirect after login
  *
  */
  public static function redirectAuthHome(){
    if ( isset($_SESSION['auth']['forceUrl']) and !empty($_SESSION['auth']['forceUrl']) ){
      $url = $_SESSION['auth']['forceUrl'];
      unset($_SESSION['auth']['forceUrl']);
      self::redirect( $url );
    }
    self::redirect();
  }

  public function runSiteTesting(){
    require_once 'site_testing.php';
  }

  public function addRouteStatic( $pattern, $controller, $action, $smap = false ){
    App_Router::addRoute(new App_Route_Static(
      $pattern,
      false,
      $controller,
      $action,
      $smap
    ));
  }

  public function addRouteWildcard( $pattern, $controller, $action, $smap = false ){
    App_Router::addRoute(new App_Route_Wildcard(
      $pattern,
      false,
      $controller,
      $action,
      $smap
    ));
  }

  public function addRouteRegExp( $pattern, $controller, $action, $smap = false ){
    App_Router::addRoute(new App_Route_RegExp(
      $pattern,
      false,
      $controller,
      $action,
      $smap
    ));
  }


}

class AppException extends Exception{}
