<?php 
class Acl extends Singleton{
  
  static protected $instance = null;
  
  public $idRole = 1;
  protected $permissionLoaded = false;
  protected $permission = array();
  
  static public function factory($idRole = 1){
    $acl = self::getInstance();
    if ( is_numeric($idRole) and $idRole > 0 ){
      $acl->idRole = $idRole;
    }
    $acl->loadPermission();
    return $acl;
  }
  
  public function loadPermission(){
    if ( $this->permissionLoaded ){ return true; }
    $this->permissionLoaded = true;
    if ( ! $this->idRole ){ return; }
    $sql = "SELECT up.* FROM `user_permissions` up 
    JOIN `user_role_has_permission` urhp ON urhp.id_permission = up.id 
    AND urhp.id_role = '".(int)$this->idRole."'";
    $result = App::db()->query($sql);
    if ( $result ){
      while( $row = $result->fetch_assoc() ){
        $this->permission[$row['alias']] = $row;
      }
    }
  }
  
  /**
  * @return bool $hasPermission
  */
  public function hasPermission($permissionName){       
    if ( array_key_exists($permissionName, $this->permission) ){
      return true;
    }
    return false;
  }
  
  static public function getPermission(){
    return static::$instance->permission;
  }
  
}