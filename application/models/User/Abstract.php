<?php
class User_Abstract extends Dao{

  protected $externalFields = array(
    'role', 'role_name'
  );

  protected $loggedIn = false;

  static protected $table = 'users';
  static protected $pk = 'userid';

  public function loggedIn(){
    return $this->loggedIn;
  }

  static public function factory($userid = false){
    if ( ! $userid ){
      $userid = (isset($_SESSION['auth']['userid']) and is_numeric($_SESSION['auth']['userid']))
      ? (int)$_SESSION['auth']['userid']
      : 0;
    }
    if ( $userid ){
      $tableRow = static::fetchOne($userid);
      if ( isset($tableRow['role']) ){
        return new User_Stalexo($tableRow);
      }
    }
    return new User_Reader();
  }

  public function logout(){
    $_SESSION['auth'] = array();
    unset($_SESSION['auth']);
    $this->loggedIn = false;
  }

  public function hasPermission($permissionName){
    return Acl::factory($this->id_role)->hasPermission($permissionName);
  }

  static protected function wrapFetchQuery( Db_Query $query ){
    $query
    ->select('ur.alias as role')
    ->select('ur.name as role_name')
    ->join('user_roles as ur', 'ur.id = '. static::getTableName().'.id_role' );
    return $query;
  }

}
