<?php
class User_Gallery_Photo extends Dao {

	static protected $table = 'photogallery';
	
	static protected $pk = 'id';
  
  static protected $tableQueryAlias = 'pg';
	
	public function afterLoad(){
    $this->loadResizedPhotos();
  }
	
	public function loadResizedPhotos() {
		$sql = ' SELECT * '
		        .' FROM `photogallery_photo` '
		        ." WHERE `id_photo` = '" . $this->id . "' ";
		$res = App::db()->query($sql);
		if (!$res) {
			return false;
		}
		
		while ( $row = $res->fetch_assoc() ) {
			$this->children[$row['id_size']] = new User_Gallery_PhotoResized( $row );
		}	
	}
  
  public function getBySize($size){
    if ( isset($this->children[$size]) ){
      return $this->children[$size];
    }
    return new User_Gallery_PhotoResized( array('file_name'=>'', 'id' => 0 ) );
  }
	
	static public function getFileName ( $filePath ) {
		return preg_replace( '#(\.[a-zA-X]+)#', '', basename( $filePath ));
	}
	
	static public function getFileExtension ( $filePath ) {
		preg_match( '#(\.[a-zA-X]+)#', $filePath, $match );
		
		if ( !empty($match) ) {
			return $match[0];
		} 
		
		return '';
	} 
  
}