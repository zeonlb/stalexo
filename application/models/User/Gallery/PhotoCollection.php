<?php 
class User_Gallery_PhotoCollection extends Collection{
  
  protected function initQuery(){
    $this->query = User_Gallery_Photo::getFetchQuery()->enableFoundRows();
    $this->query->where('pg.visible = "yes"');
  }
  
  public function LoadUserPhotogallery( User_Abstract $owner ) {
    $this->query
    ->join('photogallery_types pt', 'pt.id = pg.id_type')
    ->where('pt.name = "Пользователи" AND id_obj = '.$owner->userid);
    
    $result = $this->executeQuery();
    
    if ( !$result ){
      return false;
    }
    
    while( $row = $result->fetch_assoc() ){
      $this->add( new User_Gallery_Photo( $row ) );
    }
  }
  
}