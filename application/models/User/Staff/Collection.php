<?php 
class User_Staff_Collection extends Collection{
  
  public function initQuery(){
    $this->query = User_Staff_Model::getFetchQuery();
  }
  
  public function loadUsers(){
    $this->query
    ->leftJoin('staff_device sd', 'sd.id_staff = staff.id')
    ->where('sd.device IS NULL');
    
    $result = $this->executeQuery();
    if ( $result ){
      while( $row = $result->fetch_assoc() ){
        $this->add(new User_Staff_Model($row));
      }
    }
  }
  
}