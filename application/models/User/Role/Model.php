<?php
class User_Role_Model extends Dao{

  static protected $pk    = 'id';
  static protected $table = 'user_roles';

  public function loadPermissionList(){
    $q = User_Permission_Model::getFetchQuery()
    ->select('IF(urhp.id_role, 1, 0) as allowed')
    ->select('upg.name as group_name')
    ->join('user_permission_groups upg', 'upg.id = user_permissions.id_group')
    ->join('user_roles ur')
    ->leftJoin('user_role_has_permission urhp', 'urhp.id_role = ur.id AND urhp.id_permission = user_permissions.id')
    ->where('ur.id = "'. $this->id .'"')->orderBy('user_permissions.id_group, user_permissions.id'); 
    $this->children['permissions'] = User_Permission_Model::fetchByQuery($q); 
  }
  
  public function unjoinPermissions(){
    App::db()->query("DELETE FROM `user_role_has_permission` WHERE id_role = '". $this->id ."'");
  }
  
  public function joinPermission($id){
    App::db()->query("INSERT IGNORE INTO `user_role_has_permission` SET id_role = '".$this->id."', id_permission = '". (int)$id ."'");
  }
  
}