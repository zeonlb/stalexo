<?php
abstract class Collection extends App_Iterator{

  /**
  * @var array 
  */
  protected $filters = array();
  protected $pager = null;
  
  protected $allCount = 0;
  
  /**
  * @var Db_Query
  */
  protected $query;
  
  public function __construct(){
    $this->initPager();
    $this->initQuery();
  }
  
  protected function initQuery(){
    $this->query = App::newQuery()->enableFoundRows();
  }
  
  protected function initPager(){
    $this->pager = new Pager_Simple();
  }
  
  public function attachFilter( Filter_Abstract $filter ){
    Filter_Abstract::addMarker( $filter->getMarker() );
    $this->filters[$filter->getMarker()] = $filter;
  }
  
  protected function executeFilters(){
    foreach ( $this->filters as $filter ){
      $filter->execute( $this->query );
    }
  }
  
  public function getAllCount(){
    return $this->pager()->allCount;
  }
  
  public function executeQuery(){
    $this->executeFilters();
    $result = $this->db()->query($this->query);
    if ( $result and $this->query->isEnableFoundRows() ){
      $count = $this->db()->query("SELECT FOUND_ROWS() as `count`");
      $count = $count->fetch_assoc();
      $this->pager()->allCount = $count['count'];
    }
    return $result;
  }
  
  public function pager(){
    return $this->pager;
  }
  
  public function db(){
    return App::db();
  }
  
  public function query(){
    return $this->query;
  }
  
}