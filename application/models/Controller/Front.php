<?php
class Controller_Front{

  protected $request = null;

  protected $action = 'index';
  protected $controller = 'index';
  protected $module = '';
  protected $smapName = null;
  protected $preventAction = false;

  public function __construct(){

  }

  public function run( App_Request $request ){
    $this->request = $request;
    $this->parseUrl();
    //$this->loadSmap();
    //$this->parseSiteOptions();
    $this->action();
  }

  protected function action(){
    if ( $this->preventAction ) {
      return false;
    }
    $controller = new $this->controller;
    if ( method_exists( $controller, $this->action ) ){
      $controller->{$this->action}();
    } else {
      $controller->errorAction();
    }
  }

  public function loadSmap(){
    if ( ! $this->request->isAjax() ){
      $smap = new Smap_Model( $this->smapName );
      if ( $smap->isLoaded() ){
        App::view()->setSmap( $smap );
        if ( $smap->type == 'page' ){
          $this->preventAction = true;
        }
      }
    }
  }

  public function parseSiteOptions(){
    $options = unserialize(file_get_contents( Settings::path()->dir . Settings::path()->smap . 'siteOptions.txt'));
    Registry::set('siteOptions', $options);
  }

  public function parseUrl(){
    $getFriendly = array_merge(
      $this->request->get(),
      explode(
        '/',
        preg_replace('/(^\/)|(\.php$)|(\.html$)|(index\.php$)|(index\.html$)|(\.html\?.+$)|(\?.+$)/i',
          '',
          preg_replace('|^/|',
            '',
            $this->request->getUri()
      )))
    );
    $this->request->setGet($getFriendly);

    if ( isset($getFriendly[0]) and $getFriendly[0] ){
      $this->controller = $getFriendly[0];
    }
    if ( isset($getFriendly[1]) and $getFriendly[1] ){
      $this->action = $getFriendly[1];
    }

    $route = App_Router::getRoute( $this->controller . '/' . $this->action );
    if ( $route ){
      $this->controller = $route->controller;
      $this->action = $route->action;
      $this->smapName = $route->smap;
    }

    if ( ! Valid::alias($this->controller . $this->action) ){
      throw new Exception('Invalid url');
    }

    $this->request->setController( $this->controller );
    $this->request->setAction( $this->action );

    $this->controller = ucfirst(self::parseAction($this->controller)) . 'Controller';
    $this->action = self::parseAction($this->action) . 'Action';

    try {
      if ( class_exists($this->controller) and is_subclass_of($this->controller, 'Controller_Module') ){

        $moduleController = new $this->controller();

        if ( ! method_exists($moduleController, $this->action) ){
          $this->module = ucfirst(str_replace('Controller', '', $this->controller));
          $controllerName = ucfirst(str_replace('Action', '', $this->action));
          $this->controller = $this->module .'_'. $controllerName.'Controller';
          $actionName = 'index';
          if ( App::getRequest()->get(2) and is_string(App::getRequest()->get(2)) ){
            $actionName = self::parseAction(App::getRequest()->get(2));
          }
          $this->action = $actionName . 'Action';

          $this->request->setController( strtolower($controllerName) );
          $this->request->setAction( $actionName );
          $this->request->setModule( strtolower($this->module) );
        }
      }
    } catch( App_AutoloaderException $e ){
      App::render404('controller '. $this->controller .' not found');
    }

    $this->smapName = $this->smapName ? $this->smapName : ($this->module ? strtolower($this->module).'-'.strtolower($controllerName) : strtolower(str_replace(array('Controller', 'ModuleController'), '', $this->controller)));
  }

  public static function parseAction( $name ){
    $actionName = preg_replace_callback(
      '@[-]+\w@' ,
      function($match){
        $match = str_replace( '-', '', $match[0]);
        return strtoupper($match);
      },
      $name
    );
    return $actionName;
  }

  public function getModuleName(){
    return $this->module;
  }

  public function getControllerName(){
    return $this->controller;
  }

  public function getActionName(){
    return $this->action;
  }

  public function setRequest( App_Request $request ){
    $this->request = $request;
  }

}