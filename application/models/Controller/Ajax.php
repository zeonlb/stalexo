<?php 
abstract class Controller_Ajax{
  
  protected $smarty = null;
  
  protected $output = array( 'status' => 0 );
  
  public function __construct(){
    $this->smarty = App::newSmarty();
    $this->init();
  }
  
  protected function init(){
    
  }
  
  public function getOutput(){
    return $this->output;
  }
  
}