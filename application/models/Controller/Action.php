<?php
abstract class Controller_Action{

  protected $smarty = null;

  public function __construct(){
    $this->smarty = App::newSmarty();
    $this->init();
  }

  protected function init(){}

  public function smarty(){
    return $this->smarty;
  }

  public function render( $content ){
    App::view()->add($content);
  }

  abstract public function indexAction();

  public function errorAction(){
    $this->indexAction();
  }

  public function ajaxAction(){
    $output = array( 'status' => 0, 'error' => array() );
    $ajaxControllerName = str_replace( 'Controller', 'AjaxController', get_class($this));

    if ( App::getRequest()->isAjax() ){
      try{
        $ajaxController = new $ajaxControllerName();
        if ( ! $ajaxController instanceof Controller_Ajax ){
          throw new Exception('Controller instance class should be a subclass of Controller_Ajax');
        }
        $ajaxAction = App::getRequest()->request('action');

        if( $ajaxAction ){
          $ajaxAction = Controller_Front::parseAction($ajaxAction) . 'Action';
          if ( method_exists( $ajaxController, $ajaxAction ) ) {
            $ajaxController->{$ajaxAction}();
          }
        }
        $output = $ajaxController->getOutput();
      } catch( Exception $e ){
        $output['error'][] = /*"invalid request"*/$e->getMessage();
      }
    }
    if ( is_array($output) ){
      $output = json_encode($output);
    }
    echo $output;
    exit();
  }

}