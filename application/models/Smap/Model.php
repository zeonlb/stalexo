<?php
class Smap_Model extends Dao {

  static protected $table = 'smap_pages';

  static protected $pk = 'id';

  static protected $alias = 'alias';

  public function afterLoad() {
    if ( $this->isLoaded() and $this->type == 'folder' ) {
      $this->loadChildrenPages();
    }		 
  }

  protected function loadChildrenPages() {
    $lang_s = App::instance()->activeLanguage()->lang_alias;

    $query = ' SELECT * '
    .' FROM `' . static::$table . '` '
    ." WHERE `id_par` = '" . $this->{static::$pk} . "' AND `visible_" . $lang_s . "` = 'public_on' "
    ." ORDER BY prior ";
    $res = $this->db()->query( $query );

    if ( !$res ) {
      return false;
    }

    while ( $row = $res->fetch_assoc() ) { 
      $this->children[] = new Smap_Model($row);
    }
  }

  public function getPageContent() {
    if ( !$this->isLoaded() ) {
      return '';
    }
    $lang_s = App::instance()->activeLanguage()->lang_alias;
    $content = '';
    $modelAlias = static::$alias;
    $pagePath = Settings::path()->dir . Settings::path()->smap . $this->{$modelAlias} . '_' . $lang_s . '.html';
    if( file_exists($pagePath) ){
      $content = $this->getTextFromFile( $this->{$modelAlias} . '_' . $lang_s, 'html', Settings::path()->smap );
    }

    $pageContent = iconv('windows-1251', 'utf8', $content);  
    return $pageContent;
  }	

  public function getTextFromFile($file_name, $file_ext, $dir){
    $dir_path = Settings::path()->dir . $dir;
    if (file_exists($dir_path . $file_name . '.' . $file_ext)) {
      if ($file = fopen($dir_path . $file_name . '.' . $file_ext, 'r')) {
        $content = '';
        while (!feof($file))
          $content .= fread($file, 8192);
        fclose($file);
        if($content==='<br/>') $content = "";
        return $content;
      } else {
        throw new Exception('File is unreadeable');
      } 
    } else {
      throw new Exception('File not exists');
    } 
  } 
  
  static public function loadByClass( $id ){
    if ( ! is_numeric($id) ){
      return false;
    }
    $smapArray = array();
    $sql = "SELECT * FROM `smap_pages` WHERE id_class = '".$id."' AND locked = 'visible' AND id_par = 0 ORDER BY prior";
    $result = App::db()->query($sql);
    if ( $result ){
      while( $row = $result->fetch_assoc() ){
        $smapArray[] = new Smap_Model($row);
      }
    }
    return $smapArray;
  }

} 
