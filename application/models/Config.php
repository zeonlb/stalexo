<?php
class Config extends Registry{
  
  static protected $instance = null;
  
  static protected $iniFilename = "config.ini";
  
  protected function parseIniFile(){
    $configPath = Settings::path()->application . static::$iniFilename;
    if ( ! file_exists($configPath) ){
      throw new ConfigException("Config not found");
    }
    $configValues = parse_ini_file( $configPath, true );
    foreach ( $configValues as $key => $val ){
      $this->_set( $key, $this->makeObjectsFromArray($val) );
    }
  }
  
  protected function __construct(){
    $this->parseIniFile();
  }
  
  protected function makeObjectsFromArray( $val ){
    if ( is_array($val) ){
      foreach( $val as $k => $v ){
        $val[$k] = $this->makeObjectsFromArray($v);
      }
      return (object)$val; 
    }
    return $val;
  }
  
  public static function setIniFilename( $filename ){
    static::$instance = null;
    static::$iniFilename = $filename;
  }
  
}

class ConfigException extends Exception {}