<?php 
class Layout{
  
  protected $tpl = "layout.tpl";
  protected $smarty;
  protected $html = "";
  protected $content = "";
  protected $args = array();
  protected $initArgs = array();
  
  public function __construct( $content = '', $args = array() ){
    $this->smarty = App::newSmarty();
    $this->args = $args;
 
    if ( isset($args['tpl']) ){
      $this->tpl = $args['tpl'];
    }
    $this->content = $content;
    $this->init();
  }
  
  protected function init(){}
  
  public function setContent( $content ){
    $this->clearHtml();
    $this->content = $content;
    return $this;
  } 
  
  public function setArgs( Array $args ){
    $this->clearHtml();
    $this->args = $args;
    return $this;
  }
  
  public function addArgs( Array $args ){
    $this->clearHtml();
    $this->args = array_merge($this->args, $args);
    return $this;
  } 
    
  protected function render(){
    $this->args['content'] = $this->content;
    $this->smarty->assign($this->args);   
    $this->html = $this->smarty->fetch( $this->tpl );
    return $this->html;
  }
  
  public function __toString(){
    $this->beforeRender();
    return $this->render();
  }
  
  public function beforeRender(){
    
  }
  
  protected function clearHtml(){
    $this->html = '';
  }
  
}