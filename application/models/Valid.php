<?php
class Valid{

  public static function email( $email ){
    $isValid = filter_var( $email, FILTER_VALIDATE_EMAIL );
    return (bool)$isValid;
  }

  public static function password( $password ){
    $isValid = filter_var( $password, FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '@^[A-Za-z0-9\-\+\*\_\.]{5,15}$@iu')) );
    return (bool)$isValid;
  }

  public static function username( $username ){
    $isValid = filter_var( $username, FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '@^[a-zа-яьїіёъ\-\`]{2,50}$@iu')) );
    return (bool)$isValid;
  }

  public static function fullname( $fullname ){
    $isValid = filter_var( trim($fullname), FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '@^[a-zа-яьїіёъ\-\`\s]{2,50}$@iu')) );
    return (bool)$isValid;
  }

  /**
  * Format yyyy-mm-dd
  *
  * @param string $date
  */
  public static function date( $date ){
    $isValid = filter_var( $date, FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '@^\d{4}-\d{2}-\d{2}$@iu')) );
    return (bool)$isValid;
  }

  /**
  * Check if format dd:dd
  * @param string $time
  * @return bool
  */
  public static function time( $time ) {
    if ( preg_match('@^\d{2}:\d{2}$@', $time) ){
      return true;
    }
    return false;
  }

  /**
  * @return bool
  * Captcha validation; dependent from project
  *
  * @param string $val
  */
  public static function captcha( $val ){
    if ( isset($_SESSION['image_value']) and $_SESSION['image_value'] ==  md5(strtolower( $val )) ){
      $_SESSION['image_value'] = false;
      return true;
    }
    return false;
  }

  public static function alias( $val ){
    if ( preg_match('@^[A-Za-z][A-Za-z0-9-_]{2,50}$@', $val) ){
      return true;
    }
    return false;
  }

  public static function message( $val, $minLength = 1, $maxLength = 500 ){
    $len = mb_strlen($val);
    if ( ($len > $minLength) or ($len < $maxLength) ){
      return true;
    }
    return false;
  }

  public static function phone( $phone ){
    if ( preg_match('@^\+?[0-9\-\s\(\)]{10,18}$@i', $phone) ){
      return true;
    }
    return false;
  }

  public static function address( $address ){
    return self::message( $address, 4, 200 );
  }

  public static function latLng( $val ){
    if ( preg_match('@^[-]?[0-9]{2,3}\.[0-9]{2,8}$@', $val) ) {
      return true;
    }
    return false;
  }

  public static function id1c( $val ){
    if ( preg_match('@^[0-9]{4,11}$@i', $val) ){
      return true;
    }
    return false;
  }

  /**
  * check if date match dd.mm.yyyy
  *
  * @param string $val
  */
  public static function date1c( $val ){
    if( preg_match('@^[0-9]{2}\.[0-9]{2}\.[0-9]{4}$@', $val) ){
      return true;
    }
    return false;
  }

  public static function quantity( $val ){
    return preg_match('@^[0-9]+(\.[0-9]{1,3})?$@', $val);
  }

  public static function day($val){
    return preg_match('@^[1|2|3|4|5|6|7]$@', $val);
  }

}