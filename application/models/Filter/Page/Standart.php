<?php 
class Filter_Page_Standart extends Filter_Abstract{
  
  protected $marker = 'page';
  
  protected $pager = null;
  
  protected function wrapQuery( Db_Query $query ){
    $this->pager->page = ( (isset($this->args[0]) and is_numeric($this->args[0])) ? (int)$this->args[0] : 1 );
    $query->limit( (($this->pager->page - 1)*$this->pager->onPage), $this->pager->onPage );
    return $query;
  }
  
  public function __construct( Pager_Simple $pager ){
    $this->pager = $pager;    
    parent::__construct();
  }
  
}