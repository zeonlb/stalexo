<?php
class Filter_Member_Role extends Filter_Abstract {
  
  protected $marker = 'role';
  
  protected function wrapQuery(Db_Query $query){
    if ( count($this->args) ) {     
      $permissible = array('coach', 'coach_company', 'company', 'customer');
      $preserved = array_intersect($permissible, $this->args);
      
      $query
      ->join("vb_usergroup ug", "ug.usergroupid = vb_user.usergroupid AND ug.title = 'Зарегестрированый'")
      ->where("vbr.alias IN ('".implode("', '", $preserved)."')");
    }
    return $query;  
  }
     
}