<?php 
class Filter_Member_Geo extends Filter_Abstract{
  
  protected $marker = 'geo';
  
  protected function wrapQuery( Db_Query $query ){
    if ( count($this->args) ){
      if ( preg_match('@^([0-9]+)-?([0-9]+)?$@', $this->args[0], $matches) ){
        $country = $matches[1];
        $city = isset($matches[2]) ? $matches[2] : 0;
        $query
        ->join('user_join_prod ujp1', 'ujp1.id_user = vb_user.userid')
        ->join('production p1', "p1.id_prod = ujp1.attached_prod AND p1.prod_deleted = 'no' AND p1.prod_visible = 'public'")
        ->join('prod_geography pg', 'pg.id_prod = p1.id_prod')
        ->where('pg.country_id = "'. $country .'"')
        ->clearGroupBy()->groupBy('vb_user.userid');
        if ( $city ){
          $query->where('pg.city_id = "'. $city .'"');
        }
      }
    }
    return $query;
  }
  
}