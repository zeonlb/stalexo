<?php 
class Filter_Member_Category extends Filter_Abstract{
  
  protected $marker = 'members';
  
  protected function wrapQuery(Db_Query $query){
    Registry::set('activeMemberCategory', new Training_Folder_Model());
    
    if ( count($this->args) ){
      $category = new Training_Folder_Model($this->args[0]);      
      
      if ( ! $category->isLoaded() ){
        App::render404('Invalid alias');
      }

      Registry::set('activeMemberCategory', $category);
      
      $query
      ->join('user_join_prod ujp', 'ujp.id_user = vb_user.userid')
      ->join('production p', "p.id_prod = ujp.attached_prod AND p.prod_deleted = 'no' AND p.prod_visible = 'public'")
      ->join('prod_join_cat pjc', 'pjc.id_prod = p.id_prod')
      ->join('prod_category pc', 'pc.id_cat = pjc.id_cat')
      ->where('pc.cat_alias IN ("'. $category->cat_alias .'")')
      ->clearGroupBy()->groupBy('vb_user.userid');
    }  
    return $query;
  }
  
}