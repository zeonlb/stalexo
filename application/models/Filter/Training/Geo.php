<?php 
class Filter_Training_Geo extends Filter_Abstract{
  
  protected $marker = 'geo';
  
  protected function wrapQuery( Db_Query $query ){
    if ( count($this->args) ){
      if ( preg_match('@^([0-9]+)-?([0-9]+)?$@', $this->args[0], $matches) ){
        $country = $matches[1];
        $city = isset($matches[2]) ? $matches[2] : 0;
        $query->where('pg.country_id = "'. $country .'"');
        if ( $city ){
          $query->where('pg.city_id = "'. $city .'"');
        }
      }
    }
    return $query;
  }
  
}