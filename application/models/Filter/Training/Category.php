<?php 
class Filter_Training_Category extends Filter_Abstract{
  
  protected $marker = 'trainings';
  
  protected function wrapQuery(Db_Query $query){
    Registry::set('activeTrainingCategory', new Training_Folder_Model());
    
    if ( count($this->args) ){
      $category = new Training_Folder_Model($this->args[0]);      
      
      if ( ! $category->isLoaded() ){
        App::render404('Invalid alias');
      }

      Registry::set('activeTrainingCategory', $category);
      $query->where('pc.cat_alias IN ("'. $category->cat_alias .'")');  
    
    }  
    return $query;
  }
  
}