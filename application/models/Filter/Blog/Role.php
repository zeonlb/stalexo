<?php
class Filter_Blog_Role extends Filter_Abstract {
  
  protected $marker = 'role';
  
  protected function wrapQuery(Db_Query $query){
    if ( count($this->args) > 0 ) {     
      $query
      ->join('vb_user u', 'u.userid = n.userid')
      ->join('vb_user_roles ur', "ur.id = u.id_role AND ur.alias = '".App::db()->escape($this->args[0])."'");  
    }
    return $query;  
  }
     
}