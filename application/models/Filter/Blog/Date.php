<?php
class Filter_Blog_Date extends Filter_Abstract {
  
  protected $marker = 'date';
  
  protected function wrapQuery(Db_Query $query) {
    if ( count($this->args) == 1 && $this->args[0] === 'asc' ) {
      $query->clearOrderBy()->orderBy( News_Model::getTableQueryAlias().'.id_news ASC' );  
    }
    return $query;  
  }
     
}