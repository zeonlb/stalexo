<?php
class Filter_Blog_Popularity extends Filter_Abstract {
  
  protected $marker = 'popularity';
  
  protected function wrapQuery(Db_Query $query) {
    if ( count($this->args) == 1 && $this->args[0] === 'asc' ) {
      $query->clearOrderBy()->orderBy( News_Model::getTableQueryAlias().'.views_cnt ASC' );  
    }
    return $query;  
  }
     
}