<?php
class Filter_Blog_Tag extends Filter_Abstract {
  
  protected $marker = 'tags';
  
  protected function wrapQuery( Db_Query $query ) {
    if ( App::getRequest()->get(0) === 'blog-tags' ) {     
      $query
      ->join('news_ex_join_tags njt', 'njt.id_news = n.id_news')
      ->join('news_ex_tags nt', 'nt.id_tag = njt.id_tag AND nt.tag_name = "'.App::db()->escape(str_replace('_', ' ', urldecode( App::getRequest()->get(1) ))).'" ');
    }
    return $query;
  } 
   
}