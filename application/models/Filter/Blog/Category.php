<?php
class Filter_Blog_Category extends Filter_Abstract {
  
  protected $marker = 'blog';
  
  protected function wrapQuery(Db_Query $query){
    $group = Registry::get('activeBlogGroup');
    if ( $group && $group->isLoaded() ) {     
      $query->where('ng.alias IN ("'. $group->alias .'")');  
    }
    return $query;  
  }
     
}