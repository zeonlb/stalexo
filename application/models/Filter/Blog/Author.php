<?php
class Filter_Blog_Author extends Filter_Abstract {
  
  protected $marker = 'author';
  
  protected function wrapQuery(Db_Query $query) {
    if ( count($this->args) ) {
      $query->where('n.userid = '.(int)$this->args[0]);  
    }
    return $query;  
  }
     
}