<?php 
class Filter_Shop_Media extends Filter_Abstract{
  
  protected $marker = 'media';
  
  protected function wrapQuery(Db_Query $query){
    if (count($this->args) == 1){
      $query->join('attribute_values av_media', 'av_media.id_obj = p.id_prod AND av_media.id_atree = 2 AND av_media.id_variant = "'. (int)$this->args[0] .'"');
    }    
    return $query;
  }
  
}