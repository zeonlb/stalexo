<?php 
class Filter_Shop_Order extends Filter_Abstract{
  
  const BY_RANK         = 'rank';
  const BY_AVAILABILITY = 'availability';
  const BY_PRICE        = 'price';
  
  protected $marker = 'order';
  
  static protected $currentOrderType;
  
  protected function wrapQuery( Db_Query $query ){
    if ( count($this->args) ){
      $order = str_replace('-','',$this->args[0]);
      $orderMethodName = 'orderBy'.ucfirst($order) ;
      if ( method_exists($this, $orderMethodName) ){
        $this->{$orderMethodName}( $query );
      }
    }
  }
  
  protected function orderByPricelow( Db_Query $query ){
    $this->orderByPrice($query);
  }
  
  protected function orderByPricehigh( Db_Query $query ){
    $this->orderByPrice($query,'DESC');
  }
  
  protected function orderByRankhigh( Db_Query $query ){
    $this->orderByRank($query,'DESC');
  }
  
  protected function orderByPrice( Db_Query $query, $order = 'ASC' ){
    self::$currentOrderType = self::BY_PRICE;
    $query->clearOrderBy()->orderBy('pr.price '.$order);
  }
  
  protected function orderByRank( Db_Query $query, $order = 'ASC' ){
    self::$currentOrderType = self::BY_RANK;
    $query->clearOrderBy()->orderBy('comments_rank '.$order);
  }
  
  protected function orderByAvailability( Db_Query $query ){
    self::$currentOrderType = self::BY_AVAILABILITY;
    $query->join('attribute_values av_order', 'av_order.id_obj = p.id_prod AND av_order.id_atree = 55')->clearOrderBy()->orderBy('av_order.val_value DESC, p.prod_prior');
  }
  
  static public function getOrderType(){
    if ( ! self::$currentOrderType ){
      self::$currentOrderType = self::BY_RANK;
    }
    return self::$currentOrderType;
  }
  
}