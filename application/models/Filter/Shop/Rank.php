<?php 
class Filter_Shop_Rank extends Filter_Abstract{
  
  protected $marker = 'rank';
  
  protected function wrapQuery( Db_Query $query ){
    if ( count($this->args) ){
      $order = (isset($this->args[0]) and $this->args[0] == 'best') ? 'DESC' : false;
      if ( $order ) {
        $query->clearOrderBy()->orderBy('comments_rank ' . $order);
      }
    }
  }
   
}