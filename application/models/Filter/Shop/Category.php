<?php 
class Filter_Shop_Category extends Filter_Abstract{
  
  protected $marker = 'category';
  
  protected function wrapQuery(Db_Query $query){
    $alias = App::getRequest()->get(0);
    Registry::set('activeShopCategory', new Shop_Folder_Model());
    
    if ( preg_match('@^shop-([a-zA-Z0-9\-_]+)$@', $alias, $matches) ) {     
      $alias = $matches[1];
      $category = new Shop_Folder_Model($alias);      
      
      if ( ! $category->isLoaded() ){
        Request::render404('Invalid alias');
      }

      Registry::set('activeShopCategory', $category);
      $query->where('pc.cat_alias IN ("'. $category->cat_alias .'")');  
    }
      
    return $query;
  }
  
}