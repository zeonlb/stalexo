<?php 
class Filter_Shop_Price extends Filter_Abstract{
  
  protected $marker = 'price';
  
  protected function wrapQuery( Db_Query $query ){
    if ( count($this->args) ){
      $price1 = (isset($this->args[0]) and is_numeric($this->args[0])) ? abs($this->args[0]) : 0;
      $price2 = (isset($this->args[1]) and is_numeric($this->args[1])) ? abs($this->args[1]) : 0;
      if ( $price1 or $price2 ) {
        $query->where('pr.price BETWEEN '. min($price1,$price2) .' AND '. max($price1,$price2) );
      }
    }
  }
  
  static public function getRange(){
    $query = Shop_Product_Model::getFetchQuery()
    ->clearGroupBy()->groupBy('pc2.id_cat')
    ->clearSelect()
    ->select('MIN(FLOOR(pr.price)) as min')
    ->select('MAX(CEIL(pr.price)) as max');
    $range = array('min' => 0, 'max' => 10000);
    $result = App::db()->query($query);
    if ( $result ){
      $row = $result->fetch_assoc();
      $range = $row;
    }
    return (object)$range;
  }
   
}