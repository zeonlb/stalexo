<?php 
abstract class Filter_Abstract{
  
  protected $marker = '';
  
  protected $args = array();
  
  static protected $parameters = null;
  
  static protected $isModified = false;
  
  static protected $markers = array();
  
  static protected function parseRequest( App_Request $request ){
    $parameters = array();
    
    $argsList = $request->get();
    
    foreach ( $argsList as $value ) {      
      if ( in_array( $value, self::$markers ) ) {
        $filterMarker = $value;
        continue;
      }
      if ( !isset($filterMarker) || empty($filterMarker) ) {
        continue;
      }
      
      if ( !empty($value) ) {
        $parameters[$filterMarker][] = $value; 
      }
    }
    self::$parameters = $parameters;
  }
  
  static public function getParameters( $marker = false ){
    if ( self::$parameters == null or self::$isModified ){
      self::parseRequest( App::getRequest() );
    }
    if ( $marker ){
      if ( isset(self::$parameters[$marker]) ){
        return self::$parameters[$marker];
      }
      return array();
    }
    return self::$parameters;
  }
  
  /**
  * @param string $marker
  */
  static public function addMarker( $marker ){
    self::$isModified = true;
    if ( ! preg_match( '@[A-Za-z0-9-_]{2,50}@', $marker ) ){
      throw new Exception('Invalid marker name');
    }
    if ( ! array_key_exists( $marker, self::$markers ) ){
      self::$markers[$marker] = $marker;
    }
  }
  
  /**
  * @param string $marker
  */
  static public function dropMarker( $marker ){
    if( array_key_exists( $marker, self::$markers ) ) {
      self::$isModified = true;
      unset(self::$markers[$marker]);
    }
  }
  
  final public function execute( Db_Query $query ){
    $this->args = self::getParameters($this->marker);
    $this->wrapQuery($query);
  }
  
  abstract protected function wrapQuery( Db_Query $query );
  
  public function getMarker(){
    return $this->marker;
  }
  
  public function setMarker($marker){
    $this->marker = $marker;
  }
  
  public function __construct(){

  }
  
  /**
  * return filters url part ignoring $markers
  *  
  * @param array $markers
  */
  static public function getUrl( $markers = array() ){
    $parameters = self::getParameters();
    $url = '';
    foreach( $parameters as $marker => $args ){
      if ( ! in_array( $marker, $markers ) ){
        $url .= '/' . $marker . '/' . implode( '/', $args );
      }
    }
    return $url;
  }
  
}