<?php
class Filter_News_Category extends Filter_Abstract {
  
  protected $marker = 'news';
  
  protected function wrapQuery( Db_Query $query ) {
    $group = Registry::get('activeNewsGroup');
    if ( $group && $group->isLoaded() ) {     
      $query->where('ng.alias IN ("'. App::db()->escape($group->alias) .'")');  
    }
    return $query;
  } 
  
  public function setMeta( News_Group_Model $group ){
    if ( $group->isLoaded() ){
//      App::view()->title = $category->cat_title;
//      App::view()->keywords = $category->meta_keywords;
//      App::view()->description = $category->meta_description;
//      Registry::set('activeCategory', $category);
    }
  }
   
}