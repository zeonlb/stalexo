<?php 
class SessionRegistry {
  
  public static function get( $key ){
    return isset($_SESSION[$key]) ? $_SESSION[$key] : null;
  }
  
  public static function set( $key, $value ){
    $_SESSION[$key] = $value;
    return true;
  }
  
  public static function remove( $key, $key2 = null, $key3 = null ){
    if ( $key3 ){
      unset($_SESSION[$key][$key2][$key3]);
    } if ( $key2 ){
      unset($_SESSION[$key][$key2]);
    } else {
      unset($_SESSION[$key]);
    }
  }
  
}