<?php
/**
* Data access object
*/
abstract class Dao extends App_Iterator{

  protected $loaded = false;

  /**
  * Fields from joined tables, ignoring while saving object
  *
  * @var Array
  */
  protected $externalFields = array();

  public $children = array();

  static protected $table = null;
  static protected $pk = null;
  static protected $alias = null;

  /**
  * Table alias, using in query
  *
  * @var string
  */
  static protected $tableQueryAlias = null;

  public function __construct( $key = null ){
    if ( is_numeric($key) ){
      $key = static::fetchOne($key);
    } elseif( is_string($key) and is_string(static::$alias) ){
      $key = static::fetchOne($key, static::$alias);
    }

    if ( is_array($key) ){
      $this->populate($key);
      if ( $this->{static::$pk} ){
        $this->loaded = true;
        $this->afterLoad();
      }
    }
  }

  public function __get( $prop ){
    if ( ! isset($this->array[$prop]) ){
      return null;
    }
    return $this->array[$prop];
  }

  public function __set( $prop, $value ){
    $this->array[$prop] = $value;
  }

  public function isLoaded(){
    return $this->loaded;
  }

  protected function afterLoad(){}
  
  public function getAsArray(){
    return $this->array;
  }

  /**
  * Populate object by array
  *
  * @param array $row
  */
  protected function populate( Array $row ){
    foreach ($row as $key => $val) {
      $this->{$key} = $val;
    }
  }

  /**
  * Build object by array
  *
  * @param array $row
  * @return static
  */
  static public function build( Array $row ){
    return new static($row);
  }

  public function save($lazy = true){
    $setQuery = array();

    foreach( $this->array as $k => $v ){
      if ( in_array($k, $this->externalFields) ){
        continue;
      }
      $setQuery[] = "`" . $k . "` =  '" . $this->db()->escape($v) . "' ";
    }

    $setQuery = implode( ', ', $setQuery );

    if( $this->isLoaded() ){
      $sql = "UPDATE `".static::$table."` SET " . $setQuery . " WHERE `".static::$pk."` = '".$this->db()->escape($this->{static::$pk})."' LIMIT 1";
    } else {
      $sql = "INSERT INTO `".static::$table."` SET " . $setQuery;
    }

    $result = $this->db()->query($sql);
    $this->loaded = false;
    if ( $result ){
      $this->loaded = true;
      if ( ! $this->{static::$pk} ){
        $this->{static::$pk} = $this->db()->insert_id;
        $this->__construct($this->{static::$pk});
      } elseif( ! $lazy ){
        $this->__construct($this->{static::$pk});
      }
    }
    return $result;
  }

  public function delete(){
    if ( $this->{static::$pk} ){
      $result = $this->db()->query("DELETE FROM `".static::$table."` WHERE ".static::$pk." = ".$this->db()->escape($this->{static::$pk})." LIMIT 1");
      return $result;
    }
    return false;
  }

  public function db(){
    return App::db();
  }

  public function children( $key = false ){
    if ( ! $key ){
      return $this->children;
    }
    if (  isset($this->children[$key]) ){
      return $this->children[$key];
    }
    return null;
  }

  public static function fetch( $value = null, $key = null, $limit = null ){
    $sql = static::getFetchQuery( $value, $key, $limit );
    $result = App::db()->query($sql);

    $rows = array();
    if ( $result ){
      while( $row = $result->fetch_assoc() ){
        $rows[] = $row;
      }
    }
    return $rows;
  }

  /**
  *
  * @param mixed $value
  * @param mixed $key
  * @param int $limit
  * @return Db_Query
  */
  static public function getFetchQuery( $value = null, $key = null, $limit = null ){
    if ( ! is_string($key) ){
      $key = static::$pk;
    }
    $query = App::newQuery();
    $query->select( '`'.static::getTableQueryAlias().'`.*')->from('`' . static::$table . '` `'.static::getTableQueryAlias().'`');
    if ( ! is_null($value) ){
      $query->where( '`'.static::getTableQueryAlias()."`." .App::db()->escape($key). " = '".App::db()->escape($value)."' " );
    }
    if ( is_int($limit) ){
      $query->limit($limit);
    }
    return static::wrapFetchQuery($query);
  }

  /**
  * @return Array of static
  *
  * @param Db_Query $query
  */
  public static function fetchByQuery( $query, $asArray = false ){
    $items = array();
    $result = App::db()->query($query);
    if ( $result ){
      if ( $asArray ){
        while( $row = $result->fetch_assoc() ){
          $items[] = $row;
        }
      } else {
        while( $row = $result->fetch_assoc() ){
          $items[] = new static($row);
        }
      }
    }
    return $items;
  }

  public static function fetchAll($asArray = false){
    return static::fetchByQuery(static::getFetchQuery(), $asArray);
  }

  static protected function wrapFetchQuery( Db_Query $query ){
    return $query;
  }

  public static function fetchOne( $value, $key = null ){
    $row = static::fetch( $value, $key, 1 );
    return array_shift($row);
  }

  public static function getPk(){
    return static::$pk;
  }

  public function resetPk(){
    $this->{static::getPk()} = null;
    $this->loaded = false;
  }

  public static function getTableName(){
    return static::$table;
  }

  public static function getAlias(){
    return static::$alias;
  }

  /**
  * @return string queryAlias - alias using in sql; e.x. production p, p - alias
  */
  public static function getTableQueryAlias(){
    if( static::$tableQueryAlias ){
      return static::$tableQueryAlias;
    }
    return static::$table;
  }

}