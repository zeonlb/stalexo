<?php
class Lang_Model extends Dao {
  
	static protected $table = 'languages';
	
	static protected $pk = 'id_lang';
	
	static protected $alias = 'lang_alias';
	
	public function isActive() {
		if ( App::instance()->activeLanguage() === $this ) {
			return true;
		} 
		return false;
	}  
    
  public static function fetchAll(){
    $languages = array();
    $langs = self::fetch();
    foreach( $langs as $lang ){
      $languages[$lang['lang_alias']] = new Lang_Model($lang);
    }
    return $languages;
  }
	
}