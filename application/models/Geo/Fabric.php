<?php
class Geo_Fabric {
	static function getGeoObject( $indexFieldName, $id ) {
		if ( $id == 0 ) {
			return null;
		}
		
		switch( $indexFieldName ) {
			case 'continent_id':
				return new Geo_ContinentModel( $id );
				break;
			case 'country_id':
				return new Geo_CountryModel( $id );
				break;
			case 'region_id':
				return new Geo_RegionModel( $id );
				break;
			case 'city_id':
				return new Geo_CityModel( $id );
				break;
			case 'district_id':
				return new Geo_DistrictModel( $id );
				break;
		}
		return null;
	}
}