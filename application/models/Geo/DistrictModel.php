<?php
class Geo_DistrictModel extends AbstractModel implements Geo_Interface {
	protected $tableName = 'wr_district';
	
	protected $tableIndex = 'district_id';
	
	protected $tableAlias = 'alias';
	
	public function getParentInfoArray() {
		return array( 'country_id' => $this->country_id
		             ,'region_id' => $this->region_id
		             ,'city_id' => $this->city_id
		             ,'district_id' => $this->district_id );
	}
	public function getChildrenObjects() {
		return null;
	}
}