<?php
interface Geo_Interface{
	
	public function getParentInfoArray();
	
	public function getChildrenObjects();

}