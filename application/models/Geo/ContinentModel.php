<?php
class Geo_ContinentModel extends AbstractModel implements Geo_Interface {
  static protected $pk = 'continent_id';
  static protected $table = 'wr_continent';
  static protected $getTableQueryAlias = 'cntt';
}