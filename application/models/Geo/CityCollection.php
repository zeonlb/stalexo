<?php
class Geo_CityCollection extends Collection{
  
  protected function initQuery(){
    $this->query = Geo_CityModel::getFetchQuery()->enableFoundRows();
  }
  
  public function loadCitiesJSON(){
    
    
    
    $this->query
    ->disableFoundRows()
    ->orderBy( Geo_CityModel::getTableQueryAlias().'.name ASC' );

    $result = $this->executeQuery();
    
    if ( !$result ){
      return false;
    }
    
    while( $row = $result->fetch_assoc() ){
      $cities[$row['country_id']][$row['city_id']] = $row;
    }
    
    $this->citiesJSON = json_encode($cities);
  }

}