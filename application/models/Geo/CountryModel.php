<?php
class Geo_CountryModel extends Dao {
  static protected $pk = 'country_id';
  static protected $table = 'wr_country';
  static protected $getTableQueryAlias = 'cnt';
}