<?php
class Geo_CountryCollection extends Collection{
  
  protected function initQuery(){
    $this->query = Geo_CountryModel::getFetchQuery()->enableFoundRows();
  }
  
  public function loadCountries(){
    $this->query
    ->disableFoundRows()
    ->orderBy( Geo_CountryModel::getTableQueryAlias().'.name ASC' );

    $result = $this->executeQuery();
    
    if ( !$result ){
      return false;
    }
    
    while( $row = $result->fetch_assoc() ){
      $this->add( new Geo_CountryModel( $row ) );
    }
  }

}