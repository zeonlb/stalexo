<?php
class Geo_CityModel extends Dao {
  static protected $pk = 'city_id';
  static protected $table = 'wr_city';
  static protected $getTableQueryAlias = 'ct';
}