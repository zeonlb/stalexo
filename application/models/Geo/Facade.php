<?php
class Geo_Facade {

  /**
  * @return array $cities
  */
  static public function getCountries(){
    $countries = array();
    $query = Geo_CountryModel::getFetchQuery()->orderBy('name');
    $result = App::db()->query($query);
    if ( $result ){
      while( $row = $result->fetch_assoc() ){
        $countries[] = $row;
      }
    }
    return $countries;
  }

  /**
  * @param int $idCountry
  * @return array $cities
  */
  static public function getCities($idCountry){
    $cities = array();
    $query = Geo_CityModel::getFetchQuery()->where('country_id = "'. (int)$idCountry .'"')->orderBy('name');
    $result = App::db()->query($query);
    if ( $result ){
      while( $row = $result->fetch_assoc() ){
        $cities[] = $row;
      }
    }
    return $cities;
  }

  static public function getDistance($lat1 = 0, $lng1 = 0, $lat2 = 0, $lng2 = 0){
    $distance = 6371 * acos(sin(deg2rad($lat1))*sin(deg2rad($lat2))+cos(deg2rad($lat1))*cos(deg2rad($lat2))*cos(deg2rad($lng2)-deg2rad($lng1)));
    return $distance;
  }

}