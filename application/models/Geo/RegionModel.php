<?php
class Geo_RegionModel extends AbstractModel implements Geo_Interface {
	protected $tableName = 'wr_region';
	
	protected $tableIndex = 'region_id';
	
	protected $tableAlias = 'alias';
	
	public $childrenObj = 'CityModel';
	
	public function getParentInfoArray() {
		return array( 'country_id' => $this->country_id
					 ,'region_id' => $this->region_id
                     );
	}
	
	public function getChildrenObjects() {
		return AbstractModel::getAllModels( 'Geo_CityModel'
		                                   ,array( 'WHERE_STMT'=>array($this->tableIndex ."= '". $this->region_id ."'"
		                                                              )
		                                          )
		                                  );
	}
}