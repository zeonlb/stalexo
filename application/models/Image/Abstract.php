<?php
abstract class Image_Abstract{
  
  public function resizeImage( $image, $width, $height, $scale) {
    list( $imagewidth, $imageheight, $imageType ) = getimagesize($image);
    $imageType = image_type_to_mime_type($imageType);
    $newImageWidth = ceil( $width * $scale );
    $newImageHeight = ceil( $height * $scale );
    $newImage = imagecreatetruecolor( $newImageWidth, $newImageHeight );
    switch($imageType) {
      case "image/gif":
        $source=imagecreatefromgif($image); 
        break;
      case "image/pjpeg":
      case "image/jpeg":
      case "image/jpg":
        $source=imagecreatefromjpeg($image); 
        break;
      case "image/png":
      case "image/x-png":
        $source=imagecreatefrompng($image); 
        break;
      }
    imagecopyresampled($newImage,$source,0,0,0,0,$newImageWidth,$newImageHeight,$width,$height);
    
    switch($imageType) {
      case "image/gif":
          imagegif($newImage,$image); 
        break;
          case "image/pjpeg":
      case "image/jpeg":
      case "image/jpg":
          imagejpeg($newImage,$image,90); 
        break;
      case "image/png":
      case "image/x-png":
        imagepng($newImage,$image);  
        break;
    }
    
    chmod($image, 0777);
    return $image;
  }
  
  public function resizeThumbnailImage( $thumb_image_name, $image, $width, $height, $start_width, $start_height, $scale ){
    list( $imagewidth, $imageheight, $imageType ) = getimagesize($image);
    $imageType = image_type_to_mime_type($imageType);  
    $newImageWidth = ceil($width * $scale);
    $newImageHeight = ceil($height * $scale);
    $newImage = imagecreatetruecolor( $newImageWidth, $newImageHeight );
    switch($imageType) {
      case "image/gif":
        $source=imagecreatefromgif($image); 
        break;
      case "image/pjpeg":
      case "image/jpeg":
      case "image/jpg":
        $source=imagecreatefromjpeg($image); 
        break;
      case "image/png":
      case "image/x-png":
        $source=imagecreatefrompng($image); 
        break;
      }
    imagecopyresampled($newImage,$source,0,0,$start_width,$start_height,$newImageWidth,$newImageHeight,$width,$height);
    switch($imageType) {
      case "image/gif":
          imagegif($newImage,$thumb_image_name); 
        break;
      case "image/pjpeg":
      case "image/jpeg":
      case "image/jpg":
          imagejpeg($newImage,$thumb_image_name,90); 
        break;
      case "image/png":
      case "image/x-png":
        imagepng($newImage,$thumb_image_name);  
        break;
      }
    chmod($thumb_image_name, 0777);
    return $thumb_image_name;
  }
  
  /**
  * @return image height
  */
  protected function getHeight( $image ) {
    $size = getimagesize($image);
    $height = $size[1];
    return $height;
  }
  
  /**
  * @return image width
  */
  protected function getWidth( $image ) {
    $size = getimagesize($image);
    $width = $size[0];
    return $width;
  }
  
  abstract public function upload( $filesToken );
  
  abstract public function setConfiguration( $config );
  
  public static function changeFileLocation( $oldPath, $newPath ){
    if ( file_exists($oldPath) ){
      $filename = basename($oldPath);
      $extension = strtolower(substr($filename, strrpos($filename, '.') + 1));
      $newPath .= "." . $extension;
      if ( $newPath == $oldPath ){
        return false;
      }
      if ( file_exists( $newPath ) ){
        unlink($newPath);
      }
      $result = rename( $oldPath, $newPath );
      if( $result ){
        return $newPath;
      }
    }
    return false;
  }
  
  /**
  * Resize image
  * 
  * @param string $img         - source image
  * @param float $width
  * @param float $height
  * @param string $newFilename - destination image
  */
  public static function resize( $img, $width, $height, $newFilename ){
    $imgInfo = getimagesize( $img );

    switch ( $imgInfo[2] ){
      case 1: $im = imagecreatefromgif( $img ); break;
      case 2: $im = imagecreatefromjpeg( $img ); break;
      case 3: $im = imagecreatefrompng( $img ); break;
      default: throw new ResizingFailedException(); break; 
    }
    //IF smaller - not resize   
    if ( $imgInfo[0] <= $width and $imgInfo[1] <= $height ){
      $newHeight = $imgInfo[1];
      $newWidth = $imgInfo[0];
    } else {
      if( $width/$imgInfo[0] < $height/$imgInfo[1] ){
        $newWidth = $width;
        $newHeight = $imgInfo[1] * ( $width/$imgInfo[0] );
      } else {
        $newWidth = $imgInfo[0] * ( $height/$imgInfo[1] );
        $newHeight = $height;
      }
    }
    
    $newWidth = round($newWidth);
    $newHeight = round($newHeight);
    
    $newImage = imagecreatetruecolor( $newWidth, $newHeight );
    
    //IF PNG or GIF set it transparent
    if ( $imgInfo[2] == 1 or $imgInfo[2] == 3 ){
      imagealphablending( $newImage, false );
      imagesavealpha( $newImage, true );
      $transparent = imagecolorallocatealpha( $newImage, 255, 255, 255, 127 );
      imagefilledrectangle( $newImage, 0, 0, $newWidth, $newHeight, $transparent );
    }

    imagecopyresampled( $newImage, $im, 0, 0, 0, 0, $newWidth, $newHeight, $imgInfo[0], $imgInfo[1] );
    switch ( $imgInfo[2] ){
      case 1: imagegif( $newImage, $newFilename ); break;
      case 2: imagejpeg( $newImage, $newFilename, 100); break;
      case 3: imagepng( $newImage, $newFilename ); break;
      default: throw new ResizingFailedException(); break;
    }
    return $newFilename;
  }  
  
}

class NoImageSelectedException extends Exception{}

class InvalidImageExtensionException extends Exception{}

class InvalidImageFilesizeException extends Exception{}

class ResizingFailedException extends Exception{}