<?php 
class Image_Avatar extends Image_Abstract{
   
  protected $uploadDir = "img_user";
  protected $uploadPath = "/";
  protected $largePrefix = "large_";
  protected $thumbPrefix = "thumb_";
  protected $smallPrefix = "small_";
  
  protected $largeSize = array( 'heigth' => 100, 'width' => 100 );
  protected $thumbSize = array( 'heigth' => 100, 'width' => 100 );
  protected $smallSize = array( 'heigth' => 16, 'width' => 16 );
  
  protected $largeMaxWidth = 500;
  protected $minHeigth = 100;
  protected $userId;
  /**
  *  @user User_AbstractModel
  */
  protected $user;
  protected $allowedTypes = array(
    'image/pjpeg' => "jpg",
    'image/jpeg' => "jpeg",
    'image/jpeg' => "jpg",
    'image/jpg' => "jpg",
    'image/png' => "png",
    'image/x-png' => "png",
    'image/gif' => "gif"
  );
  protected $allowedImageExt = array();
  protected $largeImageLocation;
  protected $thumbImageLocation;
  protected $smallImageLocation;  
  
  /**
  * max filesize in megabytes
  */
  protected $maxFilesSize = 2;
  
  public function setConfiguration( $config ){
    if ( !is_array($config) ){
      return false;
    }
    $this->uploadDir = isset( $config['uploadDir'] ) ? $config['uploadDir'] : $this->uploadDir;
    $this->uploadPath = isset( $config['uploadPath'] ) ? $config['uploadPath'] : $this->uploadPath;
    
    $this->maxFilesSize = isset( $config['maxFilesSize'] ) ? $config['maxFilesSize'] : $this->maxFilesSize;
    
    $this->largePrefix = isset( $config['largePrefix'] ) ? $config['largePrefix'] : $this->largePrefix;
    $this->thumbPrefix = isset( $config['thumbPrefix'] ) ? $config['thumbPrefix'] : $this->thumbPrefix;
    $this->smallPrefix = isset( $config['smallPrefix'] ) ? $config['smallPrefix'] : $this->smallPrefix;
    
    $this->largeSize = isset( $config['largeSize'] ) ? $config['largeSize'] : $this->largeSize;
    $this->thumbSize = isset( $config['thumbSize'] ) ? $config['thumbSize'] : $this->thumbSize;
    $this->smallSize = isset( $config['smallSize'] ) ? $config['smallSize'] : $this->smallSize;
    
    $this->minHeigth = $this->largeSize['width'];
    
    $this->largeImageLocation = $this->uploadDir . $this->uploadPath . $this->largePrefix . $this->userId;
    $this->thumbImageLocation = $this->uploadDir . $this->uploadPath . $this->thumbPrefix . $this->userId;
    $this->smallImageLocation = $this->uploadDir . $this->uploadPath . $this->smallPrefix . $this->userId;
  }
  
  public function __construct( User_AbstractModel $user ){
    $this->user = $user;
    $this->userId = $user->userid;
    $this->allowedImageExt = array_unique( $this->allowedTypes );     
  }  
  
  public function upload( $filesToken ){
    
    if ( !isset($_FILES[$filesToken]) ){
      throw new NoImageSelectedException("No image selected");
    }
    
    $userfile_name = $_FILES[$filesToken]['name'];
    $userfile_tmp = $_FILES[$filesToken]['tmp_name'];
    $userfile_size = $_FILES[$filesToken]['size'];
    $userfile_type = $_FILES[$filesToken]['type'];
    $filename = basename($_FILES[$filesToken]['name']);
    $file_ext = strtolower(substr($filename, strrpos($filename, '.') + 1));
    
    //Only process if the file is a JPG, PNG or GIF and below the allowed limit
    if((!empty($_FILES[$filesToken])) && ($_FILES[$filesToken]['error'] == 0)) {
         
      $error = "extension";
      foreach ($this->allowedTypes as $mime_type => $ext) {
        //loop through the specified image types and if they match the extension then break out
        //everything is ok so go and check file size
        if( $file_ext == $ext && $userfile_type == $mime_type ){
          $error = "";
          break;
        }
      }
      if ( $error == "extension" ){
        throw new InvalidImageExtensionException("Invalid extension");
      }
    
      //check if the file size is above the allowed limit
      if ( $userfile_size > ( $this->maxFilesSize * 1048576 )) {
        throw new  InvalidImageFilesizeException("Images must be under ".$this->maxFilesSize."MB in size");
      }
      
    } else{
      throw new NoImageSelectedException("No image selected");
    }
    //Everything is ok, so we can upload the image.
    if ( strlen($error) == 0 ){
      
      if (isset($_FILES[$filesToken]['name'])){
        //this file could now has an unknown file extension (we hope it's one of the ones set above!)
        $this->largeImageLocation = $this->largeImageLocation . "." . $file_ext;
        $this->thumbImageLocation = $this->thumbImageLocation . "." . $file_ext;
        
        move_uploaded_file( $userfile_tmp, $this->largeImageLocation );
        chmod( $this->largeImageLocation, 0777 );
        
        $width = $this->getWidth($this->largeImageLocation);
        $heigth = $this->getHeight($this->largeImageLocation);
        if ( $width < $this->largeSize['width'] or $heigth < $this->minHeigth ){
          unlink( $this->largeImageLocation );
          throw new AvatarMinWidthException("defectively width");
        }
        $height = $this->getHeight($this->largeImageLocation);
        //Scale the image if it is greater than the width set above
        if ($width > $this->largeSize['width'] ){
          $scale = $this->largeSize['width']/$width;
          $uploaded = $this->resizeImage( $this->largeImageLocation, $width, $height, $scale );
        }else{
          $scale = 1;
          $uploaded = $this->resizeImage( $this->largeImageLocation, $width, $height, $scale );
        }
      }
      
      return $this->largeImageLocation;
    }
    return false;
  }
  
  public function uploadThumb( $x1, $y1, $x2, $y2, $w, $h ){
    if ( !$this->user->avatar_large ){
      throw new NoImageSelectedException("No image Selected");
    }
    //Scale the image to the thumb_width set above
    $scale = $this->thumbSize['width']/$w;
    
    $cropped = $this->resizeThumbnailImage( $this->thumbImageLocation, $this->largeImageLocation, $w, $h, $x1, $y1, $scale );
    return $this->thumbImageLocation;
  }
  
  public function uploadSmall( $x1, $y1, $x2, $y2, $w, $h ){
    if ( !$this->user->avatar_large ){
      throw new NoImageSelectedException("No image Selected");
    }
    //Scale the image to the thumb_width set above
    $scale = $this->smallSize['width']/$w;
    
    $cropped = $this->resizeThumbnailImage( $this->smallImageLocation, $this->largeImageLocation, $w, $h, $x1, $y1, $scale );
    return $this->smallImageLocation;
  }
  
  public function getImageLocation(){
    return $this->largeImageLocation;
  }
  
  public function getThumbLocation(){
    return $this->thumbImageLocation;
  }
  
  /**
  * Handle img uploading and echo filename for iframe
  */
  public function uploadUserAvatar( $filesToken ){
    try{
      $avatarPath = $this->upload( $filesToken );
      //Delete the thumbnail file so the user can create a new one
      if ( $this->user->avatar_thumb and file_exists( $this->user->avatar_thumb )) {
        unlink( $this->user->avatar_thumb );
      }
      if ( $this->user->avatar_small and file_exists( $this->user->avatar_small ) ) {
        unlink( $this->user->avatar_small );
      }
      $this->user->avatar_thumb = "";
      $this->user->avatar_small = "";
      $this->user->avatar_large = $avatarPath;
      $this->user->updateModel();
      echo $this->user->avatar_large; exit();
    } catch( NoImageSelectedException $e ){
      echo "Изображение не выбрано"; exit();
    } catch( InvalidImageExtensionException $e ){
      echo "Недопустимый формат изображения"; exit();
    } catch( InvalidImageFilesizeException $e ){
      echo "Размер изображения больше допустимого"; exit();
    } catch ( AvatarMinWidthException $e ){
      echo "Загрузите изображение размером не мение (".$this->minHeigth."х".$this->largeSize['width'].")"; exit();
    }      
  }
  
  public function uploadResizedUserAvatar( $filesToken, $x1, $y1, $x2, $y2, $w, $h ){
    $largeImgLocation = $this->user->avatar_large;
    $this->largeImageLocation = $largeImgLocation;
    $extension = strtolower( substr( $this->largeImageLocation, strrpos( $this->largeImageLocation, "." )+1 ) );
    $this->thumbImageLocation .= "." . $extension;
    $this->smallImageLocation .= "." . $extension;
    try {
      $thumbPath = $this->uploadThumb( $x1, $y1, $x2, $y2, $w, $h );
      $smallPath = $this->uploadSmall( $x1, $y1, $x2, $y2, $w, $h );
    } catch( NoImageSelectedException $e ){
      echo "noImage"; exit(); 
    }
    
    $this->user->avatar_thumb = $thumbPath;
    $this->user->avatar_small = $smallPath;
    $this->user->updateModel();
    
    echo $this->user->avatar_small; exit();
  }
  
}

class AvatarMinWidthException extends Exception{}