<?php 
class Image_EventCover extends Image_Abstract{
  
  protected $defaults = array( 
    'max_file_size' => 2,
    'min_height' => 300,
    'min_width' => 930,
    'upload_path' => 'img_user/',
    'max_width' => 1000 
  );
  
  protected $allowedTypes = array(
    'image/pjpeg' => "jpg",
    'image/jpeg' => "jpeg",
    'image/jpeg' => "jpg",
    'image/jpg' => "jpg",
    'image/png' => "png",
    'image/x-png' => "png",
    'image/gif' => "gif"
  );
  
  protected $allowedImageExt = array();
  
  protected $user = null;
  
  protected $config = null;
  
  protected $imageLocation;
  protected $imageResizedLocation;
  
  public function __construct( User_AbstractModel $user ){
    $this->user = $user;
    $this->config = (object)$this->defaults;
  }
  
  public function setConfiguration( $config ){
    $this->config = $config;
  }
  
  public function upload( $filesToken ){
    if ( !isset($_FILES[$filesToken]) ){
      throw new NoImageSelectedException("No image selected");
    }    
    $userfile_name = $_FILES[$filesToken]['name'];
    $userfile_tmp = $_FILES[$filesToken]['tmp_name'];
    $userfile_size = $_FILES[$filesToken]['size'];
    $userfile_type = $_FILES[$filesToken]['type'];
    $filename = basename($_FILES[$filesToken]['name']);
    $file_ext = strtolower(substr($filename, strrpos($filename, '.') + 1));
    $this->imageLocation = $this->config->upload_path . "tmp_" . $this->user->userid . "." . $file_ext;
    //Only process if the file is a JPG, PNG or GIF and below the allowed limit
    if((!empty($_FILES[$filesToken])) && ($_FILES[$filesToken]['error'] == 0)) {
         
      $error = "extension";
      foreach ($this->allowedTypes as $mime_type => $ext) {
        //loop through the specified image types and if they match the extension then break out
        //everything is ok so go and check file size
        if( $file_ext == $ext && $userfile_type == $mime_type ){
          $error = "";
          break;
        }
      }
      if ( $error == "extension" ){
        throw new InvalidImageExtensionException("Invalid extension");
      }
    
      //check if the file size is above the allowed limit
      if ( $userfile_size > ( $this->config->max_file_size * 1048576 )) {
        throw new  InvalidImageFilesizeException("Images must be under ".$this->config->max_file_size." MB in size");
      }
      
    } else{
      throw new NoImageSelectedException("No image selected");
    }
    
    //Everything is ok, so we can upload the image.
    if ( strlen($error) == 0 ){
      
      if (isset($_FILES[$filesToken]['name'])){
        //this file could now has an unknown file extension (we hope it's one of the ones set above!)
        
        move_uploaded_file( $userfile_tmp, $this->imageLocation );
        chmod( $this->imageLocation, 0777 );
        
        $width = $this->getWidth( $this->imageLocation );
        $height = $this->getHeight( $this->imageLocation );
        if ( $width < $this->config->image_width or $height < $this->config->image_height ){
          unlink( $this->imageLocation );
          throw new InvalidDimensionException("Invalid image dimension");
        }

        //Scale the image if it is greater than the width set above
        if ( $width > $this->config->max_width ){
          $scale = $this->config->max_width / $width;
          $uploaded = $this->resizeImage( $this->imageLocation, $width, $height, $scale );
        }else{
          $scale = 1;
          $uploaded = $this->resizeImage( $this->imageLocation, $width, $height, $scale );
        }

        /* TODO: delete resized image for event */
      }
      $this->user->tmp_image = $this->imageLocation;
      $this->user->updateModel();
      return $this->imageLocation;
    }
    return false;
  }
  
  public function uploadCover( $filesToken ){   
    try {
      $path = $this->upload( $filesToken );
      echo $path;
    } catch( NoImageSelectedException $e ){
      echo " Изображение не выбрано ";
    } catch ( InvalidDimensionException $e ){
      echo "Недопустимый размер изображения. Загрузите изображение размером не мение (".$this->config->image_width."х".$this->config->image_height.") ";
    } catch( InvalidImageFilesizeException $e ){
      echo "Изображение слишком большое";
    } catch ( InvalidImageExtensionException $e ){
      echo "Недопустимый формат изображения. Разрешены GIF, PNG, JPG";
    }
    exit();
  }
  
  /**
  * Required x,y,x1,y1,heigth,width, And make resized thumb from user tmp file (vb_user.tmp_file)
  * AJAX
  */
  public function makeResizedCover( $x1, $y1, $x2, $y2, $w, $h  ){
    $largeImgLocation = $this->user->tmp_image;

    $extension = strtolower( substr( $largeImgLocation, strrpos( $largeImgLocation, "." )+1 ) );
    
    $coverPath = "img_user/events/tmp_". $this->user->userid ."." . $extension;
    
    try {
      if ( !file_exists( $largeImgLocation ) ){
        throw new NoImageSelectedException("No image selected");
      }
      $scale = $this->config->image_width / $w;    
      $cropped = $this->resizeThumbnailImage( $coverPath, $largeImgLocation, $w, $h, $x1, $y1, $scale );
    } catch( NoImageSelectedException $e ){
      echo "noImage"; exit(); 
    }
        
    echo $coverPath; exit();  
  }
  
} 

class InvalidDimensionException extends Exception{}