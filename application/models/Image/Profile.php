<?php 
class Image_Profile extends Image_Abstract{
  
  protected $owner;
  
  protected $size = array(
    'thumb' => array(
      'width'  => 220,
      'height' => 220
    ),
    'big'   => array(
      'width'  => 198,
      'height' => 198
    )
  );
  
  protected $maxFileSize = 5;
  
  protected $allowedTypes = array(
    'image/pjpeg' => "jpg",
    'image/jpeg' => "jpeg",
    'image/jpeg' => "jpg",
    'image/jpg' => "jpg",
    'image/png' => "png",
    'image/x-png' => "png",
    'image/gif' => "gif"
  );
  
  protected $tmpUploadPath = 'img_user/user/tmp/';
  
  public function __construct(User_Abstract $owner){
    $this->owner = $owner;
  }
  
  public function upload($filesToken){
    if ( ! isset($_FILES[$filesToken]) ){
      throw new NoImageSelectedException("No image selected");
    }    
    $userfileName = $_FILES[$filesToken]['name'];
    $userfileTmp = $_FILES[$filesToken]['tmp_name'];
    $userfileSize = $_FILES[$filesToken]['size'];
    $userfileType = $_FILES[$filesToken]['type'];
    $filename = basename($_FILES[$filesToken]['name']);
    $fileExt = strtolower(substr($filename, strrpos($filename, '.') + 1));
    
    if ( $userfileSize > ($this->maxFileSize * 1048576) ){ throw new InvalidImageFilesizeException(); }
    
    $invalidType = true;
    foreach( $this->allowedTypes as $mimeType => $ext ){
      if ( $userfileType == $mimeType and $fileExt == $ext ){
        $invalidType = false;
      }
    }
    
    if ( $invalidType ){ throw new InvalidImageExtensionException(); }
    
    $imageOriginalLocation = $this->tmpUploadPath . $this->owner->userid . '_origin.' . $fileExt;
    $imageThumbLocation = $this->tmpUploadPath . $this->owner->userid . '_thumb.' . $fileExt;
    move_uploaded_file( $userfileTmp, $imageOriginalLocation );
    chmod( $imageOriginalLocation, 0777 );
    
    $width = $this->getWidth( $imageOriginalLocation );
    $height = $this->getHeight( $imageOriginalLocation );    
  
    $thumb = self::resize($imageOriginalLocation, $this->size['thumb']['width'], $this->size['thumb']['height'], $imageThumbLocation); 
    
    $this->owner->tmp_img       = $imageOriginalLocation;
    $this->owner->tmp_img_thumb = $thumb;
    $this->owner->save();
    
    return $thumb;
  }
  
  public function setConfiguration($args){
    
  }
  
}