<?php
class Import_Xml_DPContract extends Import_Xml_Abstract{

  public function execute(){
    $dpContractsArr = array();
    foreach ( $this->xml as $item ){
      $attr = $item->attributes();
      $dpContractsArr[] = array(
        'id'           => (string)$attr->Code,
        'title'        => (string)$attr->Order,
        'id_client'    => (string)$attr->KontragentCode,
        'id_manager'   => (string)$attr->ManagerCode,
        'order_type'   => (string)$attr->OrderType,
        'id_price_type'=> (string)$attr->PriceTypeCode,
        'amount'       => (string)$attr->Debet
      );
    }

    foreach( $dpContractsArr as $val ){
      $val['amount'] = $this->toNumeric($val['amount'], 2);

      if ( ! $val['id_price_type'] ){
        $val['id_price_type'] = '000000000';
      }
      if ( ! $val['id_manager'] ){
        $val['id_manager'] = '000000000';
      }

      $dpContract = new Stalexo_Dp_Contract_Model($val['id']);
      if( stripslashes($dpContract->title)           != stripslashes($val['title'])
        or $dpContract->id_client      != $val['id_client']
        or $dpContract->id_manager     != $val['id_manager']
        or $dpContract->id_price_type  != $val['id_price_type']
        or $dpContract->amount         != $val['amount']
        or $dpContract->order_type     != $val['order_type'] ){
                  echo "<pre>";
          var_dump($val);
          Debug::var_dump($dpContract);
        $dpContract->id            = $val['id'];
        $dpContract->title         = $val['title'];
        $dpContract->id_client     = $val['id_client'];
        $dpContract->id_manager    = $val['id_manager'];
        $dpContract->id_price_type = $val['id_price_type'];
        $dpContract->order_type    = $val['order_type'];
        $dpContract->amount        = $val['amount'];
        $dpContract->save();
        $this->affected++;
      }
    }

  }

}