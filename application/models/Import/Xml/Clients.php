<?php
class Import_Xml_Clients extends Import_Xml_Abstract{

  public function execute(){
    $groups = array();
    $clients = array();

    foreach ( $this->xml as $item ){
      $attr = $item->attributes();
      $groups[(string)$attr->CodeGroup] = array('id' => (string)$attr->CodeGroup, 'title' => (string)$attr->Group);
      $clients[] = array( 'id' => (string)$attr->Code, 'id_group' => (string)$attr->CodeGroup, 'title' => (string)$attr->Kontragent, 'amount' => (string)$attr->Debet );
    }
    //Update clients_group
    foreach ( $groups as $g ){
      $group = new Stalexo_Client_Group_Model($g['id']);
      if( $group->title != $g['title'] ){
        $group->id = $g['id'];
        $group->title = $g['title'];
        $group->save();
      }
    }
    //Update clients
    foreach ( $clients as $c ){
      $c['amount'] = $this->toNumeric($c['amount'], 2);


      $client = new Stalexo_Client_Model($c['id']);

      if ( $client->id_group != $c['id_group'] or $client->id != $c['id'] or $client->title != $c['title'] or $client->amount != $c['amount'] ){
        $client->id       = $c['id'];
        $client->id_group = $c['id_group'];
        $client->title    = $c['title'];
        $client->amount   = $c['amount'];
        $client->save();
        $this->affected++;
      }

    }

  }

}