<?php
class Import_Xml_SkuPrice extends Import_Xml_Abstract{

  public function execute(){
    $priceArr = array();

    foreach ( $this->xml as $item ){
      $attr = $item->attributes();
      $priceArr[] = array(
        'id'            => (string)$attr->Code,
        'id_price_type' => (string)$attr->PriceCode,
        'date'          => (string)$attr->Date,
        'price'         => (string)$attr->Price
      );
    }

    foreach( $priceArr as $val ){
      if ( ! trim($val['price']) ){
        $val['price'] = '0.00';
      }
      $val['price'] = str_replace(',','.',$val['price']);
      $val['price'] = number_format($val['price'], 2, '.', '');
      $val['date'] = Date::mySql2PhpTime($val['date']);

      $idSku       = $val['id'];
      $idPriceType = $val['id_price_type'];
      $date        = $val['date'];
      $price       = $val['price'];

      $result = App::db()->query("
        SELECT * FROM `sku_prices` WHERE
        id_sku = '". $idSku ."' AND id_price_type = '". $idPriceType ."' AND date = '". $date ."'
        LIMIT 1
      ");

      if ( $result and ($result->num_rows) ){
        $row = $result->fetch_assoc();
        if ( $row['id_sku'] == $idSku and $row['id_price_type'] == $idPriceType and $row['price'] == $price ){
          continue;
        }
        $q = "UPDATE `sku_prices` SET price = '". $price ."' WHERE
        id_sku = '". $idSku ."'
        AND id_price_type = '". $idPriceType ."'
        AND date = '". $date ."'
        LIMIT 1";
      } else {
        $q = "INSERT INTO `sku_prices` SET
        id_sku = '". $idSku ."',
        id_price_type = '". $idPriceType ."',
        date = '". $date ."',
        price = '". $price ."'
        ";
      }
      App::db()->query($q);
      $this->affected++;
    }

  }

}
