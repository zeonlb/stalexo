<?php
class Import_Xml_SkuAvailability extends Import_Xml_Abstract{

  public function execute(){
    $availabilityArr = array();

    foreach ( $this->xml as $item ){
      $attr = $item->attributes();
      $availabilityArr[] = array(
        'id'       => (string)$attr->Code,
        'quantity' => (string)$attr->availabity,
        'id_stock' => (string)$attr->SkladCode
      );
    }

    foreach ( $availabilityArr as $val ){
      if( ! trim($val['quantity']) ) {
        $val['quantity'] = '0.000';
      }
      $availability = str_replace(array(' ', ','), array('', '.'), $val['quantity']);
      $availability = number_format($availability, 3, '.', '');
      $idSku = $val['id'];
      $idStorage = $val['id_stock'];

      $result = App::db()->query("SELECT * FROM `sku_availability`
      WHERE id_sku = '". $idSku ."' AND id_storage = '". $idStorage ."'
      LIMIT 1");

      if ( $result and ($result->num_rows) ){
        $row = $result->fetch_assoc();
        if ( $row['availability'] == $availability ){
          continue;
        }

        $q = "UPDATE `sku_availability` SET
        availability = '". $availability ."',
        WHERE id_sku = '". $idSku ."' AND id_storage = '". $idStorage ."'
        LIMIT 1";
      } else {
        $q = "INSERT INTO `sku_availability` SET
        id_sku = '". $idSku ."',
        id_storage = '". $idStorage ."',
        availability = '". $availability ."'
        ";
      }
      App::db()->query($q);
      $this->affected++;
    }

  }

}
