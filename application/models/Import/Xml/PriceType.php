<?php
class Import_Xml_PriceType extends Import_Xml_Abstract{

  public function execute(){
    $ptArr = array();
    foreach ( $this->xml as $item ){
      $attr = $item->attributes();
      $ptArr[] = array('id' => (string)$attr->Code, 'title' => (string)$attr->Type);
    }

    foreach( $ptArr as $val ){
      $pt = new Stalexo_PriceType_Model($val['id']);
      if( $pt->title != $val['title'] ){
        $pt->id      = $val['id'];
        $pt->title   = $val['title'];
        $pt->save();
        $this->affected++;
      }
    }

  }

}
