<?php
class Import_Xml_Abstract {

  protected $xml;
  public $affected = 0;

  public function __construct($filename){
    if ( ! file_exists($filename) ) { throw new ExceptionImportXml('Invalid source'); }
    $xml = simplexml_load_file($filename);
    if ( ! $xml ){ throw new ExceptionImportXml('Invalid file format'); }
    $this->xml = $xml;
  }

  public function execute(){}

  public function toNumeric($val, $precision = 2){
    if ( ! trim($val) ){ $val = '0'; }
    $val = str_replace(',', '.', $val);
    $val = preg_replace('@[^.\-0-9]@', '', $val);
    $val = number_format($val, $precision, '.', '');
    return $val;
  }

}

class ExceptionImportXml extends Exception{}