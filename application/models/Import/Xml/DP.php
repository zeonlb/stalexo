<?php
/**
* Import for distribution points
*/
class Import_Xml_DP extends Import_Xml_Abstract{

  public function execute(){
    $dpArr = array();
    foreach ( $this->xml as $item ){
      $attr = $item->attributes();
      $dpArr[] = array('id' => (string)$attr->Code, 'address' => (string)$attr->TT);
    }

    foreach( $dpArr as $val ){
      $dp = new Stalexo_Dp_Model($val['id']);
      if( $dp->address != $val['address'] ){
        $dp->id      = $val['id'];
        $dp->address = $val['address'];
        $dp->save();
        $this->affected++;
      }
    }

  }

}
