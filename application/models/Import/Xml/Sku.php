<?php
class Import_Xml_Sku extends Import_Xml_Abstract{

  public function execute(){
    $skuArr = array();
    $skuGroupsArr = array();

    foreach ( $this->xml as $item ){
      $attr = $item->attributes();
      $skuArr[] = array('id' => (string)$attr->Code, 'id_group' => (string)$attr->CodeGroup, 'title' => (string)$attr->Nomenclature, 'units' => (string)$attr->BaseOne);
      $skuGroupsArr[(string)$attr->CodeGroup] = array('id' => (string)$attr->CodeGroup, 'title' => (string)$attr->Group);
    }
    // update sku_groups
    foreach( $skuGroupsArr as $val ){
      $group = new Stalexo_Sku_Group_Model($val['id']);
      if( stripslashes($group->title) != stripslashes($val['title']) ){
        $group->id      = $val['id'];
        $group->title   = $val['title'];
        $group->save();
      }
    }

    // update sku
    foreach( $skuArr as $val ){
      if ( ! trim($val['id_group']) ){
        $val['id_group'] = '000000000';
      }
      $sku = new Stalexo_Sku_Model($val['id']);

      if( stripslashes($sku->title) != stripslashes($val['title']) or stripslashes($sku->units) != stripslashes($val['units']) or $sku->id_group != $val['id_group'] ){
        $sku->id       = $val['id'];
        $sku->title    = $val['title'];
        $sku->units    = $val['units'];
        $sku->id_group = $val['id_group'];
        $sku->save();
        $this->affected++;
      }
    }

  }

}