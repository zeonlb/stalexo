<?php
class Import_Xml_Staff extends Import_Xml_Abstract{

  public function execute(){
    $staffArr = array();
    foreach ( $this->xml as $item ){
      $attr = $item->attributes();
      $staffArr[] = array(
        'id'        => (string)$attr->Code,
        'name'      => (string)$attr->User,
        'id_parent' => (string)$attr->DirectorCode
      );
    }

    foreach( $staffArr as $val ){
      if ( ! trim($val['id_parent']) ) {
        $val['id_parent'] = 0;
      }
      $staff = new User_Staff($val['id']);
      if( trim($staff->name) != trim($val['name']) or $staff->id_parent != $val['id_parent'] ){
        $staff->id        = $val['id'];
        $staff->id_parent = $val['id_parent'];
        $staff->name      = $val['name'];
        $staff->save();
        $this->affected++;
      }
    }

  }

}
