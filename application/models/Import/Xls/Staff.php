<?php
class Import_Staff extends Import_Abstract{
  
  protected $columns = array(
    'id'   => 'A',
    'name' => 'B'
  );
  
  public function execute(){
    $highestRow = $this->activeSheet->getHighestRow();
    for ( $i = 1; $i <= $highestRow; $i++ ){
      $id = trim($this->activeSheet->getCell( $this->columns['id'] . $i ));
      if ( ! Valid::id1c($id) ){ continue; }
      $name = trim($this->activeSheet->getCell( $this->columns['name'] . $i ));
      if ( $this->saveUser($id, $name) ){
        self::$affected++;
      }
    }
    return self::$affected;
  }
  
  protected function saveUser($id, $name){
    $sql = "SELECT * FROM `staff` WHERE id = '".$code."' LIMIT 1";
    $result = App::db()->query($sql);
    if ( $result and ($result->num_rows) ){
      $row = $result->fetch_assoc();
      if ( $row['name'] == $name ){ return false; }
      $q = "UPDATE `staff` SET name = '". App::db()->escape($name) ."' WHERE id = '".$id."' LIMIT 1";
    } else {
      $q = "INSERT INTO `staff` SET name = '". App::db()->escape($name) ."', id = '". $id ."' ";
    }                                                                                  
    $r = App::db()->query($q);
  
    return (bool)$r;
  }
  
}