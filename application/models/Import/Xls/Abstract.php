<?php
abstract class Import_Xls_Abstract {

  protected $activeSheet;
  protected $phpExcel;
  static protected $affected = 0;

  protected $columns = array();

  public function __construct( $filename ){
    $phpExcel = PHPExcel_IOFactory::load($filename);
    $this->activeSheet = $phpExcel->getActiveSheet();
    $this->phpExcel = $phpExcel;
    $this->isValidFormat();
  }

  abstract public function execute();

  protected function isValidFormat(){
    $columns = $this->columns;
    if ( $this->activeSheet->getHighestColumn() != array_pop($columns) ){
      throw new ImportExeption('Invalid file structure format');
    }
  }

}

class ImportExeption extends Exception {}