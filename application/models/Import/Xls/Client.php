<?php
class Import_Xls_Client extends Import_Xls_Abstract{

  protected $columns = array(
    'title'     => 'B',
    'id'        => 'C',
    'amount'    => 'D'
  );

  public function execute(){
    $highestRow = $this->activeSheet->getHighestRow();
    for ( $i = 1; $i <= $highestRow; $i++ ){
      $id = trim($this->activeSheet->getCell( $this->columns['id'] . $i ));
      if ( ! Valid::id1c($id) ){
        continue;
      }
      $title = trim($this->activeSheet->getCell( $this->columns['title'] . $i ));
      $amount = trim($this->activeSheet->getCell( $this->columns['amount'] . $i ));
      $amount = number_format($amount, 2, '.', '');

      if ( $this->saveClient($id, $title, $amount) ){
        self::$affected++;
      }
    }
    return self::$affected;
  }

  protected function saveClient($id, $title, $amount){
    if ( ! $amount) {
      $amount = '0.00';
    }
    $result = App::db()->query("SELECT * FROM `clients` WHERE id = '". $id ."' LIMIT 1");
    if ( $result and ($result->num_rows) ){
      $row = $result->fetch_assoc();
      if ( $row['title'] == $title and $amount == $row['amount'] ){ return false; }
      $q = "UPDATE `clients` SET title = '". App::db()->escape($title) ."', amount = '". $amount ."' WHERE id = '". $row['id'] ."' LIMIT 1";
    } else {
      $q = "INSERT INTO `clients` SET id = '". $id ."', title = '". App::db()->escape($title) ."', amount = '". $amount ."'";
    }
    $r = App::db()->query($q);

    return (bool)$r;
  }

}