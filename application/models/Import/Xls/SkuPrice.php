<?php
class Import_SkuPrice extends Import_Abstract{
  
  protected $columns = array(
    'title'           => 'A',
    'id_sku'          => 'B',
    'units'           => 'C',
    'price_type_name' => 'D',
    'id_price_type'   => 'E',
    'date'            => 'F',
    'price'           => 'G'
  );
  
  public function execute(){
    $highestRow = $this->activeSheet->getHighestRow();
    for ( $i = 1; $i <= $highestRow; $i++ ){
      $idSku = trim($this->activeSheet->getCell( $this->columns['id_sku'] . $i ));
      $idPriceType = trim($this->activeSheet->getCell( $this->columns['id_price_type'] . $i ));
      $date = trim($this->activeSheet->getCell( $this->columns['date'] . $i ));
      if ( !(Valid::id1c($idSku) and Valid::id1c($idPriceType) and Valid::date1c($date)) ){
        continue;
      }
      $price = trim($this->activeSheet->getCell( $this->columns['price'] . $i ));
      $price = number_format($price, 2, '.', '');
      if ( $this->savePrice($idSku, $idPriceType, $date, $price) ){
        self::$affected++;
      }
    }
    return self::$affected;
  }
  
  public function savePrice( $idSku, $idPriceType, $date, $price ){
    $date = Date::date1c2php($date);
    if ( ! $price ){
      $price = '0.00';
    }
    
    $result = App::db()->query("
    SELECT * FROM `sku_prices` WHERE 
    id_sku = '". $idSku ."' AND id_price_type = '". $idPriceType ."' AND date = '". $date ."' 
    LIMIT 1
    ");
  
    if ( $result and ($result->num_rows) ){
      $row = $result->fetch_assoc();
      if ( $row['id_sku'] == $idSku and $row['id_price_type'] == $idPriceType and $row['price'] == $price ){
        return false;
      }
      $q = "UPDATE `sku_prices` SET price = '". $price ."' WHERE 
      id_sku = '". $idSku ."'
      AND id_price_type = '". $idPriceType ."'
      AND date = '". $date ."'
      LIMIT 1";
    } else {
      $q = "INSERT INTO `sku_prices` SET 
      id_sku = '". $idSku ."',
      id_price_type = '". $idPriceType ."',
      date = '". $date ."',
      price = '". $price ."'
      ";
    }
 
    $r = App::db()->query($q);
    return (bool)$r;
  }
  
}