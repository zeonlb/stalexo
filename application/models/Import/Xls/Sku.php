<?php    
class Import_Sku extends Import_Abstract {
  
  protected $columns = array(
    'title'       => 'B',
    'id'          => 'C',
    'group_title' => 'D',
    'group'       => 'E',
    'units'       => 'F'
  );
  
  public function execute(){
    $highestRow = $this->activeSheet->getHighestRow();
    for( $i = 1; $i <= $highestRow; $i++ ){
      $id = trim($this->activeSheet->getCell( $this->columns['id'] . $i ));
      $group = trim($this->activeSheet->getCell( $this->columns['group'] . $i ));
      if ( ! Valid::id1c($id) or ! Valid::id1c($group) ){
        continue;
      }
    
      $title = trim($this->activeSheet->getCell( $this->columns['title'] . $i ));
      $units = trim($this->activeSheet->getCell( $this->columns['units'] . $i ));
      
      if ( $this->saveSku($id, $title, $group, $units) ) {
        self::$affected++;
      }
    } 
    return self::$affected;                       
  }
  
  protected function saveSku( $id, $title, $group, $units ){
    $result = App::db()->query("SELECT * FROM `sku` WHERE id = '".$id."' LIMIT 1");
    
    if ( $result and ($result->num_rows) ){
      $row = $result->fetch_assoc();

      if ( ($row['title'] == $title) and ($row['units'] == $units) and ($group == $row['id_group']) ){
        return false;
      }     
      $q = "UPDATE `sku` SET title = '". App::db()->escape($title) ."', id_group = '". $group ."', units = '". App::db()->escape($units) ."' WHERE id = '".$id."' LIMIT 1";
    } else {
      $q = "INSERT INTO `sku` SET id = '". $id ."', title = '". App::db()->escape($title) ."', id_group = '". $group ."', units = '". App::db()->escape($units) ."'";
    }
    echo $q . '<br>';
    $result = App::db()->query($q);
    return (bool) $result;
  }
  
}