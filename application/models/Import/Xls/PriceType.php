<?php 
class Import_PriceType extends Import_Abstract{
  
  protected $columns = array(
    'id'    => 'A',
    'title' => 'B'
  );
  
  public function execute(){
    $highestRow = $this->activeSheet->getHighestRow();
    for ( $i = 1; $i <= $highestRow; $i++ ){
      $id = trim($this->activeSheet->getCell( $this->columns['id'] . $i ));
      if ( ! Valid::id1c($id) ){
        continue;
      }     
      $title = trim($this->activeSheet->getCell( $this->columns['title'] . $i ));
      if ( $this->savePriceType($id, $title) ){
        self::$affected++;
      }
    }
    return self::$affected;
  }
  
  protected function savePriceType($id, $title){
    $result = App::db()->query("SELECT * FROM `price_types` WHERE id = '". $id ."' LIMIT 1");
    if ( $result and ($result->num_rows) ){
      $row = $result->fetch_assoc();
      if ( $row['title'] == $title ){ return false; }
      $q = "UPDATE `price_types` SET title = '". App::db()->escape($title) ."' WHERE id = '". $row['id'] ."' LIMIT 1";
    } else {
      $q = "INSERT INTO `price_types` SET id = '". $id ."', title = '". App::db()->escape($title) ."'";
    }
    $r = App::db()->query($q); 
 
    return (bool)$r;
  }
  
}