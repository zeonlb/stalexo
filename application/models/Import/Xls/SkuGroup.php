<?php
class Import_SkuGroup extends Import_Abstract{
  
  protected $columns = array(
    'name' => 'B',
    'id' => 'C'
  );
  
  public function execute(){
    $highestRow = $this->activeSheet->getHighestRow();
    for ( $i = 1; $i <= $highestRow; $i++ ){
      $id = trim($this->activeSheet->getCell( $this->columns['id'] . $i ));
      if ( ! Valid::id1c($id) ){
        continue;
      }     
      $name = trim($this->activeSheet->getCell( $this->columns['name'] . $i ));
      if ( $this->saveGroup($id, $name) ){
        self::$affected++;
      }
    }
    return self::$affected;
  }
  
  public function saveGroup($id, $name){
    $result = App::db()->query("SELECT * FROM sku_groups WHERE id = '". $id ."' LIMIT 1");
    if ( $result and ($result->num_rows) ){
      $row = $result->fetch_assoc();
      if ( $row['title'] == $name ){ return false; }
      $q = "UPDATE `sku_groups` SET title = '". App::db()->escape($name) ."' WHERE id = '". $row['id'] ."' LIMIT 1";
    } else {
      $q = "INSERT INTO `sku_groups` SET id = '". $id ."', title = '". App::db()->escape($name) ."'";
    }
    $r = App::db()->query($q); 
 
    return (bool)$r;
  }
  
} 