<?php 
class Import_DistributionPoint extends Import_Abstract{
  
  protected $columns = array(
    'contract'    => 'B',
    'id'          => 'C',
    'id_client'   => 'D',
    'manager'     => 'E',
    'address'     => 'F',
    'id_price_type'  => 'G',
    'amount'      => 'H'
  );
  
  public function execute(){
    $highestRow = $this->activeSheet->getHighestRow();
    for ( $i = 1; $i <= $highestRow; $i++ ){
      $id = trim($this->activeSheet->getCell( $this->columns['id'] . $i ));
      $idClient = trim($this->activeSheet->getCell( $this->columns['id_client'] . $i ));
      if ( ! Valid::id1c($id) or ! Valid::id1c($idClient) ){
        continue;
      }     
      
      $idManager   = '000000001';
      $manager     = trim($this->activeSheet->getCell( $this->columns['manager'] . $i ));
      $address     = trim($this->activeSheet->getCell( $this->columns['address'] . $i ));
      $idPriceType = trim($this->activeSheet->getCell( $this->columns['id_price_type'] . $i ));
      if ( ! $idPriceType ){ $idPriceType = '000000000'; }
      $amount      = trim($this->activeSheet->getCell( $this->columns['amount'] . $i ));
      $amount = number_format($amount,2,'.','');
      
      if ( $this->savePoint($id, $idClient, $idManager, $idPriceType, $address, $amount, $manager) ){
        self::$affected++;
      }
    }
    return self::$affected;
  }
  
  protected function savePoint($id, $idClient, $idManager, $idPriceType, $address, $amount, $manager){
    $result = App::db()->query("SELECT * FROM `distribution_points` WHERE id = '". $id ."' LIMIT 1");
    if ( $result and ($result->num_rows) ){
      $row = $result->fetch_assoc();
      
      if ( ($row['id_client'] == $idClient) 
        and ($row['id_manager'] == $idManager) 
        and ($row['id_price_type'] == $idPriceType)
        and ($row['address'] == $address) 
        and ($row['amount'] == $amount)
      ){ return false; }
      $q = "UPDATE `distribution_points` SET 
      id_client = '". $idClient ."',
      id_manager = '". $idManager ."',
      id_price_type = '". $idPriceType ."',
      amount = '". $amount ."',
      address = '". App::db()->escape($address) ."',
      manager_name = '". App::db()->escape($manager) ."' 
      WHERE id = '". $row['id'] ."' LIMIT 1";
    } else {
      $q = "INSERT INTO `distribution_points` SET 
      id = '". $id ."',
      id_client = '". $idClient ."',
      id_manager = '". $idManager ."',
      id_price_type = '". $idPriceType ."',
      amount = '". $amount ."',
      address = '". App::db()->escape($address) ."',
      manager_name = '". App::db()->escape($manager) ."' 
      ";
    }
    
    $r = App::db()->query($q); 
 
    return (bool)$r;
  }
  
}