<?php
class Import_SkuAvailability extends Import_Abstract{
  
  protected $columns = array(
    'id_sku' => 'B',
    'id_storage' => 'C',
    'availability' => 'D'
  );
  
  public function execute(){
    $highestRow = $this->activeSheet->getHighestRow();
    for ( $i = 1; $i <= $highestRow; $i++){
      $idSku = trim($this->activeSheet->getCell( $this->columns['id_sku'] . $i ));
      $idStorage = trim($this->activeSheet->getCell( $this->columns['id_storage'] . $i ));
      if ( !(Valid::id1c($idSku) and Valid::id1c($idStorage)) ){
        continue;
      }
      $availability = trim($this->activeSheet->getCell( $this->columns['availability'] . $i ));
      $availability = number_format($availability, 3, '.', '');
    
      if ( $this->saveAvailability($idSku, $idStorage, $availability) ){
        self::$affected++;
      }
    }
    return self::$affected;
  }
  
  public function saveAvailability($idSku, $idStorage, $availability){
    if( ! $availability ) {
      $availability = '0.000';
    }
    $result = App::db()->query("SELECT * FROM `sku_availability` 
    WHERE id_sku = '". $idSku ."' AND id_storage = '". $idStorage ."'
    LIMIT 1");
    if ( $result and ($result->num_rows) ){
      $row = $result->fetch_assoc();
      if ( $row['availability'] == $availability ){
        return false;
      }
      $q = "UPDATE `sku_availability` SET 
      availability = '". $availability ."',
      WHERE id_sku = '". $idSku ."' AND id_storage = '". $idStorage ."' 
      LIMIT 1";
    } else {
      $q = "INSERT INTO `sku_availability` SET 
      id_sku = '". $idSku ."',
      id_storage = '". $idStorage ."',
      availability = '". $availability ."'
      ";
    }
    $r = App::db()->query($q);
    return (bool)$r;
  }             
  
}