<?php
require_once Settings::path()->vendor . '/Swift/lib/swift_required.php'; 
class Mailer_Adapter {
  
  const EMAIL = "test@training-ocean.by.ua";
  const PRIORITY = 'normal';
  const SITE_NAME = 'Training-ocean';

  protected static function getMessage(){   
    $message = Swift_Message::newInstance()->setFrom(array( self::EMAIL => self::SITE_NAME ));
    return $message;
  }
  
  public static function send( $subject, $emailTo, Mailer_Content_Abstract $content ){        
    //$emailTo = 'zumbaaa@ukr.net';

    $message = self::getMessage()
      ->setSubject($subject)
      ->setBody($content->getHtml(),'text/html')
      ->setTo(array( $emailTo ))
      ->setPriority(self::PRIORITY)
      ->setCharset('utf-8');

    $transport = Swift_SmtpTransport::newInstance('localhost', 25)
    ->setUsername(Config::get('mail')->username)
    ->setPassword(Config::get('mail')->password);
           
    $result = Swift_Mailer::newInstance($transport)->send($message);
    return $result;  
  }
  
}