<?php 
class Mailer_Content_Auth_Activation extends Mailer_Content_Abstract{
  
  public function __construct( User_Abstract $user, $activationUrl ){
    $smarty = App::newSmarty();
    $smarty->assign(array(
      'user' => $user,
      'link' => $activationUrl
    ));
    
    $this->html = $smarty->fetch("mailer/auth/registration/activation.tpl");
  }
  
}