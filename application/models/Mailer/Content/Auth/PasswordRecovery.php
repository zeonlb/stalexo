<?php 
class Mailer_Content_Auth_PasswordRecovery extends Mailer_Content_Abstract{
  
  public function __construct( $link ){
    $smarty = App::newSmarty();
    $smarty->assign(array(
      'link' => $link
    ));
    
    $this->html = $smarty->fetch("mailer/auth/registration/password-recovery.tpl");
  }
  
}