<?php 
class Location_Collection extends Collection{
  
  protected function initQuery(){
    $this->query = Location_Model::getFetchQuery();
  }
  
  public function loadAll(){
    $this->query()->enableFoundRows();
    $result = $this->executeQuery();
    if ( $result ){
      while( $row = $result->fetch_assoc() ){
        $this->add(new Location_Model($row));
      }
    }
  }
  
}