<?php
class Location_Model extends Dao{

  static protected $pk    = 'id';
  static protected $table = 'location';

  static public function getStartEndByAgentAndPeriod($agent, $dateBegin, $dateEnd){
    $start = array('id' => 0);
    $end   = array('id' => 0);
    $s = App::db()->query("SELECT *, 1 as `start` FROM `location` WHERE id_staff = '".$agent->id."' AND `date` BETWEEN '".$dateBegin."' AND '". $dateEnd ."' ORDER BY `date` ASC LIMIT 1");
    if ( $s ){
      $start = $s->fetch_assoc();
      $start['time'] = date('H:i', $start['date']);
    }
    $e = App::db()->query("SELECT *, 1 as `end` FROM `location` WHERE id_staff = '".$agent->id."' AND `date` BETWEEN '".$dateBegin."' AND '". $dateEnd ."' ORDER BY `date` DESC LIMIT 1");
    if ( $e ){
      $end = $e->fetch_assoc();
      $end['time'] = date('H:i', $end['date']);
    }

    return array('start' => $start, 'end' => $end);
  }

}