<?php 
class Messenger{
  
  static $key = "message";
 
  public static function addError( $text ){
    $_SESSION[self::$key]['type'] = "error";
    $_SESSION[self::$key]['text'] = $text;
  }
  
  public static function addNotification( $text ){
    $_SESSION[self::$key]['type'] = "notification";
    $_SESSION[self::$key]['text'] = $text;
  }
  
  public static function displayMessage(){
    if ( App::getRequest()->isAjax() ){
      return false;
    }   
    $message = self::getMessage();
    $_SESSION[self::$key] = array();
    switch( $message['type'] ){
      case "error":
        return self::displayError( $message['text'] );
        break;
      case "notification":
        return self::displayNotification( $message['text'] );
        break;
      default:
        return "";
        break;
    }   
  }
  
  static public function getMessage(){
    if ( isset($_SESSION[self::$key]) and !empty($_SESSION[self::$key]) ){
      $message = $_SESSION[self::$key];
      return $_SESSION[self::$key];
    }
    return null;
  }
  
  protected static function displayError( $text ){
    $smarty = App::newSmarty();
    $smarty->assign(array(
      'text' => $text,
      'type' => 'error'
    ));
    return $smarty->fetch( "messenger/script.tpl" );
  }
  
  protected static function displayNotification( $text ){
    $smarty = App::newSmarty();
    $smarty->assign(array(
      'text' => $text,
      'type' => 'notification'
    ));
    return $smarty->fetch( "messenger/script.tpl" );
  }
   
}