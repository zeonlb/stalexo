<?php
class Api_Auth{

  static protected function generateToken(){
    $chars = '1234567890ABCDEFGHIGKLMOOPQRSTUVWXYZabsdefghigklmnopqrstuvwxyz0987654321';
    return substr(str_shuffle($chars), 0, 20);
  }

  static public function makePasswordHash( $password, $device ){
    return md5(md5($password) . $device);
  }

  static public function login($device, $password){
    $hash = self::makePasswordHash($password, $device);
    $q = User_Api::getFetchQuery()
    ->where("staff_device.device = '". App::db()->escape($device) ."'")
    ->where("staff_device.password = '". App::db()->escape($hash) ."'")
    ->limit(1);

    $r = App::db()->query($q);

    if ($r and $r->num_rows){
      $user = new User_Api($r->fetch_assoc());
      $user->token = Api_Auth::generateToken();
      $user->token_date = time();
      $user->save();
      return $user;
    }
    return new User_Api();
  }

  static public function logout($device){
    self::refreshToken();
  }

  static public function refreshToken($device){
    $token = self::generateToken();
    App::db()->query("
    UPDATE `staff_device` SET token = '". $token ."',
    token_date = '". time() ."'
    WHERE device = '". App::db()->escape($device) ." LIMIT 1
    ");
    return $token;
  }

  static public function addApiUser($password, $device, $idStaff, $isProduction = 1){
    $apiUser = new User_Api($idStaff);
    if ( $apiUser->isLoaded() ){ throw new ApiUserAlreadyExists(); }
    $staff = new User_Staff_Model($idStaff);
    if ( ! $staff->isLoaded() ) { throw new StaffUserNotExists(); }
    App::db()->query("INSERT INTO `staff_device`
    SET
    id_staff = '". App::db()->escape($idStaff) ."',
    password = '". self::makePasswordHash($password, $device) ."',
    device = '". App::db()->escape($device) ."',
    is_production = '". (int)$isProduction ."'
    ");

    $apiUser = new User_Api($idStaff);
    return $apiUser;
  }

  static public function loginByToken($token, $device){
    $q = User_Api::getFetchQuery()
    ->limit(1)
    ->where("staff_device.device = '". App::db()->escape($device) ."'")
    ->where("staff_device.token = '". App::db()->escape($token) ."'");
    $row = array();
    $result = App::db()->query($q);
    if ( $result ){
      $row = $result->fetch_assoc();
    }
    return new User_Api($row);
  }

}

class ApiUserAlreadyExists extends Exception{}
class StaffUserNotExists extends Exception{}