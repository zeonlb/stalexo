<?php
class Api_Request{

  public $action = 'error';
  public $valid = true;
  public $user = null;

  public function __construct(){
    $this->logPost();
    $this->logFiles();
    $this->user = new User_Api();

    if ( App::getRequest()->post('action') ){
      if ( App::getRequest()->post('action') != 'login' ){
        $token   = App::getRequest()->post('token');
        $device  = App::getRequest()->post('device');
        $this->loadUser($token, $device);
      }
      $this->action = App::getRequest()->post('action');
    } else {
      $this->valid = false;
    }
  }

  protected function loadUser($token, $device){
    $this->user = Api_Auth::loginByToken($token, $device);
  }

  public function isValid(){
    return $this->valid;
  }

  public function logPost(){
    ob_start();
    var_dump(App::getRequest()->post());
    $post = ob_get_contents();
    ob_clean();
    file_put_contents('data/logs/post.txt', $post);
    //chmod('data/logs/post.txt', 0777);
  }

  public function logFiles(){
    ob_start();
    var_dump($_FILES);
    $files = ob_get_contents();
    ob_clean();
    file_put_contents('data/logs/files.txt', $files);
    chmod('data/logs/files.txt', 0777);
  }

  public function getValue($key){
    return App::getRequest()->post($key);
  }

}