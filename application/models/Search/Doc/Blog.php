<?php
class Search_Doc_Blog implements Search_ListDocsI{
  
  public function getAllDocs(){
    $documents = array();
    $collection = new Blog_Collection();
    $collection->pager()->onPage = 2000;
    $collection->loadBlogs();
    
    foreach($collection as $b){
      $documents[] = new Search_DocumentForSearch(
        $b->id_news,
        'blog',
        '/blog-'.$b->children('group')->alias.'/'.$b->id_news,
        strip_tags($b->news_title_ru),
        strip_tags($b->news_announce_ru),
        '',
        '',
        $b->children('group')->alias,
        $b->children('group')->group_name_ru
      );
    }
    
    return $documents;
  }
  
}