<?php
class Search_Doc_News implements Search_ListDocsI{
  
  public function getAllDocs(){
    $documents = array();
    $collection = new News_Collection();
    $collection->pager()->onPage = 2000;
    $collection->loadNews();
    
    foreach($collection as $n){
      $documents[] = new Search_DocumentForSearch(
        $n->id_news,
        'news',
        '/news-'.$n->children('group')->alias.'/'.$n->id_news,
        strip_tags($n->news_title_ru),
        strip_tags($n->news_announce_ru),
        '',
        '',
        $n->group_alias,
        $n->group_name
      );
    }
    
    return $documents;
  }
  
}