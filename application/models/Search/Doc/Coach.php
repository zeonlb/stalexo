<?php
class Search_Doc_Coach implements Search_ListDocsI{
  
  public function getAllDocs(){
    $documents = array();
    $collection = new User_Collection();
    $collection->pager()->onPage = 2000;
    $collection->loadDeatails = false; 
    $collection->loadMembers(array('coach', 'coach_company'));
       
    foreach($collection as $c){
      $documents[] = new Search_DocumentForSearch(
        $c->userid,
        'coach',
        '/member/'.$c->userid,
        strip_tags($c->username .' ' . $c->secondname),
        '',
        '',
        '',
        '',
        ''
      );
    }
    
    return $documents;
  }
  
}