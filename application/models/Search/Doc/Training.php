<?php
class Search_Doc_Training implements Search_ListDocsI{
  
  public function getAllDocs(){
    $documents = array();
    $collection = new Training_Collection();
    $collection->pager()->onPage = 2000;
    $collection->loadTrainings();
    
    foreach($collection as $t){
      $documents[] = new Search_DocumentForSearch(
        $t->id_prod,
        'training',
        '/training/'.$t->id_prod,
        strip_tags($t->prod_name),
        strip_tags($t->prod_descr),
        '',
        $t->prod_article,
        $t->cat_alias,
        $t->cat_name
      );
    }
    
    return $documents;
  }
  
}