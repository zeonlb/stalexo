<?php
class Search_Doc_Product implements Search_ListDocsI{
  
  public function getAllDocs(){
    $documents = array();
    $collection = new Shop_Product_Collection();
    $collection->pager()->onPage = 2000;
    $collection->loadShopProducts();
    
    foreach($collection as $p){
      $documents[] = new Search_DocumentForSearch(
        $p->id_prod,
        'product',
        '/product/'.$p->id_prod,
        strip_tags($p->prod_name),
        strip_tags($p->prod_descr),
        '',
        $p->prod_article,
        $p->cat_alias,
        $p->cat_name
      );
    }
    
   	return $documents;
  }
  
}