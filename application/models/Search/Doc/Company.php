<?php
class Search_Doc_Company implements Search_ListDocsI{
  
  public function getAllDocs(){
    $documents = array();
    $collection = new User_Collection();
    $collection->pager()->onPage = 2000;
    $collection->loadDeatails = false; 
    $collection->loadMembers(array('company'));
       
    foreach($collection as $c){
      $documents[] = new Search_DocumentForSearch(
        $c->userid,
        'company',
        '/member/'.$c->id_prod,
        strip_tags(stripslashes($c->username)),
        '',
        '',
        '',
        '',
        ''
      );
    }
    
    return $documents;
  }
  
}