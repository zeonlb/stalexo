<?php
class Search_ListDocumentForSearch{
	/**
	 * Массив объектов докуметов, реализующих интерфейс Search_ListDocsI
	 *
	 * @var array
	 */
  protected $documents = array();
  
  /**
   * Метод добавляет объект типа Search_listDocsI
   *
   * @param Search_listDocsI $listDocs
   */
  public function addDocument($listDocs){
    if($listDocs instanceof Search_listDocsI){
      $this->documents = array_merge($this->documents, $listDocs->getAllDocs());
    }
  }
  
  /**
   * Метод возвращает список добавленных документов
   *
   * @return unknown
   */
  public function getDocuments(){
    return $this->documents;
  }
  
  /**
   * Метод создает индекс
   *
   * @param String $indexPath Путь к файлам индекса
   */
  public function createIndex($indexPath){
    // create our index
    $index = Zend_Search_Lucene::create($indexPath);
    
    // fictional function used to retrieve data from the database
    $documents = $this->getDocuments();
    foreach ($documents as $document) {  
     $index->addDocument(new Search_IndexedDocument($document));
    }
    
    // write the index to disk
    $index->commit();
  }
}
?>