<?php
class Search_DocumentForSearch{
  public  $url, $title, $body, $article, $tags, $class, $key, $parentAlias, $parentName;
  public function __construct( $key, $class, $url, $title, $body, $tags, $article = '', $parentAlias = '', $parentName = '' ){
    $this->url = $url;
    $this->title = $title;
    $this->body = $body;
    $this->tags = $tags;
    $this->parentAlias = $parentAlias;
    $this->parentName = $parentName;
    
    $this->article = $article;
    $this->class = $class;
    $this->key = $key;
  }
}