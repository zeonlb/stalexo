<?php
class Search_IndexedDocument extends Zend_Search_Lucene_Document
{
  /**
     * Constructor. Creates our indexable document and adds all
     * necessary fields to it using the passed in document
     */
  public function __construct($document)
  {
  	$this->addField(Zend_Search_Lucene_Field::Keyword('url',  $document->url, 'utf-8'));
    $this->addField(Zend_Search_Lucene_Field::Text('title',   $document->title, 'utf-8') );
    $this->addField(Zend_Search_Lucene_Field::Text('contents', strip_tags($document->body), 'utf-8' ) );
    $this->addField(Zend_Search_Lucene_Field::UnIndexed('parentAlias', $document->parentAlias, 'utf-8'));
    $this->addField(Zend_Search_Lucene_Field::Keyword('article', $document->article, 'utf-8'));
    $this->addField(Zend_Search_Lucene_Field::Text('parentName', $document->parentName, 'utf-8'));
    $this->addField(Zend_Search_Lucene_Field::Text('tags', strip_tags($document->tags), 'utf-8'));
    $this->addField(Zend_Search_Lucene_Field::UnIndexed('class', $document->class, 'utf-8'));
    $this->addField(Zend_Search_Lucene_Field::UnIndexed('key', $document->key, 'utf-8'));
  }
}
?>