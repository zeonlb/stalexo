<?php
class Search_Highlighter extends Zend_Search_Lucene_Search_Highlighter_Default
{
    public function highlight($words)
    {
        $this->_doc->highlight($words, '#fff120');
    }

}