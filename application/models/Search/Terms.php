<?php
class Search_Terms extends Dao{
  
  const ALL = 'all';
  
  protected $lang = 'ru';
  protected $termFields = array( 'title' );
  protected $termClass = array( 
    'product', 
  );
  
  static protected $pk = 'id';
  static protected $table = 'search_terms';
  
  public static function addTerm( $term, $class = self::ALL, $freq = 1, $lang ){
    $term = App::db()->escape(strip_tags( trim($term) ));
    if ( mb_strlen($term) < 3 or $freq < 2 ){ return false; }
    
    $sql = "INSERT IGNORE INTO `search_terms`
    SET term = '". $term ."', lucene = 1, freq = '". (int)$freq ."', lang = '". $lang ."' ";
    if ( $class ){
      $sql .= " , class = '". $class ."' ";
    }
    $sql .= " ON DUPLICATE KEY UPDATE matches = 1, freq = freq + '".$freq."' ";
    
    $res = App::db()->query( $sql );
    return $res;
  }
 
}