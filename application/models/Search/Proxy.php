<?php
class Search_Proxy {
  
  /**
  * @var array Search_ListDocsI
  */
  protected $docs = array();
  
  public function refreshIndex(){
    if ( ! count($this->docs) ){
      return false;
    }
    
    ini_set("max_execution_time", 900);
    set_time_limit(900);
    set_include_path( get_include_path() . PATH_SEPARATOR . Settings::path()->vendor );
    require_once 'Zend/Loader/Autoloader.php';           
    $autoloader = Zend_Loader_Autoloader::getInstance();   
    $languages = App::instance()->getLanguages();

    foreach ( $languages as $lang ) {
      $indexPath = Settings::path()->luceneIndex . $lang->lang_alias;
      $this->recursive_remove_directory( $indexPath, true );
      Zend_Search_Lucene_Analysis_Analyzer::setDefault(
        new Zend_Search_Lucene_Analysis_Analyzer_Common_Utf8_CaseInsensitive()
      );
      $listDocs = new Search_ListDocumentForSearch();
      foreach( $this->docs as $doc ){
        $listDocs->addDocument($doc);
      }
      $listDocs->createIndex($indexPath);
    }
    return true;
  }
  
  public function addDocument(Search_ListDocsI $doc){
    $this->docs[] = $doc;
  }
  
  protected function recursive_remove_directory($directory, $empty=FALSE){
    if(substr($directory,-1) == '/')
    {
      $directory = substr($directory,0,-1);
    }
    if(!file_exists($directory) || !is_dir($directory))
    {
      return FALSE;
    }elseif(is_readable($directory))
    {
      $handle = opendir($directory);
      while (FALSE !== ($item = readdir($handle)))
      {
        if($item != '.' && $item != '..')
        {
          $path = $directory.'/'.$item;
          if(is_dir($path))
          {
            $this->recursive_remove_directory($path);
          }else{
            unlink($path);
          }
        }
      }
      closedir($handle);
      if($empty == FALSE)
      {
        if(!rmdir($directory))
        {
          return FALSE;
        }
      }
    }
    return TRUE;
  }
  
  /**
  * @return similar search terms
  * 
  * @param string $class
  * @param string $term
  * @param int    $limit
  */
  public static function getSimilar( $term, $class = Search_Terms::ALL , $limit, $lang = 'ru' ){
    $sql = "SELECT * FROM search_terms WHERE term LIKE '". App::db()->escape($term) ."%' "; 
    if ( $class and $class != Search_Terms::ALL ){
      $sql .= " AND class = '". App::db()->escape($class) ."' ";
    }
    $sql .= " GROUP BY term ORDER BY lucene ASC LIMIT " . (int)$limit;
    $res = App::db()->query( $sql );
    
    $similarTerms = array();
    
    if ( $res and $res->num_rows ){
      while( $row = $res->fetch_assoc() ){
        $similarTerms[] = $row;
      }
    } 
    
    return $similarTerms;
  }
  
  public function updateTermsByClass( Zend_Search_Lucene_Proxy $index, $lang = 'ru' ){
    App::db()->query("DELETE FROM `search_terms` WHERE lucene = 1 ");
    $terms = $index->terms();

    foreach($terms as $term){
      if ( mb_strlen($term->text) < 3 ){ continue; }
      if ( ! in_array($term->field, array('title') ) ) {
        continue;
      }
      $docsId = $index->termFreqs($term);
      
      $termArr = array();
      foreach( $docsId as $docId => $freq ){
        $doc = $index->getDocument($docId);
        if ( array_key_exists( $doc->class, $termArr ) ){
          $termArr[$doc->class] += $freq ;
        } else {
          $termArr[$doc->class]  = $freq ;
        }        
      } 
      
      foreach ( $termArr as $class => $freq ){
        Search_Terms::addTerm( $term->text, $class, $freq, $lang );
      }
      
    }

  }
  
  public function getHits( $searchQuery, $lang, $class = false, $limit = 200 ){
    $index = $this->getIndex( $lang );
    Zend_Search_Lucene_Analysis_Analyzer::setDefault(
      new Zend_Search_Lucene_Analysis_Analyzer_Common_Utf8Num_CaseInsensitive(), 
      'utf-8'
    ); 
    Zend_Search_Lucene_Search_QueryParser::setDefaultEncoding("utf-8");
    Zend_Search_Lucene_Search_QueryParser::setDefaultOperator( Zend_Search_Lucene_Search_QueryParser::B_AND );
    Zend_Search_Lucene::setResultSetLimit( (int)$limit );    

    $subjectQ = ( !empty($searchQuery) and !is_numeric($searchQuery) and mb_strlen($searchQuery) > 4 ) ? $searchQuery . "~0.8^2" : $searchQuery;   
      
    $userQuery = Zend_Search_Lucene_Search_QueryParser::parse( $subjectQ );  
   
   if ( $class ){
     $userQuery = $class.':'.$subjectQ;
   }
   
    $hits = $index->find($userQuery);
    
    return $hits;
  }
  
  public function getIndex( $langAlias = 'ru' ){
    if ( ! preg_match('@^[a-z]{2}$@',$langAlias) ){
      throw new Exception('Invalid language alias');
    }
    $this->lang = $langAlias;
    
    set_include_path( get_include_path() . PATH_SEPARATOR . Settings::path()->vendor );
    require_once 'Zend/Loader/Autoloader.php';           
    $autoloader = Zend_Loader_Autoloader::getInstance(); 
    
    $index = Zend_Search_Lucene::open( Settings::path()->luceneIndex . $this->lang );
    return $index;
  }
  
}