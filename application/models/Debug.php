<?php
class Debug{

  static protected $options = array();

  static $phpDebug = null;

  public static function isDeveloper(){
    if ( isset($_SESSION['debug']['developer']) and $_SESSION['debug']['developer'] == true ){
      return true;
    }
    return false;
  }

  public static function setDeveloperMode(){
    $_SESSION['debug']['developer'] = true;
  }

  public static function setUserMode(){
    $_SESSION['debug']['developer'] = false;
  }

  public static function startDebug(){
    if ( self::isDeveloper() ){
      $dbg = self::instance();
    }
  }

  public function endDebug(){
    if ( self::isDeveloper() ){
      self::instance()->display();
    }
  }

  public static function dump( $var, $name = '' ){
    if ( self::isDeveloper() ){
      self::instance()->dump( $var, $name );
    }
  }

  public static function var_dump( $val ){
    echo "<pre>";
    var_dump($val);
    exit();
  }

  public static function instance(){
    if ( ! self::isDeveloper() ){
      return false;
    }
    if ( self::$phpDebug instanceof PHP_Debug ){
      return self::$phpDebug;
    }

    set_include_path( get_include_path().PATH_SEPARATOR. Settings::path()->vendor . "PHPDebug");
    //include_once Settings::path()->vendor . "Pear/PEAR.php";
    include_once "PHP/Debug.php";

    $optionsDiv = array(
      'HTML_DIV_images_path' => '/assets/debug/images',
      'HTML_DIV_css_path' => '/assets/debug/css',
      'HTML_DIV_js_path' => '/assets/debug/js',
      'HTML_DIV_disable_credits' => true,
      'css' => '/assets/debug/css/html_div.css',
      'js' => '/assets/debug/js/html_div.js'
    );

    $optionsTable = array(
      'render_type'            => 'HTML',
      'render_mode'            => 'Table',
      'HTML_TABLE_images_path' => '/assets/debug/images',
      'HTML_TABLE_css_path'    => '/assets/debug/css',
      'HTML_TABLE_js_path'     => '/assets/debug/js',
      'HTML_TABLE_disable_credits' => true,
      'css' => '/assets/debug/css/html_table.css',

    );

    $options = $optionsDiv;
    if ( App::getRequest()->isAjax() ){
      $options = $optionsTable;
    }
    self::$options = $options;


    self::$phpDebug = new PHP_Debug($options);
    App::view()->css()->add( $options['css'] );
    if ( isset( $options['js'] ) ){
      App::view()->js()->add( $options['js'] );
    }
    return self::$phpDebug;
  }

  public static function add( $info ){
    if ( ! self::isDeveloper() ){
      return false;
    }
    self::instance()->add( $info );
  }

  public static function fetchTable(){
    if ( ! self::isDeveloper() ){
      return false;
    }

    $outputHeader = '<?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE html
         PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
      <head>
        <link rel="stylesheet" type="text/css" media="screen" href="http://' . $_SERVER['SERVER_NAME'] . self::$options['css'] .'" />
      </head>
    <body>
    ';
    $outputFooter = '</body></html>';
    ob_start();
    Debug::instance()->display();
    $buffer = ob_get_contents();
    ob_clean();
    $content = $outputHeader . $buffer . $outputFooter;
    return $content;
  }

  public static function renderTable(){
    if ( self::isDeveloper() ){
      echo self::fetchTable(); exit;
    }
  }

  public static function loginUser( $id ){
    $_SESSION['auth']['userid'] = $id;
  }

}