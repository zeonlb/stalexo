<?php 
class Singleton{
  
  static protected $instance = null;
  
  protected function __construct(){}
  
  /**
  * @return self
  * 
  */
  final static public function getInstance(){
    if ( ! static::$instance ){
      static::$instance = new static();
    }
    return static::$instance;
  }
  
}