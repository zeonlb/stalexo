<?php 
class Pager_StalexoJumping extends Pager_Jumping{
  
  protected $tpl = 'pager/jumping-stalexo.tpl';
  protected $buttonsInSequence = 15;
  protected $currentPageRange = 6;
  protected $maxButtonsInPager = 18;
  
}