<?php 
class Pager_Simple{
  
  protected $tpl = 'pager/simple.tpl';
  public $onPage;
  public $page;
  public $allCount;
  protected $pagesCount = 0;
  public $url = '';
  protected $smarty = null;
  
  public function __construct( $onPage = 1, $page = 1, $allCount = 0){
    $this->onPage = (int)$onPage;
    $this->page = (int)$page;
    $this->allCount = (int)$allCount;
    $this->smarty = App::newSmarty();;
  }
  
  protected function makeUrl(){
    $filtersUrl = Filter_Abstract::getUrl(array('page'));
    return $this->url . $filtersUrl; 
  }
    
  public function getHtml(){
    $this->pagesCount = ceil($this->allCount/$this->onPage);   
    $this->smarty->assign(array(
      'page'       => $this->page,
      'pagesCount' => $this->pagesCount,
      'allCount'   => $this->allCount,
      'url'        => $this->makeUrl(),
      'prev'       =>  ($this->page > 1 ? ($this->page - 1) : 1 ),
      'next'       =>  ($this->page < $this->pagesCount ? ($this->page + 1) : $this->pagesCount )
    ));
    $this->beforeRender();
    return $this->smarty->fetch($this->tpl);
  }
  
  public function __toString(){
    return $this->getHtml();
  }
  
  /**
  * method calls before fetching pager template
  */
  protected function beforeRender(){}
  
}