<?php 
class Pager_Jumping extends Pager_Simple{
  
  protected $tpl = 'pager/jumping_training_ocean.tpl';
  protected $buttonsInSequence = 6;
  protected $currentPageRange = 2;
  protected $maxButtonsInPager = 9;
  
  protected function beforeRender(){
    $float = 'center';
    if ( ($this->page) < ($this->buttonsInSequence ) ) {
      $float = 'left';
    } elseif( $this->page > ($this->pagesCount - $this->buttonsInSequence + 1 ) ){
      $float = 'right';
    }
    $this->smarty->assign(array(
      'maxButtonsInPager' => $this->maxButtonsInPager,
      'buttonsInSequence' => $this->buttonsInSequence,
      'currentPageRange'  => $this->currentPageRange,
      'float' => $float 
    ));
  }
  
}