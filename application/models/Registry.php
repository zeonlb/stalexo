<?php
class Registry extends Singleton Implements ArrayAccess {

  static protected $instance = null;
  
  protected $_vars = array();

  public function _set($key, $var) {
    $this->_vars[$key] = $var;
    return true;
  }

  public function keyIsset( $key ) {
    return isset($this->_vars[$key]);
  }

  public function _get($key) {
    if (!isset($this->_vars[$key])) {
      return null;
    }
    return $this->_vars[$key];
  }

  public function remove($key) {
    unset($this->_vars[$key]);
  }

  function offsetExists($offset) {
    return isset($this->_vars[$offset]);
  }	

  function offsetGet($offset) {
    return $this->_get($offset);
  }	

  function offsetSet($offset, $value) {
    $this->_set($offset, $value);
  }

  function offsetUnset($offset) {
    unset($this->_vars[$offset]);
  }

  public static function set( $key, $val ){
    static::getInstance()->_set( $key, $val );
  }

  public static function get( $key ){
    return static::getInstance()->_get( $key );
  }

}