<?php
class Sitemap_Item{
  
  public $loc;
  public $freq;
  public $date;
  
  public function __construct( $loc, $freq, $date = '' ){
    $this->loc  = $loc;
    $this->freq = $freq;
    $this->date = $date;
  }
  
}