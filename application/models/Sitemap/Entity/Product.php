<?php 
class Sitemap_Entity_Product extends Sitemap_Entity_Abstract {
  
  public function getItems(){
    $items = array();
    $sql = "SELECT p.id_prod as id, ph.img as img FROM `production` p 
    JOIN `prod_join_cat` pjc ON p.id_prod = pjc.id_prod AND p.prod_visible = 'public'
    JOIN `prod_category` pc ON pjc.id_cat = pc.id_cat AND cat_par = 359
    JOIN `photogallery` ph ON ph.id_obj = p.id_prod";
    $result = clGlob::$db->query( $sql );
    if ( $result ){
      while ( $row = $result->fetch_assoc() ){
        $items[] = new Sitemap_Item( "/product/" . $row['id'], 0.6, $row['img'] );
      }
    }
    return $items; 
  }
  
}