<?php 
class Sitemap_Entity_Smap extends Sitemap_Entity_Abstract {
  
  public function getItems(){
    $items = array();
    $items[] = new Sitemap_Item( '', 'daily');
    $items[] = new Sitemap_Item( '/news', 'daily');
    $items[] = new Sitemap_Item( '/trainings', 'daily');
    return $items; 
  }
  
}