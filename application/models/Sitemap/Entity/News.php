<?php
class Sitemap_Entity_News extends Sitemap_Entity_Abstract{
  
  public function getItems(){
    $items = array();
    
    $groups = new News_Group_Collection();
    $groups->loadGroups();
    
    foreach( $groups as $group ){
      $items[] = new Sitemap_Item( '/news-'. $group->alias, 'daily' );
    }
    
    $n = new News_Collection();
    $n->pager()->onPage = 10000;
    $n->loadNews();
    
    foreach ( $n as $article ){
      $items[] = new Sitemap_Item( '/news-'. $article->children('group')->alias.'/'.$article->id_news, 'monthly' );
    }
    
    return $items;
  }
  
}