<?php
class Sitemap_Generator{
  
  protected $items = array();
  protected $xml;
  
  public function addEntity( Sitemap_Entity_Abstract $entity ){
    $this->items = array_merge( $this->items, $entity->getItems() );
  }
  
  public function makeXml(){
    if ( is_array($this->items) ){
      $smarty = App::newSmarty();
      $smarty->assign( 'items', $this->items );
      $this->xml = $smarty->fetch( "sitemap/index.tpl" );
    } 
  }
  
  public function create( $destination = '' ){
    if ( $this->xml ){
      file_put_contents( $destination . "sitemap.xml", $this->xml );
    }
    echo "ADD " . count( $this->items ) . " LINKS TO SITEMAP";
  }
  
}