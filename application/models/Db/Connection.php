<?php
class Db_Connection{
  
  protected $encoding = 'UTF8';
  protected $mysqli;
  protected $debug = false;
  
  public function __construct( $host, $user, $password, $dbname, $debug = false ){ 
    if( $debug instanceof PHP_Debug ){
      $this->debug = $debug;
    }
    $this->mysqli = new mysqli($host, $user, $password, $dbname); 
    if ( $this->mysqli->connect_errno ){
      throw new Exception("<b>Database error</b>");
    }  
     
    $this->query("SET NAMES " . $this->encoding ); 
  }
  
  public function __call( $method, $args ){
    if ( method_exists( $this->mysqli, $method) ){
      return $this->mysqli->$method( $args[0] );
    }
  }
  
  public function query( $sql ){
    if ( $this->debug ){
      $this->debug->query( $sql );
      $result = $this->mysqli->query( $sql );
      $this->debug->stopTimer();
      return $result;
    }
    else {
      return $this->mysqli->query( $sql );
    }
  }
  
  public function escape( $val ){
    return $this->mysqli->real_escape_string( $val );
  } 
  
  public function __get( $prop ){
    return $this->mysqli->$prop;
  }
  
  public function __set( $prop, $val ){
    return $this->mysqli->$prop = $val;
  }
  
  public function newQuery(){
    return new Db_Query();
  }
  
}