<?php
class Db_Query{

  protected $foundRows = false;
  
  private $select = array();

  private $tables = array();

  private $where = array();

  private $innerJoin = array();

  private $leftJoin = array();

  private $limits = '';

  private $orderBy = array();
  
  private $having = array();

  private $groupBy = array();

  public function select () {
    if (func_num_args()) {
      $args = func_get_args();
      foreach ($args as $arg) {
        $this->select[] = $arg;
      }
    }
    return $this;
  }
  
  public function clearSelect(){
    $this->select = array();
    return $this;
  }
  
  public function clear(){
    
    $this->select = array();

    $this->tables = array();

    $this->where = array();

    $this->innerJoin = array();

    $this->leftJoin = array();

    $this->limits = '';

    $this->orderBy = array();
    
    $this->having = array();

    $this->groupBy = array();
    
    return $this;
  }
  
  public function from() {
    if (func_num_args()) {
      $args = func_get_args();
      foreach ($args as $arg) {
        $this->tables[] = $arg;
      }
    }
    return $this;
  }

  public function where() {
    if (func_num_args()) {
      $args = func_get_args();
      foreach ($args as $arg) {
        $this->where[] = $arg;
      }
    }
    return $this;
  }
  
  public function clearWhere(){
    $this->where = array();
    return $this;
  }

  public function innerJoin($table, $on){
    $this->innerJoin[] = '
    INNER JOIN ' . $table . ' ON ' . $on;
    return $this;
  }

  public function leftJoin($table, $on){
    $this->leftJoin[] = '
    LEFT JOIN ' . $table . ' ON ' . $on;
    return $this;
  }
  
  public function join( $table, $on = null ){
    $this->leftJoin[] = '
     JOIN ' . $table . ($on ? (' ON ' . $on) : '') ;
    return $this;
  }

  public function rightJoin($table, $on){
    $this->leftJoin[] = '
    RIGHT JOIN ' . $table . ' ON ' . $on;
    return $this;
  }

  public function limit($start, $count = NULL) {
    $this->limits = '
    LIMIT ' . (int)$start;
    if ($count)
      $this->limits .= ', ' . (int)$count;
    return $this;
  }

  public function orderBy() {
    if (func_num_args()) {
      $args = func_get_args();
      foreach ($args as $arg) {
        $this->orderBy[] = $arg;
      }
    }
    return $this;
  }
  
  public function clearOrderBy(){
    $this->orderBy = array();
    return $this;
  }
  
  public function having() {
    if (func_num_args()) {
      $args = func_get_args();
      foreach ($args as $arg) {
        $this->having[] = $arg;
      }
    }
    return $this;
  }

  public function groupBy() {
    if (func_num_args()) {
      $args = func_get_args();
      foreach ($args as $arg) {
        $this->groupBy[] = $arg;
      }
    }
    return $this;
  }
  
  public function clearGroupBy(){
    $this->groupBy = array();
    return $this;
  }

  public function toString() {
    $str = $this->foundRows ? 'SELECT SQL_CALC_FOUND_ROWS ' : 'SELECT ';
    $str .=  implode(', ', $this->select);
    if (count($this->tables))
      $str .= '
      FROM ' . implode(', ', $this->tables);
    if (count($this->innerJoin))
      $str .= implode(' ', $this->innerJoin);
    if (count($this->leftJoin))
      $str .= implode(' ', $this->leftJoin);
    if (count($this->where))
      $str .= '
      WHERE ' . implode(' AND ', $this->where);
    if (count($this->groupBy))
      $str .= '
      GROUP BY ' . implode(', ', $this->groupBy);
    if ( count($this->having) )
      $str .= ' HAVING ' . implode( ' AND ', $this->having );
    if (count($this->orderBy))
      $str .= '
      ORDER BY ' . implode(', ', $this->orderBy);
    if ($this->limits)
      $str .= $this->limits;
    return $str;
  }

  public function __toString () {
    return $this->toString();
  }
  
  public function disableFoundRows(){
    $this->foundRows = false;
    return $this;
  }
  
  public function enableFoundRows(){
    $this->foundRows = true;
    return $this;
  }
  
  public function isEnableFoundRows(){
    return $this->foundRows;
  }
  
}