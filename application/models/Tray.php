<?php 
class Tray{
  
  static protected $nodes = array();
  
  static public function push( Tray_Node $node ){
    static::$nodes[] = $node;
  } 
  
  static public function prepend( Tray_Node $node ){
    array_unshift(static::$nodes, $node);
  }
  
  static public function getNodes(){
    return static::$nodes;
  }
  
  static public function clear(){
    static::$nodes = array();
  }
  
}