<?php
class Layout_Backend extends Layout{
  
  protected $tpl = 'layout/backend.tpl';
  
  protected function init(){    
    App::view()->css()->add('/assets/css/bootstrap.min.css');
    App::view()->css()->add('/assets/css/bootstrap-responsive.min.css');
   
    App::view()->js()->add('/assets/js/jquery.js');
    App::view()->js()->add('/assets/js/bootstrap.min.js');
    App::view()->js()->add('/assets/js/bootstrap-datepicker.js');
    //App::view()->css()->add('/assets/css/datepicker.css');
  }
  
  public function beforeRender(){
    $this->args['message'] = Stalexo_Messenger::getMessage();
  }
  
}