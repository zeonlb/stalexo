<?php
class Layout_Direction extends Layout{

  protected $tpl = 'layout/direction.tpl';

  public function beforeRender(){
    $agents = User_Staff::fetchByQuery(
      User_Staff::getFetchQuery()
      ->join('staff_device sd', 'sd.id_staff = staff.id')
      ->where('staff.id_parent > 0 AND sd.is_production = 1')
      ->orderBy('staff.name')
    );
	
	$sql  = "SELECT * FROM staff_device sd inner join staff st on st.id=sd.id_staff inner join staff st2 on st.id_parent=st2.id group by st2.id";
	$r = App::db()->query($sql);
	if( $r ){
	  while( $row = $r->fetch_assoc() ) {
		$directors[] = $row;
	  }
	}
	
	$sql  = "SELECT cl.id as id, cl.title as title FROM clients cl inner join distribution_points dp on dp.id_client=cl.id inner join distribution_point_contracts dpc on dpc.id_client=cl.id and dpc.id_distribution_point=dp.id inner join staff_device sd on sd.id_staff=dpc.id_manager group by cl.id";
	$r = App::db()->query($sql);
	if( $r ){
	  while( $row = $r->fetch_assoc() ) {
		$clients[] = $row;
	  }
	}
	
	if (Stalexo_Direction::getClient()) {
		$sql  = 'SELECT distinct dp.id as id, dp.address as title, dp.id_client as id_client FROM distribution_points dp inner join clients cl on dp.id_client=cl.id inner join distribution_point_contracts dpc on dpc.id_client=cl.id and dpc.id_distribution_point=dp.id inner join staff_device sd on sd.id_staff=dpc.id_manager WHERE cl.id = "'.Stalexo_Direction::getClient().'"';
		$r = App::db()->query($sql);
		if( $r ){
		  while( $row = $r->fetch_assoc() ) {
			$addresses[] = $row;
		  }
		}
		$this->args['addresses'] = $addresses;
	} else {
		$this->args['addresses'] = array();
	}
	
	$this->args['directors'] = $directors;
	$this->args['clients'] = $clients;
		

    $this->args['agents']     = $agents;
    $this->args['agent']      = Stalexo_Direction::getAgent();
	$this->args['director']      = Stalexo_Direction::getDirector();
	$this->args['client']      = Stalexo_Direction::getClient();
	$this->args['address']      = Stalexo_Direction::getAddress();
    $this->args['filterDate'] = Stalexo_Direction::getDate();
    $this->args['filterDate2'] = Stalexo_Direction::getDate2();
  }

}