<?php
class App_Autoloader{

  static protected $init = false;

  public static function init(){
    if ( self::$init ){
      return true;
    }
    self::$init = true;
    spl_autoload_register( 'App_Autoloader::load' );
  }

  protected static function load( $class ){
    $classPath = Settings::path()->models . str_replace( '_', '/', $class ) . '.php';

    $loadController = false;
    if ( substr($class, -14, 14) == 'AjaxController' ){
      $path = explode('_', $class, -1);
      $path = array_map("strtolower", $path);
      $folders = count($path) ? (implode('/', $path) . '/') : '';
      $classPath = Settings::path()->controllers . $folders . 'ajax/' . basename(str_replace('_', '/', $class) . '.php');
      $loadController = true;
    } elseif( substr($class, -10, 10) == 'Controller' ){
      $path = explode('_', $class, -1);
      $path = array_map("strtolower", $path);
      $folders = count($path) ? (implode('/', $path) . '/') : '';
      $classPath = Settings::path()->controllers . $folders . basename(str_replace('_', '/', $class) . '.php');
      $loadController = true;
    }

    if ( is_file($classPath) ) {
      require_once($classPath);
    } else {
      if ( $loadController ){
        throw new App_AutoloaderException('Invalid controller: ' . $class);
      }
    }
  }

}

class App_AutoloaderException extends Exception{}