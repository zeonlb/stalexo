<?php
require_once Settings::path()->vendor . 'smarty/SmartyBC.class.php';

class App_Smarty extends SmartyBC {

  public function __construct( $debug = false ){
    parent::__construct();
    $this->template_dir = Settings::path()->tpl;
    $this->compile_dir  = Settings::path()->tplCompile;
    $this->config_dir   = Settings::path()->vendor . 'smarty/configs/';
    $this->cache_dir    = Settings::path()->cache  . 'smarty/';

    if ( $debug ){
      $this->caching = false;
      $this->compile_check = false;
      $this->force_compile = true;
    }

  }

}