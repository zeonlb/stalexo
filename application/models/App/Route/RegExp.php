<?php 
class App_Route_RegExp extends App_Route_Abstract{

  public function match($url){
    if ( preg_match( $this->pattern, urldecode($url) ) ){
      return true;
    }
    return false;
  }
  
}