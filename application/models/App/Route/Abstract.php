<?php 
abstract class App_Route_Abstract{
  
  protected $pattern = '/';
  public $module = false;
  public $controller = 'index';
  public $action = 'index';
  public $smap = 'smap';
  
  public function __construct( $pattern, $module = false, $controller = 'index', $action = 'index', $smap = 'smap' ){
    $this->pattern = $pattern;
    $this->module = $module; 
    $this->controller = $controller;
    $this->action = $action;      
    if ( $smap ){
      $this->smap = $smap;
    } else {
      $this->smap = $this->module ? $this->module : $this->controller;
    }
  }
  
  public function getPattern(){
    return $this->pattern;
  }
  
  abstract public function match($url);
  
}