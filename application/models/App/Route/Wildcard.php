<?php 
class App_Route_Wildcard extends App_Route_Abstract{

  public function match($url){
    $urlArr = explode('/', $url);
    $wildcardUrlArr = explode('/', $this->pattern);
    $urlArrCount = count($wildcardUrlArr);
    if ( $urlArrCount != count($wildcardUrlArr) ){
      return false;
    }
    if ( ! $this->module and $urlArrCount == 2 ){
      if ( ($wildcardUrlArr[0] == $urlArr[0]) and ($wildcardUrlArr[1] == '*' or $wildcardUrlArr[1] == $urlArr[1]) ) {
        return true;
      }
    } elseif ( $this->module and $urlArrCount == 3 ){
      if ( ($wildcardUrlArr[0] == $urlArr[0])
      and ($wildcardUrlArr[1] == '*' or $wildcardUrlArr[1] == $urlArr[1])
      and ($wildcardUrlArr[2] == '*' or $wildcardUrlArr[2] == $urlArr[2])
      ) {
        return true;
      }
    }
    return false;
  }
  
}