<?php 
class App_Router{
  
  public static $routes = array();
  
  public static function addRoute( App_Route_Abstract $route ){
    self::$routes[$route->getPattern()] = $route;
  }
    
  /**
  * @return App_Route_Abstract
  * 
  * @param string $pattern
  */
  public static function getRoute( $pattern ) {
    foreach ( self::$routes as $route ){
      if ( ($route instanceof App_Route_Static) and $route->match($pattern) ){
        return $route;
      }
    }
    foreach ( self::$routes as $route ){
      if ( ($route instanceof App_Route_Wildcard) and $route->match($pattern) ){
        return $route;
      }
    }
    foreach ( self::$routes as $route ){
      if ( ($route instanceof App_Route_RegExp) and $route->match($pattern) ){
        return $route;
      }
    }
    return false;
  }
  
  public static function dropRoutes(){
    self::$routes = array();
  }
  
}