<?php 
class App_View{
  
  protected $tpl404 = '404.tpl';
  protected $tplForbidden = 'forbidden.tpl';
  
  protected $args = array();
  
  protected $layout = null;
  
  protected $wrapper = null;
  
  protected $body = '';
  protected $js;
  protected $css;
  
  protected $smap = null;
  
  public $title = '';
  public $description = '';
  public $keywords = '';
  public $pageName = '';
  
  public function __construct(){
    $this->js = new App_Iterator();
    $this->css = new App_Iterator();
    //$this->smap = new Smap_Model();
  }
  
  public function js(){
    return $this->js;
  }
  
  public function css(){
    return $this->css;
  }
  
  public function setSmap( Smap_Model $smap ){
    $this->smap = $smap;
    $this->title = $smap->title_lang;
    $this->description = $smap->description_lang;
    $this->keywords = $smap->keywords_lang;
    $this->pageName = $smap->name_lang;
    if ( $smap->type == 'page' ){
      $this->add( new Layout_Smap($smap->getPageContent()) );
    }
  }
  
  public function setLayout( Layout $layout ){
    $this->layout = $layout;
  }
  
  public function setWrapper( Layout $layout ){
    $this->wrapper = $layout;
  }
  
  public function smap(){
    return $this->smap;
  }
  
  public function clear(){
    $this->body = '';
  }
  
  public function add( $html ){
    $this->body .= $html;
  } 
  
  public function assign( $key, $val ){
    $this->args[$key] = $val;
  }
  
  public function display(){
    $layoutArgs = array(
      'js'           => $this->js,
      'css'          => $this->css,
      'title'        => $this->title,
      'description'  => $this->description,
      'keywords'     => $this->keywords,
      'flashMessage' => Messenger::displayMessage(),
      //'smap'         => $this->smap
    );
    $layoutArgs = array_merge( $this->args, $layoutArgs );
    $page = $this->layout->setContent($this->getBody())->addArgs($layoutArgs)->__toString();
    echo $page;
  }
  
  public function getBody(){
    if ( $this->wrapper ){
      $this->wrapper->setContent($this->body);
      return $this->wrapper;
    }
    return $this->body;
  }
  
  public function render404( $message = false ){
    $smarty = App::newSmarty();
    $smarty->assign(array(
      'message' => $message
    ));
    header("HTTP/1.0 404 Not Found");
    $smarty->display( $this->tpl404 );
    exit();
  }
  
  public function renderForbidden( $message = false ){
    $smarty = App::newSmarty();
    $smarty->assign(array(
      'message' => $message
    ));
    header("HTTP/1.0 404 Not Found");
    $smarty->display( $this->tplForbidden );
    exit();
  }
  
  public function clearMeta(){
    $this->title = '';
    $this->description = '';
    $this->keywords = '';
  }
  
}