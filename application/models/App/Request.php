<?php 
class App_Request{
  
  protected $post = array();
  protected $get = array();
  protected $request = array();
  protected $uri = '';
  protected $server = array();
  protected $requestMethod = null;
  
  protected $controller = 'index';
  protected $action = 'index';
  protected $module = null;
  
  public function __construct( Array $get, Array $post, Array $request, Array $server ){
    $this->get = $get;
    $this->post = $post;
    //var_dump($post);
    $this->request = $request;
    $this->server = $server;
    $this->uri = $server['REQUEST_URI'];
    $this->checkRequestMethod();
  }
  
  protected function checkRequestMethod(){
    if( isset($this->server['HTTP_X_REQUESTED_WITH']) and $this->server['HTTP_X_REQUESTED_WITH'] == "XMLHttpRequest" ){
      $this->requestMethod = 'ajax';
    } else {
      $this->requestMethod = strtolower($this->server['REQUEST_METHOD']);
    }
  }
  
  public function post( $key = false ){
    if ( $key !== false ){
      if ( isset($this->post[$key]) ){
        return $this->post[$key];
      }
      return null;
    }
    return $this->post;
  }
  
  public function get( $key = false ){
    if ( $key !== false ){
      if ( isset($this->get[$key]) ){
        return $this->get[$key];
      }
      return null;
    }
    return $this->get;
  }
  
  public function request( $key = false ){
    if ( $key !== false ){
      if ( isset($this->request[$key]) ){
        return $this->request[$key];
      }
      return null;
    }
    return $this->request;
  }
  
  public function setPost( Array $post ){
    $this->post = $post;
  }
  
  public function setGet( Array $get ){
    $this->get = $get;
  }
  
  public function setRequest( Array $request ){
    $this->request = $request;
  }
  
  public function getUri(){
    return $this->uri;
  }
  
  public function isAjax(){
    return (bool)( 'ajax' == $this->requestMethod );
  }
  
  public function isPost(){
    return (bool)( 'post' == $this->requestMethod );
  }
  
  public function isGet(){
    return (bool)( 'get' == $this->requestMethod );
  }
  
  public function getController(){
    return $this->controller;
  }
  
  public function setController( $controller ){
    $this->controller = $controller;
  }
  
  public function getAction(){
    return $this->action;
  }
  
  public function setAction( $action ){
    $this->action = $action;
  }
  
  public function setModule( $module ){
    $this->module = $module;
  }
  
  public function getModule(){
    return $this->module;
  }
  
  public static function redirect( $url = '/' ){
    header("Location: " . $url);
    exit();
  }
  
  /**
  * Send get request
  * 
  * @return string $result
  * @param string $url
  * @param Array $args
  */
  public static function sendGET( $url, $args ) {
    $ch = curl_init();  
    curl_setopt($ch, CURLOPT_URL, $url . "?" . http_build_query($args));
    curl_setopt($ch, CURLOPT_POST, false);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); 
    $result = curl_exec($ch);
    curl_close($ch); 
    return $result;
  }

  /**
  * Send post request
  * 
  * @return string $result
  * @param string $url
  * @param Array $args
  */
  public static function sendPost( $url, $args ) {
    $ch = curl_init();  
    curl_setopt($ch, CURLOPT_URL, $url );
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); 
    $result = curl_exec($ch);
    curl_close($ch); 
    return $result;
  }
  
}