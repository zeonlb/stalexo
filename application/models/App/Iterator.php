<?php 
class App_Iterator implements Iterator {
  
  protected $position = 0;
  protected $array = array();  

  public function __construct() {
    $this->position = 0;
  }

  public function count() {
    return count($this->array);
  }

  public function isEmpty() {
    return empty($this->array);
  }

  public function rewind() {
    $this->position = 0;
  }

  public function current() {
    return $this->array[$this->position];
  }

  public function key() {
    return $this->position;
  }

  public function next() {
    ++$this->position;
  }

  public function valid() {
    return isset($this->array[$this->position]);
  }

  public function add( $val, $key = null ) {
    if ( is_null($key) ) {
      array_push( $this->array, $val );
    } elseif ( is_numeric($key) ) {
      $this->array[$key] = $val;
    }
  }
  
  public function unshift( $val ) {
    array_unshift($this->array, $val);
  }

  public function remove( $key ) {
    if( is_numeric($key) ) {
      unset($this->array[$key]);
    }
  }

  public function removeByVal( $val ) {
    foreach($this->array as $key => $innerVal) {
      if ($innerVal === $val) {
        unset($this->array[$key]);    
      }  
    }
  }

  public function isExists( $val ) {
    foreach ( $this->array as $innerVal ) {
      if ( $innerVal === $val ) {
        return true;
      }
    }
    return false;
  }

  public function keyExists( $key ) {
    return array_key_exists($key, $this->array);
  }

  public function getElementByKey( $key ) {
    if ( isset($this->array[$key]) ) {
      return $this->array[$key];
    }
  }
  
  public function last(){    
    return end($this->array); 
  }
  
  public function clear(){
    $this->array = array();
  }
  
}
