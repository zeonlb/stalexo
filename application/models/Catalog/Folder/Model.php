<?php 
class Catalog_Folder_Model extends Dao{
  
  static protected $pk    = 'id_cat';
  static protected $table = 'prod_category';
  static protected $alias = 'cat_alias';
  
}