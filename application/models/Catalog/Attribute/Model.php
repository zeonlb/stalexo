<?php 
class Catalog_Attribute_Model extends Dao{
  
  static protected $pk = 'id_val';
  static protected $table = 'attribute_values';
  
  public function variant($key, $index = 0){
    if ( isset($this->children['variants'][$index][$key]) ){
      return $this->children['variants'][$index][$key];
    }
    return null;
  }
  
  protected function populate( Array $row ){
    foreach ($row as $key => $val) {
      if( is_array($val) ){
        $this->children[$key] = $val;
        continue;
      }
      $this->{$key} = $val;
    }
  }
  
  public function addVariant( $variant, $key ){
    $this->children[$key] = $variant;
  }
  
  /**
  * @return Array Variants for attribute 
  */
  public function getAllVariants(){
    $variants = array();
    $query = Catalog_Attribute_Variant::getFetchQuery();
    $query
    ->orderBy(Catalog_Attribute_Variant::getTableName().'.fixed, '.Catalog_Attribute_Variant::getTableName().'.var_val')
    ->where(Catalog_Attribute_Variant::getTableName() . '.id_attrib = ' . $this->id_attrib);
    $result = App::db()->query($query);
    if ( $result ){
      while( $row = $result->fetch_assoc() ){
        $variants[] = new Catalog_Attribute_Variant($row);
      }
    }
    return $variants;
  }
  
  public function insertValue( $idObj, $idVariant, $value ){
    $this->db()->query("DELETE FROM `attribute_values` WHERE id_obj = '". $idObj ."' AND id_atree = '". $this->id_atree ."' LIMIT 1");
    $sql = "INSERT INTO `attribute_values` SET id_obj = '". $idObj ."', id_variant = '". $idVariant ."', id_atree = '". $this->id_atree ."', val_value = '". $this->db()->escape($value) ."', val_value_ru = '". $this->db()->escape($value) ."'";
    $this->db()->query($sql);
  }
  
}