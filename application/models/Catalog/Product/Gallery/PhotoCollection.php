<?php 
class Catalog_Product_Gallery_PhotoCollection extends Collection{
  
  protected function initQuery(){
    $this->query = Catalog_Product_Gallery_Photo::getFetchQuery()->enableFoundRows();
    $this->query->where('pg.visible = "yes"');
  }
  
  public function LoadUserPhotogallery() {
    $this->query->disableFoundRows()
    ->join('photogallery_types pt', 'pt.id = pg.id_type')
    ->where('pt.name = "Пользователи" AND id_obj = '.App::user()->userid);
    
    $result = $this->executeQuery();
    
    if ( !$result ){
      return false;
    }
    
    while( $row = $result->fetch_assoc() ){
      $this->add( new Catalog_Product_Gallery_Photo( $row ) );
    }
  }
  
}