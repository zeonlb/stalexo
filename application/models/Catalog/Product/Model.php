<?php 
class Catalog_Product_Model extends Dao{
  
  static protected $pk    = 'id_prod';
  static protected $table = 'production';
  static protected $tableQueryAlias = 'p';
  static protected $alias = 'prod_alias';
  
  static public $attributesMap = array();
  static protected $attributesTreeId = 0;
  
  protected $externalFields = array('price', 'comments_rank', 'comments_count');
  
  public function getRank(){
    if ( is_numeric($this->comments_rank) ){
      return $this->comments_rank;
    }
    return 0;
  }
  
  /**
  * @return Array attributes from ::children()
  */
  public function attributes(){
    if ( isset($this->children['attributes']) and is_array($this->children['attributes']) ){
      return $this->children['attributes'];
    }
    return array();
  }
  
  public function attribute( $key ){
    if ( isset($this->children['attributes']) and isset($this->children['attributes'][$key]) ){
      return $this->children['attributes'][$key];
    }
    return new Catalog_Attribute_Model();
  }
   
  /**
  * Load attributes using id_atree from ::$attributesTreeId
  */
  public function loadProductAttributes() {
    if ( ! $this->isLoaded() ){ return false; }
    if ( empty(static::$attributesMap) ) {
      static::$attributesMap = self::loadAttributesMap(static::$attributesTreeId);
    }
    
    $lang = App::instance()->activeLanguage()->lang_alias;
    $sql = ' SELECT 
            avl.*,
            avr.var_text_'.$lang.' AS var_text, '
           .' 
              a.`attrib_alias` AS attrib_alias,
               a.`attrib_type`,
            a2.attrib_alias  AS folder_alias
          FROM
            attribute_values AS avl  
            JOIN attributes_tree AS atree
              ON atree.`id_atree` = avl.`id_atree`
            JOIN attributes_tree AS atree2
              ON atree2.`id_atree` = atree.`atree_par`
            JOIN attributes AS a 
              ON a.`id_attrib` = atree.`id_attrib`
            JOIN attributes AS a2 
              ON a2.`id_attrib` = atree2.`id_attrib` 
            LEFT JOIN attribute_variants AS avr 
              ON avl.`id_variant` = avr.`id_variant`'
        ." WHERE avl.`id_obj` = '" . $this->{static::$pk} . "' ORDER BY avr.var_prior ";
    
    
    $this->children['attributes'] = array();
    $this->children['attributes'] = static::$attributesMap;
    
    $res = App::db()->query( $sql );
    while ( $row = $res->fetch_assoc() ) {
      $textValue = in_array($row['attrib_type'], array('text', 'date', 'time'))
        ? $row['val_value']
        : $row['val_text_value'];
      
      /*
      $textValue = ( $row['id_variant'] == 0 ) 
                   ? (( in_array($row['attrib_type'], array('text', 'time', 'date'))) ? $row['val_value'] : $row['val_text_value']) 
                   : $row['var_text'];
      */             
      $infoArray = array('text_value'=>$textValue, 'id_variant' => $row['id_variant'] );
      $this->children['attributes'][$row['attrib_alias']]['variants'][] = $infoArray;
    }
    
    foreach( $this->children['attributes'] as $key => $attr ){
      $this->children['attributes'][$key] = new Catalog_Attribute_Model( $attr );
    }
       
  }
  
  public function loadComments(){
    $comments = new Comment_Collection();
    $comments->load( $this->{static::$pk}, 'shop' );
    $this->children['comments'] = $comments;
  }
  
  public function loadPhotoGallery() {
    $sql = ' SELECT * '
            .' FROM `photogallery` '
            ." WHERE `id_obj` = '" . (int)$this->{static::$pk} . "' "
            ." ORDER BY `on_title`, `prior` ASC ";
    $res = App::db()->query($sql);
    if ( !$res ) {
      return false;
    }
    $this->children['photogallery'] = array();
    while ( $row = $res->fetch_assoc() ) {
      if ( ($row['on_title'] == 'yes') && !array_key_exists( 'title', $this->children['photogallery'] ) ) {
        array_unshift($this->children['photogallery'], new Catalog_Product_Gallery_Photo( $row ) );
      } else {
        $this->children['photogallery'][] = new Catalog_Product_Gallery_Photo( $row );
      }
    }
  }
  
  static protected function loadAttributesMap( $treeId = 0 ) {
    //$lang = App::instance()->activeLanguage()->lang_alias;
    $arrayMap = array();
    $arrayMap = self::loadAttributes($treeId);
    /*
    $sql = ' SELECT atree.id_attrib, atree.id_atree, a.attrib_alias
                     ,atree.atree_end, a.id_attrib
                     ,a.attrib_description_'.$lang.' AS attrib_description '
            .' FROM attributes_tree AS atree '
            .' INNER JOIN attributes AS a ON a.id_attrib = atree.id_attrib '  
            ." WHERE atree.atree_end = 'no' "
            .' ORDER BY atree.`atree_prior` ASC ';
            
    $res = App::db()->query( $sql );
    
    while ( $row = $res->fetch_assoc() ) {
      $arrayMap[$row['attrib_alias']] = $row;
      $arrayMap[$row['attrib_alias']]['children'] = self::loadAttributes( $row['id_atree'] );
      if ( empty($arrayMap[$row['attrib_alias']]['children']) ) {
        unset($arrayMap[$row['attrib_alias']]);
      }
    }
    */        
    return $arrayMap;
  }
  
  /**
  * @var int $parentId - id of parent tree node
  */
  static protected function loadAttributes( $parentId ) {
    $lang = App::instance()->activeLanguage()->lang_alias;
    $arrayMap = array();
    $sql = ' SELECT atree.id_attrib, atree.id_atree, a.attrib_alias
                     ,atree.atree_end, a.id_attrib
                     ,a.attrib_description_'.$lang.' AS attrib_description '
            .' FROM attributes_tree AS atree '
            .' INNER JOIN attributes AS a ON a.id_attrib = atree.id_attrib '  
            .' ORDER BY atree.`atree_prior` ASC ';
    $res = App::db()->query( $sql );
    
    while ( $row = $res->fetch_assoc() ) {
      if ( $row['atree_end'] != 'no' ) {
        $arrayMap[$row['attrib_alias']] = $row;
      }
    } 
           
    return $arrayMap;
  }
  
  static protected function wrapFetchQuery( Db_Query $query ){
    $query->select('pr.price')->join('prices pr', static::$tableQueryAlias.'.id_price = pr.id_price')
    ->select( 'ROUND(AVG(IF (c.rating = 0, NULL, c.rating ))) as comments_rank' )
    ->select( 'COUNT(c.id_comment) as comments_count' )
    ->leftJoin('comments c', 'c.tovar = '.static::$tableQueryAlias.'.id_prod ');
    return $query;
  }
  
  /**
  * Update price in `prices`; update `production`.`id_price`
  * 
  * @param float $price
  */
  public function updatePrice($price) {
    if ( ! $this->isLoaded() ) { return false; }
    if ( is_numeric($this->price) and $this->price == $price ){ return true; }
    $price = number_format( $price, 2, '.', '');
    $this->db()->query("INSERT INTO `prices` SET id_prod = '".$this->id_prod."', date = '". time() ."', price = '". $price ."'");
    $this->id_price = $this->db()->insert_id;
    $this->save();
  }
  
  /**
  * Join category in `prod_join_category`
  * 
  * @param int $id
  */
  public function joinCategory($id) {
    $this->db()->query("DELETE FROM `prod_join_cat` WHERE id_prod = '". $this->id_prod ."' LIMIT 1");
    $this->db()->query("INSERT INTO `prod_join_cat` SET id_prod = '". $this->id_prod ."', id_cat = '". (int)$id ."'");
  }
  
  public function updateAttributeValue( Catalog_Attribute_Model $attr, $idVariant, $value ){
    if ( ! $this->isLoaded() ) { return false; }
    $attr->insertValue( $this->id_prod, $idVariant, $value );
  }
  
  /**
  * Join user in `user_join_prod`
  * 
  * @param User_Abstract $user
  */
  public function joinUser( User_Abstract $user ){
    $this->db()->query("INSERT IGNORE INTO `user_join_prod` SET id_user = '". $user->userid ."', attached_prod = '". $this->id_prod ."'");
  }
  
  /**
  * Join product in `prod_join_prod`
  * 
  * @param mixed $id
  */
  public function joinProduct( $id ){
    App::db()->query("INSERT IGNORE INTO `prod_join_prod` SET parent_prod = '".$this->id_prod."', attached_prod = '". (int)$id ."'");
  }
  
  /**
  * Delete joined from `prod_join_prod`
  * 
  */
  public function unjoinProducts(){
    App::db()->query("DELETE FROM `prod_join_prod` WHERE parent_prod = '". $this->id_prod ."'");
  }
  
  /**
  * Update lat, lng by address
  * 
  * @param string $address
  */
  public function updateLatLng( $address ){
    if ( $this->isLoaded() and Valid::address($address) ){
      require_once Settings::path()->vendor . 'google-map/GoogleMap.php';
      $gmap = new GoogleMapAPI();       
      $coords = $gmap->geoGetCoords($address);
      $lat = $coords['lat'];
      $lng = $coords['lon'];
      if ( $this->lat != $lat or $this->lng != $lng ) {
        $this->lat = $lat;
        $this->lng = $lng;
        $this->save();
      }
    }    
  }
  
  public function removeImages(){
    if ( ! $this->isLoaded() ){
      return false;
    }
    if ( $this->prod_img_big_orig and file_exists($this->prod_img_big_orig) ){
      unlink($this->prod_img_big_orig);
      $this->prod_img_big_orig = '';
    }
    if ( $this->prod_img_big and file_exists($this->prod_img_big) ){
      unlink($this->prod_img_big);
      $this->prod_img_big = '';
    }
    if ( $this->prod_img_thumb and file_exists($this->prod_img_thumb) ){
      unlink($this->prod_img_thumb);
      $this->prod_img_thumb = '';
    }
    $this->save(false);
  }
  
  public function joinAuthor($id){
    App::db()->query("INSERT IGNORE INTO `prod_join_author` VALUES('".$this->id_prod."', '".(int)$id."')");
  }

  public function unjoinAllAuthors(){
    App::db()->query("DELETE FROM `prod_join_author` WHERE id_prod = '".$this->id_prod."'");
  }
  
  public function loadAuthors(){
    $this->children['authors'] = array();
    $query = User_Coach::getFetchQuery()->disableFoundRows()
    ->join('prod_join_author pja', 'pja.id_prod = "'.$this->id_prod.'" AND pja.id_user = vb_user.userid')->limit(100)
    ->groupBy('vb_user.userid');
    $result = App::db()->query($query);
    if ( $result ){
      while( $row = $result->fetch_assoc() ){
        $this->children['authors'][] = new User_Coach($row);
      }
    }  
  }
  
}