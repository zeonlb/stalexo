<?php 
class Catalog_Product_Collection extends Collection{
  
  protected function initQuery(){
    $this->query = Catalog_Product_Model::getFetchQuery()->enableFoundRows();
    $this->query
    ->join('prod_join_cat pjc', 'p.id_prod = pjc.id_prod')
    ->join('prod_category pc', 'pc.id_cat = pjc.id_cat AND pc.cat_visible = "yes"')
    ->groupBy('p.id_prod')
    ->orderBy('p.prod_prior')
    ->where('p.prod_visible = "public"');
  }
  
}