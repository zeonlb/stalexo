{if isset($message) and is_array($message)}
<div class="alert{if isset($message.type)} alert-{$message.type}{/if}">
<button onclick="javascript:this.parentNode.remove();" type="button" class="close" data-dismiss="alert">×</button>
{$message.text}
</div>
<script type="text/javascript">
$(document).ready(function(){
  setTimeout(function(){
    $('.close').click();
  }, 5000);
});
</script>
{/if}