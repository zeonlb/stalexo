{if isset($text)}
<script type="text/javascript">
{literal}
$(document).ready(function(){
  setTimeout(function(){
    Messenger.{/literal}{$type}{literal}.show({ 
      text: '{/literal}{$text|htmlspecialchars}{literal}' 
    });
  }, 200);
});
{/literal}
</script>
{/if}