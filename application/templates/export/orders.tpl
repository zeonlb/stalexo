<?xml version="1.0" encoding="UTF-8"?>
<orders>
{foreach from=$orders item=order}
{if count($order->children('sku'))}
<order id="{$order->id}" id_manager="{$order->id_manager}" id_distribution_point="{$order->id_distribution_point}" date="{$order->date}" date_shipping="{$order->date_shipping}">
{foreach from=$order->children('sku') item=sku}
<sku id_order="{$order->id}" id_sku="{$sku->id}" quantity="{$sku->quantity}"></sku>
{/foreach}
</order>
{/if}
{/foreach}
</orders>