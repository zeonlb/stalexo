<h1 style="margin-left:40px;">Восстановление пароля</h1>

<p style="margin-left:40px; font:13px Tahoma; color:#333;">После заполнения формы, вам на электронную почту<br/>
  будет отправлено письмо с дальнейшими инструкциями<br/>
  по восстановлению пароля.</p>
<form name="formPasswordRecovery" action="/auth/password-recovery" method="post"> 
<div class="registration2">
  <div class="inputsRegist2">  
    <p>Эл. почта</p>
    <input type="text" name="email" class="middleInput"/>
    <div class="error_reg none">
      <span class="error" style="margin-left: 0;">Ой!</span>
      <span>Такого адреса не существует</span>
    </div>
  </div>
</div>
<div class="regorngbutt">
<a class="ornagbut" href="javascript:;" id="recoverLink">Отправить<i class="orngleft"></i><i class="orngright"></i></a>
<input type="submit" name="submit" value="continue" class="none">
</div>
</form>

<script type="text/javascript">
  {literal}
  $(document).ready(function(){
    
    Auth.passRecovery.initForm('[name="formPasswordRecovery"]');
    
    $('#recoverLink').click(function(e){
      $('[name="formPasswordRecovery"]').submit();
    });
  });
  {/literal}
</script>
