{if $linkValid eq true}
<h1 style="margin-left:40px;">Восстановление пароля</h1>
<form name="formChangePassword" action="/auth/change-password" method="post">
  <div class="registration2">
    <div class="inputsRegist2">
      <p>Новый пароль</p>
      <input type="password" name="password" class="middleInput"/><br/><br/>
      <div class="error_reg none">
        <span class="error" style="margin-left: 0;">Ой!</span>
        <span>6 - 15 букв или цифр</span>
      </div>
      {*
      <p>Повторите пароль</p>
      <input type="password" name="repeat_password" class="middleInput"/>
      *}
    </div>
  </div>
  <div class="regorngbutt"><a class="ornagbut" href="javascript:;" id="submitLink">Отправить<i class="orngleft"></i><i class="orngright"></i></a></div>
  <input type="hidden" name="hash" value="{$hash}" />
  <input type="submit" name="submit" value="continue" class="none">
</form>

<script type="text/javascript">
{literal}
$(document).ready(function(){
  Auth.changePass.initForm('[name="formChangePassword"]');
  
  $('#submitLink').click(function(e){
    e.preventDefault();
    $('[name="formChangePassword"]').submit();
  });
  
});
{/literal}
</script>
{else}
<h1>Ссылка не действительна или пароль уже изменен.</h1> 
{/if}


