<div class="infoBlock_in">
  <h3 style="margin-left:40px;">Регистрация</h3>
  <form action="/registration" name="formRegistration" method="post">
    <div class="registration">
      <div class="first">
        <label>Я&nbsp;&mdash;&nbsp;</label>
        <input type="radio" name="role" value="3" class="bla" checked="checked" /><label>Компания</label>
        <input type="radio" name="role" value="2" /><label>Тренер</label>
        <input type="radio" name="role" value="1" /><label>Посетитель</label>
      </div>
      <div class="inputsRegist">
        <div>
          <p id="textUsername">Название вашей компании</p>
          <input type="text" name="username"/>
          <div class="error_reg none">
            <span class="error">Ой!</span>
            <span>Неправильный формат</span>
          </div>
        </div>
        <div class="none">
          <p id="textUsername">Фамилия</p>
          <input type="text" name="secondname"/>
          <div class="error_reg none">
            <span class="error">Ой!</span>
            <span>Неправильный формат</span>
          </div>
        </div>
        <div>
          <p>Эл. почта</p>
          <input type="text" name="email" class="middleInput"/>
          <div class="error_reg none">
            <span class="error">Ой!</span>
            <span>Неправильный формат email</span>
          </div>
        </div>
        <div>
          <p>Пароль</p>
          <input type="password" name="password" class="middleInput"/>
          <div class="error_reg none">
            <span class="error">Ой!</span>
            <span>6 - 15 цифр или букв</span>
          </div>
        </div>

        <div>
          <p>Повторите пароль</p>
          <input type="password" name="repeatPassword" class="middleInput"/>
          <div class="error_reg none">
            <span class="error">Ой!</span>
            <span>Пароли не совпадают</span>
          </div>
        </div>
        {*
        <div class="capcha">
          <p>Символы на картинке</p>
          <input type="text" name="capcha" class="smallInput"/>
          <label><span class="capcha"><img src="us_im/capcha.jpg"/></span><span class="reboot"><a href="#" class="imgReboot"><img src="im/reboot.png"/></a><a href="#">Обновить</a></span></label>
        </div>
        *}
        <input type="submit" style="display: none;" name="submit" value="continue" />
      </div>
    </div>

    <div class="regorngbutt"><a class="ornagbut" id="submitRegistration" href="javascript:;">Зарегистрировать<i class="orngleft"></i><i class="orngright"></i></a></div>
  </form>
  <div class="footerheight"></div>
</div>

<script type="text/javascript">
  {literal}
  $(document).ready(function(){
  Auth.registration.initForm('[name="formRegistration"]');

  $('#submitRegistration').click(function(){
    $('[name="formRegistration"]').submit();
  });

  $('[name="role"]').change(function(){
    var textUsername = {
    '1' : 'Имя',
    '2' : 'Имя',
    '3' : 'Название вашей компании'
    };
    
    if ( $(this).val() == 3 ){
      $('[name="secondname"]').parent().addClass('none');
    } else {
      $('[name="secondname"]').parent().removeClass('none');
    }
    
    $('#textUsername').text( textUsername[$(this).val()] );
  });
  });
  {/literal}    
</script>