<style>
{literal}
.form-signin {
  max-width: 300px;
  padding: 19px 29px 29px;
  margin: 0 auto 20px;
  background-color: #fff;
  border: 1px solid #e5e5e5;
  -webkit-border-radius: 5px;
     -moz-border-radius: 5px;
          border-radius: 5px;
  -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
     -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
          box-shadow: 0 1px 2px rgba(0,0,0,.05);
}
.form-signin .form-signin-heading,
.form-signin .checkbox {
  margin-bottom: 10px;
}
.form-signin input[type="text"],
.form-signin input[type="password"] {
  font-size: 16px;
  height: auto;
  margin-bottom: 15px;
  padding: 7px 9px;
}
{/literal}
</style>
<br><br><br><br><br><br>
<div class="container">

      <form class="form-signin" name="formLogin" action="/login">
        <h2 class="form-signin-heading">Авторизация</h2>
        <div class="control-group">
          <input name="email" type="text" class="input-block-level" placeholder="Email">
        </div>
        <div class="control-group">
          <input name="password" type="password" class="input-block-level" placeholder="Пароль">
        </div>
        {*
        <label class="checkbox">
          <input type="checkbox" value="remember-me"> Remember me
        </label>
        *}
        <button class="btn btn-large btn-primary" id="submit_form">Войти</button>
      </form>

</div>

<script type="text/javascript">
{literal}
$(document).ready(function(){
  Login.init('[name="formLogin"]');
});

var Validator = function( options ) {

  var pattern = {
    email: /^([a-zA-Z0-9_\.\-])+\@([a-zA-Z0-9.\-])+\.([a-zA-Z0-9]{2,4})$/,
    phone: /^[0-9\-\+)(]{7,}$/,   //At least 7 symbols; format: +3 (800) 322 44 55
    notEmpty: /.+$/,              //Has at least one Alphabetical/Numerical symbol
    hasNoSpaces: /^[^\s]+$/,
    username: /^[ёЁіїєЄІЇа-яА-Яa-zA-Z\s\-\`]{2,}$/,
    fullname: /^[ёЁіїєЄІЇа-яА-Яa-zA-Z\s\-\`][ёЁіїєЄІЇа-яА-Яa-zA-Z\s\-\`\s]{2,}$/,
    password: /^[\w\d\-\+\=\_\.]{6,15}$/,
    title: /^[\w\d\s\-\+\=\.\,\`\"]{3,}$/,
    address: /^.{7,}$/m,
    captcha: /^\d{4}$/,
    video: /vimeo|youtube|youtu\.be/,
    numbers: /^[0-9]+$/,
    message: /^.{3,800}$/m
  };

  this.pattern = pattern;

  var $this = this;

  this.namespace = null;

  this.fields = {};

  this.validCallback = null;

  this.invalidCallback = null;
  //SET configuration options
  this.setOptions = function( o ){
    this.validCallback = o.valid;
    this.invalidCallback = o.invalid;
    if ( o.namespace ){
      this.namespace = o.namespace;
    }
  };

  this.validate = function(){
    if ( !this.validCallback || !this.invalidCallback ){
      console.error("Validator error: invalid callback");
      return false;
    }
    var valid = true;
    $.each( $this.fields, function(i,v){
      var selector = $this.namespace ? $this.namespace + " " + i : i;

      var item = $( selector );
      if ( item.size() ){
        var errors = [];
        var value = $.trim(item.val());
        var fieldValid = true;
        $.each( v, function( index, p ){
          if ( !pattern[p].test( value ) ){
            fieldValid = false;
            errors.push( p );
          }
        });
        if ( item.attr("placeholder") && item.attr("placeholder").length && value == item.attr("placeholder") ){
          fieldValid = false;
        }
        if ( fieldValid ) {
          $this.validCallback({ item: item });
        } else {
          valid = false;
          $this.invalidCallback({ item: item, errors: errors });
        }
      }
    });
    return valid;
  };
  //SET fields to validate
  this.setFields = function( f ){
    this.fields = f;
  }

  //  constructor
  if ( options ){
    this.setOptions( options );
  } else {
    return { pattern: pattern }
  }
  // /constructor

};

var Login = (function(){

    var o = {
      selector: '[name="formLogin"]',
      container: '#auth_login',
    };

    var initForm = function(){
      $(o.selector).submit(function(e){
        e.preventDefault();
        onSubmit();
      });
      $(o.selector+' #submit_form').click(function(){
        //$(o.selector).submit();
      });
    };


    var onSubmit = function(){
      var validator = new Validator({
        valid: setValid,
        invalid: setInvalid,
        namespace: o.selector
      });
      validator.setFields({
        '[name="email"]'    : ['email'],
        '[name="password"]' : ['password'],
      });

      if ( ! validator.validate() ){
        return false;
      }

      var ajaxData = 'action=login&' + $(o.selector).serialize();
      $.ajax({
        url: '/auth/ajax',
        type: "POST",
        dataType: "json",
        data: ajaxData,
        async: false,
        success: function( json ){
          if ( json.status == 1 ){
            location.reload();
          } else {
            var error = json.error;
            var login = $('[name="email"]');
            var password = $('[name="password"]');
            switch( error ){
              case "accNotActive":
                Messenger.error.show({ text: ' Ваш аккаунт не активирован ', time: 7 });
                break;
              case "credentials":
                setInvalid({ item: login });
                setInvalid({ item: password });
                break;
              case "password":
                setInvalid({ item: password });
                break;
              case "login":
                setInvalid({ item: login });
                break;
            }
          }
        }
      });

    };

    var setValid = function(args){
      args.item.parent().removeClass('error');
    };

    var setInvalid = function(args){
      args.item.parent().addClass('error');
    };

    return {
      init: initForm,
    }

  })();
{/literal}
</script>