<div class="row">
  <div class="span12">
    <h4>Заказы</h4>
    {if ! $orders->count()}
    <p class="lead">Нет данных</p>
    {else}
    <table class="table table-bordered table-hover">
      <thead>
        <tr>
          <th>ID</th>
          <th>Время</th>
          <th>Менеджер</th>
          <th>Торговая точка</th>
         
          <th></th>
        </tr>
      </thead>
      <tbody>
      {assign var=prevDate value=null}
      {assign var=total value=0}
      {foreach from=$orders item=o}
        {if $prevDate neq $o->date|date_format:'%d%m%Y'}
        {if $prevDate}
        <tr>
          <td colspan="4"></td>
          <td colspan="2"><b>Всего: {$total|number_format:'2':'.':' '} грн.</b></td>
        </tr>
        {assign var=total value=0}
        {/if}
        {assign var=prevDate value=$o->date|date_format:'%d%m%Y'}
        <tr class="warning" >
          <td colspan="6"><div class="lead" style="margin-bottom: 0; text-align: center;">&nbsp;{$o->date|date_format:'%d/%m/%Y'}</div></td>
        </tr>
        {/if}
        <tr>
          <td>{$o->id}</td>
          <td>{$o->date|date_format:'%H:%M'}</td>
          <td>{$o->manager_name|stripslashes|strip_tags}</td>
          <td>s<img src="/{$o->image|stripslashes|strip_tags}"></td>
          
          <td>&nbsp;&nbsp;<a class="btn" title="Детали" href="/direction/orders/view/{$o->id}">Детали</a></td>
        </tr>
        {assign var=total value=$total+$o->total}
      {/foreach}
      </tbody>
    </table>
    {$orders->pager()}
    {/if}
  </div>
</div>