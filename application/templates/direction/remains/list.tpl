<div class="row">
  <div class="span12">
    <h4>Остатки</h4>
    {if ! $collection->count()}
    <p class="lead">Нет данных</p>
    {else}
    <table class="table table-bordered table-hover">
      <thead>
        <tr>
          <th>ID</th>
          <th>Время</th>
          <th>Менеджер</th>
          <th>Торговая точка</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
      {assign var=prevDate value=null}
      {foreach from=$collection item=r}
        {if $prevDate neq $r->date|date_format:'%d%m%Y'}
        {assign var=prevDate value=$r->date|date_format:'%d%m%Y'}
        <tr class="warning" >
          <td colspan="6"><div class="lead" style="margin-bottom: 0; text-align: center;">&nbsp;{$r->date|date_format:'%d/%m/%Y'}</div></td>
        </tr>
        {/if}
        <tr>
          <td>{$r->id}</td>
          <td>{$r->date|date_format:'%H:%M'}</td>
          <td>{$r->manager_name|stripslashes|strip_tags}</td>
          <td>{$r->address|stripslashes|strip_tags}</td>
          <td>&nbsp;&nbsp;<a class="btn" title="Детали" href="/direction/remains/view/{$r->id}">Детали</a></td>
        </tr>
      {/foreach}
      </tbody>
    </table>
    {$collection->pager()}
    {/if}
  </div>
</div>