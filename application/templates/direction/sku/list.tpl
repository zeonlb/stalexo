<div class="row">
  <div class="span12">
    <h5>Sku: {$skuStats.items}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sku с изображением: {$skuStats.items_with_image}</h5>
    <table class="table table-bordered table-hover">
      <thead>
        <tr>
          <th>ID</th>
          <th>Название</th>
          <th>ед.</th>
          <th>Лого</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        {assign var=id_group value=null}
        {foreach from=$collection item=sku}
        {if $id_group neq $sku->id_group}
        {assign var=id_group value=$sku->id_group}
        <tr class="warning" >
          <td colspan="6"><div class="lead" style="margin-bottom: 0; text-align: center;">&nbsp;{$sku->group_title|stripslashes}</div></td>
        </tr>
        {/if}
        <tr>
          <td style="vertical-align: middle;">{$sku->id}</td>
          <td style="vertical-align: middle;">{$sku->title|stripslashes}</td>
          <td style="vertical-align: middle;">{$sku->units|stripslashes|trim}</td>
          <td style="text-align: center; width: 80px;">
          {assign var=thumb value=$sku->getImageSmall()}
          <img {if $thumb}src="/{$thumb}"{else}data-src="holder.js/80x80"{/if}>
          </td>
          <td style="vertical-align: middle;"><a class="btn" title="Детали" href="/direction/sku/view/{$sku->id}">Детали</a></td>
        </tr>
        {/foreach}
      </tbody>
    </table>
    {$collection->pager()}
  </div>
</div>