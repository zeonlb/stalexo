<div class="row">
  <div class="span12">
    <h4>&nbsp;{$sku->title|stripslashes}, {$sku->units|stripslashes} &nbsp;&nbsp; <a href="javascript:;" class="btn" onclick="javascript:history.back();return false;"><< Назад</a></h4>
    <hr>
    <button id="button_upload_image" class="btn btn-info">Загрузить изображение</button>
    <button id="button_delete_image" class="btn btn-danger">Удалить изображение</button>
    <hr>
    {if isset($message) and $message}
    <div class="alert alert-{if $message.type eq 'notification'}success{else}error{/if}"><strong>{$message.text}</strong><button onclick="javascript:$(this).parent().remove();" type="button" class="close" data-dismiss="alert">×</button></div>
    {/if}
    <form name="uploadSkuImage" method="POST" action="" enctype="multipart/form-data">
      <input style="display: none;" type="file" name="input_sku_image" id="input_sku_image">
    </form>
    <form name="deleteSkuImage" method="POST" action="/direction/sku/delete-image">
      <input type="hidden" name="id_sku" value="{$sku->id}">
    </form>
    {assign var=image value=$sku->getImageBig()}
    <div style="width:615px; height:600px;">
      <div>
      <button id="close_preview" class="close pull-right{if ! $image} invisible{/if}">&times;</button>
      <div style="width:600px;" id="preview">
        {if $image}<img src="/{$image}?t={$smarty.now}" />{/if}
      </div>
      </div>
      <a href="javascript:;" title="Выбрать изображение" id="select_image"{if $image} class="invisible"{/if}>
        <img data-src="holder.js/600x600">
      </a>
    </div>
  </div>
</div>

<script type="text/javascript">
  {literal}
  $(document).ready(function(){
    ImagePreview.setOptions({
      width: 600,
      height: 600,
      fileInputId: 'input_sku_image',
      previewId: 'preview',
      callbackAfterDisplay: function(){
        $('#select_image').addClass('invisible');
        $('#close_preview').removeClass('invisible');
      }
    });
    ImagePreview.init();

    $('#select_image').click(function(){
      $('#input_sku_image').trigger('click');
    });

    $('#close_preview').click(function(){
      $('#preview').html('');
      $(this).addClass('invisible');
      $('#select_image').removeClass('invisible');
      $('#input_sku_image').val('');
    });

    $('#button_upload_image').click(function(){
      if ( $('#input_sku_image').val() ){
        $('[name="uploadSkuImage"]').trigger('submit');
      }
    });

    $('#button_delete_image').click(function(){
      $('[name="deleteSkuImage"]').trigger('submit');
    });
  });

  var ImagePreview = (function(){

  var o = {
  width: 100,
  height: 100,
  fileInputId: 'fileToUpload',
  previewId: 'preview',
  callbackAfterDisplay: null
  };

  var setOptions = function(options){
  o = options;
  };

  var onSelect = function(evt) {
  if (window.File && window.FileReader && window.FileList && window.Blob) {
  var files = evt.target.files;

  var result = '';
  var file;
  for (var i = 0; file = files[i]; i++) {
  // if the file is not an image, continue
  if (!file.type.match('image.*')) {
  continue;
  }

  reader = new FileReader();
  reader.onload = (function (tFile) {
  return function (evt) {
  var div = document.createElement('div');
  div.innerHTML = '<img style="max-width: '+o.width+'px; max-height: '+o.height+'px;" src="' + evt.target.result + '" />';
  document.getElementById(o.previewId).innerHTML = '';
  document.getElementById(o.previewId).appendChild(div);
  if ( o.callbackAfterDisplay && (typeof o.callbackAfterDisplay == 'function') ){
  o.callbackAfterDisplay();
  }
  };
  }(file));
  reader.readAsDataURL(file);
  }
  } else {
  alert('The File APIs are not fully supported in this browser.');
  }
  }

  return {
  setOptions: setOptions,
  init: function(){
  document.getElementById(o.fileInputId).addEventListener('change', onSelect, false);
  }
  }

  })();
  {/literal}
</script>