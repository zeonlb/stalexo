<div class="row">
  <div class="span6">
    <h4>&nbsp;Заказ #{$order->id} &nbsp;&nbsp; <a class="btn" onclick="javascript:history.back();"><< Назад</a></h4>
    <table class="table table-bordered">
      <tr>
        <td>Торговый агент:</td>
        <td>{$order->manager_name}</td>
      </tr>
      <tr>
        <td>Клиент:</td>
        <td>{$order->client_title|stripslashes}</td>
      </tr>
      <tr>
        <td>Торговая точка:</td>
        <td>{$order->address|stripslashes}</td>
      </tr>
      <tr>
        <td>Договор:</td>
        <td>{$order->contract_title|stripslashes}</td>
      </tr>
      <tr>
        <td>Дата:</td>
        <td>{$order->date|date_format:'%H:%M %d/%m/%Y'}</td>
      </tr>
      <tr>
        <td>Сумма:</td>
        <td>{$order->total} грн.</td>
      </tr>
    </table>
  </div>
  <div class="span7">
    <table class="table table-bordered">
      <thead>
        <th>Название</th>
        <th> Ед. </th>
        <th>Количество</th>
        <th>Цена</th>
        <th>Всего</th>
      </thead>
      <tbody>
        {foreach from=$order->children('sku') item=s}
        <tr>
          <td>{$s->title|stripslashes}</td>
          <td>{$s->units}</td>
          <td>{$s->quantity}</td>
          <td>{$s->price}</td>
          <td>{($s->quantity*$s->price)|number_format:'2':'.':''}</td>
        </tr>
        {/foreach}
      </tbody>
    </table>
  </div>
</div>