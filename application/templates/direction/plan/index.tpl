<div class="row">
<table class="table table-condensed table-hover">
  <thead>
  <tr>
    <th colspan="3"><h5>{$date|date_format:'%Y - %m'} &nbsp;&nbsp;&nbsp;&nbsp;{if $agent->isLoaded()}{$agent->name|stripslashes}{/if}</h5> </th>
    <th colspan="5"><h4>План ({$ordersPlannedTotal|number_format:'0':'.':' '} грн.) / Факт ({$ordersTotal|number_format:'0':'.':' '} грн.)</h4></th>
  </tr>
  <tr>
    <th>ТТ</th>
    <th>Клиент</th>
    <th>Адрес</th>
    <th>Контракт</th>
    <th>Агент</th>
    <th style="text-align: right;">План заказов/месяц</th>
    <th style="text-align: right;">Факт заказов/месяц</th>
    <th style="text-align: right;">Возвраты</th>
  </tr>
  <tr>
    <td colspan="8"></td>
  </tr>
  </thead>
  <tbody>
    {foreach from=$collection item=i}
    <tr>
      <td><i>{$i->id_dp}</i></td>
      <td>{$i->client_name|stripslashes}</td>
      <td>{$i->address|stripslashes}</td>
      <td><i>{$i->id_dc}</i></td>
      <td>{$i->manager_name|stripslashes}</td>
      <td style="text-align: right;">{$i->plan|number_format:'0':'.':' '} грн.</td>
      <td style="text-align: right;">{$i->orders_total|number_format:'0':'.':' '} грн.</td>
      <td style="text-align: right;">{$i->callbacks_total|number_format:'0':'.':' '} грн.</td>
    </tr>
    {/foreach}
  </tbody>
</table>

{$collection->pager()}
</div>