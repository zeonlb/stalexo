<div class="row">
  <div class="span12">
  {$message|default:''}
  <h4>Продажи за месяц</h4>
  <table class="table table-condensed table-hover">
    <thead>
      <tr>
        <th colspan="5"></th>
      </tr>
      <tr>
        <th>Клиент</th>
        <th>План</th>
        <th>Факт</th>
        <th>Возвраты</th>
        <th>План/Факт %</th>
      </tr>
      <tr>
        <th colspan="5"></th>
      </tr>
    </thead>
    <tbody>
      {if $clients->count()}
      {foreach from=$clients item=c}
      <tr>
        <td>{$c->title|stripslashes}</td>
        <td>{$c->sales_plan|number_format:'0':'.':' '} грн.</td>
        <td>{$c->orders_total|number_format:'0':'.':' '} грн.</td>
        <td>{$c->callbacks_total|number_format:'0':'.':' '} грн.</td>
        <td>
        {assign var=salesRatio value=$c->getSalesOrdersRatio()}               
        {if $salesRatio gt 99}
        &nbsp;<span> {$salesRatio}%</span>
        <div class="span2 progress progress-success active">
          <div class="bar" style="width: 100%;"></div>
        </div>
        {elseif $salesRatio lt 35}
        &nbsp;<span> {$salesRatio}%</span>
        <div class="span2 progress progress-danger active">
          <div class="bar" style="width: {$salesRatio}%;"></div>
        </div>
        {else}
        &nbsp;<span> {$salesRatio}%</span>
        <div class="span2 progress progress-info active">
          <div class="bar" style="width: {$salesRatio}%;"></div>
        </div>
        {/if}
        </td>
      </tr>
      {/foreach}
      {/if}
    </tbody>
  </table>
  </div>
</div>