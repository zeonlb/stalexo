<div id="locations">
{*
<select class="span3" name="agent">
  <option value="0">Агент</option>
  {foreach from=$agents item=a}
  <option value="{$a->id}">{$a->name|stripslashes}</option>
  {/foreach}
</select>

<select class="span2" name="date">
  <option value="0">Дата</option>
</select>

<a href="javascript:;" class="btn" style="margin-bottom: 10px;" id="show_locations">Показать</a>
*}
</div>
<div>
{if 0 and $user->userid eq 3}
<button type="button" class="btn btn-primary" id="show_route_points" data-toggle="button">Отображать точки маршрута</button>
<script type="text/javascript">
$(document).ready(function(){
  $('#show_route_points').click(function(){
    $(this).toggleClass('active');
    if ( $(this).hasClass('active') ){
      SXMap.instance
      //add point
    } else {
      //hide points
    }
  });
});
</script>
{/if}
</div>
<br>
<script src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>

<script type="text/javascript">
  /*
  $(document).ready(function(){
    $('#locations [name="agent"]').change(function(){
      var selected = $('#locations [name="agent"] :selected');
      if ( selected.val() == '0' ){ return; }
      $.ajax({
        url: '/direction/location/ajax',
        type: 'POST',
        dataType: 'json',
        async: false,
        data: { action: 'get-date-range', agent: selected.val() },
        success: function(json){
          if ( json.range ){
            var option = '<option>'+$('#locations [name="date"] option:first').text()+'</option>';
            $.each( json.range, function(i, v){
              option += '<option value="'+v+'">'+v+'</option>';
            });

            $('#locations [name="date"]').html(option);
          }
        }
      });
    });
    var SXMap = new Map();

    $('#show_locations').click(function(){
      SXMap.destroy();
      var agent = $('#locations [name="agent"]').val();
      var date  = $('#locations [name="date"]').val();
      if ( agent == 0 || date == 0 ){ return; }
      $.ajax({
        url: '/direction/location/ajax',
        type: 'POST',
        dataType: 'json',
        async: false,
        data: { action: 'get-locations', agent: agent, date: date },
        success: function(json){
          if ( json.status ){
            SXMap.display(json.center, json.points);
          }
        }
      });
    });
  });
  */
  var wayPoints = {$points|json_encode};
  var pointsInfo = {$pointsInfo|json_encode};

  var mapCenter = {$center|json_encode};
  var orders    = {$orders|json_encode};
  var callbacks = {$callbacks|json_encode};
  var remains   = {$remains|json_encode};
  var encashment= {$encashment|json_encode};

  var Map = function(){

      var $this = this;

      this.instance = null;

      this.display = function(center, points){
        ymaps.ready(function(){
          $this.instance = new ymaps.Map("map", {
            center: center || [46.46, 30.44],
            zoom: 10
          });

          $this.drawRoute(points);

          $this.drawOrders(orders);

          $this.drawCallbacks(callbacks);

          $this.drawRemains(remains);

          $this.drawEncashment(encashment);
        },
        function (error) {
          alert('Возникла ошибка: ' + error.message);
        });
      };

      this.drawRoute = function(points){

        if ( points && points.length ){
          ymaps.route(points, {
            mapStateAutoApply:true
          }).then(function (route) {

            route.getWayPoints().options.set({
              hasBalloon: false,
              iconImageHref: 'http://api.yandex.ru/maps/doc/jsapi/2.x/ref/images/styles/clusterer/blue/blue-small.png',
              iconImageSize: [0,0],
              iconImageOffset: [0,0]
            });

            var i = 0;
            route.getWayPoints().each(function(v){

              v.properties.set({
              'iconContent':'',
              //'balloonContent': pointsInfo[i].time
              });

              i++;
            });

            $this.instance.geoObjects.add(route);
          });
        }

      };

      this.destroy = function(){
        if ( instance ){
          $this.instance.destroy();
        }
      };

      this.drawOrders = function(orders){
        if ( orders && orders.length ){
          $.each(orders, function(i,v){
            var placemark = new ymaps.Placemark([v.lat, v.lng], {
                balloonContent: '<p><a target="_blank" href="'+v.url+'">Заказ #'+v.id+' '+v.time+'</a><br>'+v.total+' грн.'+'</p>',
                iconContent: "#"+v.id
            }, {
                preset: "twirl#shopIcon",
                // Disabling the Close Balloon button.
                balloonCloseButton: true,
                // The balloon will be opened and closed by clicking the placemark icon.
                hideIconOnBalloonOpen: true
            });
            $this.instance.geoObjects.add(placemark);
          });
        }
      };

      this.drawCallbacks = function(items){
        if ( items && items.length ){
          $.each(items, function(i,v){
            var placemark = new ymaps.Placemark([v.lat, v.lng], {
                balloonContent: '<p><a target="_blank" href="'+v.url+'">Возврат #'+v.id+' '+v.time+'</a></p>',
                iconContent: "#"+v.id
            }, {
                preset: "twirl#truckIcon",
                // Disabling the Close Balloon button.
                balloonCloseButton: true,
                // The balloon will be opened and closed by clicking the placemark icon.
                hideIconOnBalloonOpen: true
            });
            $this.instance.geoObjects.add(placemark);
          });
        }
      };

      this.drawRemains = function(items){
        if ( items && items.length ){
          $.each(items, function(i,v){
            var placemark = new ymaps.Placemark([v.lat, v.lng], {
                balloonContent: '<p><a target="_blank" href="'+v.url+'">Снятие остатков #'+v.id+' '+v.time+'</a></p>',
                iconContent: "#"+v.id
            }, {
                preset: "twirl#storehouseIcon",
                // Disabling the Close Balloon button.
                balloonCloseButton: true,
                // The balloon will be opened and closed by clicking the placemark icon.
                hideIconOnBalloonOpen: true
            });
            $this.instance.geoObjects.add(placemark);
          });
        }
      };

      this.drawEncashment = function(items){
        if ( items && items.length ){
          $.each(items, function(i,v){
            var placemark = new ymaps.Placemark([v.lat, v.lng], {
                balloonContent: '<p><a target="_blank" href="'+v.url+'">Инкассация #'+v.id+' '+v.time+'</a><br>'+v.amount+' грн.</p>',
                iconContent: "#"+v.id
            }, {
                preset: "twirl#bankIcon",
                // Disabling the Close Balloon button.
                balloonCloseButton: true,
                // The balloon will be opened and closed by clicking the placemark icon.
                hideIconOnBalloonOpen: true
            });
            $this.instance.geoObjects.add(placemark);
          });
        }
      };

      this.getInstance = function(){};

    };

    $(document).ready(function(){
      if ( mapCenter ){
        var SXMap = new Map();
        SXMap.display(mapCenter, wayPoints, orders);
      }
    });

</script>

<div class="span11" id="map" style="height:645px"></div>

