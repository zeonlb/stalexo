<div class="row">
  <div class="span12">
    <h4>Возвраты</h4>
    {if ! $collection->count()}
    <p class="lead">Нет данных</p>
    {else}
    <table class="table table-bordered table-hover">
      <thead>
        <tr>
          <th>ID</th>
          <th>Время</th>
          <th>Менеджер</th>
          <th>Торговая точка</th>
          <th>Сумма</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
      {assign var=prevDate value=null}
      {assign var=total value=0}
      {foreach from=$collection item=c}
        {if $prevDate neq $c->date|date_format:'%d%m%Y'}
        {if $prevDate}
        <tr>
          <td colspan="5"></td>
          <td colspan="2"><b>Всего: {$total|number_format:'2':'.':' '} грн.</b></td>
        </tr>
        {assign var=total value=0}
        {/if}
        {assign var=prevDate value=$c->date|date_format:'%d%m%Y'}
        <tr class="warning" >
          <td colspan="7"><div class="lead" style="margin-bottom: 0; text-align: center;">&nbsp;{$c->date|date_format:'%d/%m/%Y'}</div></td>
        </tr>
        {/if}
        <tr>
          <td>{$c->id}</td>
          <td>{$c->date|date_format:'%H:%M'}</td>
          <td>{$c->manager_name|stripslashes|strip_tags}</td>
          <td>{$c->address|stripslashes|strip_tags}</td>
          <td style="text-align: right;">{$c->total|number_format:'2':'.':' '} грн.</td>
          <td>&nbsp;&nbsp;<a class="btn" title="Детали" href="/direction/callback/view/{$c->id}">Детали</a></td>
        </tr>
        {assign var=total value=$total+$c->total}
      {/foreach}
      </tbody>
    </table>
    {$collection->pager()}
    {/if}
  </div>
</div>