<div class="row">
  <div class="span6">
    <h4>&nbsp;Возврат #{$callback->id} &nbsp;&nbsp; <a class="btn" onclick="javascript:history.back();"><< Назад</a></h4>
    <table class="table table-bordered">
      <tr>
        <td>Торговая точка:</td>
        <td>{$callback->address}</td>
      </tr>
      <tr>
        <td>Дата:</td>
        <td>{$callback->date|date_format:'%H:%M %d/%m/%Y'}</td>
      </tr>
      <tr>
        <td>Торговый агент:</td>
        <td>{$callback->manager_name}</td>
      </tr>
    </table>
  </div>
  <div class="span7">
    <table class="table table-bordered">
      <thead>
        <th>Название</th>
        <th> Ед. </th>
        <th>Количество</th>
        <th>Причина</th>
      </thead>
      <tbody>
        {foreach from=$callback->children('sku') item=s}
        <tr>
          <td>{$s->title|stripslashes}</td>
          <td>{$s->units}</td>
          <td>{$s->quantity}</td>
          <td>{$s->reason|stripslashes}</td>
        </tr>
        {/foreach}
      </tbody>
    </table>
  </div>
</div>