<div class="row">
  <div class="span12">
    <h4>Инкассация</h4>
    {if ! $collection->count()}
    <p class="lead">Нет данных</p>
    {else}
    <table class="table table-bordered table-hover">
      <thead>
        <tr>
          <th>ID</th>
          <th>Время</th>
          <th>Менеджер</th>
          <th>Торговая точка</th>
          <th>№ номен-ры</th>
          <th>Дата</th>
          <th>Сумма</th>
        </tr>
      </thead>
      <tbody>
      {assign var=prevDate value=null}
      {assign var=total value=0}
      {foreach from=$collection item=e name="loop"}
        {if $prevDate neq $e->date|date_format:'%d%m%Y'}
        {if $prevDate}
        <tr>
          <td colspan="6"></td>
          <td colspan="2"><b>Всего: {$total|number_format:'2':'.':' '} грн.</b></td>
        </tr>
        {assign var=total value=0}
        {/if}
        {assign var=prevDate value=$e->date|date_format:'%d%m%Y'}
        <tr class="warning" >
          <td colspan="8"><div class="lead" style="margin-bottom: 0; text-align: center;">&nbsp;{$e->date|date_format:'%d/%m/%Y'}</div></td>
        </tr>
        {/if}
        <tr>
          <td>{$e->id}</td>
          <td>{$e->date|date_format:'%H:%M'}</td>
          <td>{$e->manager_name|stripslashes|strip_tags}</td>
          <td>{$e->address|stripslashes|strip_tags}</td>
          <td>{$e->nom_number|stripslashes|strip_tags}</td>
          <td>{$e->nom_date|date_format:'%d/%m/%Y'}</td>
          <td style="text-align: right;">{$e->amount|number_format:'2':'.':' '}</td>
          {assign var=total value=$total+$e->amount}
          {*
          <td>&nbsp;&nbsp;<a class="btn" title="Детали" href="/direction/encashment/view/{$e->id}">Детали</a></td>
          *}
        </tr>
      {/foreach}
      </tbody>
    </table>
    {$collection->pager()}
    {/if}
  </div>
</div>