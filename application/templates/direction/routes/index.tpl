{$message|default:''}

<style type="text/css">
.table-row{
  cursor: pointer;
}

.ignore-margin-left{
  margin-left: 0;
}
</style>

<div class="row">
{include file="direction/routes/map-legend.tpl"}
<script src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>

<div class="span12" id="map" style="height:645px"></div>

<script type="text/javascript">

var routePoints = null;
var timelines = null;

{if isset($pointsArray)}
  var routePoints = {$pointsArray|json_encode};
{/if}
{if isset($timelinesArray)}
  var timelines = {$timelinesArray|json_encode};
{/if}

{literal}
$(document).ready(function(){
  var SXMap = new Map();

  if ( routePoints ){
    SXMap.display();
  }
});

var Map = function(){

  var $this = this;

  this.instance = null;

  this.display = function(center, points){
    ymaps.ready(function(){
      $this.instance = new ymaps.Map("map", {
        center: center || [46.46, 30.7],
        zoom: 11
      });

      $this.drawPlanRoute(routePoints);

      $this.drawFactRoute(timelines);

    },
    function (error) {
      alert('Возникла ошибка: ' + error.message);
    });
  };

  this.drawFactRoute = function(points){
    if ( ! (points && points.length) ){
      return false;
    }
    var routePoints = [];

    $.each(points, function(i,v){
      routePoints.push({ 'type': 'wayPoint', point: [v.lat, v.lng] });
    });

    ymaps.route(routePoints,
      {mapStateAutoApply: true}
    ).then(function(route){
      ////////////////////////////
      route.getPaths().options.set({
        strokeColor: '1636FD', opacity: 0.7,

      });

      var i = 0;
      route.getWayPoints().each(function(v){
        var icon = "twirl#darkblueIcon";
        if( points[i].start ){
          icon = 'twirl#darkblueDotIcon';
        } else if( points[i].end ){
          icon = 'twirl#nightDotIcon';
        } else if ( ! points[i].enter ){
          icon = "twirl#nightIcon";
        } else if ( (! points[i].valid) || (points[i].enter && points[i].invalid_distance) ){
          icon  = "twirl#redIcon";
        } else if( points[i].invalid_begintime == true ){
          icon = "twirl#darkorangeIcon";
        }

        v.options.set({
          preset: icon
        });

        v.properties.set({
          'iconContent': ( (points[i].start || points[i].end) ? '' : (points[i].index+1) ),
          balloonContentBody: ( points[i].enter ?
          (points[i].time_begin+' <br><i>'+points[i].id_dp+'</i> '+points[i].address+(points[i].content ? '<hr>'+points[i].content : '') )
          : ( points[i].start ? 'Старт работы '+points[i].time+'<br>'+points[i].agent : ( points[i].end ? 'Окончание работы '+points[i].time+'<br>'+points[i].agent: 'Выход '+points[i].time_end) ) )
        });

        i++;
      });

      $.each(points, function(i,v){
        if ( ! v.enter && !v.start && ! v.end ){
          var segment = route.getPaths().get((i-1));
          if ( segment ){
            segment.options.set({
              strokeColor: 'FE1010',
              opacity: 0.6,
              strokeWidth: 5
            });
          } else {
            //console.log('invalid segment '+i)
          }

        }
      })

      $this.instance.geoObjects.add(route);
      ///////////////////////////////
    });

  };

  this.drawPlanRoute = function(points){
    if ( ! (points && points.length) ){
      return false;
    }
    var routePoints = [];
    $.each(points, function(i,v){
      routePoints.push({ 'type': 'wayPoint', point: [v.lat, v.lng] });
    });

    ymaps.route(routePoints,
      {mapStateAutoApply: true}
    ).then(function(route){
      ////////////////////////////
      route.getPaths().options.set({
        strokeColor: '4E9D3C', opacity: 0.9,

      });

      var i = 0;
      route.getWayPoints().each(function(v){

        route.getWayPoints().options.set({
          preset: "twirl#darkgreenIcon"
        });

        v.properties.set({
          'iconContent': (i+1),
          balloonContentBody: points[i].time+' '+points[i].address
        });

        i++;
      });

      $this.instance.geoObjects.add(route);
      ///////////////////////////////
    });

  };

  this.drawPoints = function(points){
    if ( ! (points && points.length) ){
      return false;
    }

    $.each(points, function(i,v){
      var placemark = new ymaps.Placemark([v.lat, v.lng], {
          balloonContent: '<p>'+v.address+'<br> '+v.time+'</p>',
          iconContent: v.prior
      }, {
          preset: "twirl#darkgreenIcon",
          // Disabling the Close Balloon button.
          balloonCloseButton: true,
          // The balloon will be opened and closed by clicking the placemark icon.
          hideIconOnBalloonOpen: true
      });
      $this.instance.geoObjects.add(placemark);
    });
  };

  this.construct = function(){

  };

  $this.construct();

};
{/literal}
</script>
<div class="clearfix"></div>
<br>
  <div class="span12">
  {if isset($timelines) and count($timelines)}
   <span class="pull-right"><a id="t_hide_all" href="javascript:;">Скрыть детали</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:;" id="t_show_all">Показать детали</a></span>
   <div class="clearfix"></div>
   <table class="table table-condensed table-hover">
        <thead>
          <tr>
            <th>#</th>
            <th>Клиент</th>
            <th>ТТ</td>
            <th>Адрес</th>
            <th>Вход</th>
            <th>Выход</th>
            <th>Вход план</th>
            <th>Выход план</th>
            <th>Длительность</th>
            <th>Длительность план</th>
          </tr>
        </thead>
        <tbody>
          {foreach from=$timelines item=t}
          <tr class="table-row">
            <td>{$t->index+1}</td>
            <td>{$t->title|stripslashes}</td>
            <td><i>{$t->id_dp}</i> </td>
            <td>{$t->address|stripslashes|strip_tags}</td>
            <td>{$t->time_begin}</td>
            <td>{$t->time_end}</td>
            <td>{if $t->plan}{$t->plan->time}{/if}</td>
            <td>{if $t->plan}{$t->plan->time_end}{/if}</td>
            <td style="text-align: center;">{$t->time_diff} мин.</td>
            <td>{if $t->plan}{$t->plan->time_diff} мин.{/if}</td>
          </tr>
          <tr class="hide" style="background-color: #FFFFE5;">
            <td colspan="10">
            <div class="span3 ignore-margin-left">
              <b>Заказы:</b><br>
              {foreach from=$t->children['orders'] item=o}
              <a href="/direction/orders/view/{$o->id}"><i>#{$o->id}</i>&nbsp;&nbsp;&nbsp;{$o->total} грн.</a><br>
              {/foreach}
              {if $t->children['total']['orders']}
              <br>
              Всего: {$t->children['total']['orders']} грн.
              {/if}
            </div>
            <div class="span3 ignore-margin-left">
              <b>Заказы план:</b><br>
            </div>
            <div class="span3 ignore-margin-left">
              <b>Остатки:</b><br>
              {foreach from=$t->children['remains'] item=r}
              <a href="/direction/remains/view/{$r->id}"><i>#{$r->id}</i></a><br>
              {/foreach}
            </div>
            <div class="span3 ignore-margin-left">
              <b>Возвраты:</b><br>
              {foreach from=$t->children['callbacks'] item=c}
              <a href="/direction/callback/view/{$c->id}"><i>#{$c->id}</i>&nbsp;&nbsp;&nbsp;{$c->total} грн.</a><br>
              {/foreach}
              {if $t->children['total']['callbacks']}
              <br>
              Всего: {$t->children['total']['callbacks']} грн.
              {/if}
            </div>
            </td>
          </tr>
          {/foreach}
        </tbody>
  </table>
  {/if}
  </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
  $('.table-row').click(function(){
    $(this).next().toggleClass('hide');
  });

  $('#t_show_all').click(function(){
    $('.table-row').next().removeClass('hide');
  });

  $('#t_hide_all').click(function(){
    $('.table-row').next().addClass('hide');
  });

});
</script>