{$message|default:''}

<style type="text/css">
.table-row{
  cursor: pointer;
}

.ignore-margin-left{
  margin-left: 0;
}
</style>

<div class="row">
{include file="direction/routes/map-legend.tpl"}
<script src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>

<div class="span12" id="map" style="height:645px"></div>

<script type="text/javascript">

var agents = [];
{foreach from=$agents item=a}
  agents.push({
    id:          "{$a->id}",
    name:        "{$a->name}",
    timelines:   {$a->timelinesArray|json_encode},
    routePoints: {$a->pointsArray|json_encode}
  });
{/foreach}

{literal}
$(document).ready(function(){
  var SXMap = new Map();

  SXMap.display();

});

var Map = function(){

  var $this = this;

  this.instance = null;

  this.display = function(center, points){
    ymaps.ready(function(){
      $this.instance = new ymaps.Map("map", {
        center: center || [46.46, 30.7],
        zoom: 11
      });

      if ( agents && agents.length ){
        for( i = 0; i < agents.length; i++ ){
          $this.drawPlanRoute(agents[i].routePoints, agents[i].name);
          $this.drawFactRoute(agents[i].timelines,  agents[i].name);
        }
      }

    },
    function (error) {
      alert('Возникла ошибка: ' + error.message);
    });
  };

  this.drawFactRoute = function(points, agentName){
    if ( ! (points && points.length) ){
      return false;
    }
    var routePoints = [];

    $.each(points, function(i,v){
      routePoints.push({ 'type': 'wayPoint', point: [v.lat, v.lng] });
    });

    ymaps.route(routePoints,
      {mapStateAutoApply: false}
    ).then(function(route){
      ////////////////////////////
      route.getPaths().options.set({
        strokeColor: '1636FD', opacity: 0.7,

      });

      var i = 0;
      route.getWayPoints().each(function(v){
        var icon = "twirl#darkblueIcon";
        if( points[i].start ){
          icon = 'twirl#darkblueDotIcon';
        } else if( points[i].end ){
          icon = 'twirl#nightDotIcon';
        } else if ( ! points[i].enter ){
          icon = "twirl#nightIcon";
        } else if ( (! points[i].valid) || (points[i].enter && points[i].invalid_distance) ){
          icon  = "twirl#redIcon";
        } else if( points[i].invalid_begintime == true ){
          icon = "twirl#darkorangeIcon";
        }

        v.options.set({
          preset: icon
        });

        v.properties.set({
          'iconContent': (points[i].index+1),
          balloonContentBody:
          ( points[i].enter ? ('<b>'+agentName+'</b><br>'+points[i].title+'<br>'+points[i].time_begin+' <br><i>'+points[i].id_dp+'</i> '+points[i].address+(points[i].content ? '<hr>'+points[i].content : ''))
          : ( points[i].start ? 'Старт работы '+points[i].time+'<br>'+points[i].agent : ( points[i].end ? 'Окончание работы '+points[i].time+'<br>'+points[i].agent: 'Выход '+points[i].time_end) ) )
        });

        i++;
      });

      $.each(points, function(i,v){
        if ( ! v.enter && !v.start && ! v.end){
          var segment = route.getPaths().get((i-1));
          if ( segment ){
            segment.options.set({
              strokeColor: 'FE1010',
              opacity: 0.6,
              strokeWidth: 5
            });
          } else {
            //console.log('invalid segment '+i)
          }

        }
      })

      $this.instance.geoObjects.add(route);
      ///////////////////////////////
    });

  };

  this.drawPlanRoute = function(points, agentName){
    if ( ! (points && points.length) ){
      return false;
    }
    var routePoints = [];
    $.each(points, function(i,v){
      routePoints.push({ 'type': 'wayPoint', point: [v.lat, v.lng] });
    });

    ymaps.route(routePoints,
      {mapStateAutoApply: false}
    ).then(function(route){
      ////////////////////////////
      route.getPaths().options.set({
        strokeColor: '4E9D3C', opacity: 0.9,

      });

      var i = 0;
      route.getWayPoints().each(function(v){

        route.getWayPoints().options.set({
          preset: "twirl#darkgreenIcon"
        });

        v.properties.set({
          'iconContent': (i+1),
          balloonContentBody: '<b>'+agentName+'</b><br>'+points[i].title +'<br>'+ points[i].time+' '+points[i].address
        });

        i++;
      });

      $this.instance.geoObjects.add(route);
      ///////////////////////////////
    });

  };

  this.drawPoints = function(points){
    if ( ! (points && points.length) ){
      return false;
    }

    $.each(points, function(i,v){
      var placemark = new ymaps.Placemark([v.lat, v.lng], {
          balloonContent: '<p>'+v.address+'<br> '+v.time+'</p>',
          iconContent: v.prior
      }, {
          preset: "twirl#darkgreenIcon",
          // Disabling the Close Balloon button.
          balloonCloseButton: true,
          // The balloon will be opened and closed by clicking the placemark icon.
          hideIconOnBalloonOpen: true
      });
      $this.instance.geoObjects.add(placemark);
    });
  };

  this.construct = function(){

  };

  $this.construct();

};
{/literal}
</script>
<div class="clearfix"></div>

</div>