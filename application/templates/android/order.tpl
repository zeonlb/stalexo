<h4>ORDER</h4>
<hr>
<p>Client</p>
<select name="clients">
  <option value="0">Контрагент</option>
  {foreach from=$clients item=c}
  <option value="{$c->id}">{$c->title|stripslashes}</option>
  {/foreach}
</select>
<br><br>

<p>Distribution point</p>
<select name="distribution_point">
</select>
<br><br>
<hr>
<h5>SKU</h5>
<div id="sku_container"></div>

<script type="text/javascript">
{literal}
$(document).ready(function(){
  Order.init();
});

var Order = (function(){
  
  var init = function(){
    $('[name="clients"]').change(function(){
      getDP( 
        $(this).val(), 
        function(json){
          if ( json.status ){
            var options = '';
            $.each(json.dp, function(i, v){
              options += '<option value="'+v.id+'">'+v.address+'</option>';
            });
            $('[name="distribution_point"]').html(options);
          }
        }
      );
    });
    
    $('[name="distribution_point"]').change(function(){
      $('#sku_container').html('');
      getSku( 
        $(this).val(), 
        function(json){
          if ( json.status ){
            $('#sku_container').html(json.content);
          }
        }
      );
    });
    
  };
  
  var getDP = function(client, callback){
    $.ajax({
      url:       '/android/ajax',
      type:      'POST',
      dataType:  'JSON',
      async:     false,
      data: { action: 'get-distribution-points', client: client },
      success: function(json){
        return callback(json);
      }
    });
  };
  
  var getSku = function(dp, callback){
    $.ajax({
      url:       '/android/ajax',
      type:      'POST',
      dataType:  'JSON',
      async:     false,
      data: { action: 'get-sku', dp: dp },
      success: function(json){
        return callback(json);
      }
    });
  };
  
  return {
    init: init,
  }
  
})();
{/literal}
</script>
