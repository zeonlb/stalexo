{literal}
<style type="text/css">
  body {
    padding-top: 60px;
    padding-bottom: 40px;
    height: 100%;
  }
	.span2 { top:80px;}
</style>
{/literal}
<div id="wrap">
  <div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
      <div class="container cont">
        <a class="brand" href="/direction"><i class="icon-home"></i></a>
        <form class="navbar-form pull-right" name="filter" method="post" action="/direction/save-settings">
          <button title="Сбросить фильтр" type="submit" name="reset_filter" value="1" class="btn null_filter"><i class="icon-remove-circle"></i></button>
          <span class="input-append" style="margin-bottom: -1px;float: left;">
            <input id="filterDate" class="input-small input1" {if $filterDate}value="{$filterDate|substr:0:10}"{/if} type="text" name="filter_date" data-date-format="yyyy-mm-dd" data-date-weekStart="1" />
            <span class="add-on calendar_new1"><i class="icon-calendar"></i></span>{assign var=mytime value="--"|explode:$filterDate}{assign var=mytime2 value="-"|explode:$mytime.1}
                                            <select name='hours1' class="hours1" style="width:50px;">
                                        <option value='none'></option>
                                        
                                        {section name="id" start=0 loop=24}
                                                <option value='{$smarty.section.id.index}' {if $smarty.section.id.index==$mytime2.0}selected{/if}>{if $smarty.section.id.index<10}0{/if}{$smarty.section.id.index}</option>
                                        {/section}
                                </select>

                                <select name='minutes1' class="minutes1" style="width:50px;">
                                        <option value='none'></option>
                                        {section name="id" start=0 loop=60}
                                                <option value='{$smarty.section.id.index}' {if isset($mytime2.1)}{if $smarty.section.id.index==$mytime2.1}selected{/if}{/if}>{if $smarty.section.id.index<10}0{/if}{$smarty.section.id.index}</option>
                                        {/section}
                                </select>
          </span>
          <span class="input-append" style="margin-bottom: -1px;">
            <input id="filterDate2" class="input-small input2" value="{if $filterDate2}{$filterDate2|substr:0:10}{/if}" type="text" name="filter_date2" data-date-format="yyyy-mm-dd" data-date-weekStart="1" />
            <span class="add-on calendar_new2"><i class="icon-calendar"></i></span>
            {assign var=mytime value="--"|explode:$filterDate2}{assign var=mytime2 value="-"|explode:$mytime.1}
                                            <select name='hours2' class="hours2" style="width:50px;">
                                        {assign var="hours" value=$item->date|date_format:"%H"}
                                        {assign var="min" value=$item->date|date_format:"%M"}
                                        <option value='none'></option>
                                        {section name="id" start=0 loop=24}
                                                <option value='{$smarty.section.id.index}' {if $smarty.section.id.index==$mytime2.0}selected{/if}{if !$mytime.0 && $smarty.section.id.index==23}selected{/if}>{if $smarty.section.id.index<10}0{/if}{$smarty.section.id.index}</option>
                                        {/section}
                                </select>

                                <select name='minutes2' class="minutes2" style="width:50px;">
                                        <option value='none'></option>
                                        {section name="id" start=0 loop=60}
                                                <option value='{$smarty.section.id.index}' {if isset($mytime2.1)}{if $smarty.section.id.index==$mytime2.1}selected{/if}{/if}>{if $smarty.section.id.index<10}0{/if}{$smarty.section.id.index}</option>
                                        {/section}
                                </select>
          </span>
			
            <select name="agent" class="span2 agent1">
				<option value="0">Агент</option>
				{foreach from=$agents item=a}
					<option {if $agent->isLoaded() and $agent->id eq $a->id}selected="selected"{/if} value="{$a->id}" rel="{$a->id_parent}">{$a->name|stripslashes}</option>
				{/foreach}
            </select>
			<select name="director" class="span2 director1">
				<option value="0">Руководитель</option>
				{foreach from=$directors item=a}
					<option {if Stalexo_Direction::getDirector() and $director->id eq $a['id']}selected="selected"{/if} value="{$a['id']}"  rel="{$a['id']}">{$a['name']|stripslashes}</option>
				{/foreach}
			</select>
			<select name="client" class="span2 client1">
				<option value="0">Клиент</option>
				{foreach from=$clients item=a}
					<option {if Stalexo_Direction::getClient() and Stalexo_Direction::getClient() eq $a['id']}selected="selected"{/if} value="{$a['id']}"  rel="{$a['id']}">{$a['title']|stripslashes}</option>
				{/foreach}
			</select>
			<select name="address" class="span2 address1">
				<option value="0">Адрес</option>
				{foreach from=$addresses item=a}
					<option {if Stalexo_Direction::getAddress() and Stalexo_Direction::getAddress() eq $a['id']}selected="selected"{/if} value="{$a['id']}"  rel="{$a['id_client']}">{$a['title']|stripslashes}</option>
				{/foreach}
			</select>
         <button title="Фильтровать" type="submit" class="btn filter1"><i class="icon-filter"></i></button>
        </form>
        <script type="text/javascript">
        {literal}
        $(document).ready(function(){
          var filterDate = $('#filterDate').datepicker()
          .on('changeDate', function(){
            $(this).datepicker('hide');
          });
                  
          var filterDate2 = $('#filterDate2').datepicker()
          .on('changeDate', function(){
            $(this).datepicker('hide');
          });
		  
		  $('[name="director"]').change(function() {
			var agents = $('[name="agent"]');
			var current_director = parseInt($(this).find('option:selected').attr('rel'),10);
			agents.find('option').show();
			if (current_director) {
				agents.find('option').each(function() {
					if ($(this).attr('rel') != current_director)
						$(this).prop('selected', false).hide();
				})
			}
		  }).change();
		  
		  $('[name="client"]').change(function() {
			var addresses = $('[name="address"]');
			var current_client = $(this).find('option:selected').attr('rel');
			if (!current_client) {
				$('[name="address"]').html('');
				return false;
			}
				
			$.ajax({
			  url:       '/direction/ajax',
			  type:      'POST',
			  dataType:  'JSON',
			  async:     false,
			  data: { action: 'getAddresses', client: current_client },
			  success: function(json){
				$('[name="address"]').html('');
				$('[name="address"]').append('<option value="">Адрес</option>');
				$(json.addresses).each(function() {
					$('[name="address"]').append('<option value="'+this.id+'">'+this.title+'</option>');
				})
			  }
			});
		  });
        });
        {/literal}
        </script>
        <div class="nav">
          <ul class="nav">
            {if $user->hasPermission('backend')}
            <li><a href="/backend"><i class="icon-wrench"></i> A</a></li>
            {/if}
            <li{if $request->get(1) eq 'orders'} class="active"{/if}><a href="/direction/orders">Заказы</a></li>
            <li{if $request->get(1) eq 'encashment'} class="active"{/if}><a href="/direction/encashment">Инкассация</a></li>
            <li{if $request->get(1) eq 'remains'} class="active"{/if}><a href="/direction/remains">Остатки</a></li>
            <li{if $request->get(1) eq 'callback'} class="active"{/if}><a href="/direction/callback">Возвраты</a></li>
            <li{if $request->get(1) eq 'photo'} class="active"{/if}><a href="/direction/photo">Фото</a></li>
            {*<li{if $request->get(1) eq 'sku'} class="active"{/if}><a href="/direction/sku">Sku</a></li>*}
            {*<li{if $request->get(1) eq 'routes'} class="active"{/if}><a title="Маршруты" href="/direction/routes"><i class="icon-road"></i> M</a></li>*}
            {*<li{if $request->get(1) eq 'plan'} class="active"{/if}><a title="План/Факт" href="/direction/plan"><i class="icon-book"></i> P</a></li>*}
            <li{if $request->get(1) eq 'location'} class="active"{/if}><a href="/direction/location"><img style="height: 22px;" src="http://buddy.com/css/images/ico-geolocation.png" title="location" /></a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
  </div>

  <div class="container main_cont">
    {$content}
    <br>
  </div>
  <div id="push"></div>
</div>

<div id="footer">
  <footer>
    <p></p>
  </footer>
</div>