{literal}
<style type="text/css">
  body {
  //padding-top: 60px;
  padding-bottom: 40px;
  height: 100%;
  }

</style>
{/literal}
<div id="wrap">
  <div class="navbar nav">
    <div class="navbar-inner">
      <div class="container">
        <a class="brand" href="/backend">&nbsp;&nbsp;Backend&nbsp;&nbsp;</a>
        
        <div class="nav">
          <ul class="nav">
            {if $user->hasPermission('users_view')}
            <li {if $request->get(1) eq 'users'}class="active"{/if}><a href="/backend/users"><i class="icon-user"></i> Users</a></li>
            {/if}
            {if $user->hasPermission('users_permission')}
            <li {if $request->get(1) eq 'permission'}class="active"{/if}><a href="/backend/permission"><i class="icon-lock"></i> Права</a></li>
            {/if}
            {if $user->hasPermission('users_crud_devices')}
            <li {if $request->get(1) eq 'devices'}class="active"{/if}><a href="/backend/devices"><i class="icon-briefcase"></i> Devices</a></li>
            {/if}
             <li {if $request->get(1) eq 'settings'}class="active"{/if}><a href="/backend/settings"><i class="icon-briefcase"></i> Настройки планшетов</a></li>
           
          </ul>
        </div>

      </div>
    </div>
  </div>
   
  <div class="container">
    {$message}
    {$content}
    <br>
  </div>

  <div id="push"></div>
</div>