<!Doctype html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="description" content="{$description|stripslashes}" />
  <meta name="keywords" content="{$keywords|stripslashes}" />
  <meta name="robots" content="index,follow" />
  <meta name="audience" content="all" />
  <meta name="content-language" content="RU" />
  {foreach from=$css item=path}
  <link href="{$path}" rel="stylesheet" type="text/css" />
  {/foreach}

  {foreach from=$js item=path}
  <script type="text/javascript" src="{$path}"></script>
  {/foreach}
  <title>{$title|stripslashes}</title>
</head>
<body>
{$content}
</body>
</html>