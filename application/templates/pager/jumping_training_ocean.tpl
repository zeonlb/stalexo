{if $pagesCount > 1}
<div class="paginate">
  <div class="pagesAll_Box">
    <div class="pageBox_last">
    {if $prev eq $page}
    Предыдущая
    {else}
    <a href="{$url}{if $prev neq 1}/page/{$prev}{/if}">Предыдущая</a>
    {/if}    
    <br>← Ctrl
    </div>
    <div class="pagesBox">
    {if $pagesCount lt $maxButtonsInPager}
      {for $p=1 to $pagesCount }
      {if $p eq $page}
      <div class="pageBox_a"><a>{$p}</a></div>
      {else}
      <div class="pageBox"><a href="{$url}{if $p neq 1}/page/{$p}{/if}">{$p}</a></div>
      {/if}
      {/for}
    {elseif $float eq 'center'}
    <div class="pageBox"><a href="{$url}">1</a></div>
    <div class="pageBox">...</div>
    {for $p=($page-$currentPageRange) to ($page+$currentPageRange)}
      {if $p eq $page}
      <div class="pageBox_a">{$p}</div>
      {else}
      <div class="pageBox"><a href="{$url}/page/{$p}">{$p}</a></div>
      {/if}
    {/for} 
    <div class="pageBox">...</div>
    <div class="pageBox"><a href="{$url}/page/{$pagesCount}">{$pagesCount}</a></div>
    {elseif $float eq 'left'} 
      {for $p=1 to $buttonsInSequence}
        {if $p eq $page}
        <div class="pageBox_a">{$p}</div>
        {else}
        <div class="pageBox"><a href="{$url}/page/{$p}">{$p}</a></div>
        {/if}
      {/for}
      <div class="pageBox">...</div>
      <div class="pageBox"><a href="{$url}/page/{$pagesCount}">{$pagesCount}</a></div>
    {elseif $float eq 'right'}
    <div class="pageBox"><a href="{$url}">1</a></div>
    <div class="pageBox">...</div>
      {for $p=($pagesCount-$buttonsInSequence+1) to $pagesCount}
        {if $p eq $page}
        <div class="pageBox_a">{$p}</div>
        {else}
        <div class="pageBox"><a href="{$url}/page/{$p}">{$p}</a></div>
        {/if}
      {/for}
    {/if}
    </div>

    <div class="pageBox_after">
    {if $next eq $page}
    Следующая
    {else}
    <a href="{$url}/page/{$next}">Следующая</a>
    {/if}
    <br>Ctrl →
    </div>
  </div>
</div>
{/if}