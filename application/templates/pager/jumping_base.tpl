{if $pagesCount > 1}
<div class="pager">
{if $pagesCount lt $maxButtonsInPager}
{for $p=1 to $pagesCount }
  {if $p eq $page}
  <span>{$p}</span>
  {else}
  <a href="{$url}/page/{$p}">{$p}</a>
  {/if}
{/for}
{elseif $float eq 'center'}
<a href="{$url}/">1</a>
<span>...</span>
{for $p=($page-$currentPageRange) to ($page+$currentPageRange)}
 {if $p eq $page}
  <span>{$p}</span>
  {else}
  <a href="{$url}/page/{$p}">{$p}</a>
  {/if}
{/for}
<span>...</span>
<a href="{$url}/page/{$pagesCount}">{$pagesCount}</a>
{elseif $float eq 'left'}
{for $p=1 to $buttonsInSequence}
 {if $p eq $page}
  <span>{$p}</span>
  {else}
  <a href="{$url}/page/{$p}">{$p}</a>
  {/if}
{/for}
<span>...</span>
<a href="{$url}/page/{$pagesCount}">{$pagesCount}</a>
{elseif $float eq 'right'}
<a href="{$url}/page/1">1</a>
<span>...</span>
{for $p=($pagesCount-$buttonsInSequence+1) to $pagesCount}
 {if $p eq $page}
  <span>{$p}</span>
  {else}
  <a href="{$url}/page/{$p}">{$p}</a>
  {/if}
{/for}
{/if}
<a href="{$url}/page/{$prev}"><-</a> | <a href="{$url}/page/{$next}">-></a>
</div>
{/if}