{if $pagesCount > 1}
<div class="pagination">
<ul>
<li {if $page eq $prev}class="disabled"{/if}><a href="{$url}/page/{$prev}">&laquo;</a></li>

{if $pagesCount lt $maxButtonsInPager}
{for $p=1 to $pagesCount }
  {if $p eq $page}
  <li class="active"><span>{$p}</span></li>
  {else}
  <li><a href="{$url}/page/{$p}">{$p}</a></li>
  {/if}
{/for}
{elseif $float eq 'center'}
<li><a href="{$url}/">1</a></li>
<li><span>...</span></li>
{for $p=($page-$currentPageRange) to ($page+$currentPageRange)}
 {if $p eq $page}
  <li class="active"><span>{$p}</span></li>
  {else}
  <li><a href="{$url}/page/{$p}">{$p}</a></li>
  {/if}
{/for}
<li><span>...</span></li>
<li><a href="{$url}/page/{$pagesCount}">{$pagesCount}</a></li>
{elseif $float eq 'left'}
{for $p=1 to $buttonsInSequence}
 {if $p eq $page}
  <li class="active"><span>{$p}</span></li>
  {else}
  <li><a href="{$url}/page/{$p}">{$p}</a></li>
  {/if}
{/for}
<li><span>...</span></li>
<li><a href="{$url}/page/{$pagesCount}">{$pagesCount}</a></li>
{elseif $float eq 'right'}
<li><a href="{$url}/page/1">1</a></li>
<li><span>...</span></li>
{for $p=($pagesCount-$buttonsInSequence+1) to $pagesCount}
 {if $p eq $page}
  <li class="active"><span>{$p}</span></li>
  {else}
  <li><a href="{$url}/page/{$p}">{$p}</a></li>
  {/if}
{/for}
{/if}

<li {if $page eq $next}class="disabled"{/if}><a href="{$url}/page/{$next}">&raquo;</a></li>

</ul>
</div>
{/if}