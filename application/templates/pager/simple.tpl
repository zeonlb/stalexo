{if $pagesCount > 1}
<div class="pager">
{for $p=1 to $pagesCount }
  {if $p eq $page}
  <span>{$p}</span>
  {else}
  <a href="{$url}/page/{$p}">{$p}</a>
  {/if}
{/for}
</div>
{/if}