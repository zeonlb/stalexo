{if $pagesCount > 1}
<div class="pagenavi_block clear rel">
<div class="pagrnavi_rl abs rig">
  <a title="{$prev}" href="{if $prev eq $page}javascript:;{else}{$url}{if $prev neq 1}/page/{$prev}{/if}{/if}" class="pagrnavi_l but_gray{if $page eq $prev}_disactive{/if}"><i class="icon pagrnavi_l_ic"></i></a>
  <a title="{$next}" href="{if $next eq $page}javascript:;{else}{$url}/page/{$next}{/if}" class="pagrnavi_r but_gray{if $page eq $next}_disactive{/if}"><i class="icon pagrnavi_r_ic"></i></a>
</div>
<ul class="pagenavi_ul fl">
{if $pagesCount lt $maxButtonsInPager}
{for $p=1 to $pagesCount }
  {if $p eq $page}
  <li class="pagenavi_li fl"><a href="javascript:;" class="pagenavi_el but_gray_active">{$p}</a></li>
  {else}
  <li class="pagenavi_li fl"><a href="{$url}/page/{$p}" class="pagenavi_el but_gray">{$p}</a></li>
  {/if}
{/for}
{elseif $float eq 'center'}
<li class="pagenavi_li fl"><a href="{$url}" class="pagenavi_el but_gray">1</a></li>
<li class="pagenavi_li fl"><a href="{$url}/page/{$prev-$currentPageRange-1}" class="pagenavi_el but_gray">...</a></li>
{for $p=($page-$currentPageRange) to ($page+$currentPageRange)}
 {if $p eq $page}
  <li class="pagenavi_li fl"><a href="javascript:;" class="pagenavi_el but_gray_active">{$p}</a></li>
  {else}
  <li class="pagenavi_li fl"><a href="{$url}/page/{$p}" class="pagenavi_el but_gray">{$p}</a></li>
  {/if}
{/for}
<li class="pagenavi_li fl"><a href="{$url}/page/{$next+$currentPageRange+1}" class="pagenavi_el but_gray">...</a></li>
<li class="pagenavi_li fl"><a href="{$url}/page/{$pagesCount}" class="pagenavi_el but_gray">{$pagesCount}</a></li>
{elseif $float eq 'left'}
{for $p=1 to $buttonsInSequence}
 {if $p eq $page}
  <li class="pagenavi_li fl"><a href="javascript:;" class="pagenavi_el but_gray_active">{$p}</a></li>
  {else}
  <li class="pagenavi_li fl"><a href="{$url}{if $p neq 1}/page/{$p}{/if}" class="pagenavi_el but_gray">{$p}</a></li>
  {/if}
{/for}
<li class="pagenavi_li fl"><a href="{$url}/page/{$buttonsInSequence+$currentPageRange}" class="pagenavi_el but_gray">...</a></li>
<li class="pagenavi_li fl"><a href="{$url}/page/{$pagesCount}" class="pagenavi_el but_gray">{$pagesCount}</a></li>
{elseif $float eq 'right'}
<li class="pagenavi_li fl"><a href="{$url}" class="pagenavi_el but_gray">1</a></li>
<li class="pagenavi_li fl"><a href="{$url}/page/{$pagesCount-$buttonsInSequence-$currentPageRange+1}" class="pagenavi_el but_gray">...</a></li>
{for $p=($pagesCount-$buttonsInSequence+1) to $pagesCount}
 {if $p eq $page}
  <li class="pagenavi_li fl"><a href="javascript:;" class="pagenavi_el but_gray_active">{$p}</a></li>
  {else}
  <li class="pagenavi_li fl"><a href="{$url}{if $p neq 1}/page/{$p}{/if}" class="pagenavi_el but_gray">{$p}</a></li>
  {/if}
{/for}
{/if}
</ul>
</div>
{/if}