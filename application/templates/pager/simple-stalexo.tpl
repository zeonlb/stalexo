{if $pagesCount > 1}
<div class="pagination pagination-small">
  <ul>
  
    {for $p=1 to $pagesCount }
    <li{if $p eq $page} class="active"{/if}>
    {if $p eq $page}
    <span>{$p}</span>
    {else}
    <a href="{$url}/page/{$p}">{$p}</a>
    {/if}
    </li>
    {/for}
  
  </ul>
</div>
{/if}