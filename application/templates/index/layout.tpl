<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="{$description}" />
  <meta name="keywords" content="{$keywords}" />
  <meta name="robots" content="index,follow" />
  <meta name="audience" content="all" />
  <meta name="content-language" content="RU" />
  <link href="/images/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
  {foreach from=$css item=path}
  <link href="{$path}" rel="stylesheet" type="text/css" />
  {/foreach}
  {foreach from=$js item=path}
  <script type="text/javascript" src="{$path}"></script>
  {/foreach}
  <title>{$title}</title>
</head>
<body>
{$content}
</body>
</html>