<div class="span4 well">
  <legend>{if $d->isLoaded()}Редактировать{else}Добавить{/if} планшет</legend>
  <form accept-charset="UTF-8" action="" method="post">
    <input type="hidden" name="id" value="{$d->id_staff}">
    <input class="span3" name="device" {if $d->isLoaded()}value="{$d->device}"{/if} placeholder="ID-DEVICE" type="text">
    <input class="span3" name="password" placeholder="{if $d->isLoaded()}New password{else}Password{/if}" type="password"> 
    <select name="staff">
      {foreach from=$staff item=s}
      <option {if $s->id_staff and $s->id_staff neq $d->id_staff}disabled="disabled"{/if} {if $d->id_staff eq $s->id}selected="selected"{/if} value="{$s->id}">{$s->name|stripslashes}{if $s->id_staff neq $d->id_staff and $s->device} {$s->device}{/if}</option>
      {/foreach}
    </select>
    <br>
    <button class="btn btn-primary" type="submit">Сохранить</button>
    <a class="btn" href="/backend/devices">Отмена</a>
  </form>
</div>