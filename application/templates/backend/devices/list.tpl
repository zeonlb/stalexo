<div class="btn-toolbar">
  <button class="btn btn-primary{if ! $user->hasPermission('users_crud_devices')} disabled{/if}" {if $user->hasPermission('users_crud_devices')}onclick="javascript:location='/backend/devices/add';"{else}title="Недостаточно полномочий"{/if}>Добавить</button>
</div>
<div class="row">
  <div class="span12">
  <table class="table table-condensed table-hover">
      <thead>
        <tr>
          <th>#</th>
          <th></th>
          <th>ID-DEVICE</th>
          <th>Агент</th>
          <th>Последний логин</th>         
        </tr>
      </thead>
      <tbody>
      {foreach from=$devices item=d}
      <tr>
        <td>{$d->id_staff}</td>
        <td>
          <a title="Edit" href="/backend/devices/add/{$d->id_staff}"><i class="icon-pencil"></i></a>
          <a title="Delete" href="/backend/devices/delete/{$d->id_staff}"><i class="icon-remove"></i></a>
        </td>
        <td>{$d->device}</td>
        <td>{$d->name|stripslashes}</td>
        <td>{if $d->token_date}{$d->token_date|date_format:'%Y-%m-%d %H:%M'}{/if}</td>
      </tr>
      {/foreach}
      </tbody>
    </table>
  </div>
</div>