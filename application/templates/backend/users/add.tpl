<div class="span4 well">
    <legend>{if $u->isLoaded()}Редактировать{else}Добавить{/if} пользователя</legend>
    <form accept-charset="UTF-8" action="" method="post">
        <input type="hidden" name="id" value="{$u->userid}">
        <input {if $u->isLoaded()}disabled="disabled"{/if} class="span3" name="email" {if $u->isLoaded()}value="{$u->email}"{/if} placeholder="Email" type="text">
        <input class="span3" name="password" placeholder="{if $u->isLoaded()}New password{else}Password{/if}" type="password"> 
        {if $u->isLoaded() and $user->hasPermission('users_edit_advanced')}
        <h3>TODO: CAN BE DELETED</h3>
        {/if}
        <select {if ! $user->hasPermission('users_change_role')}disabled="disabled"{/if} name="role">
          {foreach from=$roles item=r}
          <option {if $u->id_role eq $r->id}selected="selected"{/if} value="{$r->id}">{$r->name}</option>
          {/foreach}
        </select>
        <br>
        <button class="btn btn-primary" type="submit">Сохранить</button>
        <a class="btn" href="/backend/users">Отмена</a>
    </form>
</div>