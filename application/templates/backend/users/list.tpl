<div class="btn-toolbar">
  <button class="btn btn-primary{if ! $user->hasPermission('users_add')} disabled{/if}" {if $user->hasPermission('users_add')}onclick="javascript:location='/backend/users/add';"{else}title="Недостаточно полномочий"{/if}>Добавить пользователя</button>
</div>
<div class="well">
  <table class="table">
    <thead>
      <tr>
        <th>#</th>
        <th>Email</th>
        <th>Дата</th>
        <th>Роль</th>
        <th style="width: 36px;"></th>
      </tr>
    </thead>
    <tbody>
      {foreach from=$users item=u}
      <tr>
        <td>{$u->userid}</td>
        <td>{$u->email}</td>
        <td>{$u->joindate|date_format:'%Y/%m/%d'}</td>
        <td>{$u->role_name}</td>
        <td>
          <a href="/backend/users/add/{$u->userid}"><i class="icon-pencil"></i></a>
          <a href="/backend/users/delete/{$u->userid}" role="button" data-toggle="modal"><i class="icon-remove"></i></a>
        </td>
      </tr>
      {/foreach}
    </tbody>
  </table>
</div>