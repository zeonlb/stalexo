<div class="row">
<div class="span6">
<h3>Ваши права</h3>
<table class="table table-bordered" class="span6">
{foreach from=$permissions item=p}
<tr class="{if $p->allowed}success{else}error{/if}">
  <td>{$p->name|stripslashes}</td>
</tr>
{/foreach}
</table> 
</div>
</div>