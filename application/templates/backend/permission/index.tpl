<div class="btn-toolbar">
  <button class="btn btn-primary{if ! $user->hasPermission('users_crud_permission')} disabled{/if}" {if $user->hasPermission('users_crud_permission')}onclick="javascript:location='/backend/permission/add';"{else}title="Недостаточно полномочий"{/if}>Добавить разрешение</button>
  {if $user->hasPermission('users_crud_permission_groups')}
  <a class="btn btn-link" href="/backend/permission/groups">Групы полномочий</a>
  {/if}
  {if $user->hasPermission('users_crud_roles')}
  <a class="btn btn-link" href="/backend/permission/roles">Роли в системе</a>
  {/if}
</div>
<div class="row">
  <div class="span12">
    <form name="role-permission-form" action="/backend/permission/edit-role-permission" method="POST">
    <table class="table table-condensed table-hover">
      <thead>
        <tr>
          <th> </th>
          {foreach from=$roles item=r}
          <th style="text-align: center;">{$r->name}</th>
          {/foreach}
        </tr>
      </thead>
      <tbody>
        {assign var=group value=false}
        {foreach from=$roles[0]->children('permissions') item=permission key=key}       
        {if $permission->id_group neq $group}
        {assign var=group value=$permission->id_group}
        <tr class="warning">
        <td colspan="{$roles|count+1}" style="text-align: center;">
          <h4>{$permission->group_name|stripslashes}</h4>
        </td>      
        </tr>
        {/if}
        <tr>
          <td><a href="/backend/permission/add/{$permission->id}"><i class="icon-pencil"></i></a>&nbsp;&nbsp; <b>{$permission->name}</b> <br><a href="/backend/permission/delete/{$permission->id}"><i class="icon-remove"></i></a>&nbsp;&nbsp; ({$permission->alias})</td>
          {foreach from=$roles item=r}
          <td style="text-align: center;"><input name="permission[{$r->id}][{$permission->id}]" value="1" type="checkbox" {if $r->children['permissions'][$key]->allowed}checked="checked"{/if} /></td>
          {/foreach}
        </tr>
        {/foreach}
        
        <tr>
          <td> </td>
          {foreach from=$roles item=r}
            <td style="text-align: center;"><button name="edit_role" value="{$r->id}" class="btn btn-success" href="#">Сохранить</button></td>
          {/foreach}
       </tr>
      </tbody>
    </table>
    </form>
  </div>
</div>