<div class="span4 well">
    <legend>{if $role->isLoaded()}Редактировать{else}Добавить{/if} роль</legend>
    <form accept-charset="UTF-8" action="" method="post">
        <input type="hidden" name="id" value="{$role->id}">
        <input {if $role->isLoaded()}disabled="disabled"{/if} class="span3" name="alias" {if $role->isLoaded()}value="{$role->alias}"{/if} placeholder="Alias" type="text">
        <input {if $role->isLoaded()}value="{$role->name}"{/if} class="span3" name="name" placeholder="Name" type="text"> 
        <br>
        <button class="btn btn-primary" type="submit">Сохранить</button>
        <a class="btn" href="/backend/permission/roles">Отмена</a>
    </form>
</div>