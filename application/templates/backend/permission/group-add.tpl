<div class="span4 well">
    <legend>{if $group->isLoaded()}Редактировать{else}Добавить{/if} групу</legend>
    <form accept-charset="UTF-8" action="" method="post">
        <input type="hidden" name="id" value="{$group->id}">
        <input {if $group->isLoaded()} value="{$group->name|stripslashes}"{/if} class="span3" name="name" placeholder="Name" type="text"> 
        <br>
        <button class="btn btn-primary" type="submit">Сохранить</button>
        <a class="btn" href="/backend/permission/groups">Отмена</a>
    </form>
</div>