<div class="btn-toolbar">
  <button class="btn btn-primary{if ! $user->hasPermission('users_crud_roles')} disabled{/if}" {if $user->hasPermission('users_crud_roles')}onclick="javascript:location='/backend/permission/add-role';"{else}title="Недостаточно полномочий"{/if}>Добавить роль</button>
</div>
<div class="row">
  <div class="span12">
  <table class="table table-condensed table-hover">
      <thead>
        <tr>
          <th>#</th>
          <th></th>
          <th>Роль</th>
          <th>Алиас</th>         
        </tr>
      </thead>
      <tbody>
      {foreach from=$roles item=r}
      <tr>
        <td>{$r->id}</td>
        <td>
          <a title="Edit" href="/backend/permission/add-role/{$r->id}"><i class="icon-pencil"></i></a>
          <a title="Delete" href="/backend/permission/delete-role/{$r->id}"><i class="icon-remove"></i></a>
        </td>
        <td>{$r->name}</td>
        <td>{$r->alias}</td>
      </tr>
      {/foreach}
      </tbody>
    </table>
  </div>
</div>