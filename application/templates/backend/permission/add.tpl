<div class="span4 well">
    <legend>{if $p->isLoaded()}Редактировать{else}Добавить{/if} разрешение</legend>
    <form accept-charset="UTF-8" action="" method="post">
        <input type="hidden" name="id" value="{$p->id}">
        <input {if $p->isLoaded()}disabled="disabled"{/if} class="span3" name="alias" {if $p->isLoaded()}value="{$p->alias}"{/if} placeholder="Alias" type="text">
        <input {if $p->isLoaded()}value="{$p->name}"{/if} class="span3" name="name" placeholder="Name" type="text"> 
        <select name="group">
          <option value="0" disabled="disabled">Група</option>
          {foreach from=$groups item=g}
          <option {if $g->id eq $p->id_group}selected="selected"{/if} value="{$g->id}">{$g->name|stripslashes}</option>
          {/foreach}
        </select>
        <br>
        <button class="btn btn-primary" type="submit">Сохранить</button>
        <a class="btn" href="/backend/permission">Отмена</a>
    </form>
</div>