<div class="btn-toolbar">
  <button class="btn btn-primary{if ! $user->hasPermission('users_crud_permission_groups')} disabled{/if}" {if $user->hasPermission('users_crud_permission_groups')}onclick="javascript:location='/backend/permission/add-group';"{else}title="Недостаточно полномочий"{/if}>Добавить групу</button>
</div>
<div class="row">
  <div class="span6">
  <table class="table table-condensed table-hover">
      <thead>
        <tr>
          <th>#</th>
          <th></th>
          <th>Група</th>        
        </tr>
      </thead>
      <tbody>
      {foreach from=$groups item=g}
      <tr>
        <td>{$g->id}</td>
        <td>
          <a title="Edit" href="/backend/permission/add-group/{$g->id}"><i class="icon-pencil"></i></a>
          <a title="Delete" href="/backend/permission/delete-group/{$g->id}"><i class="icon-remove"></i></a>
        </td>
        <td>{$g->name|stripslashes}</td>
      </tr>
      {/foreach}
      </tbody>
    </table>
  </div>
</div>